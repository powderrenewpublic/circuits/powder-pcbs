EESchema Schematic File Version 4
LIBS:gpsdo_buffer-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gpsdo_buffer-rescue:CONN_01X08 P2
U 1 1 5B178BC7
P 3550 2900
F 0 "P2" H 3550 3350 50  0000 C CNN
F 1 "CONN_01X08" V 3650 2900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 3550 2900 50  0000 C CNN
F 3 "" H 3550 2900 50  0000 C CNN
	1    3550 2900
	1    0    0    -1  
$EndComp
$Comp
L gpsdo_buffer-rescue:CONN_01X07 P1
U 1 1 5B178C2E
P 5100 2850
F 0 "P1" H 5100 3250 50  0000 C CNN
F 1 "CONN_01X07" V 5200 2850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 5100 2850 50  0000 C CNN
F 3 "" H 5100 2850 50  0000 C CNN
	1    5100 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B178C9B
P 2700 2650
F 0 "#PWR?" H 2700 2400 50  0001 C CNN
F 1 "GND" H 2700 2500 50  0000 C CNN
F 2 "" H 2700 2650 50  0000 C CNN
F 3 "" H 2700 2650 50  0000 C CNN
	1    2700 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B178CB5
P 2900 3400
F 0 "#PWR?" H 2900 3150 50  0001 C CNN
F 1 "GND" H 2900 3250 50  0000 C CNN
F 2 "" H 2900 3400 50  0000 C CNN
F 3 "" H 2900 3400 50  0000 C CNN
	1    2900 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2650 2700 2550
Wire Wire Line
	2700 2550 3350 2550
Wire Wire Line
	2900 3400 2900 3150
Wire Wire Line
	2850 2650 3350 2650
Wire Wire Line
	2900 3150 3350 3150
$Comp
L power:+3.3V #PWR?
U 1 1 5B178DBD
P 2550 3050
F 0 "#PWR?" H 2550 2900 50  0001 C CNN
F 1 "+3.3V" H 2550 3190 50  0000 C CNN
F 2 "" H 2550 3050 50  0000 C CNN
F 3 "" H 2550 3050 50  0000 C CNN
	1    2550 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 3050 2550 3250
Wire Wire Line
	2550 3250 3350 3250
Text Label 2900 2650 0    60   ~ 0
CLK_10MHz
Wire Wire Line
	3350 2850 2850 2850
Text Label 2900 2850 0    60   ~ 0
PPS
$Comp
L power:GND #PWR?
U 1 1 5B178E59
P 4600 3250
F 0 "#PWR?" H 4600 3000 50  0001 C CNN
F 1 "GND" H 4600 3100 50  0000 C CNN
F 2 "" H 4600 3250 50  0000 C CNN
F 3 "" H 4600 3250 50  0000 C CNN
	1    4600 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B178E6C
P 4450 3050
F 0 "#PWR?" H 4450 2800 50  0001 C CNN
F 1 "GND" H 4450 2900 50  0000 C CNN
F 2 "" H 4450 3050 50  0000 C CNN
F 3 "" H 4450 3050 50  0000 C CNN
	1    4450 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3250 4600 3150
Wire Wire Line
	4600 3150 4900 3150
Wire Wire Line
	4900 2950 4450 2950
Wire Wire Line
	4450 2950 4450 3050
$Comp
L symbols:SN74AC14_D_14 U2
U 1 1 5B179169
P 3950 4000
F 0 "U2" H 5050 4400 60  0000 C CNN
F 1 "SN74AC14_D_14" H 5050 4300 60  0000 C CNN
F 2 "footprints:SN74AC14DR" H 5050 4240 60  0001 C CNN
F 3 "" H 3950 4000 60  0000 C CNN
	1    3950 4000
	1    0    0    -1  
$EndComp
$Comp
L symbols:SN74AC14_D_14 U1
U 1 1 5B18551E
P 3950 1400
F 0 "U1" H 5050 1800 60  0000 C CNN
F 1 "SN74AC14_D_14" H 5050 1700 60  0000 C CNN
F 2 "footprints:SN74AC14DR" H 5050 1640 60  0001 C CNN
F 3 "" H 3950 1400 60  0000 C CNN
	1    3950 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 1400 3750 1400
Text Label 3200 1400 0    60   ~ 0
CLK_10MHz
Wire Wire Line
	6950 1500 6350 1500
Text Label 6200 1500 0    60   ~ 0
CLK_10MHz
Wire Wire Line
	3750 1400 3750 1600
Wire Wire Line
	3750 1600 3950 1600
Connection ~ 3750 1400
Wire Wire Line
	3750 1800 3950 1800
Connection ~ 3750 1600
Wire Wire Line
	2800 1500 3650 1500
Wire Wire Line
	3950 1700 3650 1700
Wire Wire Line
	3650 1500 3650 1700
Connection ~ 3650 1500
Wire Wire Line
	3650 1900 3950 1900
Connection ~ 3650 1700
Wire Wire Line
	6150 1700 6350 1700
Wire Wire Line
	6350 1500 6350 1700
Connection ~ 6350 1500
Wire Wire Line
	6350 1900 6150 1900
Connection ~ 6350 1700
Wire Wire Line
	6150 1600 6250 1600
Wire Wire Line
	6150 1800 6250 1800
Wire Wire Line
	6250 1600 6250 1800
Connection ~ 6250 1600
Wire Wire Line
	6250 2000 6150 2000
Connection ~ 6250 1800
Text Label 3150 1500 0    60   ~ 0
CLK_10MHz_OUT_A
Text Label 6450 1600 0    60   ~ 0
CLK_10MHz_OUT_B
$Comp
L power:GND #PWR?
U 1 1 5B185941
P 3850 2100
F 0 "#PWR?" H 3850 1850 50  0001 C CNN
F 1 "GND" H 3850 1950 50  0000 C CNN
F 2 "" H 3850 2100 50  0000 C CNN
F 3 "" H 3850 2100 50  0000 C CNN
	1    3850 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2000 3850 2000
Wire Wire Line
	3850 2000 3850 2100
$Comp
L power:+3.3V #PWR?
U 1 1 5B185982
P 6250 1300
F 0 "#PWR?" H 6250 1150 50  0001 C CNN
F 1 "+3.3V" H 6250 1440 50  0000 C CNN
F 2 "" H 6250 1300 50  0000 C CNN
F 3 "" H 6250 1300 50  0000 C CNN
	1    6250 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 1300 6250 1400
Wire Wire Line
	6250 1400 6150 1400
$Comp
L Connector:Conn_Coaxial J2
U 1 1 5B185A8B
P 7700 1600
F 0 "J2" H 7710 1720 50  0000 C CNN
F 1 "Conn_Coaxial" V 7815 1600 50  0000 C CNN
F 2 "Connectors_Amphenol:Amphenol_RF_SMA_132289" H 7700 1600 50  0001 C CNN
F 3 "" H 7700 1600 50  0001 C CNN
	1    7700 1600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5B185B75
P 2600 1500
F 0 "J1" H 2610 1620 50  0000 C CNN
F 1 "Conn_Coaxial" V 2715 1500 50  0000 C CNN
F 2 "Connectors_Amphenol:Amphenol_RF_SMA_132289" H 2600 1500 50  0001 C CNN
F 3 "" H 2600 1500 50  0001 C CNN
	1    2600 1500
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B185F5D
P 2600 1800
F 0 "#PWR?" H 2600 1550 50  0001 C CNN
F 1 "GND" H 2600 1650 50  0000 C CNN
F 2 "" H 2600 1800 50  0000 C CNN
F 3 "" H 2600 1800 50  0000 C CNN
	1    2600 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B185F80
P 7700 1900
F 0 "#PWR?" H 7700 1650 50  0001 C CNN
F 1 "GND" H 7700 1750 50  0000 C CNN
F 2 "" H 7700 1900 50  0000 C CNN
F 3 "" H 7700 1900 50  0000 C CNN
	1    7700 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 1900 7700 1800
Wire Wire Line
	2600 1800 2600 1700
Wire Wire Line
	2800 4100 3700 4100
$Comp
L Connector:Conn_Coaxial J3
U 1 1 5B18606B
P 2600 4100
F 0 "J3" H 2610 4220 50  0000 C CNN
F 1 "Conn_Coaxial" V 2715 4100 50  0000 C CNN
F 2 "Connectors_Amphenol:Amphenol_RF_SMA_132289" H 2600 4100 50  0001 C CNN
F 3 "" H 2600 4100 50  0001 C CNN
	1    2600 4100
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B186071
P 2600 4400
F 0 "#PWR?" H 2600 4150 50  0001 C CNN
F 1 "GND" H 2600 4250 50  0000 C CNN
F 2 "" H 2600 4400 50  0000 C CNN
F 3 "" H 2600 4400 50  0000 C CNN
	1    2600 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4400 2600 4300
Wire Wire Line
	6150 4200 6400 4200
$Comp
L Connector:Conn_Coaxial J4
U 1 1 5B186121
P 7700 4200
F 0 "J4" H 7710 4320 50  0000 C CNN
F 1 "Conn_Coaxial" V 7815 4200 50  0000 C CNN
F 2 "Connectors_Amphenol:Amphenol_RF_SMA_132289" H 7700 4200 50  0001 C CNN
F 3 "" H 7700 4200 50  0001 C CNN
	1    7700 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B186127
P 7700 4500
F 0 "#PWR?" H 7700 4250 50  0001 C CNN
F 1 "GND" H 7700 4350 50  0000 C CNN
F 2 "" H 7700 4500 50  0000 C CNN
F 3 "" H 7700 4500 50  0000 C CNN
	1    7700 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 4500 7700 4400
Wire Wire Line
	3950 4300 3700 4300
Wire Wire Line
	3700 4100 3700 4300
Connection ~ 3700 4100
Wire Wire Line
	3700 4500 3950 4500
Connection ~ 3700 4300
Wire Wire Line
	6150 4400 6400 4400
Wire Wire Line
	6400 4200 6400 4400
Connection ~ 6400 4200
Wire Wire Line
	6400 4600 6150 4600
Connection ~ 6400 4400
$Comp
L power:GND #PWR?
U 1 1 5B186234
P 3700 4800
F 0 "#PWR?" H 3700 4550 50  0001 C CNN
F 1 "GND" H 3700 4650 50  0000 C CNN
F 2 "" H 3700 4800 50  0000 C CNN
F 3 "" H 3700 4800 50  0000 C CNN
	1    3700 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 4800 3700 4600
Wire Wire Line
	3700 4600 3950 4600
$Comp
L power:+3.3V #PWR?
U 1 1 5B186299
P 6400 3800
F 0 "#PWR?" H 6400 3650 50  0001 C CNN
F 1 "+3.3V" H 6400 3940 50  0000 C CNN
F 2 "" H 6400 3800 50  0000 C CNN
F 3 "" H 6400 3800 50  0000 C CNN
	1    6400 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4000 6400 4000
Wire Wire Line
	6400 4000 6400 3800
Wire Wire Line
	3950 4000 3500 4000
Text Label 3300 4000 0    60   ~ 0
PPS
Wire Wire Line
	3950 4200 3500 4200
Wire Wire Line
	3500 4000 3500 4200
Connection ~ 3500 4000
Wire Wire Line
	3500 4400 3950 4400
Connection ~ 3500 4200
Wire Wire Line
	6900 4100 6600 4100
Text Label 6250 4100 0    60   ~ 0
PPS
Wire Wire Line
	6150 4300 6600 4300
Wire Wire Line
	6600 4100 6600 4300
Connection ~ 6600 4100
Wire Wire Line
	6600 4500 6150 4500
Connection ~ 6600 4300
$Comp
L gpsdo_buffer-rescue:C C1
U 1 1 5B1865B4
P 6300 2550
F 0 "C1" H 6325 2650 50  0000 L CNN
F 1 "1u" H 6325 2450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6338 2400 50  0000 C CNN
F 3 "" H 6300 2550 50  0000 C CNN
	1    6300 2550
	1    0    0    -1  
$EndComp
$Comp
L gpsdo_buffer-rescue:C C2
U 1 1 5B186686
P 6700 2550
F 0 "C2" H 6725 2650 50  0000 L CNN
F 1 "10n" H 6725 2450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6738 2400 50  0000 C CNN
F 3 "" H 6700 2550 50  0000 C CNN
	1    6700 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5B1867C4
P 6300 2200
F 0 "#PWR?" H 6300 2050 50  0001 C CNN
F 1 "+3.3V" H 6300 2340 50  0000 C CNN
F 2 "" H 6300 2200 50  0000 C CNN
F 3 "" H 6300 2200 50  0000 C CNN
	1    6300 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B1867ED
P 6300 2900
F 0 "#PWR?" H 6300 2650 50  0001 C CNN
F 1 "GND" H 6300 2750 50  0000 C CNN
F 2 "" H 6300 2900 50  0000 C CNN
F 3 "" H 6300 2900 50  0000 C CNN
	1    6300 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 2900 6300 2800
Wire Wire Line
	6700 2800 6300 2800
Connection ~ 6300 2800
Wire Wire Line
	6700 2800 6700 2700
Wire Wire Line
	6300 2200 6300 2300
Wire Wire Line
	6700 2400 6700 2300
Wire Wire Line
	6700 2300 6300 2300
Connection ~ 6300 2300
$Comp
L gpsdo_buffer-rescue:C C3
U 1 1 5B186C3C
P 6200 5450
F 0 "C3" H 6225 5550 50  0000 L CNN
F 1 "1u" H 6225 5350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6238 5300 50  0000 C CNN
F 3 "" H 6200 5450 50  0000 C CNN
	1    6200 5450
	1    0    0    -1  
$EndComp
$Comp
L gpsdo_buffer-rescue:C C4
U 1 1 5B186C42
P 6600 5450
F 0 "C4" H 6625 5550 50  0000 L CNN
F 1 "10n" H 6625 5350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6638 5300 50  0000 C CNN
F 3 "" H 6600 5450 50  0000 C CNN
	1    6600 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5B186C48
P 6200 5100
F 0 "#PWR?" H 6200 4950 50  0001 C CNN
F 1 "+3.3V" H 6200 5240 50  0000 C CNN
F 2 "" H 6200 5100 50  0000 C CNN
F 3 "" H 6200 5100 50  0000 C CNN
	1    6200 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B186C4E
P 6200 5800
F 0 "#PWR?" H 6200 5550 50  0001 C CNN
F 1 "GND" H 6200 5650 50  0000 C CNN
F 2 "" H 6200 5800 50  0000 C CNN
F 3 "" H 6200 5800 50  0000 C CNN
	1    6200 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 5800 6200 5700
Wire Wire Line
	6600 5700 6200 5700
Connection ~ 6200 5700
Wire Wire Line
	6600 5700 6600 5600
Wire Wire Line
	6200 5100 6200 5200
Wire Wire Line
	6600 5300 6600 5200
Wire Wire Line
	6600 5200 6200 5200
Connection ~ 6200 5200
Wire Wire Line
	3750 1400 3150 1400
Wire Wire Line
	3750 1600 3750 1800
Wire Wire Line
	3650 1500 3950 1500
Wire Wire Line
	3650 1700 3650 1900
Wire Wire Line
	6350 1500 6150 1500
Wire Wire Line
	6350 1700 6350 1900
Wire Wire Line
	6250 1600 7500 1600
Wire Wire Line
	6250 1800 6250 2000
Wire Wire Line
	3700 4100 3950 4100
Wire Wire Line
	3700 4300 3700 4500
Wire Wire Line
	6400 4200 7500 4200
Wire Wire Line
	6400 4400 6400 4600
Wire Wire Line
	3500 4000 3200 4000
Wire Wire Line
	3500 4200 3500 4400
Wire Wire Line
	6600 4100 6150 4100
Wire Wire Line
	6600 4300 6600 4500
Wire Wire Line
	6300 2800 6300 2700
Wire Wire Line
	6300 2300 6300 2400
Wire Wire Line
	6200 5700 6200 5600
Wire Wire Line
	6200 5200 6200 5300
$EndSCHEMATC
