EESchema Schematic File Version 4
LIBS:fixed_endpoint_monitor-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L POWDER_RF:SKY13575-639LF U101
U 1 1 5C5DF8E5
P 2650 5500
F 0 "U101" H 3050 5900 50  0000 L CNN
F 1 "SKY13575-639LF" H 3050 4700 50  0000 L CNN
F 2 "POWDER_RF:SKY13575-639LF" H 2700 5500 50  0001 C CNN
F 3 "" H 2700 5500 50  0001 C CNN
F 4 "Skyworks Solutions Inc." H 2650 5500 50  0001 C CNN "Manufacturer"
F 5 "SKY13575-639LF" H 2650 5500 50  0001 C CNN "PartNumber"
	1    2650 5500
	1    0    0    -1  
$EndComp
$Comp
L POWDER_RF:SKY13575-639LF U104
U 1 1 5C5DFEEB
P 5900 5350
F 0 "U104" H 6250 5750 50  0000 L CNN
F 1 "SKY13575-639LF" H 6300 4600 50  0000 L CNN
F 2 "POWDER_RF:SKY13575-639LF" H 5950 5350 50  0001 C CNN
F 3 "" H 5950 5350 50  0001 C CNN
F 4 "Skyworks Solutions Inc." H 2700 -1450 50  0001 C CNN "Manufacturer"
F 5 "SKY13575-639LF" H 2700 -1450 50  0001 C CNN "PartNumber"
	1    5900 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C6059C0
P 10700 4650
F 0 "#PWR?" H 10700 4400 50  0001 C CNN
F 1 "GND" H 10705 4477 50  0000 C CNN
F 2 "" H 10700 4650 50  0001 C CNN
F 3 "" H 10700 4650 50  0001 C CNN
	1    10700 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10700 4150 10700 4250
Wire Wire Line
	2850 5050 2850 4850
Wire Wire Line
	2850 4850 3200 4850
Wire Wire Line
	3200 4950 2950 4950
Wire Wire Line
	2950 4950 2950 5050
Text Label 2950 4850 0    50   ~ 0
coupled_vc1
Text Label 2950 4950 0    50   ~ 0
coupler_vc2
Wire Wire Line
	6100 4900 6100 4700
Wire Wire Line
	6100 4700 6450 4700
Wire Wire Line
	6450 4800 6200 4800
Wire Wire Line
	6200 4800 6200 4900
Text Label 6200 4700 0    50   ~ 0
isolated_vc1
Text Label 6200 4800 0    50   ~ 0
isolated_vc2
Text Label 10350 3400 0    50   ~ 0
isolated_vc1
Text Label 10350 3500 0    50   ~ 0
isolated_vc2
Wire Wire Line
	10650 3400 10250 3400
Wire Wire Line
	10650 3500 10250 3500
Text Label 9650 3500 2    50   ~ 0
coupled_vc1
Text Label 9650 3400 2    50   ~ 0
coupler_vc2
Wire Wire Line
	9350 3500 9750 3500
Wire Wire Line
	9350 3400 9750 3400
$Comp
L Connector:Conn_Coaxial J101
U 1 1 5C6F3424
P 1050 4650
F 0 "J101" H 980 4888 50  0000 C CNN
F 1 "Conn_Coaxial" H 980 4797 50  0000 C CNN
F 2 "POWDER_RF:SMA_EndLaunch_142-0761-881" H 1050 4650 50  0001 C CNN
F 3 " ~" H 1050 4650 50  0001 C CNN
	1    1050 4650
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J105
U 1 1 5C6F34F6
P 4300 4800
F 0 "J105" H 4230 5038 50  0000 C CNN
F 1 "Conn_Coaxial" H 4230 4947 50  0000 C CNN
F 2 "POWDER_RF:SMA_EndLaunch_142-0761-881" H 4300 4800 50  0001 C CNN
F 3 " ~" H 4300 4800 50  0001 C CNN
	1    4300 4800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5900 4800 5900 4900
Wire Wire Line
	2650 5050 2650 4650
$Comp
L power:GND #PWR?
U 1 1 5C71FACB
P 4300 5100
F 0 "#PWR?" H 4300 4850 50  0001 C CNN
F 1 "GND" H 4305 4927 50  0000 C CNN
F 2 "" H 4300 5100 50  0001 C CNN
F 3 "" H 4300 5100 50  0001 C CNN
	1    4300 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C71FB5E
P 1050 4950
F 0 "#PWR?" H 1050 4700 50  0001 C CNN
F 1 "GND" H 1055 4777 50  0000 C CNN
F 2 "" H 1050 4950 50  0001 C CNN
F 3 "" H 1050 4950 50  0001 C CNN
	1    1050 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 4950 1050 4850
Wire Wire Line
	4300 5000 4300 5100
Text Label 5200 5300 3    50   ~ 0
rf_isolated_1
Wire Wire Line
	6550 5300 6450 5300
Text Label 5850 4800 1    50   ~ 0
rf_isolated_mid
Text Label 1350 4650 0    50   ~ 0
rf_coupled
Text Label 5300 5600 3    50   ~ 0
rf_isolated_2
Wire Wire Line
	6550 5600 6450 5600
Text Label 6550 5600 0    50   ~ 0
rf_isolated_3
Text Label 6550 5300 0    50   ~ 0
rf_isolated_4
Wire Wire Line
	5200 5300 5350 5300
Wire Wire Line
	5300 5600 5350 5600
Text Label 3400 5450 3    50   ~ 0
rf_coupled_1
Text Label 3250 5750 3    50   ~ 0
rf_coupled_2
Wire Wire Line
	3300 5750 3200 5750
Wire Wire Line
	3200 5450 3400 5450
Text Label 2050 5750 3    50   ~ 0
rf_coupled_3
Text Label 1950 5450 3    50   ~ 0
rf_coupled_4
Wire Wire Line
	1950 5450 2100 5450
Wire Wire Line
	2050 5750 2100 5750
$Comp
L power:GND #PWR?
U 1 1 5C78DEFF
P 5900 6250
F 0 "#PWR?" H 5900 6000 50  0001 C CNN
F 1 "GND" H 5905 6077 50  0000 C CNN
F 2 "" H 5900 6250 50  0001 C CNN
F 3 "" H 5900 6250 50  0001 C CNN
	1    5900 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 6250 5900 6200
Wire Wire Line
	5600 6150 5600 6200
Wire Wire Line
	5600 6200 5700 6200
Connection ~ 5900 6200
Wire Wire Line
	5900 6200 5900 6150
Wire Wire Line
	5700 6150 5700 6200
Connection ~ 5700 6200
Wire Wire Line
	5700 6200 5800 6200
Wire Wire Line
	5800 6150 5800 6200
Connection ~ 5800 6200
Wire Wire Line
	5800 6200 5900 6200
Wire Wire Line
	5900 6200 6000 6200
Wire Wire Line
	6000 6200 6000 6150
Wire Wire Line
	6000 6200 6100 6200
Wire Wire Line
	6100 6200 6100 6150
Connection ~ 6000 6200
Wire Wire Line
	6100 6200 6200 6200
Wire Wire Line
	6200 6200 6200 6150
Connection ~ 6100 6200
$Comp
L power:GND #PWR?
U 1 1 5C7EE96C
P 2650 6400
F 0 "#PWR?" H 2650 6150 50  0001 C CNN
F 1 "GND" H 2655 6227 50  0000 C CNN
F 2 "" H 2650 6400 50  0001 C CNN
F 3 "" H 2650 6400 50  0001 C CNN
	1    2650 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 6400 2650 6350
Wire Wire Line
	2350 6300 2350 6350
Wire Wire Line
	2350 6350 2450 6350
Connection ~ 2650 6350
Wire Wire Line
	2650 6350 2650 6300
Wire Wire Line
	2450 6300 2450 6350
Connection ~ 2450 6350
Wire Wire Line
	2450 6350 2550 6350
Wire Wire Line
	2550 6300 2550 6350
Connection ~ 2550 6350
Wire Wire Line
	2550 6350 2650 6350
Wire Wire Line
	2650 6350 2750 6350
Wire Wire Line
	2750 6350 2750 6300
Wire Wire Line
	2750 6350 2850 6350
Wire Wire Line
	2850 6350 2850 6300
Connection ~ 2750 6350
Wire Wire Line
	2850 6350 2950 6350
Wire Wire Line
	2950 6350 2950 6300
Connection ~ 2850 6350
$Comp
L Device:C C101
U 1 1 5C6339BD
P 10700 4400
F 0 "C101" H 10815 4446 50  0000 L CNN
F 1 "1u" H 10815 4355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10738 4250 50  0001 C CNN
F 3 "~" H 10700 4400 50  0001 C CNN
	1    10700 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10700 4550 10700 4650
$Sheet
S 2800 800  1050 750 
U 5CA067DF
F0 "rf_path_1" 50
F1 "rf_path.sch" 50
F2 "rf_isolated" O L 2800 1000 50 
F3 "rf_coupled" O R 3850 1000 50 
F4 "tx_sw_1" I L 2800 1200 50 
F5 "tx_sw_2" I L 2800 1300 50 
F6 "rx_sw_1" I R 3850 1200 50 
F7 "rx_sw_2" I R 3850 1300 50 
$EndSheet
$Comp
L power:+3.3V #PWR?
U 1 1 5CA9E371
P 2450 4150
F 0 "#PWR?" H 2450 4000 50  0001 C CNN
F 1 "+3.3V" H 2465 4323 50  0000 C CNN
F 2 "" H 2450 4150 50  0001 C CNN
F 3 "" H 2450 4150 50  0001 C CNN
	1    2450 4150
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5CA9E5E7
P 5700 4300
F 0 "#PWR?" H 5700 4150 50  0001 C CNN
F 1 "+3.3V" H 5715 4473 50  0000 C CNN
F 2 "" H 5700 4300 50  0001 C CNN
F 3 "" H 5700 4300 50  0001 C CNN
	1    5700 4300
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5CA9E6A4
P 10700 4150
F 0 "#PWR?" H 10700 4000 50  0001 C CNN
F 1 "+3.3V" H 10715 4323 50  0000 C CNN
F 2 "" H 10700 4150 50  0001 C CNN
F 3 "" H 10700 4150 50  0001 C CNN
	1    10700 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 4300 5700 4350
Text Notes 1900 3750 0    50   ~ 0
VDD and +3.3V are both 3.3V rails.\nThey are separate so that the monitor\nSDR can monitor what is going out\nregardless of the state of the Arduino.
$Sheet
S 4950 1000 1050 750 
U 5CB2EF3D
F0 "rf_path_2" 50
F1 "rf_path.sch" 50
F2 "rf_isolated" O L 4950 1200 50 
F3 "rf_coupled" O R 6000 1200 50 
F4 "tx_sw_1" I L 4950 1400 50 
F5 "tx_sw_2" I L 4950 1500 50 
F6 "rx_sw_1" I R 6000 1400 50 
F7 "rx_sw_2" I R 6000 1500 50 
$EndSheet
$Sheet
S 7000 800  1050 750 
U 5CB39532
F0 "rf_path_3" 50
F1 "rf_path.sch" 50
F2 "rf_isolated" O L 7000 1000 50 
F3 "rf_coupled" O R 8050 1000 50 
F4 "tx_sw_1" I L 7000 1200 50 
F5 "tx_sw_2" I L 7000 1300 50 
F6 "rx_sw_1" I R 8050 1200 50 
F7 "rx_sw_2" I R 8050 1300 50 
$EndSheet
$Sheet
S 9150 1000 1050 750 
U 5CB3953A
F0 "rf_path_4" 50
F1 "rf_path.sch" 50
F2 "rf_isolated" O L 9150 1200 50 
F3 "rf_coupled" O R 10200 1200 50 
F4 "tx_sw_1" I L 9150 1400 50 
F5 "tx_sw_2" I L 9150 1500 50 
F6 "rx_sw_1" I R 10200 1400 50 
F7 "rx_sw_2" I R 10200 1500 50 
$EndSheet
$Comp
L Connector_Generic:Conn_02x09_Odd_Even J102
U 1 1 5CB4E3D3
P 6450 3050
F 0 "J102" H 6500 3667 50  0000 C CNN
F 1 "Conn_02x09_Odd_Even" H 6500 3576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x09_P2.54mm_Vertical" H 6450 3050 50  0001 C CNN
F 3 "~" H 6450 3050 50  0001 C CNN
	1    6450 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CB4E590
P 7050 3600
F 0 "#PWR?" H 7050 3350 50  0001 C CNN
F 1 "GND" H 7055 3427 50  0000 C CNN
F 2 "" H 7050 3600 50  0001 C CNN
F 3 "" H 7050 3600 50  0001 C CNN
	1    7050 3600
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 5CB50D58
P 5700 3450
AR Path="/5CA067DF/5CB50D58" Ref="#PWR?"  Part="1" 
AR Path="/5CB2EF3D/5CB50D58" Ref="#PWR?"  Part="1" 
AR Path="/5CB39532/5CB50D58" Ref="#PWR?"  Part="1" 
AR Path="/5CB3953A/5CB50D58" Ref="#PWR?"  Part="1" 
AR Path="/5CB50D58" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5700 3300 50  0001 C CNN
F 1 "VDD" H 5717 3623 50  0000 C CNN
F 2 "" H 5700 3450 50  0001 C CNN
F 3 "" H 5700 3450 50  0001 C CNN
	1    5700 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3450 6850 3450
Wire Wire Line
	7050 3450 7050 3600
$Comp
L Device:C C110
U 1 1 5CB6FE7F
P 6500 3700
F 0 "C110" H 6615 3746 50  0000 L CNN
F 1 "1u" H 6615 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6538 3550 50  0001 C CNN
F 3 "~" H 6500 3700 50  0001 C CNN
	1    6500 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 3700 6850 3700
Wire Wire Line
	6850 3700 6850 3450
Connection ~ 6850 3450
Wire Wire Line
	6850 3450 7050 3450
Wire Wire Line
	6350 3700 6150 3700
Wire Wire Line
	6150 3700 6150 3450
Connection ~ 6150 3450
Wire Wire Line
	6150 3450 6250 3450
Wire Wire Line
	6250 2650 2450 2650
Wire Wire Line
	2450 2650 2450 1200
Wire Wire Line
	2450 1200 2800 1200
Wire Wire Line
	2800 1300 2550 1300
Wire Wire Line
	2550 1300 2550 2750
Wire Wire Line
	2550 2750 6250 2750
Wire Wire Line
	6250 2850 4100 2850
Wire Wire Line
	4100 2850 4100 1200
Wire Wire Line
	4100 1200 3850 1200
Wire Wire Line
	3850 1300 4000 1300
Wire Wire Line
	4000 1300 4000 2950
Wire Wire Line
	4000 2950 6250 2950
Wire Wire Line
	4950 1400 4750 1400
Wire Wire Line
	4750 1400 4750 3050
Wire Wire Line
	4750 3050 6250 3050
Wire Wire Line
	4950 1500 4850 1500
Wire Wire Line
	4850 1500 4850 3150
Wire Wire Line
	4850 3150 6250 3150
Wire Wire Line
	6000 1400 6100 1400
Wire Wire Line
	6100 1400 6100 3250
Wire Wire Line
	6100 3250 6250 3250
Wire Wire Line
	6250 3350 6200 3350
Wire Wire Line
	6200 3350 6200 1500
Wire Wire Line
	6200 1500 6000 1500
Wire Wire Line
	7000 1200 6900 1200
Wire Wire Line
	6800 1300 7000 1300
Wire Wire Line
	8050 1200 8150 1200
Wire Wire Line
	8050 1300 8250 1300
Wire Wire Line
	8650 1400 9150 1400
Wire Wire Line
	9150 1500 8750 1500
Wire Wire Line
	8750 1500 8750 2750
Wire Wire Line
	10300 1400 10200 1400
Wire Wire Line
	10200 1500 10400 1500
Wire Wire Line
	10400 1500 10400 2950
$Comp
L Device:C C?
U 1 1 5CC72F76
P 7200 4550
AR Path="/5CA067DF/5CC72F76" Ref="C?"  Part="1" 
AR Path="/5CB2EF3D/5CC72F76" Ref="C?"  Part="1" 
AR Path="/5CB39532/5CC72F76" Ref="C?"  Part="1" 
AR Path="/5CB3953A/5CC72F76" Ref="C?"  Part="1" 
AR Path="/5CC72F76" Ref="C105"  Part="1" 
F 0 "C105" H 7315 4596 50  0000 L CNN
F 1 "1n" H 7315 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7238 4400 50  0001 C CNN
F 3 "~" H 7200 4550 50  0001 C CNN
	1    7200 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5CC72F7D
P 6850 4550
AR Path="/5CA067DF/5CC72F7D" Ref="C?"  Part="1" 
AR Path="/5CB2EF3D/5CC72F7D" Ref="C?"  Part="1" 
AR Path="/5CB39532/5CC72F7D" Ref="C?"  Part="1" 
AR Path="/5CB3953A/5CC72F7D" Ref="C?"  Part="1" 
AR Path="/5CC72F7D" Ref="C104"  Part="1" 
F 0 "C104" H 6965 4596 50  0000 L CNN
F 1 "10p" H 6965 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6888 4400 50  0001 C CNN
F 3 "~" H 6850 4550 50  0001 C CNN
	1    6850 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CC72F85
P 7000 4800
AR Path="/5CA067DF/5CC72F85" Ref="#PWR?"  Part="1" 
AR Path="/5CB2EF3D/5CC72F85" Ref="#PWR?"  Part="1" 
AR Path="/5CB39532/5CC72F85" Ref="#PWR?"  Part="1" 
AR Path="/5CB3953A/5CC72F85" Ref="#PWR?"  Part="1" 
AR Path="/5CC72F85" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7000 4550 50  0001 C CNN
F 1 "GND" H 7005 4627 50  0000 C CNN
F 2 "" H 7000 4800 50  0001 C CNN
F 3 "" H 7000 4800 50  0001 C CNN
	1    7000 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 4800 7000 4750
Wire Wire Line
	7000 4750 6850 4750
Wire Wire Line
	7200 4750 7000 4750
Connection ~ 7000 4750
Connection ~ 5700 4350
Wire Wire Line
	5700 4350 5700 4900
$Comp
L Device:C C?
U 1 1 5CCAB48D
P 3350 4400
AR Path="/5CA067DF/5CCAB48D" Ref="C?"  Part="1" 
AR Path="/5CB2EF3D/5CCAB48D" Ref="C?"  Part="1" 
AR Path="/5CB39532/5CCAB48D" Ref="C?"  Part="1" 
AR Path="/5CB3953A/5CCAB48D" Ref="C?"  Part="1" 
AR Path="/5CCAB48D" Ref="C102"  Part="1" 
F 0 "C102" H 3465 4446 50  0000 L CNN
F 1 "1n" H 3465 4355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3388 4250 50  0001 C CNN
F 3 "~" H 3350 4400 50  0001 C CNN
	1    3350 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5CCAB493
P 3650 4400
AR Path="/5CA067DF/5CCAB493" Ref="C?"  Part="1" 
AR Path="/5CB2EF3D/5CCAB493" Ref="C?"  Part="1" 
AR Path="/5CB39532/5CCAB493" Ref="C?"  Part="1" 
AR Path="/5CB3953A/5CCAB493" Ref="C?"  Part="1" 
AR Path="/5CCAB493" Ref="C103"  Part="1" 
F 0 "C103" H 3765 4446 50  0000 L CNN
F 1 "10p" H 3765 4355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3688 4250 50  0001 C CNN
F 3 "~" H 3650 4400 50  0001 C CNN
	1    3650 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4250 3350 4250
$Comp
L power:GND #PWR?
U 1 1 5CCAB49A
P 3500 4650
AR Path="/5CA067DF/5CCAB49A" Ref="#PWR?"  Part="1" 
AR Path="/5CB2EF3D/5CCAB49A" Ref="#PWR?"  Part="1" 
AR Path="/5CB39532/5CCAB49A" Ref="#PWR?"  Part="1" 
AR Path="/5CB3953A/5CCAB49A" Ref="#PWR?"  Part="1" 
AR Path="/5CCAB49A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3500 4400 50  0001 C CNN
F 1 "GND" H 3505 4477 50  0000 C CNN
F 2 "" H 3500 4650 50  0001 C CNN
F 3 "" H 3500 4650 50  0001 C CNN
	1    3500 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 4650 3500 4600
Wire Wire Line
	3500 4600 3350 4600
Wire Wire Line
	3650 4550 3650 4600
Wire Wire Line
	3650 4600 3500 4600
Connection ~ 3500 4600
Wire Wire Line
	3350 4550 3350 4600
Wire Wire Line
	3350 4250 2450 4250
Connection ~ 3350 4250
Wire Wire Line
	2450 4150 2450 4250
Connection ~ 2450 4250
Wire Wire Line
	2450 4250 2450 5050
Wire Wire Line
	5700 3450 6150 3450
$Comp
L power:+3.3V #PWR?
U 1 1 5CD3D8A8
P 8950 3550
F 0 "#PWR?" H 8950 3400 50  0001 C CNN
F 1 "+3.3V" H 8965 3723 50  0000 C CNN
F 2 "" H 8950 3550 50  0001 C CNN
F 3 "" H 8950 3550 50  0001 C CNN
	1    8950 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 3550 8950 3700
Wire Wire Line
	7200 4700 7200 4750
Wire Wire Line
	6850 4700 6850 4750
Wire Wire Line
	5700 4350 6850 4350
Wire Wire Line
	6850 4400 6850 4350
Connection ~ 6850 4350
Wire Wire Line
	6850 4350 7200 4350
Wire Wire Line
	7200 4400 7200 4350
Text Label 3950 1000 0    50   ~ 0
rf_coupled_1
Text Label 2700 1000 2    50   ~ 0
rf_isolated_1
Text Label 6100 1200 0    50   ~ 0
rf_coupled_2
Text Label 4850 1200 2    50   ~ 0
rf_isolated_2
Wire Wire Line
	6000 1200 6200 1200
Wire Wire Line
	4750 1200 4950 1200
Wire Wire Line
	3850 1000 4050 1000
Wire Wire Line
	2600 1000 2800 1000
Text Label 8150 1000 0    50   ~ 0
rf_coupled_3
Wire Wire Line
	8250 1000 8050 1000
Text Label 6900 1000 2    50   ~ 0
rf_isolated_3
Wire Wire Line
	7000 1000 6800 1000
Text Label 9050 1200 2    50   ~ 0
rf_isolated_4
Wire Wire Line
	9150 1200 8950 1200
Text Label 10300 1200 0    50   ~ 0
rf_coupled_4
Wire Wire Line
	10200 1200 10400 1200
$Comp
L Mechanical:MountingHole_Pad H101
U 1 1 5CF9AB79
P 9200 5250
F 0 "H101" H 9300 5301 50  0000 L CNN
F 1 "MountingHole_Pad" H 9300 5210 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 9200 5250 50  0001 C CNN
F 3 "~" H 9200 5250 50  0001 C CNN
	1    9200 5250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H103
U 1 1 5CF9AC2D
P 10300 5250
F 0 "H103" H 10400 5301 50  0000 L CNN
F 1 "MountingHole_Pad" H 10400 5210 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 10300 5250 50  0001 C CNN
F 3 "~" H 10300 5250 50  0001 C CNN
	1    10300 5250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H102
U 1 1 5CF9ACBF
P 9200 5950
F 0 "H102" H 9300 6001 50  0000 L CNN
F 1 "MountingHole_Pad" H 9300 5910 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 9200 5950 50  0001 C CNN
F 3 "~" H 9200 5950 50  0001 C CNN
	1    9200 5950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H104
U 1 1 5CF9AD57
P 10300 5950
F 0 "H104" H 10400 6001 50  0000 L CNN
F 1 "MountingHole_Pad" H 10400 5910 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 10300 5950 50  0001 C CNN
F 3 "~" H 10300 5950 50  0001 C CNN
	1    10300 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CF9ADF3
P 9200 5450
F 0 "#PWR?" H 9200 5200 50  0001 C CNN
F 1 "GND" H 9205 5277 50  0000 C CNN
F 2 "" H 9200 5450 50  0001 C CNN
F 3 "" H 9200 5450 50  0001 C CNN
	1    9200 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CF9AE68
P 10300 5450
F 0 "#PWR?" H 10300 5200 50  0001 C CNN
F 1 "GND" H 10305 5277 50  0000 C CNN
F 2 "" H 10300 5450 50  0001 C CNN
F 3 "" H 10300 5450 50  0001 C CNN
	1    10300 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CF9AEDD
P 10300 6150
F 0 "#PWR?" H 10300 5900 50  0001 C CNN
F 1 "GND" H 10305 5977 50  0000 C CNN
F 2 "" H 10300 6150 50  0001 C CNN
F 3 "" H 10300 6150 50  0001 C CNN
	1    10300 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CF9AF52
P 9200 6150
F 0 "#PWR?" H 9200 5900 50  0001 C CNN
F 1 "GND" H 9205 5977 50  0000 C CNN
F 2 "" H 9200 6150 50  0001 C CNN
F 3 "" H 9200 6150 50  0001 C CNN
	1    9200 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 6150 9200 6050
Wire Wire Line
	9200 5450 9200 5350
Wire Wire Line
	10300 5350 10300 5450
Wire Wire Line
	10300 6050 10300 6150
Text Label 5550 2650 2    50   ~ 0
tx_sw_1_1
Text Label 5550 2750 2    50   ~ 0
tx_sw_2_1
Text Label 5550 2850 2    50   ~ 0
rx_sw_1_1
Text Label 5550 2950 2    50   ~ 0
rx_sw_2_1
Text Label 5550 3050 2    50   ~ 0
tx_sw_1_2
Text Label 5550 3150 2    50   ~ 0
tx_sw_2_2
Text Label 6100 1950 2    50   ~ 0
rx_sw_1_2
Text Label 6200 2050 2    50   ~ 0
rx_sw_2_2
Text Label 6800 1950 2    50   ~ 0
tx_sw_1_3
Text Label 6900 2050 2    50   ~ 0
tx_sw_2_3
Text Label 8150 2100 1    50   ~ 0
rx_sw_1_3
Text Label 8250 2100 1    50   ~ 0
rx_sw_2_3
Text Label 7150 2650 0    50   ~ 0
tx_sw_1_4
Text Label 7150 2750 0    50   ~ 0
tx_sw_2_4
Text Label 7150 2850 0    50   ~ 0
rx_sw_1_4
Text Label 7150 2950 0    50   ~ 0
rx_sw_2_4
Wire Wire Line
	6800 3050 6750 3050
Wire Wire Line
	6800 1300 6800 3050
Wire Wire Line
	6900 3150 6750 3150
Wire Wire Line
	6900 1200 6900 3150
Wire Wire Line
	8150 3250 6750 3250
Wire Wire Line
	8150 1200 8150 3250
Wire Wire Line
	8250 3350 6750 3350
Wire Wire Line
	8250 1300 8250 3350
Wire Wire Line
	6750 2650 8650 2650
Wire Wire Line
	8650 2650 8650 1400
Wire Wire Line
	6750 2750 8750 2750
Wire Wire Line
	6750 2850 10300 2850
Wire Wire Line
	10300 2850 10300 1400
Wire Wire Line
	6750 2950 10400 2950
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J107
U 1 1 5D5382E4
P 9950 3600
F 0 "J107" H 10000 4017 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 10000 3926 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x05_P1.27mm_Vertical_SMD" H 9950 3600 50  0001 C CNN
F 3 "~" H 9950 3600 50  0001 C CNN
	1    9950 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D5383FC
P 10350 3900
F 0 "#PWR?" H 10350 3650 50  0001 C CNN
F 1 "GND" H 10355 3727 50  0000 C CNN
F 2 "" H 10350 3900 50  0001 C CNN
F 3 "" H 10350 3900 50  0001 C CNN
	1    10350 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 3900 10350 3800
Wire Wire Line
	10350 3800 10250 3800
$Comp
L power:GND #PWR?
U 1 1 5D53DD31
P 9650 3900
F 0 "#PWR?" H 9650 3650 50  0001 C CNN
F 1 "GND" H 9655 3727 50  0000 C CNN
F 2 "" H 9650 3900 50  0001 C CNN
F 3 "" H 9650 3900 50  0001 C CNN
	1    9650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 3900 9650 3800
Wire Wire Line
	9650 3800 9750 3800
Wire Wire Line
	9400 3600 9750 3600
Wire Wire Line
	9750 3700 9400 3700
Connection ~ 9400 3700
Wire Wire Line
	9400 3700 9400 3600
Wire Wire Line
	10250 3600 10500 3600
Wire Wire Line
	10500 3600 10500 3700
Wire Wire Line
	10500 3700 10250 3700
$Comp
L power:+3.3V #PWR?
U 1 1 5D57DE87
P 11000 3600
F 0 "#PWR?" H 11000 3450 50  0001 C CNN
F 1 "+3.3V" H 11015 3773 50  0000 C CNN
F 2 "" H 11000 3600 50  0001 C CNN
F 3 "" H 11000 3600 50  0001 C CNN
	1    11000 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 3600 11000 3700
Wire Wire Line
	11000 3700 10500 3700
Connection ~ 10500 3700
Wire Wire Line
	8950 3700 9400 3700
Wire Wire Line
	5900 4800 5450 4800
$Comp
L Device:C C?
U 1 1 5D61457D
P 5300 4800
AR Path="/5CA067DF/5D61457D" Ref="C?"  Part="1" 
AR Path="/5CB2EF3D/5D61457D" Ref="C?"  Part="1" 
AR Path="/5CB39532/5D61457D" Ref="C?"  Part="1" 
AR Path="/5CB3953A/5D61457D" Ref="C?"  Part="1" 
AR Path="/5D61457D" Ref="C107"  Part="1" 
F 0 "C107" H 5415 4846 50  0000 L CNN
F 1 "1n" H 5415 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 5338 4650 50  0001 C CNN
F 3 "~" H 5300 4800 50  0001 C CNN
	1    5300 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 4800 4500 4800
Wire Wire Line
	2650 4650 2100 4650
Wire Wire Line
	1800 4650 1250 4650
$Comp
L Device:C C?
U 1 1 5D63492A
P 1950 4650
AR Path="/5CA067DF/5D63492A" Ref="C?"  Part="1" 
AR Path="/5CB2EF3D/5D63492A" Ref="C?"  Part="1" 
AR Path="/5CB39532/5D63492A" Ref="C?"  Part="1" 
AR Path="/5CB3953A/5D63492A" Ref="C?"  Part="1" 
AR Path="/5D63492A" Ref="C106"  Part="1" 
F 0 "C106" H 2065 4696 50  0000 L CNN
F 1 "1n" H 2065 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 1988 4500 50  0001 C CNN
F 3 "~" H 1950 4650 50  0001 C CNN
	1    1950 4650
	0    1    1    0   
$EndComp
Text Label 2250 4650 0    50   ~ 0
rf_coupled_mid
Text Label 4650 4800 0    50   ~ 0
rf_isolated
$EndSCHEMATC
