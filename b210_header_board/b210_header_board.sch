EESchema Schematic File Version 4
LIBS:b210_header_board-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5CF6A4AB
P 3700 3300
F 0 "J1" H 3750 3717 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 3750 3626 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x05_P2.54mm_Vertical" H 3700 3300 50  0001 C CNN
F 3 "~" H 3700 3300 50  0001 C CNN
	1    3700 3300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5CF6AA14
P 3700 4300
F 0 "J2" H 3750 4717 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 3750 4626 50  0000 C CNN
F 2 "Connector_PinSocket_1.27mm:PinSocket_2x05_P1.27mm_Horizontal" H 3700 4300 50  0001 C CNN
F 3 "~" H 3700 4300 50  0001 C CNN
	1    3700 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3100 3400 3100
Wire Wire Line
	3400 3100 3400 4100
Wire Wire Line
	3400 4100 3500 4100
Wire Wire Line
	3500 4200 3300 4200
Wire Wire Line
	3300 4200 3300 3200
Wire Wire Line
	3300 3200 3500 3200
Wire Wire Line
	3500 3300 3200 3300
Wire Wire Line
	3200 3300 3200 4300
Wire Wire Line
	3200 4300 3500 4300
Wire Wire Line
	3500 3400 3100 3400
Wire Wire Line
	3100 3400 3100 4400
Wire Wire Line
	3100 4400 3500 4400
Wire Wire Line
	3500 4500 3000 4500
Wire Wire Line
	3000 4500 3000 3500
Wire Wire Line
	3000 3500 3500 3500
Wire Wire Line
	4000 4100 4100 4100
Wire Wire Line
	4100 4100 4100 3100
Wire Wire Line
	4100 3100 4000 3100
Wire Wire Line
	4000 4200 4200 4200
Wire Wire Line
	4200 4200 4200 3200
Wire Wire Line
	4200 3200 4000 3200
Wire Wire Line
	4000 3300 4300 3300
Wire Wire Line
	4300 3300 4300 4300
Wire Wire Line
	4300 4300 4000 4300
Wire Wire Line
	4000 4400 4400 4400
Wire Wire Line
	4400 4400 4400 3400
Wire Wire Line
	4400 3400 4000 3400
Wire Wire Line
	4000 3500 4500 3500
Wire Wire Line
	4500 3500 4500 4500
Wire Wire Line
	4500 4500 4000 4500
$EndSCHEMATC
