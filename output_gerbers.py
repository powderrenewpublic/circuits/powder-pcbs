import pcbnew
import os
import wx

plot_dir = 'gerbers-zip/'
plot_dir_2 = 'gerbers-zip-2/'

std_layers = {
    pcbnew.F_Cu: ("F.Cu", "Front copper"),
    pcbnew.B_Cu: ("B.Cu", "Back copper"),
    pcbnew.In1_Cu: ("In1.Cu", "First inner copper layer, directly below F.Cu"),
    pcbnew.In2_Cu: ("In2.Cu", "Inner copper layer 2, below In1.Cu"),
    pcbnew.In3_Cu: ("In3.Cu", "Inner copper layer 3, below In2.Cu"),
    pcbnew.In4_Cu: ("In4.Cu", "Inner copper layer 4, below In3.Cu"),
    pcbnew.In5_Cu: ("In5.Cu", "Inner copper layer 5, below In4.Cu"),
    pcbnew.In6_Cu: ("In6.Cu", "Inner copper layer 6, below In5.Cu"),
    pcbnew.In7_Cu: ("In7.Cu", "Inner copper layer 7, below In6.Cu"),
    pcbnew.In8_Cu: ("In8.Cu", "Inner copper layer 8, below In7.Cu"),
    pcbnew.In9_Cu: ("In9.Cu", "Inner copper layer 9, below In8.Cu"),
    pcbnew.In10_Cu: ("In10.Cu", "Inner copper layer 10, below In9.Cu"),
    pcbnew.In11_Cu: ("In11.Cu", "Inner copper layer 11, below In10.Cu"),
    pcbnew.In12_Cu: ("In12.Cu", "Inner copper layer 12, below In11.Cu"),
    pcbnew.In13_Cu: ("In13.Cu", "Inner copper layer 13, below In12.Cu"),
    pcbnew.In14_Cu: ("In14.Cu", "Inner copper layer 14, below In13.Cu"),
    pcbnew.In15_Cu: ("In15.Cu", "Inner copper layer 15, below In14.Cu"),
    pcbnew.In16_Cu: ("In16.Cu", "Inner copper layer 16, below In15.Cu"),
    pcbnew.In17_Cu: ("In17.Cu", "Inner copper layer 17, below In16.Cu"),
    pcbnew.In18_Cu: ("In18.Cu", "Inner copper layer 18, below In17.Cu"),
    pcbnew.In19_Cu: ("In19.Cu", "Inner copper layer 19, below In18.Cu"),
    pcbnew.In20_Cu: ("In20.Cu", "Inner copper layer 20, below In19.Cu"),
    pcbnew.In21_Cu: ("In21.Cu", "Inner copper layer 21, below In20.Cu"),
    pcbnew.In22_Cu: ("In22.Cu", "Inner copper layer 22, below In21.Cu"),
    pcbnew.In23_Cu: ("In23.Cu", "Inner copper layer 23, below In22.Cu"),
    pcbnew.In24_Cu: ("In24.Cu", "Inner copper layer 24, below In23.Cu"),
    pcbnew.In25_Cu: ("In25.Cu", "Inner copper layer 25, below In24.Cu"),
    pcbnew.In26_Cu: ("In26.Cu", "Inner copper layer 26, below In25.Cu"),
    pcbnew.In27_Cu: ("In27.Cu", "Inner copper layer 27, below In26.Cu"),
    pcbnew.In28_Cu: ("In28.Cu", "Inner copper layer 28, below In27.Cu"),
    pcbnew.In29_Cu: ("In29.Cu", "Inner copper layer 29, below In28.Cu"),
    pcbnew.In30_Cu: ("In30.Cu", "Inner copper layer 30, below In29.Cu"),
    pcbnew.F_SilkS: ("F.SilkS", "Front Silkscreen"),
    pcbnew.B_SilkS: ("B.SilkS", "Back Silkscreen"),
    pcbnew.F_Mask: ("F.Mask", "Front Soldermask"),
    pcbnew.B_Mask: ("B.Mask", "Back Soldermask"),
    pcbnew.Dwgs_User: ("Dwgs.User", "Fab drawing"),
    pcbnew.Edge_Cuts: ("Edge.Cuts", "Edge routing"),
    pcbnew.F_Paste: ("F.Paste", "Front Solderpaste"),
    pcbnew.B_Paste: ("B.Paste", "Back Solderpaste"),
    pcbnew.F_Fab: ("F.Fab", "Front Fabrication (assembly)"),
    pcbnew.B_Fab: ("B.Fab", "Back Fabrication (assembly)"),
}

fab_house_layers = [
    # Copper layers
    pcbnew.F_Mask,
    pcbnew.B_Mask,
    pcbnew.F_SilkS,
    pcbnew.B_SilkS,
    pcbnew.Dwgs_User,
    pcbnew.Edge_Cuts,
]

assembly_house_layers = fab_house_layers + [
    pcbnew.F_Paste,
    pcbnew.B_Paste,
    pcbnew.F_Fab,
    pcbnew.B_Fab,
]

def plot_stuff(layers, pcb, plot_dir_full, plot_dir):
    pctl = pcbnew.PLOT_CONTROLLER(pcb)
    plot_options = pctl.GetPlotOptions()

    plot_options.SetOutputDirectory(plot_dir)

    # Whether or not to plot the title block
    plot_options.SetPlotFrameRef(False)
    plot_options.SetLineWidth(pcbnew.FromMM(0.10))

    plot_options.SetAutoScale(False)
    plot_options.SetScale(1)
    plot_options.SetMirror(False)
    plot_options.SetUseGerberAttributes(False)
    plot_options.SetIncludeGerberNetlistInfo(False)
    plot_options.SetUseGerberProtelExtensions(False)
    plot_options.SetExcludeEdgeLayer(False)
    plot_options.SetScale(1)
    plot_options.SetUseAuxOrigin(False)

    plot_options.SetSubtractMaskFromSilk(True)
    plot_options.SetDrillMarksType(pcbnew.PCB_PLOT_PARAMS.NO_DRILL_SHAPE)
    plot_options.SetSkipPlotNPTH_Pads(False)
    #plot_options. - for more later

    jobfile_writer = pcbnew.GERBER_JOBFILE_WRITER(pcb)

    plot_options.SetSkipPlotNPTH_Pads(True)

    for copper_layer in pcbnew.LSET_AllCuMask().Seq():
        if pcb.IsLayerEnabled(copper_layer):
            pctl.SetLayer(copper_layer)
            pctl.OpenPlotfile(std_layers[copper_layer][0],
                              pcbnew.PLOT_FORMAT_GERBER,
                              std_layers[copper_layer][1])
            if not pctl.PlotLayer():
                wx.MessageBox(f"Whoops, failed on layer: {std_layers[copper_layer]}")

    plot_options.SetSkipPlotNPTH_Pads(False)

    for other_layer in layers:
        plot_options.SetPlotFrameRef(other_layer == pcbnew.Dwgs_User)
        pctl.SetLayer(other_layer)
        pctl.OpenPlotfile(std_layers[other_layer][0],
                          pcbnew.PLOT_FORMAT_GERBER,
                          std_layers[other_layer][1])
        if not pctl.PlotLayer():
            wx.MessageBox(f"Whoops, failed on layer: {std_layers[other_layer]}")

    pctl.ClosePlot()

    drill_writer = pcbnew.EXCELLON_WRITER(pcb)
    drill_writer.SetOptions(False, # mirror
                            False, # minimal header
                            pcbnew.wxPoint(0, 0), # offset
                            False, # merge NPTH
                           )
    drill_writer.SetFormat(True) # Set to metric
    drill_writer.CreateDrillandMapFilesSet(pctl.GetPlotDirName(),
                                           True, # Drill file
                                           False) # Map file
    drill_writer.GenDrillReportFile(os.path.join(pctl.GetPlotDirName(),
                                                 "drill_report.rpt"))


class OutputGerbersPlugin(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "Output_Gerbers"
        self.category = "Fab Output"
        self.description = "Output Gerbers and Drills in the proper way"
        self.show_toolbar_button = True

    def Run(self):
        pcb = pcbnew.GetBoard()

        board_dir = os.path.dirname(pcb.GetFileName())

        plot_dir_full = os.path.join(board_dir, plot_dir.strip('/'))
        plot_dir_full_2 = os.path.join(board_dir, plot_dir_2.strip('/'))

        if os.path.exists(plot_dir_full) or os.path.exists(plot_dir_full_2):
            wx.MessageBox("Path already exists, kill it first")
            return

        plot_stuff(fab_house_layers, pcb, plot_dir_full, plot_dir)
        plot_stuff(assembly_house_layers, pcb, plot_dir_full_2, plot_dir_2)


OutputGerbersPlugin().register()
