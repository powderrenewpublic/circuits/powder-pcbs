EESchema Schematic File Version 4
LIBS:cc2500-cc2592-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CC2500:CC2500_RTK_20 U1
U 1 1 58C2F6CD
P 3750 4400
F 0 "U1" H 5150 4800 60  0000 C CNN
F 1 "CC2500_RTK_20" H 5150 4700 60  0000 C CNN
F 2 "MyLib:CC2500_RTK_20" H 5150 4640 60  0001 C CNN
F 3 "" H 3750 4400 60  0000 C CNN
F 4 "CC2500RGPR" H 3750 4400 60  0001 C CNN "PartNumber"
F 5 "1:$2.48; 10:$2.23; 25:$2.03" H 3750 4400 60  0001 C CNN "kicost:TI_store:pricing"
F 6 "https://store.ti.com/CC2500RGPR.aspx" H 3750 4400 60  0001 C CNN "kicost:TI_store:link"
	1    3750 4400
	1    0    0    -1  
$EndComp
Text Label 3600 4400 0    60   ~ 0
SCLK
Text Label 3600 5000 0    60   ~ 0
CS
Text Label 3600 4900 0    60   ~ 0
GDO0
Text Label 3600 4600 0    60   ~ 0
GDO2
Text Label 3600 4500 0    60   ~ 0
SOMI
Text Label 6600 4500 0    60   ~ 0
SIMO
$Comp
L cc2500-cc2592-rescue:+3.3V #PWR?
U 1 1 58C2F7F4
P 3250 4600
F 0 "#PWR?" H 3250 4450 50  0001 C CNN
F 1 "+3.3V" H 3250 4740 50  0000 C CNN
F 2 "" H 3250 4600 50  0000 C CNN
F 3 "" H 3250 4600 50  0000 C CNN
	1    3250 4600
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:+3.3V #PWR?
U 1 1 58C2F82A
P 1000 5350
F 0 "#PWR?" H 1000 5200 50  0001 C CNN
F 1 "+3.3V" H 1000 5490 50  0000 C CNN
F 2 "" H 1000 5350 50  0000 C CNN
F 3 "" H 1000 5350 50  0000 C CNN
	1    1000 5350
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:INDUCTOR L1
U 1 1 58C2F839
P 1400 5450
F 0 "L1" V 1350 5450 50  0000 C CNN
F 1 "1uH" V 1500 5450 50  0000 C CNN
F 2 "Inductors_NEOSID:Neosid_Inductor_SM0603CG" H 1400 5450 50  0000 C CNN
F 3 "" H 1400 5450 50  0000 C CNN
F 4 "LQM18FN1R0M00D" V 1400 5450 60  0001 C CNN "PartNumber"
	1    1400 5450
	0    -1   -1   0   
$EndComp
$Comp
L cc2500-cc2592-rescue:C C1
U 1 1 58C2FA49
P 1700 5700
F 0 "C1" H 1725 5800 50  0000 L CNN
F 1 "0.1uF" H 1725 5600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" V 1550 5450 50  0000 C CNN
F 3 "" H 1700 5700 50  0000 C CNN
F 4 "06033C104K4T4A" H 1700 5700 60  0001 C CNN "PartNumber"
	1    1700 5700
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C2
U 1 1 58C2FAC1
P 2100 5700
F 0 "C2" H 2125 5800 50  0000 L CNN
F 1 "0.1uF" H 2125 5600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" V 1950 5450 50  0000 C CNN
F 3 "" H 2100 5700 50  0000 C CNN
F 4 "06033C104K4T4A" H 2100 5700 60  0001 C CNN "PartNumber"
	1    2100 5700
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C3
U 1 1 58C2FAF1
P 2500 5700
F 0 "C3" H 2525 5800 50  0000 L CNN
F 1 "220pF" H 2525 5600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" V 2350 5450 50  0000 C CNN
F 3 "" H 2500 5700 50  0000 C CNN
F 4 "GCM1885C1H221JA16D" H 2500 5700 60  0001 C CNN "PartNumber"
	1    2500 5700
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C4
U 1 1 58C2FB18
P 2950 5700
F 0 "C4" H 2975 5800 50  0000 L CNN
F 1 "220pF" H 2975 5600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" V 2800 5450 50  0000 C CNN
F 3 "" H 2950 5700 50  0000 C CNN
F 4 "GCM1885C1H221JA16D" H 2950 5700 60  0001 C CNN "PartNumber"
	1    2950 5700
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C2FE07
P 1700 5950
F 0 "#PWR?" H 1700 5700 50  0001 C CNN
F 1 "GND" H 1700 5800 50  0000 C CNN
F 2 "" H 1700 5950 50  0000 C CNN
F 3 "" H 1700 5950 50  0000 C CNN
	1    1700 5950
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C2FE48
P 2100 5950
F 0 "#PWR?" H 2100 5700 50  0001 C CNN
F 1 "GND" H 2100 5800 50  0000 C CNN
F 2 "" H 2100 5950 50  0000 C CNN
F 3 "" H 2100 5950 50  0000 C CNN
	1    2100 5950
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C2FE65
P 2500 5950
F 0 "#PWR?" H 2500 5700 50  0001 C CNN
F 1 "GND" H 2500 5800 50  0000 C CNN
F 2 "" H 2500 5950 50  0000 C CNN
F 3 "" H 2500 5950 50  0000 C CNN
	1    2500 5950
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C2FE82
P 2950 5950
F 0 "#PWR?" H 2950 5700 50  0001 C CNN
F 1 "GND" H 2950 5800 50  0000 C CNN
F 2 "" H 2950 5950 50  0000 C CNN
F 3 "" H 2950 5950 50  0000 C CNN
	1    2950 5950
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:Crystal Y1
U 1 1 58C2FF3F
P 2700 5200
F 0 "Y1" H 2700 5350 50  0000 C CNN
F 1 "Crystal-26MHz" H 2700 5050 50  0000 C CNN
F 2 "MyLib:Crystal_SMD_3225-4pin_3.2x2.5mm" H 2100 4950 50  0000 C CNN
F 3 "" H 2700 5200 50  0000 C CNN
F 4 "TSX-3225 26.0000MF09Z-AC3" H 2700 5200 60  0001 C CNN "PartNumber"
	1    2700 5200
	0    1    1    0   
$EndComp
$Comp
L cc2500-cc2592-rescue:C C5
U 1 1 58C303F4
P 3450 5700
F 0 "C5" H 3475 5800 50  0000 L CNN
F 1 "0.1uF" H 3475 5600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" V 3300 5450 50  0000 C CNN
F 3 "" H 3450 5700 50  0000 C CNN
F 4 "06033C104K4T4A" H 3450 5700 60  0001 C CNN "PartNumber"
	1    3450 5700
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C30464
P 3450 5950
F 0 "#PWR?" H 3450 5700 50  0001 C CNN
F 1 "GND" H 3450 5800 50  0000 C CNN
F 2 "" H 3450 5950 50  0000 C CNN
F 3 "" H 3450 5950 50  0000 C CNN
	1    3450 5950
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C3050C
P 7250 5950
F 0 "#PWR?" H 7250 5700 50  0001 C CNN
F 1 "GND" H 7250 5800 50  0000 C CNN
F 2 "" H 7250 5950 50  0000 C CNN
F 3 "" H 7250 5950 50  0000 C CNN
	1    7250 5950
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:R R1
U 1 1 58C305AF
P 7100 5500
F 0 "R1" V 7180 5500 50  0000 C CNN
F 1 "56k" V 7100 5500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7030 5500 50  0000 C CNN
F 3 "" H 7100 5500 50  0000 C CNN
F 4 "RC0603FR-0756KL" V 7100 5500 60  0001 C CNN "PartNumber"
	1    7100 5500
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C30663
P 7100 5950
F 0 "#PWR?" H 7100 5700 50  0001 C CNN
F 1 "GND" H 7100 5800 50  0000 C CNN
F 2 "" H 7100 5950 50  0000 C CNN
F 3 "" H 7100 5950 50  0000 C CNN
	1    7100 5950
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:+3.3V #PWR?
U 1 1 58C307DB
P 7400 4600
F 0 "#PWR?" H 7400 4450 50  0001 C CNN
F 1 "+3.3V" H 7400 4740 50  0000 C CNN
F 2 "" H 7400 4600 50  0000 C CNN
F 3 "" H 7400 4600 50  0000 C CNN
	1    7400 4600
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C6
U 1 1 58C308E3
P 7650 5700
F 0 "C6" H 7675 5800 50  0000 L CNN
F 1 "220pF" H 7675 5600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" V 7500 5450 50  0000 C CNN
F 3 "" H 7650 5700 50  0000 C CNN
F 4 "GCM1885C1H221JA16D" H 7650 5700 60  0001 C CNN "PartNumber"
	1    7650 5700
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C30B35
P 7650 5950
F 0 "#PWR?" H 7650 5700 50  0001 C CNN
F 1 "GND" H 7650 5800 50  0000 C CNN
F 2 "" H 7650 5950 50  0000 C CNN
F 3 "" H 7650 5950 50  0000 C CNN
	1    7650 5950
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:INDUCTOR L2
U 1 1 58C31849
P 5400 1300
F 0 "L2" V 5350 1300 50  0000 C CNN
F 1 "2nH" V 5500 1300 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" V 5550 1300 50  0000 C CNN
F 3 "" H 5400 1300 50  0000 C CNN
F 4 "LQG15HS2N0S02D" V 5400 1300 60  0001 C CNN "PartNumber"
	1    5400 1300
	0    -1   -1   0   
$EndComp
$Comp
L cc2500-cc2592-rescue:C C12
U 1 1 58C318E1
P 5850 1600
F 0 "C12" H 5875 1700 50  0000 L CNN
F 1 "2.2pF" H 5875 1500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 5700 1750 50  0000 C CNN
F 3 "" H 5850 1600 50  0000 C CNN
F 4 "GRM1555C1E2R2BA01D" H 5850 1600 60  0001 C CNN "PartNumber"
	1    5850 1600
	-1   0    0    1   
$EndComp
$Comp
L cc2500-cc2592-rescue:C C8
U 1 1 58C319A9
P 4950 1600
F 0 "C8" H 4975 1700 50  0000 L CNN
F 1 "0.2pF" H 4975 1500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 4800 1750 50  0000 C CNN
F 3 "" H 4950 1600 50  0000 C CNN
F 4 "GRM1555C1HR20BA01D" H 4950 1600 60  0001 C CNN "PartNumber"
	1    4950 1600
	-1   0    0    1   
$EndComp
$Comp
L cc2500-cc2592-rescue:C C7
U 1 1 58C31A61
P 4600 1300
F 0 "C7" H 4625 1400 50  0000 L CNN
F 1 "100pF" H 4625 1200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 4450 1050 50  0000 C CNN
F 3 "" H 4600 1300 50  0000 C CNN
F 4 "GRM1555C1H101JA01D" H 4600 1300 60  0001 C CNN "PartNumber"
	1    4600 1300
	0    1    1    0   
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C31C96
P 5850 1850
F 0 "#PWR?" H 5850 1600 50  0001 C CNN
F 1 "GND" H 5850 1700 50  0000 C CNN
F 2 "" H 5850 1850 50  0000 C CNN
F 3 "" H 5850 1850 50  0000 C CNN
	1    5850 1850
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C31CE0
P 4950 1850
F 0 "#PWR?" H 4950 1600 50  0001 C CNN
F 1 "GND" H 4950 1700 50  0000 C CNN
F 2 "" H 4950 1850 50  0000 C CNN
F 3 "" H 4950 1850 50  0000 C CNN
	1    4950 1850
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:+3.3V #PWR?
U 1 1 58C319AF
P 3950 6650
F 0 "#PWR?" H 3950 6500 50  0001 C CNN
F 1 "+3.3V" H 3950 6790 50  0000 C CNN
F 2 "" H 3950 6650 50  0000 C CNN
F 3 "" H 3950 6650 50  0000 C CNN
	1    3950 6650
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C15
U 1 1 58C319FC
P 3950 7000
F 0 "C15" H 3975 7100 50  0000 L CNN
F 1 "0.1uF" H 3975 6900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" V 3800 6750 50  0000 C CNN
F 3 "" H 3950 7000 50  0000 C CNN
F 4 "06033C104K4T4A" H 3950 7000 60  0001 C CNN "PartNumber"
	1    3950 7000
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C31B08
P 3950 7350
F 0 "#PWR?" H 3950 7100 50  0001 C CNN
F 1 "GND" H 3950 7200 50  0000 C CNN
F 2 "" H 3950 7350 50  0000 C CNN
F 3 "" H 3950 7350 50  0000 C CNN
	1    3950 7350
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:CONN_02X04 P1
U 1 1 58C31D71
P 1650 2600
F 0 "P1" H 1650 2850 50  0000 C CNN
F 1 "CONN_02X04" H 1650 2350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 1650 2250 50  0000 C CNN
F 3 "" H 1650 1400 50  0000 C CNN
F 4 "9-146251-0" H 1650 2600 60  0001 C CNN "PartNumber"
	1    1650 2600
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:+3.3V #PWR?
U 1 1 58C31E48
P 1300 2350
F 0 "#PWR?" H 1300 2200 50  0001 C CNN
F 1 "+3.3V" H 1300 2490 50  0000 C CNN
F 2 "" H 1300 2350 50  0000 C CNN
F 3 "" H 1300 2350 50  0000 C CNN
	1    1300 2350
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C32236
P 2300 2550
F 0 "#PWR?" H 2300 2300 50  0001 C CNN
F 1 "GND" H 2300 2400 50  0000 C CNN
F 2 "" H 2300 2550 50  0000 C CNN
F 3 "" H 2300 2550 50  0000 C CNN
	1    2300 2550
	1    0    0    -1  
$EndComp
Text Label 1250 2550 0    60   ~ 0
CS
Text Label 1250 2650 0    60   ~ 0
SIMO
Text Label 1250 2750 0    60   ~ 0
GDO0
Text Label 1950 2750 0    60   ~ 0
GDO2
Text Label 1950 2650 0    60   ~ 0
SOMI
Text Label 1950 2550 0    60   ~ 0
SCLK
$Comp
L cc2500-cc2592-rescue:C C16
U 1 1 58C322EF
P 2150 4900
F 0 "C16" H 2175 5000 50  0000 L CNN
F 1 "15pF" H 2175 4800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" V 2000 4650 50  0000 C CNN
F 3 "" H 2150 4900 50  0000 C CNN
F 4 "GCM1885C1H150JA16D" H 2150 4900 60  0001 C CNN "PartNumber"
	1    2150 4900
	0    1    1    0   
$EndComp
$Comp
L cc2500-cc2592-rescue:C C17
U 1 1 58C323FB
P 2150 5250
F 0 "C17" H 2175 5350 50  0000 L CNN
F 1 "15pF" H 2175 5150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" V 2000 5000 50  0000 C CNN
F 3 "" H 2150 5250 50  0000 C CNN
F 4 "GCM1885C1H150JA16D" H 2150 5250 60  0001 C CNN "PartNumber"
	1    2150 5250
	0    1    1    0   
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C3255E
P 1700 5150
F 0 "#PWR?" H 1700 4900 50  0001 C CNN
F 1 "GND" H 1700 5000 50  0000 C CNN
F 2 "" H 1700 5150 50  0000 C CNN
F 3 "" H 1700 5150 50  0000 C CNN
	1    1700 5150
	1    0    0    -1  
$EndComp
Text Label 7350 5200 0    60   ~ 0
RF_N
Text Label 7350 5300 0    60   ~ 0
RF_P
Text Label 6900 1300 0    60   ~ 0
RF_1
Text Label 5700 1300 0    60   ~ 0
RF_PI_1
Text Label 4800 1300 0    60   ~ 0
RF_PI_2
Text Label 4100 1300 0    60   ~ 0
ANT_FEED
Text Label 6600 4800 0    60   ~ 0
R_BIAS
Text Label 3450 4800 0    60   ~ 0
D_COUPLE
Text Label 3100 5100 0    60   ~ 0
XOSC_Q1
Text Label 3100 5300 0    60   ~ 0
XOSC_Q2
Text Label 3150 5200 0    60   ~ 0
AVDD
$Comp
L CC2592:CC2592_RGV_16 U2
U 1 1 58C4ECED
P 7100 1050
F 0 "U2" H 8700 1450 60  0000 C CNN
F 1 "CC2592_RGV_16" H 8700 1350 60  0000 C CNN
F 2 "MyLib:CC2592_RGV_16" H 8700 1290 60  0001 C CNN
F 3 "" H 6300 400 60  0000 C CNN
F 4 "CC2592RGVR" H 7100 1050 60  0001 C CNN "PartNumber"
F 5 "1:$3.00; 10:$2.70; 25:$2.46" H 7100 1050 60  0001 C CNN "kicost:TI_store:pricing"
F 6 "https://store.ti.com/CC2592RGVR.aspx" H 7100 1050 60  0001 C CNN "kicost:TI_store:link"
	1    7100 1050
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C4FBB5
P 8600 3200
F 0 "#PWR?" H 8600 2950 50  0001 C CNN
F 1 "GND" H 8600 3050 50  0000 C CNN
F 2 "" H 8600 3200 50  0000 C CNN
F 3 "" H 8600 3200 50  0000 C CNN
	1    8600 3200
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C4FCBC
P 8750 3200
F 0 "#PWR?" H 8750 2950 50  0001 C CNN
F 1 "GND" H 8750 3050 50  0000 C CNN
F 2 "" H 8750 3200 50  0000 C CNN
F 3 "" H 8750 3200 50  0000 C CNN
	1    8750 3200
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C4FD06
P 8900 3200
F 0 "#PWR?" H 8900 2950 50  0001 C CNN
F 1 "GND" H 8900 3050 50  0000 C CNN
F 2 "" H 8900 3200 50  0000 C CNN
F 3 "" H 8900 3200 50  0000 C CNN
	1    8900 3200
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C4FD50
P 9050 3200
F 0 "#PWR?" H 9050 2950 50  0001 C CNN
F 1 "GND" H 9050 3050 50  0000 C CNN
F 2 "" H 9050 3200 50  0000 C CNN
F 3 "" H 9050 3200 50  0000 C CNN
	1    9050 3200
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C4FD9A
P 9200 3200
F 0 "#PWR?" H 9200 2950 50  0001 C CNN
F 1 "GND" H 9200 3050 50  0000 C CNN
F 2 "" H 9200 3200 50  0000 C CNN
F 3 "" H 9200 3200 50  0000 C CNN
	1    9200 3200
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C4FDE4
P 9350 3200
F 0 "#PWR?" H 9350 2950 50  0001 C CNN
F 1 "GND" H 9350 3050 50  0000 C CNN
F 2 "" H 9350 3200 50  0000 C CNN
F 3 "" H 9350 3200 50  0000 C CNN
	1    9350 3200
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C4FE2E
P 9500 3200
F 0 "#PWR?" H 9500 2950 50  0001 C CNN
F 1 "GND" H 9500 3050 50  0000 C CNN
F 2 "" H 9500 3200 50  0000 C CNN
F 3 "" H 9500 3200 50  0000 C CNN
	1    9500 3200
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:INDUCTOR L6
U 1 1 58C50A7D
P 8850 5250
F 0 "L6" V 8800 5250 50  0000 C CNN
F 1 "19.25nH" V 8950 5250 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" V 8700 5250 50  0000 C CNN
F 3 "" H 8850 5250 50  0000 C CNN
F 4 "LQW15AN19NJ00D" V 8850 5250 60  0001 C CNN "PartNumber"
	1    8850 5250
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C20
U 1 1 58C50C16
P 8550 4900
F 0 "C20" H 8575 5000 50  0000 L CNN
F 1 "100pF" H 8575 4800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 8300 4900 50  0000 C CNN
F 3 "" H 8550 4900 50  0000 C CNN
F 4 "GRM1555C1H101JA01D" H 8550 4900 60  0001 C CNN "PartNumber"
	1    8550 4900
	0    1    1    0   
$EndComp
$Comp
L cc2500-cc2592-rescue:C C21
U 1 1 58C50C96
P 8550 5600
F 0 "C21" H 8575 5700 50  0000 L CNN
F 1 "100pF" H 8575 5500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 8950 5600 50  0000 C CNN
F 3 "" H 8550 5600 50  0000 C CNN
F 4 "GRM1555C1H101JA01D" H 8550 5600 60  0001 C CNN "PartNumber"
	1    8550 5600
	0    1    1    0   
$EndComp
$Comp
L cc2500-cc2592-rescue:C C23
U 1 1 58C50D06
P 9200 5600
F 0 "C23" H 9225 5700 50  0000 L CNN
F 1 "100pF" H 9225 5500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 9500 5600 50  0000 C CNN
F 3 "" H 9200 5600 50  0000 C CNN
F 4 "GRM1555C1H101JA01D" H 9200 5600 60  0001 C CNN "PartNumber"
	1    9200 5600
	0    1    1    0   
$EndComp
$Comp
L cc2500-cc2592-rescue:C C22
U 1 1 58C50D7B
P 9200 4900
F 0 "C22" H 9225 5000 50  0000 L CNN
F 1 "100pF" H 9225 4800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 9050 4900 50  0000 C CNN
F 3 "" H 9200 4900 50  0000 C CNN
F 4 "GRM1555C1H101JA01D" H 9200 4900 60  0001 C CNN "PartNumber"
	1    9200 4900
	0    1    1    0   
$EndComp
$Comp
L cc2500-cc2592-rescue:R R2
U 1 1 58C51FF9
P 6950 3000
F 0 "R2" V 7030 3000 50  0000 C CNN
F 1 "3.9k" V 6950 3000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6880 3000 50  0000 C CNN
F 3 "" H 6950 3000 50  0000 C CNN
F 4 "RC0603FR-073K9L" V 6950 3000 60  0001 C CNN "PartNumber"
	1    6950 3000
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C521A6
P 6950 3400
F 0 "#PWR?" H 6950 3150 50  0001 C CNN
F 1 "GND" H 6950 3250 50  0000 C CNN
F 2 "" H 6950 3400 50  0000 C CNN
F 3 "" H 6950 3400 50  0000 C CNN
	1    6950 3400
	1    0    0    -1  
$EndComp
Text Label 8250 550  0    60   ~ 0
VDD_CC2592
$Comp
L cc2500-cc2592-rescue:R R3
U 1 1 58C52EBF
P 10250 3050
F 0 "R3" V 10330 3050 50  0000 C CNN
F 1 "10k" V 10250 3050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 10180 3050 50  0000 C CNN
F 3 "" H 10250 3050 50  0000 C CNN
F 4 "RC0603FR-0710KL" V 10250 3050 60  0001 C CNN "PartNumber"
	1    10250 3050
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:R R4
U 1 1 58C52F64
P 10350 3800
F 0 "R4" V 10430 3800 50  0000 C CNN
F 1 "10k" V 10350 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 10280 3800 50  0000 C CNN
F 3 "" H 10350 3800 50  0000 C CNN
F 4 "RC0603FR-0710KL" V 10350 3800 60  0001 C CNN "PartNumber"
	1    10350 3800
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:R R5
U 1 1 58C530D2
P 10450 3050
F 0 "R5" V 10530 3050 50  0000 C CNN
F 1 "10k" V 10450 3050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 10380 3050 50  0000 C CNN
F 3 "" H 10450 3050 50  0000 C CNN
F 4 "RC0603FR-0710KL" V 10450 3050 60  0001 C CNN "PartNumber"
	1    10450 3050
	1    0    0    -1  
$EndComp
Text Label 9650 4150 0    60   ~ 0
HGM
Text Label 9650 4250 0    60   ~ 0
LNA_EN
Text Label 9650 4350 0    60   ~ 0
PA_EN
$Comp
L cc2500-cc2592-rescue:CONN_02X03 P2
U 1 1 58C53E7A
P 9000 4250
F 0 "P2" H 9000 4450 50  0000 C CNN
F 1 "CONN_02X03" H 9000 4050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 9000 3050 50  0000 C CNN
F 3 "" H 9000 3050 50  0000 C CNN
F 4 "9-146251-0" H 9000 4250 60  0001 C CNN "PartNumber"
	1    9000 4250
	1    0    0    -1  
$EndComp
Text Label 8500 4350 0    60   ~ 0
GDO2
Text Label 8500 4250 0    60   ~ 0
GDO2
Text Label 8500 4150 0    60   ~ 0
GDO0
$Comp
L cc2500-cc2592-rescue:INDUCTOR L3
U 1 1 58C54933
P 4800 3150
F 0 "L3" V 4750 3150 50  0000 C CNN
F 1 "BLM18HE152SN1" V 4900 3150 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603" V 5000 3150 50  0000 C CNN
F 3 "" H 4800 3150 50  0000 C CNN
F 4 "BLM18HE152SN1D" V 4800 3150 60  0001 C CNN "PartNumber"
	1    4800 3150
	0    -1   -1   0   
$EndComp
$Comp
L cc2500-cc2592-rescue:C C9
U 1 1 58C54E8C
P 5300 3450
F 0 "C9" H 5325 3550 50  0000 L CNN
F 1 "2.2uF" H 5325 3350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 5150 3450 50  0000 C CNN
F 3 "" H 5300 3450 50  0000 C CNN
F 4 "0402ZD225KAT2A" H 5300 3450 60  0001 C CNN "PartNumber"
	1    5300 3450
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C11
U 1 1 58C54FBC
P 5750 3450
F 0 "C11" H 5775 3550 50  0000 L CNN
F 1 "12pF" H 5775 3350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 5600 3450 50  0000 C CNN
F 3 "" H 5750 3450 50  0000 C CNN
F 4 "GRM1555C1H120GA01D" H 5750 3450 60  0001 C CNN "PartNumber"
	1    5750 3450
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C14
U 1 1 58C5503A
P 6150 3450
F 0 "C14" H 6175 3550 50  0000 L CNN
F 1 "1pF" H 6175 3350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 6000 3450 50  0000 C CNN
F 3 "" H 6150 3450 50  0000 C CNN
F 4 "GRM1555C1H1R0CA01D" H 6150 3450 60  0001 C CNN "PartNumber"
	1    6150 3450
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C5587A
P 6150 3750
F 0 "#PWR?" H 6150 3500 50  0001 C CNN
F 1 "GND" H 6150 3600 50  0000 C CNN
F 2 "" H 6150 3750 50  0000 C CNN
F 3 "" H 6150 3750 50  0000 C CNN
	1    6150 3750
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C558EE
P 5750 3750
F 0 "#PWR?" H 5750 3500 50  0001 C CNN
F 1 "GND" H 5750 3600 50  0000 C CNN
F 2 "" H 5750 3750 50  0000 C CNN
F 3 "" H 5750 3750 50  0000 C CNN
	1    5750 3750
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C55962
P 5300 3750
F 0 "#PWR?" H 5300 3500 50  0001 C CNN
F 1 "GND" H 5300 3600 50  0000 C CNN
F 2 "" H 5300 3750 50  0000 C CNN
F 3 "" H 5300 3750 50  0000 C CNN
	1    5300 3750
	1    0    0    -1  
$EndComp
Text Label 6100 3150 0    60   ~ 0
VDD_CC2592
$Comp
L cc2500-cc2592-rescue:INDUCTOR L5
U 1 1 58C55E55
P 6850 1800
F 0 "L5" V 6800 1800 50  0000 C CNN
F 1 "4.7nH" V 6950 1800 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" V 6750 1950 50  0000 C CNN
F 3 "" H 6850 1800 50  0000 C CNN
F 4 "LQG15HZ4N7C02D" V 6850 1800 60  0001 C CNN "PartNumber"
	1    6850 1800
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C10
U 1 1 58C565F4
P 5600 2500
F 0 "C10" H 5625 2600 50  0000 L CNN
F 1 "1uF" H 5625 2400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 5450 2500 50  0000 C CNN
F 3 "" H 5600 2500 50  0000 C CNN
F 4 "GRM155R60J105ME19D" H 5600 2500 60  0001 C CNN "PartNumber"
	1    5600 2500
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C13
U 1 1 58C565FA
P 6050 2500
F 0 "C13" H 6075 2600 50  0000 L CNN
F 1 "12pF" H 6075 2400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 5900 2500 50  0000 C CNN
F 3 "" H 6050 2500 50  0000 C CNN
F 4 "GRM1555C1H120GA01D" H 6050 2500 60  0001 C CNN "PartNumber"
	1    6050 2500
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C18
U 1 1 58C56600
P 6450 2500
F 0 "C18" H 6475 2600 50  0000 L CNN
F 1 "1pF" H 6475 2400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 6300 2500 50  0000 C CNN
F 3 "" H 6450 2500 50  0000 C CNN
F 4 "GRM1555C1H1R0CA01D" H 6450 2500 60  0001 C CNN "PartNumber"
	1    6450 2500
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C5660C
P 6450 2800
F 0 "#PWR?" H 6450 2550 50  0001 C CNN
F 1 "GND" H 6450 2650 50  0000 C CNN
F 2 "" H 6450 2800 50  0000 C CNN
F 3 "" H 6450 2800 50  0000 C CNN
	1    6450 2800
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C56612
P 6050 2800
F 0 "#PWR?" H 6050 2550 50  0001 C CNN
F 1 "GND" H 6050 2650 50  0000 C CNN
F 2 "" H 6050 2800 50  0000 C CNN
F 3 "" H 6050 2800 50  0000 C CNN
	1    6050 2800
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C56618
P 5600 2800
F 0 "#PWR?" H 5600 2550 50  0001 C CNN
F 1 "GND" H 5600 2650 50  0000 C CNN
F 2 "" H 5600 2800 50  0000 C CNN
F 3 "" H 5600 2800 50  0000 C CNN
	1    5600 2800
	1    0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:C C19
U 1 1 58C57045
P 7100 1700
F 0 "C19" H 7125 1800 50  0000 L CNN
F 1 "2.2pF" H 7125 1600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" V 7250 1700 50  0000 C CNN
F 3 "" H 7100 1700 50  0000 C CNN
F 4 "GRM1555C1E2R2BA01D" H 7100 1700 60  0001 C CNN "PartNumber"
	1    7100 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 4400 3550 4400
Wire Wire Line
	3750 4500 3550 4500
Wire Wire Line
	3750 4600 3550 4600
Wire Wire Line
	3250 4700 3750 4700
Wire Wire Line
	6550 4500 6750 4500
Wire Wire Line
	3750 4900 3550 4900
Wire Wire Line
	3750 5000 3550 5000
Wire Wire Line
	3450 4800 3750 4800
Wire Wire Line
	2950 5100 3750 5100
Wire Wire Line
	2950 5200 3650 5200
Wire Wire Line
	3050 5300 3750 5300
Wire Wire Line
	3250 4700 3250 4600
Wire Wire Line
	1000 5350 1000 5450
Wire Wire Line
	1000 5450 1100 5450
Wire Wire Line
	1700 5450 2100 5450
Wire Wire Line
	2950 5200 2950 5450
Wire Wire Line
	2500 5550 2500 5450
Connection ~ 2500 5450
Wire Wire Line
	2100 5550 2100 5450
Connection ~ 2100 5450
Wire Wire Line
	1700 5550 1700 5450
Wire Wire Line
	1700 5950 1700 5850
Wire Wire Line
	2950 5950 2950 5850
Wire Wire Line
	2500 5950 2500 5850
Wire Wire Line
	2100 5950 2100 5850
Connection ~ 2950 5450
Wire Wire Line
	2700 5050 2950 5050
Wire Wire Line
	2950 5050 2950 5100
Wire Wire Line
	2300 5350 2700 5350
Wire Wire Line
	3050 5350 3050 5300
Wire Wire Line
	3450 4800 3450 5550
Wire Wire Line
	3450 5850 3450 5950
Wire Wire Line
	7250 4400 7250 4600
Wire Wire Line
	7250 4900 6550 4900
Wire Wire Line
	6550 4600 7250 4600
Connection ~ 7250 4900
Wire Wire Line
	6550 4400 7250 4400
Connection ~ 7250 4600
Wire Wire Line
	6550 4800 7100 4800
Wire Wire Line
	7100 4800 7100 5350
Wire Wire Line
	7100 5950 7100 5650
Wire Wire Line
	3650 5200 3650 5700
Wire Wire Line
	3650 5700 6650 5700
Wire Wire Line
	6650 5700 6650 5400
Wire Wire Line
	6650 5400 6550 5400
Connection ~ 3650 5200
Wire Wire Line
	6650 5100 6550 5100
Connection ~ 6650 5400
Wire Wire Line
	6650 5000 6550 5000
Connection ~ 6650 5100
Wire Wire Line
	7400 4600 7400 4700
Wire Wire Line
	6550 4700 7400 4700
Wire Wire Line
	6550 5200 8100 5200
Wire Wire Line
	6550 5300 8100 5300
Wire Wire Line
	7650 5950 7650 5850
Wire Wire Line
	7650 4700 7650 5550
Connection ~ 7400 4700
Wire Wire Line
	6650 1300 6850 1300
Wire Wire Line
	5700 1300 5850 1300
Wire Wire Line
	5850 1450 5850 1300
Connection ~ 5850 1300
Wire Wire Line
	4750 1300 4950 1300
Wire Wire Line
	4950 1450 4950 1300
Connection ~ 4950 1300
Wire Wire Line
	4950 1850 4950 1750
Wire Wire Line
	5850 1850 5850 1750
Wire Wire Line
	3950 6850 3950 6650
Wire Wire Line
	3950 7350 3950 7150
Wire Wire Line
	1400 2450 1300 2450
Wire Wire Line
	1300 2450 1300 2350
Wire Wire Line
	1400 2550 1200 2550
Wire Wire Line
	1400 2650 1200 2650
Wire Wire Line
	1400 2750 1200 2750
Wire Wire Line
	1900 2750 2100 2750
Wire Wire Line
	2100 2650 1900 2650
Wire Wire Line
	1900 2550 2100 2550
Wire Wire Line
	1900 2450 2300 2450
Wire Wire Line
	2300 2450 2300 2550
Wire Wire Line
	2300 5350 2300 5250
Connection ~ 2700 5350
Wire Wire Line
	2700 5050 2700 4900
Wire Wire Line
	2700 4900 2300 4900
Wire Wire Line
	1700 5150 1850 5150
Wire Wire Line
	1850 4900 1850 5150
Wire Wire Line
	1850 4900 2000 4900
Wire Wire Line
	1850 5250 2000 5250
Connection ~ 1850 5150
Wire Wire Line
	9500 3200 9500 3100
Wire Wire Line
	9350 3100 9350 3200
Wire Wire Line
	9200 3100 9200 3200
Wire Wire Line
	9050 3200 9050 3100
Wire Wire Line
	8900 3100 8900 3200
Wire Wire Line
	8750 3200 8750 3100
Wire Wire Line
	8600 3100 8600 3200
Wire Wire Line
	8700 4900 8850 4900
Wire Wire Line
	8700 5600 8850 5600
Wire Wire Line
	8850 5600 8850 5550
Connection ~ 8850 5600
Wire Wire Line
	8850 4950 8850 4900
Connection ~ 8850 4900
Wire Wire Line
	8400 4900 8100 4900
Wire Wire Line
	8100 4900 8100 5200
Wire Wire Line
	8100 5300 8100 5600
Wire Wire Line
	8100 5600 8400 5600
Wire Wire Line
	9350 4900 10700 4900
Wire Wire Line
	10700 4900 10700 1650
Wire Wire Line
	10700 1650 10200 1650
Wire Wire Line
	10200 1300 10950 1300
Wire Wire Line
	10950 1300 10950 5600
Wire Wire Line
	10950 5600 9350 5600
Wire Wire Line
	6950 2850 6950 2650
Wire Wire Line
	6950 2650 7200 2650
Wire Wire Line
	6950 3150 6950 3400
Wire Wire Line
	8150 550  8150 650 
Wire Wire Line
	8000 650  8000 550 
Wire Wire Line
	7850 650  7850 550 
Wire Wire Line
	7850 550  8000 550 
Connection ~ 8000 550 
Connection ~ 8150 550 
Wire Wire Line
	10250 2900 10250 2500
Wire Wire Line
	10250 2500 10200 2500
Wire Wire Line
	10200 2300 10450 2300
Wire Wire Line
	10450 2300 10450 2900
Wire Wire Line
	10200 2400 10350 2400
Wire Wire Line
	10350 2400 10350 3650
Wire Wire Line
	10350 4250 10350 3950
Wire Wire Line
	9250 4250 10350 4250
Wire Wire Line
	10450 4350 10450 3200
Wire Wire Line
	9500 4350 10450 4350
Wire Wire Line
	10250 3200 10250 4150
Wire Wire Line
	10250 4150 9600 4150
Wire Wire Line
	8750 4150 8450 4150
Wire Wire Line
	8750 4250 8450 4250
Wire Wire Line
	8450 4350 8750 4350
Wire Wire Line
	4250 3150 4500 3150
Wire Wire Line
	5300 3300 5300 3150
Wire Wire Line
	6750 3150 6150 3150
Wire Wire Line
	5750 3150 5750 3300
Connection ~ 5300 3150
Wire Wire Line
	6150 3150 6150 3300
Connection ~ 5750 3150
Wire Wire Line
	5300 3600 5300 3750
Wire Wire Line
	5750 3600 5750 3750
Wire Wire Line
	6150 3750 6150 3600
Connection ~ 6150 3150
Wire Wire Line
	6850 1500 6850 1300
Connection ~ 6850 1300
Wire Wire Line
	6750 2100 6750 2200
Wire Wire Line
	5600 2350 5600 2200
Wire Wire Line
	6050 2200 6050 2350
Wire Wire Line
	6450 2200 6450 2350
Connection ~ 6050 2200
Wire Wire Line
	5600 2650 5600 2800
Wire Wire Line
	6050 2650 6050 2800
Wire Wire Line
	6450 2800 6450 2650
Connection ~ 6450 2200
Wire Wire Line
	5600 2200 6050 2200
Connection ~ 6750 2200
Wire Wire Line
	6750 2100 6850 2100
Wire Wire Line
	7100 1550 7100 1300
Connection ~ 7100 1300
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 58C57457
P 7100 2050
F 0 "#PWR?" H 7100 1800 50  0001 C CNN
F 1 "GND" H 7100 1900 50  0000 C CNN
F 2 "" H 7100 2050 50  0000 C CNN
F 3 "" H 7100 2050 50  0000 C CNN
	1    7100 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 2050 7100 1850
$Comp
L cc2500-cc2592-rescue:INDUCTOR L4
U 1 1 58C57792
P 6350 1300
F 0 "L4" V 6300 1300 50  0000 C CNN
F 1 "1nH" V 6450 1300 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" V 6550 1300 50  0000 C CNN
F 3 "" H 6350 1300 50  0000 C CNN
F 4 "LQG15HS1N0S02D" V 6350 1300 60  0001 C CNN "PartNumber"
	1    6350 1300
	0    -1   -1   0   
$EndComp
Text Label 8800 4900 0    60   ~ 0
RF_N_T
Text Label 8750 5600 0    60   ~ 0
RF_P_T
Text Label 9500 4900 0    60   ~ 0
RF_N_O
Text Label 9500 5600 0    60   ~ 0
RF_P_O
Text Label 10250 2500 0    60   ~ 0
HGM_R
Text Label 10250 2400 0    60   ~ 0
LNA_EN_R
Text Label 10250 2300 0    60   ~ 0
PA_EN_R
Wire Wire Line
	9500 4350 9500 4150
Wire Wire Line
	9500 4150 9250 4150
Wire Wire Line
	9600 4150 9600 4300
Wire Wire Line
	9600 4300 9400 4300
Wire Wire Line
	9400 4300 9400 4350
Wire Wire Line
	9400 4350 9250 4350
Text Label 7000 2650 0    60   ~ 0
BIAS
Text Label 4300 3150 0    60   ~ 0
AVDD
Wire Wire Line
	2500 5450 2950 5450
Wire Wire Line
	2100 5450 2500 5450
Wire Wire Line
	2950 5450 2950 5550
Wire Wire Line
	7250 4900 7250 5950
Wire Wire Line
	7250 4600 7250 4900
Wire Wire Line
	3650 5200 3750 5200
Wire Wire Line
	6650 5400 6650 5100
Wire Wire Line
	6650 5100 6650 5000
Wire Wire Line
	7400 4700 7650 4700
Wire Wire Line
	5850 1300 6050 1300
Wire Wire Line
	4950 1300 5100 1300
Wire Wire Line
	2700 5350 3050 5350
Wire Wire Line
	1850 5150 1850 5250
Wire Wire Line
	8850 5600 9050 5600
Wire Wire Line
	8850 4900 9050 4900
Wire Wire Line
	8000 550  8150 550 
Wire Wire Line
	8150 550  8500 550 
Wire Wire Line
	5300 3150 5100 3150
Wire Wire Line
	5750 3150 5300 3150
Wire Wire Line
	6150 3150 5750 3150
Wire Wire Line
	6850 1300 7100 1300
Wire Wire Line
	6050 2200 6450 2200
Wire Wire Line
	6450 2200 6750 2200
Wire Wire Line
	6750 2200 6750 3150
Wire Wire Line
	7100 1300 7200 1300
Wire Wire Line
	3750 1300 4450 1300
$Comp
L Connector:Conn_Coaxial J101
U 1 1 5B923DBE
P 3550 1300
F 0 "J101" H 3480 1538 50  0000 C CNN
F 1 "Conn_Coaxial" H 3480 1447 50  0000 C CNN
F 2 "rf2:SMA_endlaunch_SD-73251-115" H 3550 1300 50  0001 C CNN
F 3 " ~" H 3550 1300 50  0001 C CNN
	1    3550 1300
	-1   0    0    -1  
$EndComp
$Comp
L cc2500-cc2592-rescue:GND #PWR?
U 1 1 5B923E8B
P 3550 1850
F 0 "#PWR?" H 3550 1600 50  0001 C CNN
F 1 "GND" H 3550 1700 50  0000 C CNN
F 2 "" H 3550 1850 50  0000 C CNN
F 3 "" H 3550 1850 50  0000 C CNN
	1    3550 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 1850 3550 1500
$EndSCHEMATC
