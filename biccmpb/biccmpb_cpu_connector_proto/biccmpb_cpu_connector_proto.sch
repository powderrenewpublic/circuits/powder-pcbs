EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L POWDER_Connector:Conn_02x70_Odd_Even J102
U 1 1 5F0CA06A
P 3050 4450
F 0 "J102" H 3100 8067 50  0000 C CNN
F 1 "Conn_02x70_Odd_Even" H 3100 7976 50  0000 C CNN
F 2 "POWDER_Connector:Amphenol_ICC_61082-141402LF" H 3050 4450 50  0001 C CNN
F 3 "" H 3050 4450 50  0001 C CNN
	1    3050 4450
	1    0    0    -1  
$EndComp
$Comp
L POWDER_Connector:Conn_02x70_Odd_Even J103
U 1 1 5F0D4D2C
P 5050 4450
F 0 "J103" H 5100 8067 50  0000 C CNN
F 1 "Conn_02x70_Odd_Even" H 5100 7976 50  0000 C CNN
F 2 "POWDER_Connector:Amphenol_ICC_61083-141402LF" H 5050 4450 50  0001 C CNN
F 3 "" H 5050 4450 50  0001 C CNN
	1    5050 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x40_Odd_Even J101
U 1 1 5F0DBBC8
P 1550 4450
F 0 "J101" H 1600 6567 50  0000 C CNN
F 1 "Conn_02x40_Odd_Even" H 1600 6476 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x40_P1.27mm_Vertical_SMD" H 1550 4450 50  0001 C CNN
F 3 "~" H 1550 4450 50  0001 C CNN
	1    1550 4450
	1    0    0    -1  
$EndComp
Text Notes 2650 8200 0    50   ~ 0
^ Power board side, i.e. top side
Text Notes 4750 8200 0    50   ~ 0
^ CPU board, i.e. down side
$Comp
L Connector_Generic:Conn_02x40_Odd_Even J104
U 1 1 5F0F3A8E
P 7050 4450
F 0 "J104" H 7100 6567 50  0000 C CNN
F 1 "Conn_02x40_Odd_Even" H 7100 6476 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x40_P1.27mm_Vertical_SMD" H 7050 4450 50  0001 C CNN
F 3 "~" H 7050 4450 50  0001 C CNN
	1    7050 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1050 2500 1050
Text Label 2550 1050 0    50   ~ 0
c1p1
Wire Wire Line
	2850 1150 2500 1150
Text Label 2550 1150 0    50   ~ 0
c1p3
Wire Wire Line
	2850 1250 2500 1250
Text Label 2550 1250 0    50   ~ 0
c1p5
Wire Wire Line
	2850 1350 2500 1350
Text Label 2550 1350 0    50   ~ 0
c1p7
Wire Wire Line
	2850 1450 2500 1450
Text Label 2550 1450 0    50   ~ 0
c1p9
Wire Wire Line
	2850 1550 2500 1550
Text Label 2550 1550 0    50   ~ 0
c1p11
Wire Wire Line
	2850 1650 2500 1650
Text Label 2550 1650 0    50   ~ 0
c1p13
Wire Wire Line
	2850 1750 2500 1750
Text Label 2550 1750 0    50   ~ 0
c1p15
Wire Wire Line
	2850 1850 2500 1850
Text Label 2550 1850 0    50   ~ 0
c1p17
Wire Wire Line
	2850 1950 2500 1950
Text Label 2550 1950 0    50   ~ 0
c1p19
Wire Wire Line
	2850 2050 2500 2050
Text Label 2550 2050 0    50   ~ 0
c1p21
Wire Wire Line
	2850 2150 2500 2150
Text Label 2550 2150 0    50   ~ 0
c1p23
Wire Wire Line
	2850 2250 2500 2250
Text Label 2550 2250 0    50   ~ 0
c1p25
Wire Wire Line
	2850 2350 2500 2350
Text Label 2550 2350 0    50   ~ 0
c1p27
Wire Wire Line
	2850 2450 2500 2450
Text Label 2550 2450 0    50   ~ 0
c1p29
Wire Wire Line
	2850 2550 2500 2550
Text Label 2550 2550 0    50   ~ 0
c1p31
Wire Wire Line
	2850 2650 2500 2650
Text Label 2550 2650 0    50   ~ 0
c1p33
Wire Wire Line
	2850 2750 2500 2750
Text Label 2550 2750 0    50   ~ 0
c1p35
Wire Wire Line
	2850 2850 2500 2850
Text Label 2550 2850 0    50   ~ 0
c1p37
Wire Wire Line
	2850 2950 2500 2950
Text Label 2550 2950 0    50   ~ 0
c1p39
Wire Wire Line
	2850 3050 2500 3050
Text Label 2550 3050 0    50   ~ 0
c1p41
Wire Wire Line
	2850 3150 2500 3150
Text Label 2550 3150 0    50   ~ 0
c1p43
Wire Wire Line
	2850 3250 2500 3250
Text Label 2550 3250 0    50   ~ 0
c1p45
Wire Wire Line
	2850 3350 2500 3350
Text Label 2550 3350 0    50   ~ 0
c1p47
Wire Wire Line
	2850 3450 2500 3450
Text Label 2550 3450 0    50   ~ 0
c1p49
Wire Wire Line
	2850 3550 2500 3550
Text Label 2550 3550 0    50   ~ 0
c1p51
Wire Wire Line
	2850 3650 2500 3650
Text Label 2550 3650 0    50   ~ 0
c1p53
Wire Wire Line
	2850 3750 2500 3750
Text Label 2550 3750 0    50   ~ 0
c1p55
Wire Wire Line
	2850 3850 2500 3850
Text Label 2550 3850 0    50   ~ 0
c1p57
Wire Wire Line
	2850 3950 2500 3950
Text Label 2550 3950 0    50   ~ 0
c1p59
Wire Wire Line
	2850 4050 2500 4050
Text Label 2550 4050 0    50   ~ 0
c1p61
Wire Wire Line
	2850 4150 2500 4150
Text Label 2550 4150 0    50   ~ 0
c1p63
Wire Wire Line
	2850 4250 2500 4250
Text Label 2550 4250 0    50   ~ 0
c1p65
Wire Wire Line
	2850 4350 2500 4350
Text Label 2550 4350 0    50   ~ 0
c1p67
Wire Wire Line
	2850 4450 2500 4450
Text Label 2550 4450 0    50   ~ 0
c1p69
Wire Wire Line
	2850 4550 2500 4550
Text Label 2550 4550 0    50   ~ 0
c1p71
Wire Wire Line
	2850 4650 2500 4650
Text Label 2550 4650 0    50   ~ 0
c1p73
Wire Wire Line
	2850 4750 2500 4750
Text Label 2550 4750 0    50   ~ 0
c1p75
Wire Wire Line
	2850 4850 2500 4850
Text Label 2550 4850 0    50   ~ 0
c1p77
Wire Wire Line
	2850 4950 2500 4950
Wire Wire Line
	2850 5050 2500 5050
Text Label 2550 5050 0    50   ~ 0
c1p81
Wire Wire Line
	2850 5150 2500 5150
Text Label 2550 5150 0    50   ~ 0
c1p83
Wire Wire Line
	2850 5250 2500 5250
Text Label 2550 5250 0    50   ~ 0
c1p85
Wire Wire Line
	2850 5350 2500 5350
Text Label 2550 5350 0    50   ~ 0
c1p87
Wire Wire Line
	2850 5450 2500 5450
Text Label 2550 5450 0    50   ~ 0
c1p89
Wire Wire Line
	2850 5550 2500 5550
Text Label 2550 5550 0    50   ~ 0
c1p91
Wire Wire Line
	2850 5650 2500 5650
Text Label 2550 5650 0    50   ~ 0
c1p93
Wire Wire Line
	2850 5750 2500 5750
Text Label 2550 5750 0    50   ~ 0
c1p95
Wire Wire Line
	2850 5850 2500 5850
Text Label 2550 5850 0    50   ~ 0
c1p97
Wire Wire Line
	2850 5950 2500 5950
Text Label 2550 5950 0    50   ~ 0
c1p99
Wire Wire Line
	2850 6050 2500 6050
Text Label 2550 6050 0    50   ~ 0
c1p101
Wire Wire Line
	2850 6150 2500 6150
Text Label 2550 6150 0    50   ~ 0
c1p103
Wire Wire Line
	2850 6250 2500 6250
Text Label 2550 6250 0    50   ~ 0
c1p105
Wire Wire Line
	2850 6350 2500 6350
Text Label 2550 6350 0    50   ~ 0
c1p107
Wire Wire Line
	2850 6450 2500 6450
Text Label 2550 6450 0    50   ~ 0
c1p109
Wire Wire Line
	2850 6550 2500 6550
Text Label 2550 6550 0    50   ~ 0
c1p111
Wire Wire Line
	2850 6650 2500 6650
Text Label 2550 6650 0    50   ~ 0
c1p113
Wire Wire Line
	2850 6750 2500 6750
Text Label 2550 6750 0    50   ~ 0
c1p115
Wire Wire Line
	2850 6850 2500 6850
Text Label 2550 6850 0    50   ~ 0
c1p117
Wire Wire Line
	2850 6950 2500 6950
Text Label 2550 6950 0    50   ~ 0
c1p119
Wire Wire Line
	2850 7050 2500 7050
Text Label 2550 7050 0    50   ~ 0
c1p121
Wire Wire Line
	2850 7150 2500 7150
Text Label 2550 7150 0    50   ~ 0
c1p123
Wire Wire Line
	2850 7250 2500 7250
Text Label 2550 7250 0    50   ~ 0
c1p125
Wire Wire Line
	2850 7350 2500 7350
Text Label 2550 7350 0    50   ~ 0
c1p127
Wire Wire Line
	2850 7450 2500 7450
Text Label 2550 7450 0    50   ~ 0
c1p129
Wire Wire Line
	2850 7550 2500 7550
Text Label 2550 7550 0    50   ~ 0
c1p131
Wire Wire Line
	2850 7650 2500 7650
Text Label 2550 7650 0    50   ~ 0
c1p133
Wire Wire Line
	2850 7750 2500 7750
Text Label 2550 7750 0    50   ~ 0
c1p135
Wire Wire Line
	2850 7850 2500 7850
Text Label 2550 7850 0    50   ~ 0
c1p137
Wire Wire Line
	2850 7950 2500 7950
Text Label 2550 7950 0    50   ~ 0
c1p139
Wire Wire Line
	3700 2950 3350 2950
Text Label 3400 2950 0    50   ~ 0
c1p40
Wire Wire Line
	3700 3050 3350 3050
Text Label 3400 3050 0    50   ~ 0
c1p42
Wire Wire Line
	3700 3150 3350 3150
Text Label 3400 3150 0    50   ~ 0
c1p44
Wire Wire Line
	3700 3250 3350 3250
Text Label 3400 3250 0    50   ~ 0
c1p46
Wire Wire Line
	3700 3350 3350 3350
Text Label 3400 3350 0    50   ~ 0
c1p48
Wire Wire Line
	3700 3450 3350 3450
Text Label 3400 3450 0    50   ~ 0
c1p50
Wire Wire Line
	3700 3550 3350 3550
Text Label 3400 3550 0    50   ~ 0
c1p52
Wire Wire Line
	3700 3650 3350 3650
Text Label 3400 3650 0    50   ~ 0
c1p54
Wire Wire Line
	3700 3750 3350 3750
Text Label 3400 3750 0    50   ~ 0
c1p56
Wire Wire Line
	3700 3850 3350 3850
Text Label 3400 3850 0    50   ~ 0
c1p58
Wire Wire Line
	3700 3950 3350 3950
Text Label 3400 3950 0    50   ~ 0
c1p60
Wire Wire Line
	3700 4050 3350 4050
Text Label 3400 4050 0    50   ~ 0
c1p62
Wire Wire Line
	3700 4150 3350 4150
Text Label 3400 4150 0    50   ~ 0
c1p64
Wire Wire Line
	3700 4250 3350 4250
Text Label 3400 4250 0    50   ~ 0
c1p66
Wire Wire Line
	3700 4350 3350 4350
Text Label 3400 4350 0    50   ~ 0
c1p68
Wire Wire Line
	3700 4450 3350 4450
Text Label 3400 4450 0    50   ~ 0
c1p70
Wire Wire Line
	3700 4550 3350 4550
Text Label 3400 4550 0    50   ~ 0
c1p72
Wire Wire Line
	3700 4650 3350 4650
Text Label 3400 4650 0    50   ~ 0
c1p74
Wire Wire Line
	3700 4750 3350 4750
Text Label 3400 4750 0    50   ~ 0
c1p76
Wire Wire Line
	3700 4850 3350 4850
Text Label 3400 4850 0    50   ~ 0
c1p78
Wire Wire Line
	3700 4950 3350 4950
Wire Wire Line
	3700 5050 3350 5050
Text Label 3400 5050 0    50   ~ 0
c1p82
Wire Wire Line
	3700 5150 3350 5150
Text Label 3400 5150 0    50   ~ 0
c1p84
Wire Wire Line
	3700 5250 3350 5250
Text Label 3400 5250 0    50   ~ 0
c1p86
Wire Wire Line
	3700 5350 3350 5350
Text Label 3400 5350 0    50   ~ 0
c1p88
Wire Wire Line
	3700 5450 3350 5450
Text Label 3400 5450 0    50   ~ 0
c1p90
Wire Wire Line
	3700 5550 3350 5550
Text Label 3400 5550 0    50   ~ 0
c1p92
Wire Wire Line
	3700 5650 3350 5650
Text Label 3400 5650 0    50   ~ 0
c1p94
Wire Wire Line
	3700 5750 3350 5750
Text Label 3400 5750 0    50   ~ 0
c1p96
Wire Wire Line
	3700 5850 3350 5850
Text Label 3400 5850 0    50   ~ 0
c1p98
Wire Wire Line
	3700 5950 3350 5950
Text Label 3400 5950 0    50   ~ 0
c1p100
Wire Wire Line
	3700 6050 3350 6050
Text Label 3400 6050 0    50   ~ 0
c1p102
Wire Wire Line
	3700 6150 3350 6150
Text Label 3400 6150 0    50   ~ 0
c1p104
Wire Wire Line
	3700 6250 3350 6250
Text Label 3400 6250 0    50   ~ 0
c1p106
Wire Wire Line
	3700 6350 3350 6350
Text Label 3400 6350 0    50   ~ 0
c1p108
Wire Wire Line
	3700 6450 3350 6450
Text Label 3400 6450 0    50   ~ 0
c1p110
Wire Wire Line
	3700 6550 3350 6550
Text Label 3400 6550 0    50   ~ 0
c1p112
Wire Wire Line
	3700 6650 3350 6650
Text Label 3400 6650 0    50   ~ 0
c1p114
Wire Wire Line
	3700 6750 3350 6750
Text Label 3400 6750 0    50   ~ 0
c1p116
Wire Wire Line
	3700 6850 3350 6850
Text Label 3400 6850 0    50   ~ 0
c1p118
Wire Wire Line
	3700 6950 3350 6950
Text Label 3400 6950 0    50   ~ 0
c1p120
Wire Wire Line
	3700 7050 3350 7050
Text Label 3400 7050 0    50   ~ 0
c1p122
Wire Wire Line
	3700 7150 3350 7150
Text Label 3400 7150 0    50   ~ 0
c1p124
Wire Wire Line
	3700 7250 3350 7250
Text Label 3400 7250 0    50   ~ 0
c1p126
Wire Wire Line
	3700 7350 3350 7350
Text Label 3400 7350 0    50   ~ 0
c1p128
Wire Wire Line
	3700 7450 3350 7450
Text Label 3400 7450 0    50   ~ 0
c1p130
Wire Wire Line
	3700 7550 3350 7550
Text Label 3400 7550 0    50   ~ 0
c1p132
Wire Wire Line
	3700 7650 3350 7650
Text Label 3400 7650 0    50   ~ 0
c1p134
Wire Wire Line
	3700 7750 3350 7750
Text Label 3400 7750 0    50   ~ 0
c1p136
Wire Wire Line
	3700 7850 3350 7850
Text Label 3400 7850 0    50   ~ 0
c1p138
Wire Wire Line
	3700 7950 3350 7950
Text Label 3400 7950 0    50   ~ 0
c1p140
Text Label 2550 4950 0    50   ~ 0
c1p79
Text Label 3400 4950 0    50   ~ 0
c1p80
Wire Wire Line
	4850 1050 4500 1050
Text Label 4550 1050 0    50   ~ 0
c1p1
Wire Wire Line
	4850 1150 4500 1150
Text Label 4550 1150 0    50   ~ 0
c1p3
Wire Wire Line
	4850 1250 4500 1250
Text Label 4550 1250 0    50   ~ 0
c1p5
Wire Wire Line
	4850 1350 4500 1350
Text Label 4550 1350 0    50   ~ 0
c1p7
Wire Wire Line
	4850 1450 4500 1450
Text Label 4550 1450 0    50   ~ 0
c1p9
Wire Wire Line
	4850 1550 4500 1550
Text Label 4550 1550 0    50   ~ 0
c1p11
Wire Wire Line
	4850 1650 4500 1650
Text Label 4550 1650 0    50   ~ 0
c1p13
Wire Wire Line
	4850 1750 4500 1750
Text Label 4550 1750 0    50   ~ 0
c1p15
Wire Wire Line
	4850 1850 4500 1850
Text Label 4550 1850 0    50   ~ 0
c1p17
Wire Wire Line
	4850 1950 4500 1950
Text Label 4550 1950 0    50   ~ 0
c1p19
Wire Wire Line
	4850 2050 4500 2050
Text Label 4550 2050 0    50   ~ 0
c1p21
Wire Wire Line
	4850 2150 4500 2150
Text Label 4550 2150 0    50   ~ 0
c1p23
Wire Wire Line
	4850 2250 4500 2250
Text Label 4550 2250 0    50   ~ 0
c1p25
Wire Wire Line
	4850 2350 4500 2350
Text Label 4550 2350 0    50   ~ 0
c1p27
Wire Wire Line
	4850 2450 4500 2450
Text Label 4550 2450 0    50   ~ 0
c1p29
Wire Wire Line
	4850 2550 4500 2550
Text Label 4550 2550 0    50   ~ 0
c1p31
Wire Wire Line
	4850 2650 4500 2650
Text Label 4550 2650 0    50   ~ 0
c1p33
Wire Wire Line
	4850 2750 4500 2750
Text Label 4550 2750 0    50   ~ 0
c1p35
Wire Wire Line
	4850 2850 4500 2850
Text Label 4550 2850 0    50   ~ 0
c1p37
Wire Wire Line
	4850 2950 4500 2950
Text Label 4550 2950 0    50   ~ 0
c1p39
Wire Wire Line
	4850 3050 4500 3050
Text Label 4550 3050 0    50   ~ 0
c1p41
Wire Wire Line
	4850 3150 4500 3150
Text Label 4550 3150 0    50   ~ 0
c1p43
Wire Wire Line
	4850 3250 4500 3250
Text Label 4550 3250 0    50   ~ 0
c1p45
Wire Wire Line
	4850 3350 4500 3350
Text Label 4550 3350 0    50   ~ 0
c1p47
Wire Wire Line
	4850 3450 4500 3450
Text Label 4550 3450 0    50   ~ 0
c1p49
Wire Wire Line
	4850 3550 4500 3550
Text Label 4550 3550 0    50   ~ 0
c1p51
Wire Wire Line
	4850 3650 4500 3650
Text Label 4550 3650 0    50   ~ 0
c1p53
Wire Wire Line
	4850 3750 4500 3750
Text Label 4550 3750 0    50   ~ 0
c1p55
Wire Wire Line
	4850 3850 4500 3850
Text Label 4550 3850 0    50   ~ 0
c1p57
Wire Wire Line
	4850 3950 4500 3950
Text Label 4550 3950 0    50   ~ 0
c1p59
Wire Wire Line
	4850 4050 4500 4050
Text Label 4550 4050 0    50   ~ 0
c1p61
Wire Wire Line
	4850 4150 4500 4150
Text Label 4550 4150 0    50   ~ 0
c1p63
Wire Wire Line
	4850 4250 4500 4250
Text Label 4550 4250 0    50   ~ 0
c1p65
Wire Wire Line
	4850 4350 4500 4350
Text Label 4550 4350 0    50   ~ 0
c1p67
Wire Wire Line
	4850 4450 4500 4450
Text Label 4550 4450 0    50   ~ 0
c1p69
Wire Wire Line
	4850 4550 4500 4550
Text Label 4550 4550 0    50   ~ 0
c1p71
Wire Wire Line
	4850 4650 4500 4650
Text Label 4550 4650 0    50   ~ 0
c1p73
Wire Wire Line
	4850 4750 4500 4750
Text Label 4550 4750 0    50   ~ 0
c1p75
Wire Wire Line
	4850 4850 4500 4850
Text Label 4550 4850 0    50   ~ 0
c1p77
Wire Wire Line
	4850 4950 4500 4950
Wire Wire Line
	4850 5050 4500 5050
Text Label 4550 5050 0    50   ~ 0
c1p81
Wire Wire Line
	4850 5150 4500 5150
Text Label 4550 5150 0    50   ~ 0
c1p83
Wire Wire Line
	4850 5250 4500 5250
Text Label 4550 5250 0    50   ~ 0
c1p85
Wire Wire Line
	4850 5350 4500 5350
Text Label 4550 5350 0    50   ~ 0
c1p87
Wire Wire Line
	4850 5450 4500 5450
Text Label 4550 5450 0    50   ~ 0
c1p89
Wire Wire Line
	4850 5550 4500 5550
Text Label 4550 5550 0    50   ~ 0
c1p91
Wire Wire Line
	4850 5650 4500 5650
Text Label 4550 5650 0    50   ~ 0
c1p93
Wire Wire Line
	4850 5750 4500 5750
Text Label 4550 5750 0    50   ~ 0
c1p95
Wire Wire Line
	4850 5850 4500 5850
Text Label 4550 5850 0    50   ~ 0
c1p97
Wire Wire Line
	4850 5950 4500 5950
Text Label 4550 5950 0    50   ~ 0
c1p99
Wire Wire Line
	4850 6050 4500 6050
Text Label 4550 6050 0    50   ~ 0
c1p101
Wire Wire Line
	4850 6150 4500 6150
Text Label 4550 6150 0    50   ~ 0
c1p103
Wire Wire Line
	4850 6250 4500 6250
Text Label 4550 6250 0    50   ~ 0
c1p105
Wire Wire Line
	4850 6350 4500 6350
Text Label 4550 6350 0    50   ~ 0
c1p107
Wire Wire Line
	4850 6450 4500 6450
Text Label 4550 6450 0    50   ~ 0
c1p109
Wire Wire Line
	4850 6550 4500 6550
Text Label 4550 6550 0    50   ~ 0
c1p111
Wire Wire Line
	4850 6650 4500 6650
Text Label 4550 6650 0    50   ~ 0
c1p113
Wire Wire Line
	4850 6750 4500 6750
Text Label 4550 6750 0    50   ~ 0
c1p115
Wire Wire Line
	4850 6850 4500 6850
Text Label 4550 6850 0    50   ~ 0
c1p117
Wire Wire Line
	4850 6950 4500 6950
Text Label 4550 6950 0    50   ~ 0
c1p119
Wire Wire Line
	4850 7050 4500 7050
Text Label 4550 7050 0    50   ~ 0
c1p121
Wire Wire Line
	4850 7150 4500 7150
Text Label 4550 7150 0    50   ~ 0
c1p123
Wire Wire Line
	4850 7250 4500 7250
Text Label 4550 7250 0    50   ~ 0
c1p125
Wire Wire Line
	4850 7350 4500 7350
Text Label 4550 7350 0    50   ~ 0
c1p127
Wire Wire Line
	4850 7450 4500 7450
Text Label 4550 7450 0    50   ~ 0
c1p129
Wire Wire Line
	4850 7550 4500 7550
Text Label 4550 7550 0    50   ~ 0
c1p131
Wire Wire Line
	4850 7650 4500 7650
Text Label 4550 7650 0    50   ~ 0
c1p133
Wire Wire Line
	4850 7750 4500 7750
Text Label 4550 7750 0    50   ~ 0
c1p135
Wire Wire Line
	4850 7850 4500 7850
Text Label 4550 7850 0    50   ~ 0
c1p137
Wire Wire Line
	4850 7950 4500 7950
Text Label 4550 7950 0    50   ~ 0
c1p139
Text Label 4550 4950 0    50   ~ 0
c1p79
Wire Wire Line
	5700 1050 5350 1050
Text Label 5400 1050 0    50   ~ 0
c1p2
Wire Wire Line
	5700 1150 5350 1150
Text Label 5400 1150 0    50   ~ 0
c1p4
Wire Wire Line
	5700 1250 5350 1250
Text Label 5400 1250 0    50   ~ 0
c1p6
Wire Wire Line
	5700 1350 5350 1350
Text Label 5400 1350 0    50   ~ 0
c1p8
Wire Wire Line
	5700 1450 5350 1450
Text Label 5400 1450 0    50   ~ 0
c1p10
Wire Wire Line
	5700 1550 5350 1550
Text Label 5400 1550 0    50   ~ 0
c1p12
Wire Wire Line
	5700 1650 5350 1650
Text Label 5400 1650 0    50   ~ 0
c1p14
Wire Wire Line
	5700 1750 5350 1750
Text Label 5400 1750 0    50   ~ 0
c1p16
Wire Wire Line
	5700 1850 5350 1850
Text Label 5400 1850 0    50   ~ 0
c1p18
Wire Wire Line
	5700 1950 5350 1950
Text Label 5400 1950 0    50   ~ 0
c1p20
Wire Wire Line
	5700 2050 5350 2050
Text Label 5400 2050 0    50   ~ 0
c1p22
Wire Wire Line
	5700 2150 5350 2150
Text Label 5400 2150 0    50   ~ 0
c1p24
Wire Wire Line
	5700 2250 5350 2250
Text Label 5400 2250 0    50   ~ 0
c1p26
Wire Wire Line
	5700 2350 5350 2350
Text Label 5400 2350 0    50   ~ 0
c1p28
Wire Wire Line
	5700 2450 5350 2450
Text Label 5400 2450 0    50   ~ 0
c1p30
Wire Wire Line
	5700 2550 5350 2550
Text Label 5400 2550 0    50   ~ 0
c1p32
Wire Wire Line
	5700 2650 5350 2650
Text Label 5400 2650 0    50   ~ 0
c1p34
Wire Wire Line
	5700 2750 5350 2750
Text Label 5400 2750 0    50   ~ 0
c1p36
Wire Wire Line
	5700 2850 5350 2850
Text Label 5400 2850 0    50   ~ 0
c1p38
Wire Wire Line
	5700 2950 5350 2950
Text Label 5400 2950 0    50   ~ 0
c1p40
Wire Wire Line
	5700 3050 5350 3050
Text Label 5400 3050 0    50   ~ 0
c1p42
Wire Wire Line
	5700 3150 5350 3150
Text Label 5400 3150 0    50   ~ 0
c1p44
Wire Wire Line
	5700 3250 5350 3250
Text Label 5400 3250 0    50   ~ 0
c1p46
Wire Wire Line
	5700 3350 5350 3350
Text Label 5400 3350 0    50   ~ 0
c1p48
Wire Wire Line
	5700 3450 5350 3450
Text Label 5400 3450 0    50   ~ 0
c1p50
Wire Wire Line
	5700 3550 5350 3550
Text Label 5400 3550 0    50   ~ 0
c1p52
Wire Wire Line
	5700 3650 5350 3650
Text Label 5400 3650 0    50   ~ 0
c1p54
Wire Wire Line
	5700 3750 5350 3750
Text Label 5400 3750 0    50   ~ 0
c1p56
Wire Wire Line
	5700 3850 5350 3850
Text Label 5400 3850 0    50   ~ 0
c1p58
Wire Wire Line
	5700 3950 5350 3950
Text Label 5400 3950 0    50   ~ 0
c1p60
Wire Wire Line
	5700 4050 5350 4050
Text Label 5400 4050 0    50   ~ 0
c1p62
Wire Wire Line
	5700 4150 5350 4150
Text Label 5400 4150 0    50   ~ 0
c1p64
Wire Wire Line
	5700 4250 5350 4250
Text Label 5400 4250 0    50   ~ 0
c1p66
Wire Wire Line
	5700 4350 5350 4350
Text Label 5400 4350 0    50   ~ 0
c1p68
Wire Wire Line
	5700 4450 5350 4450
Text Label 5400 4450 0    50   ~ 0
c1p70
Wire Wire Line
	5700 4550 5350 4550
Text Label 5400 4550 0    50   ~ 0
c1p72
Wire Wire Line
	5700 4650 5350 4650
Text Label 5400 4650 0    50   ~ 0
c1p74
Wire Wire Line
	5700 4750 5350 4750
Text Label 5400 4750 0    50   ~ 0
c1p76
Wire Wire Line
	5700 4850 5350 4850
Text Label 5400 4850 0    50   ~ 0
c1p78
Wire Wire Line
	5700 4950 5350 4950
Wire Wire Line
	5700 5050 5350 5050
Text Label 5400 5050 0    50   ~ 0
c1p82
Wire Wire Line
	5700 5150 5350 5150
Text Label 5400 5150 0    50   ~ 0
c1p84
Wire Wire Line
	5700 5250 5350 5250
Text Label 5400 5250 0    50   ~ 0
c1p86
Wire Wire Line
	5700 5350 5350 5350
Text Label 5400 5350 0    50   ~ 0
c1p88
Wire Wire Line
	5700 5450 5350 5450
Text Label 5400 5450 0    50   ~ 0
c1p90
Wire Wire Line
	5700 5550 5350 5550
Text Label 5400 5550 0    50   ~ 0
c1p92
Wire Wire Line
	5700 5650 5350 5650
Text Label 5400 5650 0    50   ~ 0
c1p94
Wire Wire Line
	5700 5750 5350 5750
Text Label 5400 5750 0    50   ~ 0
c1p96
Wire Wire Line
	5700 5850 5350 5850
Text Label 5400 5850 0    50   ~ 0
c1p98
Wire Wire Line
	5700 5950 5350 5950
Text Label 5400 5950 0    50   ~ 0
c1p100
Wire Wire Line
	5700 6050 5350 6050
Text Label 5400 6050 0    50   ~ 0
c1p102
Wire Wire Line
	5700 6150 5350 6150
Text Label 5400 6150 0    50   ~ 0
c1p104
Wire Wire Line
	5700 6250 5350 6250
Text Label 5400 6250 0    50   ~ 0
c1p106
Wire Wire Line
	5700 6350 5350 6350
Text Label 5400 6350 0    50   ~ 0
c1p108
Wire Wire Line
	5700 6450 5350 6450
Text Label 5400 6450 0    50   ~ 0
c1p110
Wire Wire Line
	5700 6550 5350 6550
Text Label 5400 6550 0    50   ~ 0
c1p112
Wire Wire Line
	5700 6650 5350 6650
Text Label 5400 6650 0    50   ~ 0
c1p114
Wire Wire Line
	5700 6750 5350 6750
Text Label 5400 6750 0    50   ~ 0
c1p116
Wire Wire Line
	5700 6850 5350 6850
Text Label 5400 6850 0    50   ~ 0
c1p118
Wire Wire Line
	5700 6950 5350 6950
Text Label 5400 6950 0    50   ~ 0
c1p120
Wire Wire Line
	5700 7050 5350 7050
Text Label 5400 7050 0    50   ~ 0
c1p122
Wire Wire Line
	5700 7150 5350 7150
Text Label 5400 7150 0    50   ~ 0
c1p124
Wire Wire Line
	5700 7250 5350 7250
Text Label 5400 7250 0    50   ~ 0
c1p126
Wire Wire Line
	5700 7350 5350 7350
Text Label 5400 7350 0    50   ~ 0
c1p128
Wire Wire Line
	5700 7450 5350 7450
Text Label 5400 7450 0    50   ~ 0
c1p130
Wire Wire Line
	5700 7550 5350 7550
Text Label 5400 7550 0    50   ~ 0
c1p132
Wire Wire Line
	5700 7650 5350 7650
Text Label 5400 7650 0    50   ~ 0
c1p134
Wire Wire Line
	5700 7750 5350 7750
Text Label 5400 7750 0    50   ~ 0
c1p136
Wire Wire Line
	5700 7850 5350 7850
Text Label 5400 7850 0    50   ~ 0
c1p138
Wire Wire Line
	5700 7950 5350 7950
Text Label 5400 7950 0    50   ~ 0
c1p140
Text Label 5400 4950 0    50   ~ 0
c1p80
Wire Wire Line
	1350 2550 1000 2550
Text Label 1050 2550 0    50   ~ 0
c1p1
Wire Wire Line
	2200 2550 1850 2550
Text Label 1900 2550 0    50   ~ 0
c1p3
Wire Wire Line
	1350 2650 1000 2650
Text Label 1050 2650 0    50   ~ 0
c1p5
Wire Wire Line
	2200 2650 1850 2650
Text Label 1900 2650 0    50   ~ 0
c1p7
Wire Wire Line
	1350 2750 1000 2750
Text Label 1050 2750 0    50   ~ 0
c1p9
Wire Wire Line
	2200 2750 1850 2750
Text Label 1900 2750 0    50   ~ 0
c1p11
Wire Wire Line
	1350 2850 1000 2850
Text Label 1050 2850 0    50   ~ 0
c1p13
Wire Wire Line
	2200 2850 1850 2850
Text Label 1900 2850 0    50   ~ 0
c1p15
Wire Wire Line
	1350 2950 1000 2950
Text Label 1050 2950 0    50   ~ 0
c1p17
Wire Wire Line
	1350 3050 1000 3050
Text Label 1050 3050 0    50   ~ 0
c1p21
Wire Wire Line
	2200 3050 1850 3050
Text Label 1900 3050 0    50   ~ 0
c1p23
Wire Wire Line
	1350 3150 1000 3150
Text Label 1050 3150 0    50   ~ 0
c1p25
Wire Wire Line
	2200 3150 1850 3150
Text Label 1900 3150 0    50   ~ 0
c1p27
Wire Wire Line
	1350 3250 1000 3250
Text Label 1050 3250 0    50   ~ 0
c1p29
Wire Wire Line
	2200 3250 1850 3250
Text Label 1900 3250 0    50   ~ 0
c1p31
Wire Wire Line
	1350 3350 1000 3350
Text Label 1050 3350 0    50   ~ 0
c1p33
Wire Wire Line
	2200 3350 1850 3350
Text Label 1900 3350 0    50   ~ 0
c1p35
Wire Wire Line
	1350 3450 1000 3450
Text Label 1050 3450 0    50   ~ 0
c1p37
Wire Wire Line
	2200 3450 1850 3450
Text Label 1900 3450 0    50   ~ 0
c1p39
Wire Wire Line
	1350 3550 1000 3550
Text Label 1050 3550 0    50   ~ 0
c1p41
Wire Wire Line
	2200 3550 1850 3550
Text Label 1900 3550 0    50   ~ 0
c1p43
Wire Wire Line
	1350 3650 1000 3650
Text Label 1050 3650 0    50   ~ 0
c1p45
Wire Wire Line
	2200 3650 1850 3650
Text Label 1900 3650 0    50   ~ 0
c1p47
Wire Wire Line
	1350 3750 1000 3750
Text Label 1050 3750 0    50   ~ 0
c1p49
Wire Wire Line
	2200 3750 1850 3750
Text Label 1900 3750 0    50   ~ 0
c1p51
Wire Wire Line
	1350 3850 1000 3850
Text Label 1050 3850 0    50   ~ 0
c1p53
Wire Wire Line
	2200 3850 1850 3850
Text Label 1900 3850 0    50   ~ 0
c1p55
Wire Wire Line
	1350 3950 1000 3950
Text Label 1050 3950 0    50   ~ 0
c1p57
Wire Wire Line
	2200 3950 1850 3950
Text Label 1900 3950 0    50   ~ 0
c1p59
Wire Wire Line
	1350 4050 1000 4050
Text Label 1050 4050 0    50   ~ 0
c1p61
Wire Wire Line
	2200 4050 1850 4050
Text Label 1900 4050 0    50   ~ 0
c1p63
Wire Wire Line
	1350 4150 1000 4150
Text Label 1050 4150 0    50   ~ 0
c1p65
Wire Wire Line
	2200 4150 1850 4150
Text Label 1900 4150 0    50   ~ 0
c1p67
Wire Wire Line
	1350 4250 1000 4250
Text Label 1050 4250 0    50   ~ 0
c1p69
Wire Wire Line
	2200 4250 1850 4250
Text Label 1900 4250 0    50   ~ 0
c1p71
Wire Wire Line
	1350 4350 1000 4350
Text Label 1050 4350 0    50   ~ 0
c1p73
Wire Wire Line
	2200 4350 1850 4350
Text Label 1900 4350 0    50   ~ 0
c1p75
Wire Wire Line
	1350 4450 1000 4450
Text Label 1050 4450 0    50   ~ 0
c1p77
Wire Wire Line
	2200 4450 1850 4450
Wire Wire Line
	1350 4550 1000 4550
Text Label 1050 4550 0    50   ~ 0
c1p81
Wire Wire Line
	2200 4550 1850 4550
Text Label 1900 4550 0    50   ~ 0
c1p83
Wire Wire Line
	1350 4650 1000 4650
Text Label 1050 4650 0    50   ~ 0
c1p85
Wire Wire Line
	2200 4650 1850 4650
Text Label 1900 4650 0    50   ~ 0
c1p87
Wire Wire Line
	1350 4750 1000 4750
Text Label 1050 4750 0    50   ~ 0
c1p89
Wire Wire Line
	2200 4750 1850 4750
Text Label 1900 4750 0    50   ~ 0
c1p91
Wire Wire Line
	1350 4850 1000 4850
Text Label 1050 4850 0    50   ~ 0
c1p93
Wire Wire Line
	2200 4850 1850 4850
Text Label 1900 4850 0    50   ~ 0
c1p95
Wire Wire Line
	1350 4950 1000 4950
Text Label 1050 4950 0    50   ~ 0
c1p97
Wire Wire Line
	2200 4950 1850 4950
Text Label 1900 4950 0    50   ~ 0
c1p99
Wire Wire Line
	1350 5050 1000 5050
Text Label 1050 5050 0    50   ~ 0
c1p101
Wire Wire Line
	2200 5050 1850 5050
Text Label 1900 5050 0    50   ~ 0
c1p103
Wire Wire Line
	1350 5150 1000 5150
Text Label 1050 5150 0    50   ~ 0
c1p105
Wire Wire Line
	2200 5150 1850 5150
Text Label 1900 5150 0    50   ~ 0
c1p107
Wire Wire Line
	1350 5250 1000 5250
Text Label 1050 5250 0    50   ~ 0
c1p109
Wire Wire Line
	2200 5250 1850 5250
Text Label 1900 5250 0    50   ~ 0
c1p111
Wire Wire Line
	1350 5350 1000 5350
Text Label 1050 5350 0    50   ~ 0
c1p113
Wire Wire Line
	2200 5350 1850 5350
Text Label 1900 5350 0    50   ~ 0
c1p115
Wire Wire Line
	1350 5450 1000 5450
Text Label 1050 5450 0    50   ~ 0
c1p117
Wire Wire Line
	2200 5450 1850 5450
Text Label 1900 5450 0    50   ~ 0
c1p119
Wire Wire Line
	1350 5550 1000 5550
Text Label 1050 5550 0    50   ~ 0
c1p121
Wire Wire Line
	2200 5550 1850 5550
Text Label 1900 5550 0    50   ~ 0
c1p123
Wire Wire Line
	1350 5650 1000 5650
Text Label 1050 5650 0    50   ~ 0
c1p125
Wire Wire Line
	2200 5650 1850 5650
Text Label 1900 5650 0    50   ~ 0
c1p127
Wire Wire Line
	1350 5750 1000 5750
Text Label 1050 5750 0    50   ~ 0
c1p129
Wire Wire Line
	2200 5750 1850 5750
Text Label 1900 5750 0    50   ~ 0
c1p131
Wire Wire Line
	1350 5850 1000 5850
Text Label 1050 5850 0    50   ~ 0
c1p133
Wire Wire Line
	2200 5850 1850 5850
Text Label 1900 5850 0    50   ~ 0
c1p135
Wire Wire Line
	1350 5950 1000 5950
Text Label 1050 5950 0    50   ~ 0
c1p137
Wire Wire Line
	2200 5950 1850 5950
Text Label 1900 5950 0    50   ~ 0
c1p139
Text Label 1900 4450 0    50   ~ 0
c1p79
Text Label 1900 2950 0    50   ~ 0
c1p19
Wire Wire Line
	2200 2950 1850 2950
Text Label 3400 2850 0    50   ~ 0
c1p38
Wire Wire Line
	3700 2850 3350 2850
Text Label 3400 2750 0    50   ~ 0
c1p36
Wire Wire Line
	3700 2750 3350 2750
Text Label 3400 2650 0    50   ~ 0
c1p34
Wire Wire Line
	3700 2650 3350 2650
Text Label 3400 2550 0    50   ~ 0
c1p32
Wire Wire Line
	3700 2550 3350 2550
Text Label 3400 2450 0    50   ~ 0
c1p30
Wire Wire Line
	3700 2450 3350 2450
Text Label 3400 2350 0    50   ~ 0
c1p28
Wire Wire Line
	3700 2350 3350 2350
Text Label 3400 2250 0    50   ~ 0
c1p26
Wire Wire Line
	3700 2250 3350 2250
Text Label 3400 2150 0    50   ~ 0
c1p24
Wire Wire Line
	3700 2150 3350 2150
Text Label 3400 2050 0    50   ~ 0
c1p22
Wire Wire Line
	3700 2050 3350 2050
Text Label 3400 1950 0    50   ~ 0
c1p20
Wire Wire Line
	3700 1950 3350 1950
Text Label 3400 1850 0    50   ~ 0
c1p18
Wire Wire Line
	3700 1850 3350 1850
Text Label 3400 1750 0    50   ~ 0
c1p16
Wire Wire Line
	3700 1750 3350 1750
Text Label 3400 1650 0    50   ~ 0
c1p14
Wire Wire Line
	3700 1650 3350 1650
Text Label 3400 1550 0    50   ~ 0
c1p12
Wire Wire Line
	3700 1550 3350 1550
Text Label 3400 1450 0    50   ~ 0
c1p10
Wire Wire Line
	3700 1450 3350 1450
Text Label 3400 1350 0    50   ~ 0
c1p8
Wire Wire Line
	3700 1350 3350 1350
Text Label 3400 1250 0    50   ~ 0
c1p6
Wire Wire Line
	3700 1250 3350 1250
Text Label 3400 1150 0    50   ~ 0
c1p4
Wire Wire Line
	3700 1150 3350 1150
Text Label 3400 1050 0    50   ~ 0
c1p2
Wire Wire Line
	3700 1050 3350 1050
$Comp
L POWDER_Connector:Conn_02x70_Odd_Even J106
U 1 1 60DEAEDD
P 9550 4450
F 0 "J106" H 9600 8067 50  0000 C CNN
F 1 "Conn_02x70_Odd_Even" H 9600 7976 50  0000 C CNN
F 2 "POWDER_Connector:Amphenol_ICC_61082-141402LF" H 9550 4450 50  0001 C CNN
F 3 "" H 9550 4450 50  0001 C CNN
	1    9550 4450
	1    0    0    -1  
$EndComp
$Comp
L POWDER_Connector:Conn_02x70_Odd_Even J107
U 1 1 60DEAEE3
P 11550 4450
F 0 "J107" H 11600 8067 50  0000 C CNN
F 1 "Conn_02x70_Odd_Even" H 11600 7976 50  0000 C CNN
F 2 "POWDER_Connector:Amphenol_ICC_61083-141402LF" H 11550 4450 50  0001 C CNN
F 3 "" H 11550 4450 50  0001 C CNN
	1    11550 4450
	1    0    0    -1  
$EndComp
Text Notes 9150 8200 0    50   ~ 0
^ Power board side, i.e. top side
Text Notes 11250 8200 0    50   ~ 0
^ CPU board, i.e. down side
Wire Wire Line
	9350 1050 9000 1050
Text Label 9050 1050 0    50   ~ 0
c2p1
Wire Wire Line
	9350 1150 9000 1150
Text Label 9050 1150 0    50   ~ 0
c2p3
Wire Wire Line
	9350 1250 9000 1250
Text Label 9050 1250 0    50   ~ 0
c2p5
Wire Wire Line
	9350 1350 9000 1350
Text Label 9050 1350 0    50   ~ 0
c2p7
Wire Wire Line
	9350 1450 9000 1450
Text Label 9050 1450 0    50   ~ 0
c2p9
Wire Wire Line
	9350 1550 9000 1550
Text Label 9050 1550 0    50   ~ 0
c2p11
Wire Wire Line
	9350 1650 9000 1650
Text Label 9050 1650 0    50   ~ 0
c2p13
Wire Wire Line
	9350 1750 9000 1750
Text Label 9050 1750 0    50   ~ 0
c2p15
Wire Wire Line
	9350 1850 9000 1850
Text Label 9050 1850 0    50   ~ 0
c2p17
Wire Wire Line
	9350 1950 9000 1950
Text Label 9050 1950 0    50   ~ 0
c2p19
Wire Wire Line
	9350 2050 9000 2050
Text Label 9050 2050 0    50   ~ 0
c2p21
Wire Wire Line
	9350 2150 9000 2150
Text Label 9050 2150 0    50   ~ 0
c2p23
Wire Wire Line
	9350 2250 9000 2250
Text Label 9050 2250 0    50   ~ 0
c2p25
Wire Wire Line
	9350 2350 9000 2350
Text Label 9050 2350 0    50   ~ 0
c2p27
Wire Wire Line
	9350 2450 9000 2450
Text Label 9050 2450 0    50   ~ 0
c2p29
Wire Wire Line
	9350 2550 9000 2550
Text Label 9050 2550 0    50   ~ 0
c2p31
Wire Wire Line
	9350 2650 9000 2650
Text Label 9050 2650 0    50   ~ 0
c2p33
Wire Wire Line
	9350 2750 9000 2750
Text Label 9050 2750 0    50   ~ 0
c2p35
Wire Wire Line
	9350 2850 9000 2850
Text Label 9050 2850 0    50   ~ 0
c2p37
Wire Wire Line
	9350 2950 9000 2950
Text Label 9050 2950 0    50   ~ 0
c2p39
Wire Wire Line
	9350 3050 9000 3050
Text Label 9050 3050 0    50   ~ 0
c2p41
Wire Wire Line
	9350 3150 9000 3150
Text Label 9050 3150 0    50   ~ 0
c2p43
Wire Wire Line
	9350 3250 9000 3250
Text Label 9050 3250 0    50   ~ 0
c2p45
Wire Wire Line
	9350 3350 9000 3350
Text Label 9050 3350 0    50   ~ 0
c2p47
Wire Wire Line
	9350 3450 9000 3450
Text Label 9050 3450 0    50   ~ 0
c2p49
Wire Wire Line
	9350 3550 9000 3550
Text Label 9050 3550 0    50   ~ 0
c2p51
Wire Wire Line
	9350 3650 9000 3650
Text Label 9050 3650 0    50   ~ 0
c2p53
Wire Wire Line
	9350 3750 9000 3750
Text Label 9050 3750 0    50   ~ 0
c2p55
Wire Wire Line
	9350 3850 9000 3850
Text Label 9050 3850 0    50   ~ 0
c2p57
Wire Wire Line
	9350 3950 9000 3950
Text Label 9050 3950 0    50   ~ 0
c2p59
Wire Wire Line
	9350 4050 9000 4050
Text Label 9050 4050 0    50   ~ 0
c2p61
Wire Wire Line
	9350 4150 9000 4150
Text Label 9050 4150 0    50   ~ 0
c2p63
Wire Wire Line
	9350 4250 9000 4250
Text Label 9050 4250 0    50   ~ 0
c2p65
Wire Wire Line
	9350 4350 9000 4350
Text Label 9050 4350 0    50   ~ 0
c2p67
Wire Wire Line
	9350 4450 9000 4450
Text Label 9050 4450 0    50   ~ 0
c2p69
Wire Wire Line
	9350 4550 9000 4550
Text Label 9050 4550 0    50   ~ 0
c2p71
Wire Wire Line
	9350 4650 9000 4650
Text Label 9050 4650 0    50   ~ 0
c2p73
Wire Wire Line
	9350 4750 9000 4750
Text Label 9050 4750 0    50   ~ 0
c2p75
Wire Wire Line
	9350 4850 9000 4850
Text Label 9050 4850 0    50   ~ 0
c2p77
Wire Wire Line
	9350 4950 9000 4950
Wire Wire Line
	9350 5050 9000 5050
Text Label 9050 5050 0    50   ~ 0
c2p81
Wire Wire Line
	9350 5150 9000 5150
Text Label 9050 5150 0    50   ~ 0
c2p83
Wire Wire Line
	9350 5250 9000 5250
Text Label 9050 5250 0    50   ~ 0
c2p85
Wire Wire Line
	9350 5350 9000 5350
Text Label 9050 5350 0    50   ~ 0
c2p87
Wire Wire Line
	9350 5450 9000 5450
Text Label 9050 5450 0    50   ~ 0
c2p89
Wire Wire Line
	9350 5550 9000 5550
Text Label 9050 5550 0    50   ~ 0
c2p91
Wire Wire Line
	9350 5650 9000 5650
Text Label 9050 5650 0    50   ~ 0
c2p93
Wire Wire Line
	9350 5750 9000 5750
Text Label 9050 5750 0    50   ~ 0
c2p95
Wire Wire Line
	9350 5850 9000 5850
Text Label 9050 5850 0    50   ~ 0
c2p97
Wire Wire Line
	9350 5950 9000 5950
Text Label 9050 5950 0    50   ~ 0
c2p99
Wire Wire Line
	9350 6050 9000 6050
Text Label 9050 6050 0    50   ~ 0
c2p101
Wire Wire Line
	9350 6150 9000 6150
Text Label 9050 6150 0    50   ~ 0
c2p103
Wire Wire Line
	9350 6250 9000 6250
Text Label 9050 6250 0    50   ~ 0
c2p105
Wire Wire Line
	9350 6350 9000 6350
Text Label 9050 6350 0    50   ~ 0
c2p107
Wire Wire Line
	9350 6450 9000 6450
Text Label 9050 6450 0    50   ~ 0
c2p109
Wire Wire Line
	9350 6550 9000 6550
Text Label 9050 6550 0    50   ~ 0
c2p111
Wire Wire Line
	9350 6650 9000 6650
Text Label 9050 6650 0    50   ~ 0
c2p113
Wire Wire Line
	9350 6750 9000 6750
Text Label 9050 6750 0    50   ~ 0
c2p115
Wire Wire Line
	9350 6850 9000 6850
Text Label 9050 6850 0    50   ~ 0
c2p117
Wire Wire Line
	9350 6950 9000 6950
Text Label 9050 6950 0    50   ~ 0
c2p119
Wire Wire Line
	9350 7050 9000 7050
Text Label 9050 7050 0    50   ~ 0
c2p121
Wire Wire Line
	9350 7150 9000 7150
Text Label 9050 7150 0    50   ~ 0
c2p123
Wire Wire Line
	9350 7250 9000 7250
Text Label 9050 7250 0    50   ~ 0
c2p125
Wire Wire Line
	9350 7350 9000 7350
Text Label 9050 7350 0    50   ~ 0
c2p127
Wire Wire Line
	9350 7450 9000 7450
Text Label 9050 7450 0    50   ~ 0
c2p129
Wire Wire Line
	9350 7550 9000 7550
Text Label 9050 7550 0    50   ~ 0
c2p131
Wire Wire Line
	9350 7650 9000 7650
Text Label 9050 7650 0    50   ~ 0
c2p133
Wire Wire Line
	9350 7750 9000 7750
Text Label 9050 7750 0    50   ~ 0
c2p135
Wire Wire Line
	9350 7850 9000 7850
Text Label 9050 7850 0    50   ~ 0
c2p137
Wire Wire Line
	9350 7950 9000 7950
Text Label 9050 7950 0    50   ~ 0
c2p139
Wire Wire Line
	10200 2950 9850 2950
Text Label 9900 2950 0    50   ~ 0
c2p40
Wire Wire Line
	10200 3050 9850 3050
Text Label 9900 3050 0    50   ~ 0
c2p42
Wire Wire Line
	10200 3150 9850 3150
Text Label 9900 3150 0    50   ~ 0
c2p44
Wire Wire Line
	10200 3250 9850 3250
Text Label 9900 3250 0    50   ~ 0
c2p46
Wire Wire Line
	10200 3350 9850 3350
Text Label 9900 3350 0    50   ~ 0
c2p48
Wire Wire Line
	10200 3450 9850 3450
Text Label 9900 3450 0    50   ~ 0
c2p50
Wire Wire Line
	10200 3550 9850 3550
Text Label 9900 3550 0    50   ~ 0
c2p52
Wire Wire Line
	10200 3650 9850 3650
Text Label 9900 3650 0    50   ~ 0
c2p54
Wire Wire Line
	10200 3750 9850 3750
Text Label 9900 3750 0    50   ~ 0
c2p56
Wire Wire Line
	10200 3850 9850 3850
Text Label 9900 3850 0    50   ~ 0
c2p58
Wire Wire Line
	10200 3950 9850 3950
Text Label 9900 3950 0    50   ~ 0
c2p60
Wire Wire Line
	10200 4050 9850 4050
Text Label 9900 4050 0    50   ~ 0
c2p62
Wire Wire Line
	10200 4150 9850 4150
Text Label 9900 4150 0    50   ~ 0
c2p64
Wire Wire Line
	10200 4250 9850 4250
Text Label 9900 4250 0    50   ~ 0
c2p66
Wire Wire Line
	10200 4350 9850 4350
Text Label 9900 4350 0    50   ~ 0
c2p68
Wire Wire Line
	10200 4450 9850 4450
Text Label 9900 4450 0    50   ~ 0
c2p70
Wire Wire Line
	10200 4550 9850 4550
Text Label 9900 4550 0    50   ~ 0
c2p72
Wire Wire Line
	10200 4650 9850 4650
Text Label 9900 4650 0    50   ~ 0
c2p74
Wire Wire Line
	10200 4750 9850 4750
Text Label 9900 4750 0    50   ~ 0
c2p76
Wire Wire Line
	10200 4850 9850 4850
Text Label 9900 4850 0    50   ~ 0
c2p78
Wire Wire Line
	10200 4950 9850 4950
Wire Wire Line
	10200 5050 9850 5050
Text Label 9900 5050 0    50   ~ 0
c2p82
Wire Wire Line
	10200 5150 9850 5150
Text Label 9900 5150 0    50   ~ 0
c2p84
Wire Wire Line
	10200 5250 9850 5250
Text Label 9900 5250 0    50   ~ 0
c2p86
Wire Wire Line
	10200 5350 9850 5350
Text Label 9900 5350 0    50   ~ 0
c2p88
Wire Wire Line
	10200 5450 9850 5450
Text Label 9900 5450 0    50   ~ 0
c2p90
Wire Wire Line
	10200 5550 9850 5550
Text Label 9900 5550 0    50   ~ 0
c2p92
Wire Wire Line
	10200 5650 9850 5650
Text Label 9900 5650 0    50   ~ 0
c2p94
Wire Wire Line
	10200 5750 9850 5750
Text Label 9900 5750 0    50   ~ 0
c2p96
Wire Wire Line
	10200 5850 9850 5850
Text Label 9900 5850 0    50   ~ 0
c2p98
Wire Wire Line
	10200 5950 9850 5950
Text Label 9900 5950 0    50   ~ 0
c2p100
Wire Wire Line
	10200 6050 9850 6050
Text Label 9900 6050 0    50   ~ 0
c2p102
Wire Wire Line
	10200 6150 9850 6150
Text Label 9900 6150 0    50   ~ 0
c2p104
Wire Wire Line
	10200 6250 9850 6250
Text Label 9900 6250 0    50   ~ 0
c2p106
Wire Wire Line
	10200 6350 9850 6350
Text Label 9900 6350 0    50   ~ 0
c2p108
Wire Wire Line
	10200 6450 9850 6450
Text Label 9900 6450 0    50   ~ 0
c2p110
Wire Wire Line
	10200 6550 9850 6550
Text Label 9900 6550 0    50   ~ 0
c2p112
Wire Wire Line
	10200 6650 9850 6650
Text Label 9900 6650 0    50   ~ 0
c2p114
Wire Wire Line
	10200 6750 9850 6750
Text Label 9900 6750 0    50   ~ 0
c2p116
Wire Wire Line
	10200 6850 9850 6850
Text Label 9900 6850 0    50   ~ 0
c2p118
Wire Wire Line
	10200 6950 9850 6950
Text Label 9900 6950 0    50   ~ 0
c2p120
Wire Wire Line
	10200 7050 9850 7050
Text Label 9900 7050 0    50   ~ 0
c2p122
Wire Wire Line
	10200 7150 9850 7150
Text Label 9900 7150 0    50   ~ 0
c2p124
Wire Wire Line
	10200 7250 9850 7250
Text Label 9900 7250 0    50   ~ 0
c2p126
Wire Wire Line
	10200 7350 9850 7350
Text Label 9900 7350 0    50   ~ 0
c2p128
Wire Wire Line
	10200 7450 9850 7450
Text Label 9900 7450 0    50   ~ 0
c2p130
Wire Wire Line
	10200 7550 9850 7550
Text Label 9900 7550 0    50   ~ 0
c2p132
Wire Wire Line
	10200 7650 9850 7650
Text Label 9900 7650 0    50   ~ 0
c2p134
Wire Wire Line
	10200 7750 9850 7750
Text Label 9900 7750 0    50   ~ 0
c2p136
Wire Wire Line
	10200 7850 9850 7850
Text Label 9900 7850 0    50   ~ 0
c2p138
Wire Wire Line
	10200 7950 9850 7950
Text Label 9900 7950 0    50   ~ 0
c2p140
Text Label 9050 4950 0    50   ~ 0
c2p79
Text Label 9900 4950 0    50   ~ 0
c2p80
Text Label 9900 2850 0    50   ~ 0
c2p38
Wire Wire Line
	10200 2850 9850 2850
Text Label 9900 2750 0    50   ~ 0
c2p36
Wire Wire Line
	10200 2750 9850 2750
Text Label 9900 2650 0    50   ~ 0
c2p34
Wire Wire Line
	10200 2650 9850 2650
Text Label 9900 2550 0    50   ~ 0
c2p32
Wire Wire Line
	10200 2550 9850 2550
Text Label 9900 2450 0    50   ~ 0
c2p30
Wire Wire Line
	10200 2450 9850 2450
Text Label 9900 2350 0    50   ~ 0
c2p28
Wire Wire Line
	10200 2350 9850 2350
Text Label 9900 2250 0    50   ~ 0
c2p26
Wire Wire Line
	10200 2250 9850 2250
Text Label 9900 2150 0    50   ~ 0
c2p24
Wire Wire Line
	10200 2150 9850 2150
Text Label 9900 2050 0    50   ~ 0
c2p22
Wire Wire Line
	10200 2050 9850 2050
Text Label 9900 1950 0    50   ~ 0
c2p20
Wire Wire Line
	10200 1950 9850 1950
Text Label 9900 1850 0    50   ~ 0
c2p18
Wire Wire Line
	10200 1850 9850 1850
Text Label 9900 1750 0    50   ~ 0
c2p16
Wire Wire Line
	10200 1750 9850 1750
Text Label 9900 1650 0    50   ~ 0
c2p14
Wire Wire Line
	10200 1650 9850 1650
Text Label 9900 1550 0    50   ~ 0
c2p12
Wire Wire Line
	10200 1550 9850 1550
Text Label 9900 1450 0    50   ~ 0
c2p10
Wire Wire Line
	10200 1450 9850 1450
Text Label 9900 1350 0    50   ~ 0
c2p8
Wire Wire Line
	10200 1350 9850 1350
Text Label 9900 1250 0    50   ~ 0
c2p6
Wire Wire Line
	10200 1250 9850 1250
Text Label 9900 1150 0    50   ~ 0
c2p4
Wire Wire Line
	10200 1150 9850 1150
Text Label 9900 1050 0    50   ~ 0
c2p2
Wire Wire Line
	10200 1050 9850 1050
$Comp
L Connector_Generic:Conn_02x40_Odd_Even J108
U 1 1 60F6FC60
P 14050 2950
F 0 "J108" H 14100 5067 50  0000 C CNN
F 1 "Conn_02x40_Odd_Even" H 14100 4976 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x40_P1.27mm_Vertical_SMD" H 14050 2950 50  0001 C CNN
F 3 "~" H 14050 2950 50  0001 C CNN
	1    14050 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x40_Odd_Even J105
U 1 1 6102AED0
P 7050 8950
F 0 "J105" H 7100 11067 50  0000 C CNN
F 1 "Conn_02x40_Odd_Even" H 7100 10976 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x40_P1.27mm_Vertical_SMD" H 7050 8950 50  0001 C CNN
F 3 "~" H 7050 8950 50  0001 C CNN
	1    7050 8950
	1    0    0    -1  
$EndComp
Wire Wire Line
	11350 1050 11000 1050
Text Label 11050 1050 0    50   ~ 0
c2p1
Wire Wire Line
	11350 1150 11000 1150
Text Label 11050 1150 0    50   ~ 0
c2p3
Wire Wire Line
	11350 1250 11000 1250
Text Label 11050 1250 0    50   ~ 0
c2p5
Wire Wire Line
	11350 1350 11000 1350
Text Label 11050 1350 0    50   ~ 0
c2p7
Wire Wire Line
	11350 1450 11000 1450
Text Label 11050 1450 0    50   ~ 0
c2p9
Wire Wire Line
	11350 1550 11000 1550
Text Label 11050 1550 0    50   ~ 0
c2p11
Wire Wire Line
	11350 1650 11000 1650
Text Label 11050 1650 0    50   ~ 0
c2p13
Wire Wire Line
	11350 1750 11000 1750
Text Label 11050 1750 0    50   ~ 0
c2p15
Wire Wire Line
	11350 1850 11000 1850
Text Label 11050 1850 0    50   ~ 0
c2p17
Wire Wire Line
	11350 1950 11000 1950
Text Label 11050 1950 0    50   ~ 0
c2p19
Wire Wire Line
	11350 2050 11000 2050
Text Label 11050 2050 0    50   ~ 0
c2p21
Wire Wire Line
	11350 2150 11000 2150
Text Label 11050 2150 0    50   ~ 0
c2p23
Wire Wire Line
	11350 2250 11000 2250
Text Label 11050 2250 0    50   ~ 0
c2p25
Wire Wire Line
	11350 2350 11000 2350
Text Label 11050 2350 0    50   ~ 0
c2p27
Wire Wire Line
	11350 2450 11000 2450
Text Label 11050 2450 0    50   ~ 0
c2p29
Wire Wire Line
	11350 2550 11000 2550
Text Label 11050 2550 0    50   ~ 0
c2p31
Wire Wire Line
	11350 2650 11000 2650
Text Label 11050 2650 0    50   ~ 0
c2p33
Wire Wire Line
	11350 2750 11000 2750
Text Label 11050 2750 0    50   ~ 0
c2p35
Wire Wire Line
	11350 2850 11000 2850
Text Label 11050 2850 0    50   ~ 0
c2p37
Wire Wire Line
	11350 2950 11000 2950
Text Label 11050 2950 0    50   ~ 0
c2p39
Wire Wire Line
	11350 3050 11000 3050
Text Label 11050 3050 0    50   ~ 0
c2p41
Wire Wire Line
	11350 3150 11000 3150
Text Label 11050 3150 0    50   ~ 0
c2p43
Wire Wire Line
	11350 3250 11000 3250
Text Label 11050 3250 0    50   ~ 0
c2p45
Wire Wire Line
	11350 3350 11000 3350
Text Label 11050 3350 0    50   ~ 0
c2p47
Wire Wire Line
	11350 3450 11000 3450
Text Label 11050 3450 0    50   ~ 0
c2p49
Wire Wire Line
	11350 3550 11000 3550
Text Label 11050 3550 0    50   ~ 0
c2p51
Wire Wire Line
	11350 3650 11000 3650
Text Label 11050 3650 0    50   ~ 0
c2p53
Wire Wire Line
	11350 3750 11000 3750
Text Label 11050 3750 0    50   ~ 0
c2p55
Wire Wire Line
	11350 3850 11000 3850
Text Label 11050 3850 0    50   ~ 0
c2p57
Wire Wire Line
	11350 3950 11000 3950
Text Label 11050 3950 0    50   ~ 0
c2p59
Wire Wire Line
	11350 4050 11000 4050
Text Label 11050 4050 0    50   ~ 0
c2p61
Wire Wire Line
	11350 4150 11000 4150
Text Label 11050 4150 0    50   ~ 0
c2p63
Wire Wire Line
	11350 4250 11000 4250
Text Label 11050 4250 0    50   ~ 0
c2p65
Wire Wire Line
	11350 4350 11000 4350
Text Label 11050 4350 0    50   ~ 0
c2p67
Wire Wire Line
	11350 4450 11000 4450
Text Label 11050 4450 0    50   ~ 0
c2p69
Wire Wire Line
	11350 4550 11000 4550
Text Label 11050 4550 0    50   ~ 0
c2p71
Wire Wire Line
	11350 4650 11000 4650
Text Label 11050 4650 0    50   ~ 0
c2p73
Wire Wire Line
	11350 4750 11000 4750
Text Label 11050 4750 0    50   ~ 0
c2p75
Wire Wire Line
	11350 4850 11000 4850
Text Label 11050 4850 0    50   ~ 0
c2p77
Wire Wire Line
	11350 4950 11000 4950
Wire Wire Line
	11350 5050 11000 5050
Text Label 11050 5050 0    50   ~ 0
c2p81
Wire Wire Line
	11350 5150 11000 5150
Text Label 11050 5150 0    50   ~ 0
c2p83
Wire Wire Line
	11350 5250 11000 5250
Text Label 11050 5250 0    50   ~ 0
c2p85
Wire Wire Line
	11350 5350 11000 5350
Text Label 11050 5350 0    50   ~ 0
c2p87
Wire Wire Line
	11350 5450 11000 5450
Text Label 11050 5450 0    50   ~ 0
c2p89
Wire Wire Line
	11350 5550 11000 5550
Text Label 11050 5550 0    50   ~ 0
c2p91
Wire Wire Line
	11350 5650 11000 5650
Text Label 11050 5650 0    50   ~ 0
c2p93
Wire Wire Line
	11350 5750 11000 5750
Text Label 11050 5750 0    50   ~ 0
c2p95
Wire Wire Line
	11350 5850 11000 5850
Text Label 11050 5850 0    50   ~ 0
c2p97
Wire Wire Line
	11350 5950 11000 5950
Text Label 11050 5950 0    50   ~ 0
c2p99
Wire Wire Line
	11350 6050 11000 6050
Text Label 11050 6050 0    50   ~ 0
c2p101
Wire Wire Line
	11350 6150 11000 6150
Text Label 11050 6150 0    50   ~ 0
c2p103
Wire Wire Line
	11350 6250 11000 6250
Text Label 11050 6250 0    50   ~ 0
c2p105
Wire Wire Line
	11350 6350 11000 6350
Text Label 11050 6350 0    50   ~ 0
c2p107
Wire Wire Line
	11350 6450 11000 6450
Text Label 11050 6450 0    50   ~ 0
c2p109
Wire Wire Line
	11350 6550 11000 6550
Text Label 11050 6550 0    50   ~ 0
c2p111
Wire Wire Line
	11350 6650 11000 6650
Text Label 11050 6650 0    50   ~ 0
c2p113
Wire Wire Line
	11350 6750 11000 6750
Text Label 11050 6750 0    50   ~ 0
c2p115
Wire Wire Line
	11350 6850 11000 6850
Text Label 11050 6850 0    50   ~ 0
c2p117
Wire Wire Line
	11350 6950 11000 6950
Text Label 11050 6950 0    50   ~ 0
c2p119
Wire Wire Line
	11350 7050 11000 7050
Text Label 11050 7050 0    50   ~ 0
c2p121
Wire Wire Line
	11350 7150 11000 7150
Text Label 11050 7150 0    50   ~ 0
c2p123
Wire Wire Line
	11350 7250 11000 7250
Text Label 11050 7250 0    50   ~ 0
c2p125
Wire Wire Line
	11350 7350 11000 7350
Text Label 11050 7350 0    50   ~ 0
c2p127
Wire Wire Line
	11350 7450 11000 7450
Text Label 11050 7450 0    50   ~ 0
c2p129
Wire Wire Line
	11350 7550 11000 7550
Text Label 11050 7550 0    50   ~ 0
c2p131
Wire Wire Line
	11350 7650 11000 7650
Text Label 11050 7650 0    50   ~ 0
c2p133
Wire Wire Line
	11350 7750 11000 7750
Text Label 11050 7750 0    50   ~ 0
c2p135
Wire Wire Line
	11350 7850 11000 7850
Text Label 11050 7850 0    50   ~ 0
c2p137
Wire Wire Line
	11350 7950 11000 7950
Text Label 11050 7950 0    50   ~ 0
c2p139
Text Label 11050 4950 0    50   ~ 0
c2p79
Wire Wire Line
	7700 2550 7350 2550
Text Label 7400 2550 0    50   ~ 0
c2p1
Wire Wire Line
	7700 2650 7350 2650
Text Label 7400 2650 0    50   ~ 0
c2p3
Wire Wire Line
	7700 2750 7350 2750
Text Label 7400 2750 0    50   ~ 0
c2p5
Wire Wire Line
	7700 2850 7350 2850
Text Label 7400 2850 0    50   ~ 0
c2p7
Wire Wire Line
	7700 2950 7350 2950
Text Label 7400 2950 0    50   ~ 0
c2p9
Wire Wire Line
	7700 3050 7350 3050
Text Label 7400 3050 0    50   ~ 0
c2p11
Wire Wire Line
	7700 3150 7350 3150
Text Label 7400 3150 0    50   ~ 0
c2p13
Wire Wire Line
	7700 3250 7350 3250
Text Label 7400 3250 0    50   ~ 0
c2p15
Wire Wire Line
	7700 3350 7350 3350
Text Label 7400 3350 0    50   ~ 0
c2p17
Wire Wire Line
	7700 3450 7350 3450
Text Label 7400 3450 0    50   ~ 0
c2p19
Wire Wire Line
	7700 3550 7350 3550
Text Label 7400 3550 0    50   ~ 0
c2p21
Wire Wire Line
	7700 3650 7350 3650
Text Label 7400 3650 0    50   ~ 0
c2p23
Wire Wire Line
	7700 3750 7350 3750
Text Label 7400 3750 0    50   ~ 0
c2p25
Wire Wire Line
	7700 3850 7350 3850
Text Label 7400 3850 0    50   ~ 0
c2p27
Wire Wire Line
	7700 3950 7350 3950
Text Label 7400 3950 0    50   ~ 0
c2p29
Wire Wire Line
	7700 4050 7350 4050
Text Label 7400 4050 0    50   ~ 0
c2p31
Wire Wire Line
	7700 4150 7350 4150
Text Label 7400 4150 0    50   ~ 0
c2p33
Wire Wire Line
	7700 4250 7350 4250
Text Label 7400 4250 0    50   ~ 0
c2p35
Wire Wire Line
	7700 4350 7350 4350
Text Label 7400 4350 0    50   ~ 0
c2p37
Wire Wire Line
	7700 4450 7350 4450
Text Label 7400 4450 0    50   ~ 0
c2p39
Wire Wire Line
	7700 4550 7350 4550
Text Label 7400 4550 0    50   ~ 0
c2p41
Wire Wire Line
	7700 4650 7350 4650
Text Label 7400 4650 0    50   ~ 0
c2p43
Wire Wire Line
	7700 4750 7350 4750
Text Label 7400 4750 0    50   ~ 0
c2p45
Wire Wire Line
	7700 4850 7350 4850
Text Label 7400 4850 0    50   ~ 0
c2p47
Wire Wire Line
	7700 4950 7350 4950
Text Label 7400 4950 0    50   ~ 0
c2p49
Wire Wire Line
	7700 5050 7350 5050
Text Label 7400 5050 0    50   ~ 0
c2p51
Wire Wire Line
	7700 5150 7350 5150
Text Label 7400 5150 0    50   ~ 0
c2p53
Wire Wire Line
	7700 5250 7350 5250
Text Label 7400 5250 0    50   ~ 0
c2p55
Wire Wire Line
	7700 5350 7350 5350
Text Label 7400 5350 0    50   ~ 0
c2p57
Wire Wire Line
	7700 5450 7350 5450
Text Label 7400 5450 0    50   ~ 0
c2p59
Wire Wire Line
	7700 5550 7350 5550
Text Label 7400 5550 0    50   ~ 0
c2p61
Wire Wire Line
	7700 5650 7350 5650
Text Label 7400 5650 0    50   ~ 0
c2p63
Wire Wire Line
	7700 5750 7350 5750
Text Label 7400 5750 0    50   ~ 0
c2p65
Wire Wire Line
	7700 5850 7350 5850
Text Label 7400 5850 0    50   ~ 0
c2p67
Wire Wire Line
	7700 5950 7350 5950
Text Label 7400 5950 0    50   ~ 0
c2p69
Wire Wire Line
	7700 6050 7350 6050
Text Label 7400 6050 0    50   ~ 0
c2p71
Wire Wire Line
	7700 6150 7350 6150
Text Label 7400 6150 0    50   ~ 0
c2p73
Wire Wire Line
	7700 6250 7350 6250
Text Label 7400 6250 0    50   ~ 0
c2p75
Wire Wire Line
	7700 6350 7350 6350
Text Label 7400 6350 0    50   ~ 0
c2p77
Wire Wire Line
	7700 6450 7350 6450
Wire Wire Line
	7700 7050 7350 7050
Text Label 7400 7050 0    50   ~ 0
c2p81
Wire Wire Line
	7700 7150 7350 7150
Text Label 7400 7150 0    50   ~ 0
c2p83
Wire Wire Line
	7700 7250 7350 7250
Text Label 7400 7250 0    50   ~ 0
c2p85
Wire Wire Line
	7700 7350 7350 7350
Text Label 7400 7350 0    50   ~ 0
c2p87
Wire Wire Line
	7700 7450 7350 7450
Text Label 7400 7450 0    50   ~ 0
c2p89
Wire Wire Line
	7700 7550 7350 7550
Text Label 7400 7550 0    50   ~ 0
c2p91
Wire Wire Line
	7700 7650 7350 7650
Text Label 7400 7650 0    50   ~ 0
c2p93
Wire Wire Line
	7700 7750 7350 7750
Text Label 7400 7750 0    50   ~ 0
c2p95
Wire Wire Line
	7700 7850 7350 7850
Text Label 7400 7850 0    50   ~ 0
c2p97
Wire Wire Line
	7700 7950 7350 7950
Text Label 7400 7950 0    50   ~ 0
c2p99
Wire Wire Line
	7700 8050 7350 8050
Text Label 7400 8050 0    50   ~ 0
c2p101
Wire Wire Line
	7700 8150 7350 8150
Text Label 7400 8150 0    50   ~ 0
c2p103
Wire Wire Line
	7700 8250 7350 8250
Text Label 7400 8250 0    50   ~ 0
c2p105
Wire Wire Line
	7700 8350 7350 8350
Text Label 7400 8350 0    50   ~ 0
c2p107
Wire Wire Line
	7700 8450 7350 8450
Text Label 7400 8450 0    50   ~ 0
c2p109
Wire Wire Line
	7700 8550 7350 8550
Text Label 7400 8550 0    50   ~ 0
c2p111
Wire Wire Line
	7700 8650 7350 8650
Text Label 7400 8650 0    50   ~ 0
c2p113
Wire Wire Line
	7700 8750 7350 8750
Text Label 7400 8750 0    50   ~ 0
c2p115
Wire Wire Line
	7700 8850 7350 8850
Text Label 7400 8850 0    50   ~ 0
c2p117
Wire Wire Line
	7700 8950 7350 8950
Text Label 7400 8950 0    50   ~ 0
c2p119
Wire Wire Line
	7700 9050 7350 9050
Text Label 7400 9050 0    50   ~ 0
c2p121
Wire Wire Line
	7700 9150 7350 9150
Text Label 7400 9150 0    50   ~ 0
c2p123
Wire Wire Line
	7700 9250 7350 9250
Text Label 7400 9250 0    50   ~ 0
c2p125
Wire Wire Line
	7700 9350 7350 9350
Text Label 7400 9350 0    50   ~ 0
c2p127
Wire Wire Line
	7700 9450 7350 9450
Text Label 7400 9450 0    50   ~ 0
c2p129
Wire Wire Line
	7700 9550 7350 9550
Text Label 7400 9550 0    50   ~ 0
c2p131
Wire Wire Line
	7700 9650 7350 9650
Text Label 7400 9650 0    50   ~ 0
c2p133
Wire Wire Line
	7700 9750 7350 9750
Text Label 7400 9750 0    50   ~ 0
c2p135
Wire Wire Line
	7700 9850 7350 9850
Text Label 7400 9850 0    50   ~ 0
c2p137
Wire Wire Line
	7700 9950 7350 9950
Text Label 7400 9950 0    50   ~ 0
c2p139
Text Label 7400 6450 0    50   ~ 0
c2p79
Wire Wire Line
	12200 2950 11850 2950
Text Label 11900 2950 0    50   ~ 0
c2p40
Wire Wire Line
	12200 3050 11850 3050
Text Label 11900 3050 0    50   ~ 0
c2p42
Wire Wire Line
	12200 3150 11850 3150
Text Label 11900 3150 0    50   ~ 0
c2p44
Wire Wire Line
	12200 3250 11850 3250
Text Label 11900 3250 0    50   ~ 0
c2p46
Wire Wire Line
	12200 3350 11850 3350
Text Label 11900 3350 0    50   ~ 0
c2p48
Wire Wire Line
	12200 3450 11850 3450
Text Label 11900 3450 0    50   ~ 0
c2p50
Wire Wire Line
	12200 3550 11850 3550
Text Label 11900 3550 0    50   ~ 0
c2p52
Wire Wire Line
	12200 3650 11850 3650
Text Label 11900 3650 0    50   ~ 0
c2p54
Wire Wire Line
	12200 3750 11850 3750
Text Label 11900 3750 0    50   ~ 0
c2p56
Wire Wire Line
	12200 3850 11850 3850
Text Label 11900 3850 0    50   ~ 0
c2p58
Wire Wire Line
	12200 3950 11850 3950
Text Label 11900 3950 0    50   ~ 0
c2p60
Wire Wire Line
	12200 4050 11850 4050
Text Label 11900 4050 0    50   ~ 0
c2p62
Wire Wire Line
	12200 4150 11850 4150
Text Label 11900 4150 0    50   ~ 0
c2p64
Wire Wire Line
	12200 4250 11850 4250
Text Label 11900 4250 0    50   ~ 0
c2p66
Wire Wire Line
	12200 4350 11850 4350
Text Label 11900 4350 0    50   ~ 0
c2p68
Wire Wire Line
	12200 4450 11850 4450
Text Label 11900 4450 0    50   ~ 0
c2p70
Wire Wire Line
	12200 4550 11850 4550
Text Label 11900 4550 0    50   ~ 0
c2p72
Wire Wire Line
	12200 4650 11850 4650
Text Label 11900 4650 0    50   ~ 0
c2p74
Wire Wire Line
	12200 4750 11850 4750
Text Label 11900 4750 0    50   ~ 0
c2p76
Wire Wire Line
	12200 4850 11850 4850
Text Label 11900 4850 0    50   ~ 0
c2p78
Wire Wire Line
	12200 4950 11850 4950
Wire Wire Line
	12200 5050 11850 5050
Text Label 11900 5050 0    50   ~ 0
c2p82
Wire Wire Line
	12200 5150 11850 5150
Text Label 11900 5150 0    50   ~ 0
c2p84
Wire Wire Line
	12200 5250 11850 5250
Text Label 11900 5250 0    50   ~ 0
c2p86
Wire Wire Line
	12200 5350 11850 5350
Text Label 11900 5350 0    50   ~ 0
c2p88
Wire Wire Line
	12200 5450 11850 5450
Text Label 11900 5450 0    50   ~ 0
c2p90
Wire Wire Line
	12200 5550 11850 5550
Text Label 11900 5550 0    50   ~ 0
c2p92
Wire Wire Line
	12200 5650 11850 5650
Text Label 11900 5650 0    50   ~ 0
c2p94
Wire Wire Line
	12200 5750 11850 5750
Text Label 11900 5750 0    50   ~ 0
c2p96
Wire Wire Line
	12200 5850 11850 5850
Text Label 11900 5850 0    50   ~ 0
c2p98
Wire Wire Line
	12200 5950 11850 5950
Text Label 11900 5950 0    50   ~ 0
c2p100
Wire Wire Line
	12200 6050 11850 6050
Text Label 11900 6050 0    50   ~ 0
c2p102
Wire Wire Line
	12200 6150 11850 6150
Text Label 11900 6150 0    50   ~ 0
c2p104
Wire Wire Line
	12200 6250 11850 6250
Text Label 11900 6250 0    50   ~ 0
c2p106
Wire Wire Line
	12200 6350 11850 6350
Text Label 11900 6350 0    50   ~ 0
c2p108
Wire Wire Line
	12200 6450 11850 6450
Text Label 11900 6450 0    50   ~ 0
c2p110
Wire Wire Line
	12200 6550 11850 6550
Text Label 11900 6550 0    50   ~ 0
c2p112
Wire Wire Line
	12200 6650 11850 6650
Text Label 11900 6650 0    50   ~ 0
c2p114
Wire Wire Line
	12200 6750 11850 6750
Text Label 11900 6750 0    50   ~ 0
c2p116
Wire Wire Line
	12200 6850 11850 6850
Text Label 11900 6850 0    50   ~ 0
c2p118
Wire Wire Line
	12200 6950 11850 6950
Text Label 11900 6950 0    50   ~ 0
c2p120
Wire Wire Line
	12200 7050 11850 7050
Text Label 11900 7050 0    50   ~ 0
c2p122
Wire Wire Line
	12200 7150 11850 7150
Text Label 11900 7150 0    50   ~ 0
c2p124
Wire Wire Line
	12200 7250 11850 7250
Text Label 11900 7250 0    50   ~ 0
c2p126
Wire Wire Line
	12200 7350 11850 7350
Text Label 11900 7350 0    50   ~ 0
c2p128
Wire Wire Line
	12200 7450 11850 7450
Text Label 11900 7450 0    50   ~ 0
c2p130
Wire Wire Line
	12200 7550 11850 7550
Text Label 11900 7550 0    50   ~ 0
c2p132
Wire Wire Line
	12200 7650 11850 7650
Text Label 11900 7650 0    50   ~ 0
c2p134
Wire Wire Line
	12200 7750 11850 7750
Text Label 11900 7750 0    50   ~ 0
c2p136
Wire Wire Line
	12200 7850 11850 7850
Text Label 11900 7850 0    50   ~ 0
c2p138
Wire Wire Line
	12200 7950 11850 7950
Text Label 11900 7950 0    50   ~ 0
c2p140
Text Label 11900 4950 0    50   ~ 0
c2p80
Text Label 11900 2850 0    50   ~ 0
c2p38
Wire Wire Line
	12200 2850 11850 2850
Text Label 11900 2750 0    50   ~ 0
c2p36
Wire Wire Line
	12200 2750 11850 2750
Text Label 11900 2650 0    50   ~ 0
c2p34
Wire Wire Line
	12200 2650 11850 2650
Text Label 11900 2550 0    50   ~ 0
c2p32
Wire Wire Line
	12200 2550 11850 2550
Text Label 11900 2450 0    50   ~ 0
c2p30
Wire Wire Line
	12200 2450 11850 2450
Text Label 11900 2350 0    50   ~ 0
c2p28
Wire Wire Line
	12200 2350 11850 2350
Text Label 11900 2250 0    50   ~ 0
c2p26
Wire Wire Line
	12200 2250 11850 2250
Text Label 11900 2150 0    50   ~ 0
c2p24
Wire Wire Line
	12200 2150 11850 2150
Text Label 11900 2050 0    50   ~ 0
c2p22
Wire Wire Line
	12200 2050 11850 2050
Text Label 11900 1950 0    50   ~ 0
c2p20
Wire Wire Line
	12200 1950 11850 1950
Text Label 11900 1850 0    50   ~ 0
c2p18
Wire Wire Line
	12200 1850 11850 1850
Text Label 11900 1750 0    50   ~ 0
c2p16
Wire Wire Line
	12200 1750 11850 1750
Text Label 11900 1650 0    50   ~ 0
c2p14
Wire Wire Line
	12200 1650 11850 1650
Text Label 11900 1550 0    50   ~ 0
c2p12
Wire Wire Line
	12200 1550 11850 1550
Text Label 11900 1450 0    50   ~ 0
c2p10
Wire Wire Line
	12200 1450 11850 1450
Text Label 11900 1350 0    50   ~ 0
c2p8
Wire Wire Line
	12200 1350 11850 1350
Text Label 11900 1250 0    50   ~ 0
c2p6
Wire Wire Line
	12200 1250 11850 1250
Text Label 11900 1150 0    50   ~ 0
c2p4
Wire Wire Line
	12200 1150 11850 1150
Text Label 11900 1050 0    50   ~ 0
c2p2
Wire Wire Line
	12200 1050 11850 1050
Wire Wire Line
	14700 1950 14350 1950
Text Label 14400 1950 0    50   ~ 0
c2p40
Wire Wire Line
	13850 2050 13500 2050
Text Label 13550 2050 0    50   ~ 0
c2p42
Wire Wire Line
	14700 2050 14350 2050
Text Label 14400 2050 0    50   ~ 0
c2p44
Wire Wire Line
	13850 2150 13500 2150
Text Label 13550 2150 0    50   ~ 0
c2p46
Wire Wire Line
	14700 2150 14350 2150
Text Label 14400 2150 0    50   ~ 0
c2p48
Wire Wire Line
	13850 2250 13500 2250
Text Label 13550 2250 0    50   ~ 0
c2p50
Wire Wire Line
	14700 2250 14350 2250
Text Label 14400 2250 0    50   ~ 0
c2p52
Wire Wire Line
	13850 2350 13500 2350
Text Label 13550 2350 0    50   ~ 0
c2p54
Wire Wire Line
	14700 2350 14350 2350
Text Label 14400 2350 0    50   ~ 0
c2p56
Wire Wire Line
	13850 2450 13500 2450
Text Label 13550 2450 0    50   ~ 0
c2p58
Wire Wire Line
	14700 2450 14350 2450
Text Label 14400 2450 0    50   ~ 0
c2p60
Wire Wire Line
	13850 2550 13500 2550
Text Label 13550 2550 0    50   ~ 0
c2p62
Wire Wire Line
	14700 2550 14350 2550
Text Label 14400 2550 0    50   ~ 0
c2p64
Wire Wire Line
	13850 2650 13500 2650
Text Label 13550 2650 0    50   ~ 0
c2p66
Wire Wire Line
	14700 2650 14350 2650
Text Label 14400 2650 0    50   ~ 0
c2p68
Wire Wire Line
	13850 2750 13500 2750
Text Label 13550 2750 0    50   ~ 0
c2p70
Wire Wire Line
	14700 2750 14350 2750
Text Label 14400 2750 0    50   ~ 0
c2p72
Wire Wire Line
	13850 2850 13500 2850
Text Label 13550 2850 0    50   ~ 0
c2p74
Wire Wire Line
	14700 2850 14350 2850
Text Label 14400 2850 0    50   ~ 0
c2p76
Wire Wire Line
	13850 2950 13500 2950
Text Label 13550 2950 0    50   ~ 0
c2p78
Wire Wire Line
	14700 2950 14350 2950
Wire Wire Line
	13850 3050 13500 3050
Text Label 13550 3050 0    50   ~ 0
c2p82
Wire Wire Line
	14700 3050 14350 3050
Text Label 14400 3050 0    50   ~ 0
c2p84
Wire Wire Line
	13850 3150 13500 3150
Text Label 13550 3150 0    50   ~ 0
c2p86
Wire Wire Line
	14700 3150 14350 3150
Text Label 14400 3150 0    50   ~ 0
c2p88
Wire Wire Line
	13850 3250 13500 3250
Text Label 13550 3250 0    50   ~ 0
c2p90
Wire Wire Line
	14700 3250 14350 3250
Text Label 14400 3250 0    50   ~ 0
c2p92
Wire Wire Line
	13850 3350 13500 3350
Text Label 13550 3350 0    50   ~ 0
c2p94
Wire Wire Line
	14700 3350 14350 3350
Text Label 14400 3350 0    50   ~ 0
c2p96
Wire Wire Line
	13850 3450 13500 3450
Text Label 13550 3450 0    50   ~ 0
c2p98
Wire Wire Line
	14700 3450 14350 3450
Text Label 14400 3450 0    50   ~ 0
c2p100
Wire Wire Line
	13850 3550 13500 3550
Text Label 13550 3550 0    50   ~ 0
c2p102
Wire Wire Line
	14700 3550 14350 3550
Text Label 14400 3550 0    50   ~ 0
c2p104
Wire Wire Line
	13850 3650 13500 3650
Text Label 13550 3650 0    50   ~ 0
c2p106
Wire Wire Line
	14700 3650 14350 3650
Text Label 14400 3650 0    50   ~ 0
c2p108
Wire Wire Line
	13850 3750 13500 3750
Text Label 13550 3750 0    50   ~ 0
c2p110
Wire Wire Line
	14700 3750 14350 3750
Text Label 14400 3750 0    50   ~ 0
c2p112
Wire Wire Line
	13850 3850 13500 3850
Text Label 13550 3850 0    50   ~ 0
c2p114
Wire Wire Line
	14700 3850 14350 3850
Text Label 14400 3850 0    50   ~ 0
c2p116
Wire Wire Line
	13850 3950 13500 3950
Text Label 13550 3950 0    50   ~ 0
c2p118
Wire Wire Line
	14700 3950 14350 3950
Text Label 14400 3950 0    50   ~ 0
c2p120
Wire Wire Line
	13850 4050 13500 4050
Text Label 13550 4050 0    50   ~ 0
c2p122
Wire Wire Line
	14700 4050 14350 4050
Text Label 14400 4050 0    50   ~ 0
c2p124
Wire Wire Line
	13850 4150 13500 4150
Text Label 13550 4150 0    50   ~ 0
c2p126
Wire Wire Line
	14700 4150 14350 4150
Text Label 14400 4150 0    50   ~ 0
c2p128
Wire Wire Line
	13850 4250 13500 4250
Text Label 13550 4250 0    50   ~ 0
c2p130
Wire Wire Line
	14700 4250 14350 4250
Text Label 14400 4250 0    50   ~ 0
c2p132
Wire Wire Line
	13850 4350 13500 4350
Text Label 13550 4350 0    50   ~ 0
c2p134
Wire Wire Line
	14700 4350 14350 4350
Text Label 14400 4350 0    50   ~ 0
c2p136
Wire Wire Line
	13850 4450 13500 4450
Text Label 13550 4450 0    50   ~ 0
c2p138
Wire Wire Line
	14700 4450 14350 4450
Text Label 14400 4450 0    50   ~ 0
c2p140
Text Label 14400 2950 0    50   ~ 0
c2p80
Text Label 13550 1950 0    50   ~ 0
c2p38
Wire Wire Line
	13850 1950 13500 1950
Text Label 14400 1850 0    50   ~ 0
c2p36
Wire Wire Line
	14700 1850 14350 1850
Text Label 13550 1850 0    50   ~ 0
c2p34
Wire Wire Line
	13850 1850 13500 1850
Text Label 14400 1750 0    50   ~ 0
c2p32
Wire Wire Line
	14700 1750 14350 1750
Text Label 13550 1750 0    50   ~ 0
c2p30
Wire Wire Line
	13850 1750 13500 1750
Text Label 14400 1650 0    50   ~ 0
c2p28
Wire Wire Line
	14700 1650 14350 1650
Text Label 13550 1650 0    50   ~ 0
c2p26
Wire Wire Line
	13850 1650 13500 1650
Text Label 14400 1550 0    50   ~ 0
c2p24
Wire Wire Line
	14700 1550 14350 1550
Text Label 13550 1550 0    50   ~ 0
c2p22
Wire Wire Line
	13850 1550 13500 1550
Text Label 14400 1450 0    50   ~ 0
c2p20
Wire Wire Line
	14700 1450 14350 1450
Text Label 13550 1450 0    50   ~ 0
c2p18
Wire Wire Line
	13850 1450 13500 1450
Text Label 14400 1350 0    50   ~ 0
c2p16
Wire Wire Line
	14700 1350 14350 1350
Text Label 13550 1350 0    50   ~ 0
c2p14
Wire Wire Line
	13850 1350 13500 1350
Text Label 14400 1250 0    50   ~ 0
c2p12
Wire Wire Line
	14700 1250 14350 1250
Text Label 13550 1250 0    50   ~ 0
c2p10
Wire Wire Line
	13850 1250 13500 1250
Text Label 14400 1150 0    50   ~ 0
c2p8
Wire Wire Line
	14700 1150 14350 1150
Text Label 13550 1150 0    50   ~ 0
c2p6
Wire Wire Line
	13850 1150 13500 1150
Text Label 14400 1050 0    50   ~ 0
c2p4
Wire Wire Line
	14700 1050 14350 1050
Text Label 13550 1050 0    50   ~ 0
c2p2
Wire Wire Line
	13850 1050 13500 1050
Wire Wire Line
	6850 2550 6500 2550
Text Label 6550 2550 0    50   ~ 0
c1p2
Wire Wire Line
	6850 2650 6500 2650
Text Label 6550 2650 0    50   ~ 0
c1p4
Wire Wire Line
	6850 2750 6500 2750
Text Label 6550 2750 0    50   ~ 0
c1p6
Wire Wire Line
	6850 2850 6500 2850
Text Label 6550 2850 0    50   ~ 0
c1p8
Wire Wire Line
	6850 2950 6500 2950
Text Label 6550 2950 0    50   ~ 0
c1p10
Wire Wire Line
	6850 3050 6500 3050
Text Label 6550 3050 0    50   ~ 0
c1p12
Wire Wire Line
	6850 3150 6500 3150
Text Label 6550 3150 0    50   ~ 0
c1p14
Wire Wire Line
	6850 3250 6500 3250
Text Label 6550 3250 0    50   ~ 0
c1p16
Wire Wire Line
	6850 3350 6500 3350
Text Label 6550 3350 0    50   ~ 0
c1p18
Wire Wire Line
	6850 3450 6500 3450
Text Label 6550 3450 0    50   ~ 0
c1p20
Wire Wire Line
	6850 3550 6500 3550
Text Label 6550 3550 0    50   ~ 0
c1p22
Wire Wire Line
	6850 3650 6500 3650
Text Label 6550 3650 0    50   ~ 0
c1p24
Wire Wire Line
	6850 3750 6500 3750
Text Label 6550 3750 0    50   ~ 0
c1p26
Wire Wire Line
	6850 3850 6500 3850
Text Label 6550 3850 0    50   ~ 0
c1p28
Wire Wire Line
	6850 3950 6500 3950
Text Label 6550 3950 0    50   ~ 0
c1p30
Wire Wire Line
	6850 4050 6500 4050
Text Label 6550 4050 0    50   ~ 0
c1p32
Wire Wire Line
	6850 4150 6500 4150
Text Label 6550 4150 0    50   ~ 0
c1p34
Wire Wire Line
	6850 4250 6500 4250
Text Label 6550 4250 0    50   ~ 0
c1p36
Wire Wire Line
	6850 4350 6500 4350
Text Label 6550 4350 0    50   ~ 0
c1p38
Wire Wire Line
	6850 4450 6500 4450
Text Label 6550 4450 0    50   ~ 0
c1p40
Wire Wire Line
	6850 4550 6500 4550
Text Label 6550 4550 0    50   ~ 0
c1p42
Wire Wire Line
	6850 4650 6500 4650
Text Label 6550 4650 0    50   ~ 0
c1p44
Wire Wire Line
	6850 4750 6500 4750
Text Label 6550 4750 0    50   ~ 0
c1p46
Wire Wire Line
	6850 4850 6500 4850
Text Label 6550 4850 0    50   ~ 0
c1p48
Wire Wire Line
	6850 4950 6500 4950
Text Label 6550 4950 0    50   ~ 0
c1p50
Wire Wire Line
	6850 5050 6500 5050
Text Label 6550 5050 0    50   ~ 0
c1p52
Wire Wire Line
	6850 5150 6500 5150
Text Label 6550 5150 0    50   ~ 0
c1p54
Wire Wire Line
	6850 5250 6500 5250
Text Label 6550 5250 0    50   ~ 0
c1p56
Wire Wire Line
	6850 5350 6500 5350
Text Label 6550 5350 0    50   ~ 0
c1p58
Wire Wire Line
	6850 5450 6500 5450
Text Label 6550 5450 0    50   ~ 0
c1p60
Wire Wire Line
	6850 5550 6500 5550
Text Label 6550 5550 0    50   ~ 0
c1p62
Wire Wire Line
	6850 5650 6500 5650
Text Label 6550 5650 0    50   ~ 0
c1p64
Wire Wire Line
	6850 5750 6500 5750
Text Label 6550 5750 0    50   ~ 0
c1p66
Wire Wire Line
	6850 5850 6500 5850
Text Label 6550 5850 0    50   ~ 0
c1p68
Wire Wire Line
	6850 5950 6500 5950
Text Label 6550 5950 0    50   ~ 0
c1p70
Wire Wire Line
	6850 6050 6500 6050
Text Label 6550 6050 0    50   ~ 0
c1p72
Wire Wire Line
	6850 6150 6500 6150
Text Label 6550 6150 0    50   ~ 0
c1p74
Wire Wire Line
	6850 6250 6500 6250
Text Label 6550 6250 0    50   ~ 0
c1p76
Wire Wire Line
	6850 6350 6500 6350
Text Label 6550 6350 0    50   ~ 0
c1p78
Wire Wire Line
	6850 6450 6500 6450
Wire Wire Line
	6850 7050 6500 7050
Text Label 6550 7050 0    50   ~ 0
c1p82
Wire Wire Line
	6850 7150 6500 7150
Text Label 6550 7150 0    50   ~ 0
c1p84
Wire Wire Line
	6850 7250 6500 7250
Text Label 6550 7250 0    50   ~ 0
c1p86
Wire Wire Line
	6850 7350 6500 7350
Text Label 6550 7350 0    50   ~ 0
c1p88
Wire Wire Line
	6850 7450 6500 7450
Text Label 6550 7450 0    50   ~ 0
c1p90
Wire Wire Line
	6850 7550 6500 7550
Text Label 6550 7550 0    50   ~ 0
c1p92
Wire Wire Line
	6850 7650 6500 7650
Text Label 6550 7650 0    50   ~ 0
c1p94
Wire Wire Line
	6850 7750 6500 7750
Text Label 6550 7750 0    50   ~ 0
c1p96
Wire Wire Line
	6850 7850 6500 7850
Text Label 6550 7850 0    50   ~ 0
c1p98
Wire Wire Line
	6850 7950 6500 7950
Text Label 6550 7950 0    50   ~ 0
c1p100
Wire Wire Line
	6850 8050 6500 8050
Text Label 6550 8050 0    50   ~ 0
c1p102
Wire Wire Line
	6850 8150 6500 8150
Text Label 6550 8150 0    50   ~ 0
c1p104
Wire Wire Line
	6850 8250 6500 8250
Text Label 6550 8250 0    50   ~ 0
c1p106
Wire Wire Line
	6850 8350 6500 8350
Text Label 6550 8350 0    50   ~ 0
c1p108
Wire Wire Line
	6850 8450 6500 8450
Text Label 6550 8450 0    50   ~ 0
c1p110
Wire Wire Line
	6850 8550 6500 8550
Text Label 6550 8550 0    50   ~ 0
c1p112
Wire Wire Line
	6850 8650 6500 8650
Text Label 6550 8650 0    50   ~ 0
c1p114
Wire Wire Line
	6850 8750 6500 8750
Text Label 6550 8750 0    50   ~ 0
c1p116
Wire Wire Line
	6850 8850 6500 8850
Text Label 6550 8850 0    50   ~ 0
c1p118
Wire Wire Line
	6850 8950 6500 8950
Text Label 6550 8950 0    50   ~ 0
c1p120
Wire Wire Line
	6850 9050 6500 9050
Text Label 6550 9050 0    50   ~ 0
c1p122
Wire Wire Line
	6850 9150 6500 9150
Text Label 6550 9150 0    50   ~ 0
c1p124
Wire Wire Line
	6850 9250 6500 9250
Text Label 6550 9250 0    50   ~ 0
c1p126
Wire Wire Line
	6850 9350 6500 9350
Text Label 6550 9350 0    50   ~ 0
c1p128
Wire Wire Line
	6850 9450 6500 9450
Text Label 6550 9450 0    50   ~ 0
c1p130
Wire Wire Line
	6850 9550 6500 9550
Text Label 6550 9550 0    50   ~ 0
c1p132
Wire Wire Line
	6850 9650 6500 9650
Text Label 6550 9650 0    50   ~ 0
c1p134
Wire Wire Line
	6850 9750 6500 9750
Text Label 6550 9750 0    50   ~ 0
c1p136
Wire Wire Line
	6850 9850 6500 9850
Text Label 6550 9850 0    50   ~ 0
c1p138
Wire Wire Line
	6850 9950 6500 9950
Text Label 6550 9950 0    50   ~ 0
c1p140
Text Label 6550 6450 0    50   ~ 0
c1p80
$EndSCHEMATC
