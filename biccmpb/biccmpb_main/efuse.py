import itertools

UVLO_hys = 0.18
UVLO_th = 1.6 - UVLO_hys
UVLO_bias = 5.5e-6
VIN_MIN = 9.3
VIN_MAX = 60
OVP_th = 2

R1 = 100e3
R3 = ((UVLO_th * R1) / (VIN_MIN - UVLO_th - (UVLO_bias*R1)) + R1) / \
        (VIN_MAX / OVP_th - (UVLO_bias * R1) / OVP_th)
R2 = R3 * VIN_MAX / OVP_th - R3 - R1 - UVLO_bias * R1 * R3 / OVP_th

print("R1", R1)
print("R2", R2)
print("R3", R3)

def vin_max(r1, r2, r3, ovp_th, uvlo_bias):
    return r1*(ovp_th/r3 + uvlo_bias) + r2 * ovp_th/r3 + ovp_th

def vin_min(r1, r2, r3, uvlo_th, uvlo_bias):
    return (uvlo_th / (r2+r3) + uvlo_bias)*r1 + uvlo_th

print(vin_min(R1, R2, R3, UVLO_th, UVLO_bias))
print(vin_max(R1, R2, R3, OVP_th, UVLO_bias))

r1_tol = 0.01
r2_tol = 0.01
r3_tol = 0.01

R1 = 100e3
R2 = 15.4e3
R3 = 4.02e3

r1_bounds = (R1*(1-r1_tol), R1*(1+r1_tol))
r2_bounds = (R2*(1-r2_tol), R2*(1+r2_tol))
r3_bounds = (R3*(1-r3_tol), R3*(1+r3_tol))

uvlo_th_bounds = (1.45, 1.75) # Across -40 - 125 C
uvlo_th_bounds = (1.6, 1.65) # From graph
uvlo_bias_bounds = (3.8e-6, 7.2e-6) # Across -40 - 125 C
uvlo_hys_bounds = (0.12, 0.23) # Across -40 - 125 C

ovp_th_bounds = (1.88, 2.12) # Across -40 - 125 C

uvlo_drop_th_bounds = (uvlo_th_bounds[0] - uvlo_hys_bounds[0],
                       uvlo_th_bounds[1] - uvlo_hys_bounds[1])

vin_mins = [vin_min(r1, r2, r3, uvlo_th, uvlo_bias)
            for r1, r2, r3, uvlo_th, uvlo_bias
            in itertools.product(r1_bounds,
                                 r2_bounds,
                                 r3_bounds,
                                 uvlo_th_bounds,
                                 uvlo_bias_bounds)]
print(min(vin_mins), max(vin_mins))
if False:
    print(vin_mins)
    print(list(itertools.product(r1_bounds,
                                     r2_bounds,
                                     r3_bounds,
                                     uvlo_th_bounds,
                                     uvlo_bias_bounds)))

vin_drops = [vin_min(r1, r2, r3, uvlo_th, uvlo_bias)
             for r1, r2, r3, uvlo_th, uvlo_bias
             in itertools.product(r1_bounds,
                                  r2_bounds,
                                  r3_bounds,
                                  uvlo_drop_th_bounds,
                                  uvlo_bias_bounds)]
print(min(vin_drops), max(vin_drops))


vin_maxs = [vin_max(r1, r2, r3, ovp_th, uvlo_bias)
            for r1, r2, r3, ovp_th, uvlo_bias
            in itertools.product(r1_bounds,
                                 r2_bounds,
                                 r3_bounds,
                                 ovp_th_bounds,
                                 uvlo_bias_bounds)]
print(min(vin_maxs), max(vin_maxs))

print("********************************************************************************")
R_ds = 2e-3
V_offset = 1e-3
I_sense = 16e-6
R_s = 10e3
VDS_th = R_s * I_sense - V_offset
IDS_th = VDS_th / (2*R_ds)

def IDS_th(r_ds, r_s, i_sense, v_offset):
    vds_th = r_s*i_sense - v_offset
    return vds_th / (2*r_ds)

R_s_tol = 0.01

R_ds_bounds = (2e-3, 2.4e-3)
V_offset_bounds = (-7e-3, 7e-3) # -40 - 125 C
V_offset_bounds = (-0.7e-3, 0.7e-3) # From graph
I_sense_bounds = (13.6e-6, 18e-6) # -40 - 125 C
#I_sense_bounds = (14.5e-6, 17.5e-6) # Guessing from graph
#I_sense_bounds = (15.5e-6, 16.5e-6) # Guessing from graph
R_s_bounds = (R_s*(1-R_s_tol), R_s*(1+R_s_tol))

print(IDS_th(R_ds, R_s, I_sense, V_offset))

ids_ths = [IDS_th(r_ds, r_s, i_sense, v_offset)
           for r_ds, r_s, i_sense, v_offset
           in itertools.product(R_ds_bounds,
                                R_s_bounds,
                                I_sense_bounds,
                                V_offset_bounds)]

print(min(ids_ths), max(ids_ths))
print(ids_ths)
