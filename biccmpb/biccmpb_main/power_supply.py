import math

eff_assumed = 0.9

V_in_max = 60
V_in_min = 9
V_out = 12
F_sw = 600e3
I_out_max = 8

L_buck = ((V_in_max - V_out)*V_out)/(0.4*I_out_max*F_sw*V_in_max)
print(L_buck)

L_boost = (V_in_min**2*(V_out-V_in_min))/(0.3*I_out_max*F_sw*V_out**2)
print(L_boost)

I_L_max = V_out*I_out_max/(eff_assumed*V_in_min)

print(I_L_max)

L = 10e-6
R_L = 7.9e-3

I_L_peak = I_L_max + (V_in_min*(V_out-V_in_min)) / (2*L*F_sw*V_out)
print(I_L_peak)

print(R_L*I_L_max**2)

print("I_Cout_rms = ", I_out_max*math.sqrt(V_out/V_in_min-1))
