EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 27
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1650 1000 1000 500 
U 5E177DA4
F0 "power_protection" 50
F1 "power_protection.sch" 50
F2 "v_in" I L 1650 1250 50 
F3 "v_out" O R 2650 1250 50 
F4 "p_good" O R 2650 1350 50 
$EndSheet
$Comp
L Device:Fuse F101
U 1 1 5E1780C4
P 1400 1250
F 0 "F101" V 1480 1250 50  0000 C CNN
F 1 "Fuse" V 1325 1250 50  0000 C CNN
F 2 "Fuse:Fuseholder_Cylinder-5x20mm_Stelvio-Kontek_PTF78_Horizontal_Open" V 1330 1250 50  0001 C CNN
F 3 "~" H 1400 1250 50  0001 C CNN
F 4 "3000-0000000419" V 1400 1250 50  0001 C CNN "PartSpecification"
	1    1400 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 1250 1050 1250
$Comp
L Connector_Generic:Conn_01x02 J101
U 1 1 5E1788BA
P 850 1350
F 0 "J101" H 850 1450 50  0000 C CNN
F 1 "Conn_01x02" H 850 1150 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 850 1350 50  0001 C CNN
F 3 "~" H 850 1350 50  0001 C CNN
F 4 "3000-0000000184" H 850 1350 50  0001 C CNN "PartSpecification"
	1    850  1350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E178DD7
P 1150 1500
F 0 "#PWR?" H 1150 1250 50  0001 C CNN
F 1 "GND" H 1150 1350 50  0000 C CNN
F 2 "" H 1150 1500 50  0001 C CNN
F 3 "" H 1150 1500 50  0001 C CNN
	1    1150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 1500 1150 1350
Wire Wire Line
	1150 1350 1050 1350
$Sheet
S 4800 1200 1000 1000
U 5E17D17C
F0 "synchronizer" 50
F1 "synchronizer.sch" 50
F2 "v_in" I L 4800 1250 50 
F3 "p1" O L 4800 1450 50 
F4 "p2" O L 4800 1550 50 
F5 "p3" O L 4800 1650 50 
F6 "p4" O L 4800 1750 50 
F7 "p5" O L 4800 1850 50 
F8 "en" I L 4800 1350 50 
F9 "pg" O R 5800 1950 50 
F10 "v_3_3_pre" O R 5800 1850 50 
$EndSheet
Wire Wire Line
	4000 1250 4000 2650
Wire Wire Line
	4000 2650 4800 2650
Wire Wire Line
	4800 1450 4200 1450
Wire Wire Line
	4200 1450 4200 2850
Wire Wire Line
	4200 2850 4800 2850
$Sheet
S 4800 5500 1200 2000
U 5E182A9D
F0 "medium_power" 50
F1 "medium_power.sch" 50
F2 "v_in" I L 4800 5600 50 
F3 "p2" I L 4800 5800 50 
F4 "p3" I L 4800 5900 50 
F5 "p4" I L 4800 6000 50 
F6 "p5" I L 4800 6100 50 
F7 "en_nuc" I R 6000 5800 50 
F8 "en_exp" I R 6000 5900 50 
F9 "en_n300" I R 6000 6000 50 
F10 "en_byod_2" I R 6000 6200 50 
F11 "pg_nuc" O R 6000 6400 50 
F12 "pg_exp" O R 6000 6500 50 
F13 "pg_n300" O R 6000 6600 50 
F14 "pg_byod_1" O R 6000 6700 50 
F15 "pg_byod_2" O R 6000 6800 50 
F16 "i_sense_exp" O R 6000 7100 50 
F17 "i_sense_n300" O R 6000 7200 50 
F18 "i_sense_byod_2" O R 6000 7400 50 
F19 "v_sense_nuc" O L 4800 6400 50 
F20 "v_sense_exp" O L 4800 6500 50 
F21 "v_sense_n300" O L 4800 6600 50 
F22 "v_sense_byod_2" O L 4800 6800 50 
F23 "i_sense_nuc" O R 6000 7000 50 
$EndSheet
Wire Wire Line
	4300 1550 4300 5800
Wire Wire Line
	4300 1550 4800 1550
Wire Wire Line
	4800 1650 4400 1650
Wire Wire Line
	4400 1650 4400 5900
Wire Wire Line
	4500 6000 4500 1750
Wire Wire Line
	4500 1750 4800 1750
Wire Wire Line
	4800 1850 4600 1850
Wire Wire Line
	4600 1850 4600 6100
Wire Wire Line
	4000 2650 4000 5600
Connection ~ 4000 2650
$Comp
L Device:R_Shunt R101
U 1 1 5E19B919
P 3000 1250
F 0 "R101" V 3080 1250 50  0000 C CNN
F 1 "1m" V 3000 1250 50  0000 C CNN
F 2 "Resistor_SMD:R_Shunt_Vishay_WSKW0612" V 2930 1250 50  0001 C CNN
F 3 "~" H 3000 1250 50  0001 C CNN
F 4 "3000-0000000497" H 3000 1250 50  0001 C CNN "PartSpecification"
	1    3000 1250
	0    1    1    0   
$EndComp
$Comp
L Device:R R102
U 1 1 5E1A0331
P 3250 4750
F 0 "R102" V 3330 4750 50  0000 C CNN
F 1 "191k" V 3250 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_2512_6332Metric" V 3180 4750 50  0001 C CNN
F 3 "~" H 3250 4750 50  0001 C CNN
F 4 "3000-0000000537" H 3250 4750 50  0001 C CNN "PartSpecification"
	1    3250 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R103
U 1 1 5E1A0515
P 3250 5250
F 0 "R103" V 3330 5250 50  0000 C CNN
F 1 "10k" V 3250 5250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3180 5250 50  0001 C CNN
F 3 "~" H 3250 5250 50  0001 C CNN
F 4 "3000-0000000549" H 3250 5250 50  0001 C CNN "PartSpecification"
	1    3250 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E1A073D
P 3250 5500
F 0 "#PWR?" H 3250 5250 50  0001 C CNN
F 1 "GND" H 3250 5350 50  0000 C CNN
F 2 "" H 3250 5500 50  0001 C CNN
F 3 "" H 3250 5500 50  0001 C CNN
	1    3250 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 5400 3250 5500
Wire Wire Line
	3250 5100 3250 5000
Wire Wire Line
	3250 5000 3700 5000
Connection ~ 3250 5000
Wire Wire Line
	3250 5000 3250 4900
Text Label 3750 5000 0    50   ~ 0
v_sense10
Text Label 6000 4100 0    50   ~ 0
i_sense0
Text Label 6000 4200 0    50   ~ 0
i_sense1
Text Label 6000 4300 0    50   ~ 0
i_sense2
Text Label 6000 4400 0    50   ~ 0
i_sense3
Text Label 6000 4500 0    50   ~ 0
i_sense4
Text Label 6000 4600 0    50   ~ 0
i_sense5
Text Label 6000 4700 0    50   ~ 0
i_sense6
Text Label 6050 7000 0    50   ~ 0
i_sense7
Text Label 6050 7100 0    50   ~ 0
i_sense8
Text Label 6050 7200 0    50   ~ 0
i_sense9
Text Label 2250 3000 0    50   ~ 0
i_sense10
Text Label 6000 2750 0    50   ~ 0
pg0
Text Label 6000 3600 0    50   ~ 0
pg1
Text Label 6050 6700 0    50   ~ 0
pg8
Text Label 6000 3700 0    50   ~ 0
pg2
Text Label 6050 6800 0    50   ~ 0
pg9
Text Label 6050 6400 0    50   ~ 0
pg5
Text Label 6050 6500 0    50   ~ 0
pg6
Text Label 6050 6600 0    50   ~ 0
pg7
Text Label 6000 3000 0    50   ~ 0
en0
Text Label 6000 3100 0    50   ~ 0
en1
Text Label 6000 3200 0    50   ~ 0
en2
Text Label 6000 3300 0    50   ~ 0
en3
Text Label 6050 5800 0    50   ~ 0
en5
Text Label 6050 5900 0    50   ~ 0
en6
Text Label 6050 6000 0    50   ~ 0
en7
Wire Wire Line
	4800 1250 4000 1250
Wire Wire Line
	5800 1950 5950 1950
Wire Wire Line
	5950 1950 5950 2350
Wire Wire Line
	5950 2350 4100 2350
Wire Wire Line
	4100 2350 4100 2750
Wire Wire Line
	4100 2750 4800 2750
Wire Wire Line
	3700 5000 3900 5000
Connection ~ 3700 5000
Text Label 1250 2800 1    50   ~ 0
rsp_main
Text Label 1150 2800 1    50   ~ 0
rsm_main
Wire Wire Line
	1650 1250 1550 1250
Wire Wire Line
	2650 1350 4800 1350
Wire Wire Line
	4000 1250 3250 1250
Connection ~ 4000 1250
Wire Wire Line
	2800 1250 2650 1250
Wire Wire Line
	3100 1400 3100 1900
Wire Wire Line
	2900 1400 2900 2000
Wire Wire Line
	3250 1250 3250 4600
$Comp
L Device:D_Schottky D?
U 1 1 5EE81157
P 3700 4700
AR Path="/5E177DA4/5EE81157" Ref="D?"  Part="1" 
AR Path="/5EE81157" Ref="D101"  Part="1" 
F 0 "D101" H 3700 4800 50  0000 C CNN
F 1 "D_Schottky" H 3700 4600 50  0000 C CNN
F 2 "POWDER_Power:SMP" H 3700 4700 50  0001 C CNN
F 3 "~" H 3700 4700 50  0001 C CNN
F 4 "V2PM10L" H 3700 4700 50  0001 C CNN "PartNumber"
F 5 "3000-0000000478" H 3700 4700 50  0001 C CNN "PartSpecification"
	1    3700 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 4850 3700 5000
$Comp
L power:+3.3V #PWR?
U 1 1 5EE8EB8A
P 3700 4450
F 0 "#PWR?" H 3700 4300 50  0001 C CNN
F 1 "+3.3V" H 3700 4590 50  0000 C CNN
F 2 "" H 3700 4450 50  0001 C CNN
F 3 "" H 3700 4450 50  0001 C CNN
	1    3700 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 4450 3700 4550
$Comp
L POWDER_Analog:INA240-Q1 U101
U 1 1 5EEA0B5C
P 1800 3000
F 0 "U101" H 2000 3550 50  0000 C CNN
F 1 "INA240-Q1" H 1800 3200 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_4.4x3mm_P0.65mm" H 1800 3000 50  0001 C CNN
F 3 "" H 1800 3000 50  0001 C CNN
F 4 "INA240A3-Q1" H 1800 3000 50  0001 C CNN "PartNumber"
F 5 "3000-0000000503" H 1800 3000 50  0001 C CNN "PartSpecification"
	1    1800 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EEBF670
P 1800 3700
F 0 "#PWR?" H 1800 3450 50  0001 C CNN
F 1 "GND" H 1800 3550 50  0000 C CNN
F 2 "" H 1800 3700 50  0001 C CNN
F 3 "" H 1800 3700 50  0001 C CNN
	1    1800 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 3600 1800 3650
$Comp
L power:+3.3V #PWR?
U 1 1 5EEC44F1
P 1800 2300
F 0 "#PWR?" H 1800 2150 50  0001 C CNN
F 1 "+3.3V" H 1800 2440 50  0000 C CNN
F 2 "" H 1800 2300 50  0001 C CNN
F 3 "" H 1800 2300 50  0001 C CNN
	1    1800 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2300 1800 2400
$Comp
L 000_PreferredParts:C_0_1uF C101
U 1 1 5EEC920B
P 750 2750
F 0 "C101" H 775 2850 50  0000 L CNN
F 1 "C_0_1uF" H 775 2650 50  0001 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 788 2600 50  0001 C CNN
F 3 "~" H 750 2750 50  0001 C CNN
F 4 "0.1uF" H 900 2650 50  0000 C CNN "DisplayValue"
F 5 "3000-0000000125" H 750 2750 50  0001 C CNN "PartSpecification"
	1    750  2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EEC96F9
P 750 3000
F 0 "#PWR?" H 750 2750 50  0001 C CNN
F 1 "GND" H 750 2850 50  0000 C CNN
F 2 "" H 750 3000 50  0001 C CNN
F 3 "" H 750 3000 50  0001 C CNN
	1    750  3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  3000 750  2900
$Comp
L power:+3.3V #PWR?
U 1 1 5EECE1C9
P 750 2500
F 0 "#PWR?" H 750 2350 50  0001 C CNN
F 1 "+3.3V" H 750 2640 50  0000 C CNN
F 2 "" H 750 2500 50  0001 C CNN
F 3 "" H 750 2500 50  0001 C CNN
	1    750  2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  2500 750  2600
Wire Wire Line
	2200 3200 2300 3200
Wire Wire Line
	2300 3200 2300 3300
Wire Wire Line
	2300 3300 2200 3300
Wire Wire Line
	2300 3300 2300 3650
Wire Wire Line
	2300 3650 1800 3650
Connection ~ 2300 3300
Connection ~ 1800 3650
Wire Wire Line
	1800 3650 1800 3700
Wire Wire Line
	2200 3000 2500 3000
Text Label 6000 3800 0    50   ~ 0
pg3
Text Label 6000 3900 0    50   ~ 0
pg4
Text Label 6000 3400 0    50   ~ 0
en4
Text Label 6050 6200 0    50   ~ 0
en8
Wire Wire Line
	6350 7100 6000 7100
Wire Wire Line
	6350 7200 6000 7200
Wire Wire Line
	6350 4700 5950 4700
Wire Wire Line
	6350 4600 5950 4600
Wire Wire Line
	6350 4500 5950 4500
Wire Wire Line
	6350 4400 5950 4400
Wire Wire Line
	6350 4300 5950 4300
Wire Wire Line
	6350 4100 5950 4100
Wire Wire Line
	6350 4200 5950 4200
Wire Wire Line
	5950 3400 6750 3400
Entry Wire Line
	6850 3300 6750 3400
Wire Wire Line
	6550 3900 5950 3900
Wire Wire Line
	6550 3800 5950 3800
Entry Wire Line
	6650 3700 6550 3800
Entry Wire Line
	6650 3800 6550 3900
Text Label 8500 2300 0    50   ~ 0
fan_tach[0..4]
Wire Wire Line
	10800 3950 10800 4950
Wire Wire Line
	8600 2800 9000 2800
Wire Wire Line
	8600 3950 8600 2800
Wire Wire Line
	10800 3950 8600 3950
Wire Wire Line
	10900 3850 10900 5050
Wire Wire Line
	8700 3850 10900 3850
Wire Wire Line
	8700 2900 8700 3850
Wire Wire Line
	9000 2900 8700 2900
Wire Wire Line
	11000 3750 11000 5150
Wire Wire Line
	8800 3750 11000 3750
Wire Wire Line
	8800 3000 8800 3750
Wire Wire Line
	9000 3000 8800 3000
Wire Wire Line
	10250 5150 11000 5150
Wire Wire Line
	10250 5050 10900 5050
Wire Wire Line
	10250 4950 10800 4950
Wire Wire Line
	11100 5250 10250 5250
Wire Wire Line
	11100 3650 11100 5250
Wire Wire Line
	8900 3650 11100 3650
Wire Wire Line
	8900 3100 8900 3650
Wire Wire Line
	9000 3100 8900 3100
Wire Wire Line
	9150 4950 9250 4950
Wire Wire Line
	9150 4400 9150 4950
Wire Wire Line
	10700 4400 9150 4400
Wire Wire Line
	9050 4300 9050 5050
Wire Wire Line
	9050 5050 9250 5050
Wire Wire Line
	10600 2900 10500 2900
Wire Wire Line
	10600 4300 10600 2900
Wire Wire Line
	9050 4300 10600 4300
Wire Wire Line
	10700 2800 10700 4400
Wire Wire Line
	10500 2800 10700 2800
Text Label 10550 2500 0    50   ~ 0
en[0..9]
Entry Wire Line
	6850 5700 6750 5800
Entry Wire Line
	6850 5800 6750 5900
Entry Wire Line
	6850 5900 6750 6000
Wire Wire Line
	6750 6200 6000 6200
Wire Wire Line
	6000 5800 6750 5800
Wire Wire Line
	6750 5900 6000 5900
Wire Wire Line
	6000 6000 6750 6000
Entry Wire Line
	6850 6100 6750 6200
Wire Wire Line
	5950 3300 6750 3300
Wire Wire Line
	6750 3200 5950 3200
Entry Wire Line
	6850 3200 6750 3300
Entry Wire Line
	6850 3100 6750 3200
Wire Wire Line
	6750 3100 5950 3100
Entry Wire Line
	6850 3000 6750 3100
Wire Wire Line
	6750 3000 5950 3000
Entry Wire Line
	6850 2900 6750 3000
Wire Wire Line
	6550 2750 5950 2750
Entry Wire Line
	6650 2650 6550 2750
Wire Wire Line
	6550 6600 6000 6600
Wire Wire Line
	6000 6500 6550 6500
Wire Wire Line
	6550 6400 6000 6400
Wire Wire Line
	6550 6700 6000 6700
Entry Wire Line
	6650 6300 6550 6400
Entry Wire Line
	6650 6400 6550 6500
Entry Wire Line
	6650 6500 6550 6600
Entry Wire Line
	6650 6600 6550 6700
Entry Wire Line
	6650 6700 6550 6800
Wire Bus Line
	10900 1750 6850 1750
Wire Bus Line
	10900 2500 10900 1750
Wire Bus Line
	10500 2500 10900 2500
Wire Wire Line
	6550 3700 5950 3700
Wire Wire Line
	6550 3600 5950 3600
Entry Wire Line
	6650 3500 6550 3600
Entry Wire Line
	6650 3600 6550 3700
Text Label 8500 2400 0    50   ~ 0
pg[0..9]
Entry Wire Line
	6450 7000 6350 7100
Entry Wire Line
	6450 7100 6350 7200
Entry Wire Line
	6450 6900 6350 7000
Entry Wire Line
	6450 4600 6350 4700
Entry Wire Line
	6450 4500 6350 4600
Entry Wire Line
	6450 4400 6350 4500
Entry Wire Line
	6450 4300 6350 4400
Entry Wire Line
	6450 4200 6350 4300
Entry Wire Line
	6450 4000 6350 4100
Entry Wire Line
	6450 4100 6350 4200
Text Label 8250 2500 0    50   ~ 0
i_sense_filt[0..12]
Wire Bus Line
	8400 2600 9000 2600
Text Label 8400 2600 0    50   ~ 0
v_sense[0..12]
Text Notes 9250 5700 0    50   ~ 0
GPSDO connections
$Sheet
S 9250 4500 1000 1000
U 5E186189
F0 "GPSDO_buffer" 50
F1 "GPSDO_buffer.sch" 50
F2 "ser_tx" O R 10250 4950 50 
F3 "nmea_txd" O R 10250 5050 50 
F4 "lock_ok" O R 10250 5150 50 
F5 "pps_out" O R 10250 5250 50 
F6 "ser_rx" I L 9250 4950 50 
F7 "enter_isp" I L 9250 5050 50 
$EndSheet
$Sheet
S 9000 2000 1500 1500
U 5E1922DE
F0 "mcu" 50
F1 "mcu.sch" 50
F2 "pg[0..9]" I L 9000 2400 50 
F3 "en[0..9]" O R 10500 2500 50 
F4 "i_sense[0..12]" I L 9000 2500 50 
F5 "gpsdo_ser_output" I L 9000 2800 50 
F6 "gpsdo_nmea_txd" I L 9000 2900 50 
F7 "gpsdo_ser_input" O R 10500 2800 50 
F8 "gpsdo_enter_isp" O R 10500 2900 50 
F9 "gpsdo_lock_ok" I L 9000 3000 50 
F10 "gpsdo_pps_in" I L 9000 3100 50 
F11 "v_sense[0..12]" I L 9000 2600 50 
F12 "fan_tach[0..4]" I L 9000 2300 50 
$EndSheet
$Sheet
S 4800 2500 1150 2500
U 5E179620
F0 "low_voltage_low_power" 50
F1 "low_voltage_low_power.sch" 50
F2 "v_in" I L 4800 2650 50 
F3 "en" I L 4800 2750 50 
F4 "sync" I L 4800 2850 50 
F5 "en_switch" I R 5950 3000 50 
F6 "en_iris" I R 5950 3100 50 
F7 "en_b210_1" I R 5950 3200 50 
F8 "en_b210_2" I R 5950 3300 50 
F9 "v_12" O R 5950 2650 50 
F10 "pg_12" O R 5950 2750 50 
F11 "pg_b210_1" O R 5950 3600 50 
F12 "pg_b210_2" O R 5950 3700 50 
F13 "i_sense_12" O R 5950 4100 50 
F14 "i_sense_switch" O R 5950 4200 50 
F15 "i_sense_iris" O R 5950 4300 50 
F16 "i_sense_b210_1" O R 5950 4400 50 
F17 "i_sense_b210_2" O R 5950 4500 50 
F18 "i_sense_3v3" O R 5950 4700 50 
F19 "pg_3_3" O R 5950 3900 50 
F20 "en_b210_3" I R 5950 3400 50 
F21 "pg_b210_3" O R 5950 3800 50 
F22 "i_sense_b210_3" O R 5950 4600 50 
F23 "v_sense_12" O L 4800 3000 50 
F24 "v_sense_3_3" O L 4800 3600 50 
F25 "v_sense_b210_1" O L 4800 3300 50 
F26 "v_sense_b210_2" O L 4800 3400 50 
F27 "v_sense_b210_3" O L 4800 3500 50 
F28 "v_sense_switch" O L 4800 3100 50 
F29 "v_sense_iris" O L 4800 3200 50 
F30 "v_3_3_pre" I L 4800 2550 50 
$EndSheet
$Sheet
S 6150 1000 1000 500 
U 5E3A7260
F0 "fans" 50
F1 "fans.sch" 50
F2 "fan_tach[0..4]" O R 7150 1250 50 
F3 "v_12" I L 6150 1250 50 
$EndSheet
Wire Wire Line
	6150 1250 6050 1250
Wire Wire Line
	6050 1250 6050 2650
Wire Wire Line
	6050 2650 5950 2650
Wire Bus Line
	7150 1250 8300 1250
Wire Bus Line
	8300 1250 8300 2300
Text Label 6050 7400 0    50   ~ 0
i_sense11
Wire Wire Line
	1400 2950 1250 2950
Wire Wire Line
	1250 2950 1250 2000
Wire Wire Line
	1250 2000 2900 2000
Wire Wire Line
	3100 1900 1150 1900
Wire Wire Line
	1150 1900 1150 3050
Wire Wire Line
	1150 3050 1400 3050
Connection ~ 3250 1250
Wire Wire Line
	3250 1250 3200 1250
$Sheet
S 7000 2200 1000 500 
U 5EAD3833
F0 "i_sense_filt" 50
F1 "i_sense_filt.sch" 50
F2 "i_sense[0..12]" I L 7000 2400 50 
F3 "i_sense_filt[0..12]" O R 8000 2500 50 
$EndSheet
Wire Bus Line
	6650 2000 8100 2000
Wire Bus Line
	8100 2000 8100 2400
Wire Bus Line
	8000 2500 9000 2500
Wire Bus Line
	6450 2400 7000 2400
Text Label 6450 2600 1    50   ~ 0
i_sense[0..12]
Wire Wire Line
	6000 7400 6350 7400
Wire Wire Line
	4800 6100 4600 6100
Wire Wire Line
	4800 6000 4500 6000
Wire Wire Line
	4800 5600 4000 5600
Wire Wire Line
	4300 5800 4800 5800
Wire Wire Line
	4800 5900 4400 5900
Entry Wire Line
	4100 2900 4200 3000
Wire Wire Line
	4200 3000 4800 3000
Text Label 4200 3000 0    50   ~ 0
v_sense0
Wire Bus Line
	8400 2600 8400 2400
Wire Bus Line
	8400 900  4100 900 
Wire Bus Line
	8400 2300 8400 900 
Wire Bus Line
	8400 2400 8400 2300
Entry Wire Line
	4100 3000 4200 3100
Wire Wire Line
	4200 3100 4800 3100
Text Label 4200 3100 0    50   ~ 0
v_sense1
Entry Wire Line
	4100 3100 4200 3200
Wire Wire Line
	4200 3200 4800 3200
Text Label 4200 3200 0    50   ~ 0
v_sense2
Entry Wire Line
	4100 3200 4200 3300
Wire Wire Line
	4200 3300 4800 3300
Text Label 4200 3300 0    50   ~ 0
v_sense3
Entry Wire Line
	4100 3300 4200 3400
Wire Wire Line
	4200 3400 4800 3400
Text Label 4200 3400 0    50   ~ 0
v_sense4
Entry Wire Line
	4100 3400 4200 3500
Wire Wire Line
	4200 3500 4800 3500
Text Label 4200 3500 0    50   ~ 0
v_sense5
Entry Wire Line
	4100 3500 4200 3600
Wire Wire Line
	4200 3600 4800 3600
Text Label 4200 3600 0    50   ~ 0
v_sense6
Wire Wire Line
	4200 6800 4800 6800
Text Label 4200 6800 0    50   ~ 0
v_sense11
Entry Wire Line
	4100 6500 4200 6600
Wire Wire Line
	4200 6600 4800 6600
Text Label 4200 6600 0    50   ~ 0
v_sense9
Entry Wire Line
	4100 6400 4200 6500
Wire Wire Line
	4200 6500 4800 6500
Text Label 4200 6500 0    50   ~ 0
v_sense8
Entry Wire Line
	4100 6300 4200 6400
Wire Wire Line
	4200 6400 4800 6400
Text Label 4200 6400 0    50   ~ 0
v_sense7
Wire Bus Line
	8300 2300 9000 2300
Wire Bus Line
	8100 2400 9000 2400
Text Label 1150 1250 1    50   ~ 0
v_main
Text Label 1600 1250 1    50   ~ 0
v_main_fuse
Wire Wire Line
	4800 2550 4750 2550
Wire Wire Line
	4750 2550 4750 2400
Wire Wire Line
	4750 2400 6000 2400
Wire Wire Line
	6000 2400 6000 1850
Wire Wire Line
	6000 1850 5800 1850
Wire Wire Line
	6000 6800 6550 6800
Wire Wire Line
	6350 7000 6000 7000
$Sheet
S 1000 5500 1000 1000
U 5E3179A3
F0 "mounting" 50
F1 "mounting.sch" 50
$EndSheet
Entry Wire Line
	4100 6700 4200 6800
Entry Wire Line
	6350 7400 6450 7300
Wire Bus Line
	6650 2000 6650 6700
Wire Bus Line
	4100 900  4100 6700
Wire Bus Line
	6850 1750 6850 6100
Wire Bus Line
	6450 2400 6450 7300
$EndSCHEMATC
