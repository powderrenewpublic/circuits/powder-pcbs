EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 28 27
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1500 1000 0    50   Input ~ 0
i_sense[0..12]
Text HLabel 6000 7000 2    50   Output ~ 0
i_sense_filt[0..12]
$Comp
L Device:R R2801
U 1 1 5EAE082A
P 2750 1200
F 0 "R2801" V 2830 1200 50  0000 C CNN
F 1 "10k" V 2750 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2680 1200 50  0001 C CNN
F 3 "~" H 2750 1200 50  0001 C CNN
F 4 "3000-0000000131" H 2750 1200 50  0001 C CNN "PartSpecification"
	1    2750 1200
	0    1    1    0   
$EndComp
Wire Bus Line
	1500 1000 2000 1000
Entry Wire Line
	2000 1100 2100 1200
Text Label 2150 1200 0    50   ~ 0
i_sense0
Wire Wire Line
	2100 1200 2600 1200
$Comp
L Device:R R2802
U 1 1 5EAE2C14
P 2750 2000
F 0 "R2802" V 2830 2000 50  0000 C CNN
F 1 "10k" V 2750 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2680 2000 50  0001 C CNN
F 3 "~" H 2750 2000 50  0001 C CNN
F 4 "3000-0000000131" H 2750 2000 50  0001 C CNN "PartSpecification"
	1    2750 2000
	0    1    1    0   
$EndComp
Entry Wire Line
	2000 1900 2100 2000
Text Label 2150 2000 0    50   ~ 0
i_sense1
Wire Wire Line
	2100 2000 2600 2000
$Comp
L Device:R R2803
U 1 1 5EAE32D6
P 2750 3000
F 0 "R2803" V 2830 3000 50  0000 C CNN
F 1 "10k" V 2750 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2680 3000 50  0001 C CNN
F 3 "~" H 2750 3000 50  0001 C CNN
F 4 "3000-0000000131" H 2750 3000 50  0001 C CNN "PartSpecification"
	1    2750 3000
	0    1    1    0   
$EndComp
Entry Wire Line
	2000 2900 2100 3000
Text Label 2150 3000 0    50   ~ 0
i_sense2
Wire Wire Line
	2100 3000 2600 3000
$Comp
L Device:R R2804
U 1 1 5EAE39DE
P 2750 4000
F 0 "R2804" V 2830 4000 50  0000 C CNN
F 1 "10k" V 2750 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2680 4000 50  0001 C CNN
F 3 "~" H 2750 4000 50  0001 C CNN
F 4 "3000-0000000131" H 2750 4000 50  0001 C CNN "PartSpecification"
	1    2750 4000
	0    1    1    0   
$EndComp
Entry Wire Line
	2000 3900 2100 4000
Text Label 2150 4000 0    50   ~ 0
i_sense3
Wire Wire Line
	2100 4000 2600 4000
$Comp
L Device:R R2805
U 1 1 5EAE44C7
P 2750 5000
F 0 "R2805" V 2830 5000 50  0000 C CNN
F 1 "10k" V 2750 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2680 5000 50  0001 C CNN
F 3 "~" H 2750 5000 50  0001 C CNN
F 4 "3000-0000000131" H 2750 5000 50  0001 C CNN "PartSpecification"
	1    2750 5000
	0    1    1    0   
$EndComp
Entry Wire Line
	2000 4900 2100 5000
Text Label 2150 5000 0    50   ~ 0
i_sense4
Wire Wire Line
	2100 5000 2600 5000
$Comp
L Device:R R2806
U 1 1 5EAE4D4B
P 2750 6000
F 0 "R2806" V 2830 6000 50  0000 C CNN
F 1 "10k" V 2750 6000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2680 6000 50  0001 C CNN
F 3 "~" H 2750 6000 50  0001 C CNN
F 4 "3000-0000000131" H 2750 6000 50  0001 C CNN "PartSpecification"
	1    2750 6000
	0    1    1    0   
$EndComp
Entry Wire Line
	2000 5900 2100 6000
Text Label 2150 6000 0    50   ~ 0
i_sense5
Wire Wire Line
	2100 6000 2600 6000
$Comp
L Device:R R2807
U 1 1 5EAE56BA
P 2750 7000
F 0 "R2807" V 2830 7000 50  0000 C CNN
F 1 "10k" V 2750 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2680 7000 50  0001 C CNN
F 3 "~" H 2750 7000 50  0001 C CNN
F 4 "3000-0000000131" H 2750 7000 50  0001 C CNN "PartSpecification"
	1    2750 7000
	0    1    1    0   
$EndComp
Entry Wire Line
	2000 6900 2100 7000
Text Label 2150 7000 0    50   ~ 0
i_sense6
Wire Wire Line
	2100 7000 2600 7000
Entry Wire Line
	3400 1200 3500 1300
Text Label 2950 1200 0    50   ~ 0
i_sense_filt0
Entry Wire Line
	3400 2000 3500 2100
Text Label 2950 2000 0    50   ~ 0
i_sense_filt1
Entry Wire Line
	3400 3000 3500 3100
Text Label 2950 3000 0    50   ~ 0
i_sense_filt2
Entry Wire Line
	3400 4000 3500 4100
Text Label 2950 4000 0    50   ~ 0
i_sense_filt3
Entry Wire Line
	3400 5000 3500 5100
Text Label 2950 5000 0    50   ~ 0
i_sense_filt4
Entry Wire Line
	3400 6000 3500 6100
Text Label 2950 6000 0    50   ~ 0
i_sense_filt5
Entry Wire Line
	3400 7000 3500 7100
Text Label 2950 7000 0    50   ~ 0
i_sense_filt6
$Comp
L Device:R R2808
U 1 1 5EAF5E39
P 4750 1200
F 0 "R2808" V 4830 1200 50  0000 C CNN
F 1 "10k" V 4750 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4680 1200 50  0001 C CNN
F 3 "~" H 4750 1200 50  0001 C CNN
F 4 "3000-0000000131" H 4750 1200 50  0001 C CNN "PartSpecification"
	1    4750 1200
	0    1    1    0   
$EndComp
Entry Wire Line
	4000 1100 4100 1200
Text Label 4150 1200 0    50   ~ 0
i_sense7
Wire Wire Line
	4100 1200 4600 1200
$Comp
L Device:R R2809
U 1 1 5EAF5E55
P 4750 2000
F 0 "R2809" V 4830 2000 50  0000 C CNN
F 1 "10k" V 4750 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4680 2000 50  0001 C CNN
F 3 "~" H 4750 2000 50  0001 C CNN
F 4 "3000-0000000131" H 4750 2000 50  0001 C CNN "PartSpecification"
	1    4750 2000
	0    1    1    0   
$EndComp
Entry Wire Line
	4000 1900 4100 2000
Text Label 4150 2000 0    50   ~ 0
i_sense8
Wire Wire Line
	4100 2000 4600 2000
$Comp
L Device:R R2810
U 1 1 5EAF5E70
P 4750 3000
F 0 "R2810" V 4830 3000 50  0000 C CNN
F 1 "10k" V 4750 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4680 3000 50  0001 C CNN
F 3 "~" H 4750 3000 50  0001 C CNN
F 4 "3000-0000000131" H 4750 3000 50  0001 C CNN "PartSpecification"
	1    4750 3000
	0    1    1    0   
$EndComp
Entry Wire Line
	4000 2900 4100 3000
Text Label 4150 3000 0    50   ~ 0
i_sense9
Wire Wire Line
	4100 3000 4600 3000
$Comp
L Device:R R2811
U 1 1 5EAF5E8B
P 4750 4000
F 0 "R2811" V 4830 4000 50  0000 C CNN
F 1 "10k" V 4750 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4680 4000 50  0001 C CNN
F 3 "~" H 4750 4000 50  0001 C CNN
F 4 "3000-0000000131" H 4750 4000 50  0001 C CNN "PartSpecification"
	1    4750 4000
	0    1    1    0   
$EndComp
Entry Wire Line
	4000 3900 4100 4000
Text Label 4150 4000 0    50   ~ 0
i_sense10
Wire Wire Line
	4100 4000 4600 4000
$Comp
L Device:R R2812
U 1 1 5EAF5EA6
P 4750 5000
F 0 "R2812" V 4830 5000 50  0000 C CNN
F 1 "10k" V 4750 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4680 5000 50  0001 C CNN
F 3 "~" H 4750 5000 50  0001 C CNN
F 4 "3000-0000000131" H 4750 5000 50  0001 C CNN "PartSpecification"
	1    4750 5000
	0    1    1    0   
$EndComp
Entry Wire Line
	4000 4900 4100 5000
Text Label 4150 5000 0    50   ~ 0
i_sense11
Wire Wire Line
	4100 5000 4600 5000
$Comp
L Device:R R2813
U 1 1 5EAF5EC1
P 4750 6000
F 0 "R2813" V 4830 6000 50  0000 C CNN
F 1 "10k" V 4750 6000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4680 6000 50  0001 C CNN
F 3 "~" H 4750 6000 50  0001 C CNN
F 4 "3000-0000000131" H 4750 6000 50  0001 C CNN "PartSpecification"
	1    4750 6000
	0    1    1    0   
$EndComp
Entry Wire Line
	4000 5900 4100 6000
Text Label 4150 6000 0    50   ~ 0
i_sense12
Wire Wire Line
	4100 6000 4600 6000
Entry Wire Line
	5400 1200 5500 1300
Text Label 4950 1200 0    50   ~ 0
i_sense_filt7
Entry Wire Line
	5400 2000 5500 2100
Text Label 4950 2000 0    50   ~ 0
i_sense_filt8
Entry Wire Line
	5400 3000 5500 3100
Text Label 4950 3000 0    50   ~ 0
i_sense_filt9
Entry Wire Line
	5400 4000 5500 4100
Text Label 4950 4000 0    50   ~ 0
i_sense_filt10
Entry Wire Line
	5400 5000 5500 5100
Text Label 4950 5000 0    50   ~ 0
i_sense_filt11
Entry Wire Line
	5400 6000 5500 6100
Text Label 4950 6000 0    50   ~ 0
i_sense_filt12
Wire Bus Line
	2000 1000 4000 1000
Connection ~ 2000 1000
Wire Bus Line
	3500 7000 5500 7000
Connection ~ 3500 7000
Wire Bus Line
	3500 7000 3500 7100
Connection ~ 5500 7000
Wire Bus Line
	5500 7000 6000 7000
Wire Wire Line
	2900 7000 3400 7000
Wire Wire Line
	4900 6000 5400 6000
Wire Wire Line
	4900 5000 5400 5000
Wire Wire Line
	4900 4000 5400 4000
Wire Wire Line
	2900 6000 3400 6000
Wire Wire Line
	2900 5000 3400 5000
Wire Wire Line
	2900 4000 3400 4000
Wire Wire Line
	4900 3000 5400 3000
Wire Wire Line
	2900 3000 3400 3000
Wire Wire Line
	2900 2000 3400 2000
Wire Wire Line
	2900 1200 3400 1200
Wire Wire Line
	4900 1200 5400 1200
Wire Wire Line
	4900 2000 5400 2000
Wire Bus Line
	2000 1000 2000 6900
Wire Bus Line
	3500 1300 3500 7000
Wire Bus Line
	4000 1000 4000 5900
Wire Bus Line
	5500 1300 5500 7000
$EndSCHEMATC
