import pcbnew
import re

refdes_re = re.compile(r"^([a-zA-Z]+)9(\d+)")

pcb = pcbnew.GetBoard()

# Simple numerical resub
selected_modules = [_ for _ in pcb.GetModules() if _.IsSelected()]

for module in selected_modules:
    refdes = module.GetReference()
    if refdes_re.match(refdes):
        module.SetReference(refdes_re.sub(r"\g<1>6\2", refdes))

# Dict based remapping
selected_modules = [_ for _ in pcb.GetModules() if _.IsSelected()]

remapping_dict = {
    "C602": "C701",
    "C605": "C703",
    "C607": "C704",
    "C610": "C706",
    "D601": "D701",
    "R608": "R702",
    "R611": "R704",
    "C616": "C711",
    "R614": "R706",
    "C623": "C713",
    "R635": "R723",
    "R636": "R724",
    "D602": "D702",
    "R621": "R709",
    "C624": "C714",
    "R619": "R708",
    "U601": "U701",
    "U602": "U702",
    "U603": "U703",
    "U604": "U704",
    "U605": "U705",
    "U606": "U706",
    "L601": "L701",
    "L602": "L702",
    "R622": "R710",
    "R623": "R711",
    "C625": "C715",
    "R627": "R715",
    "R628": "R716",
    "R629": "R717",
    "R632": "R720",
    "C626": "C716",
    "D603": "D703",
    "R637": "R725",
    "R633": "R721",
    "R634": "R722",
    "R630": "R718",
    "R631": "R719",
    "D604": "D704",
    "C603": "C702",
    "C609": "C705",
    "R606": "R701",
    "R609": "R703",
    "C612": "C709",
    "C617": "C712",
    "R604": "R707",
    "C611": "C707",
    "C615": "C708",
    "C619": "C710",
    "R612": "R705",
    "R624": "R712",
    "C627": "C717",
    "R625": "R713",
    "R626": "R714",
    "C630": "C718",
    "C631": "C719",
    "C632": "C720",
    "C633": "C721",
    "C634": "C722",
    "C637": "C725",
    "C641": "C729",
    "C647": "C735",
    "C635": "C723",
    "C638": "C726",
    "C642": "C730",
    "C644": "C732",
    "C648": "C736",
    "C636": "C724",
    "C639": "C727",
    "C643": "C731",
    "C645": "C733",
    "C649": "C737",
    "C640": "C728",
    "C646": "C734",
    "C650": "C738",
    "C651": "C739",
    "C652": "C740",
    "C653": "C741",
    "C601": None,
    "R601": None,
    "R602": None,
    "R603": None,
    "C604": None,
    "C606": None,
    "C608": None,
    "R605": None,
    "R607": None,
    "R610": None,
    "C613": None,
    "C614": None,
    "C618": None,
    "R613": None,
    "R615": None,
    "R616": None,
    "C620": None,
    "C621": None,
    "C622": None,
    "R617": None,
    "R618": None,
    "R620": None,
    "C628": None,
    "C629": None,
    "SW601": None,
    "NT601": "NT701",
}

remapping_out_set = set()
for key, value in remapping_dict.items():
    if value is None:
        continue
    if value in remapping_out_set:
        print("FAIL:", key, value)
    remapping_out_set.add(value)

for module in selected_modules:
    refdes = module.GetReference()
    if refdes in remapping_dict:
        new_refdes = remapping_dict[refdes]
        if new_refdes is None:
            new_refdes = "REF***"
        module.SetReference(new_refdes)
    else:
        print("Unknown REFDES: ", refdes)
