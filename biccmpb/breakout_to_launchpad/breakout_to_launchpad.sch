EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 616751F9
P 2100 2450
F 0 "J1" H 2150 3567 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 2150 3476 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x20_P1.27mm_Vertical_SMD" H 2100 2450 50  0001 C CNN
F 3 "~" H 2100 2450 50  0001 C CNN
	1    2100 2450
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J2
U 1 1 616751FF
P 2100 5100
F 0 "J2" H 2150 6217 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 2150 6126 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x20_P1.27mm_Vertical_SMD" H 2100 5100 50  0001 C CNN
F 3 "~" H 2100 5100 50  0001 C CNN
	1    2100 5100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2300 1550 2700 1550
Text Label 2700 1650 2    50   ~ 0
vs_cnuc
Wire Wire Line
	2300 1650 2700 1650
Text Label 2700 1550 2    50   ~ 0
is_cnuc
Wire Wire Line
	1400 1650 1800 1650
Wire Wire Line
	2300 1850 2700 1850
Wire Wire Line
	2300 1950 2700 1950
Wire Wire Line
	2300 2150 2700 2150
Wire Wire Line
	2300 2250 2700 2250
Wire Wire Line
	2300 2450 2700 2450
Wire Wire Line
	2300 2550 2700 2550
Wire Wire Line
	2300 2750 2700 2750
Wire Wire Line
	2300 2850 2700 2850
Wire Wire Line
	2300 3050 2700 3050
Wire Wire Line
	2300 3150 2700 3150
Wire Wire Line
	2300 3350 2700 3350
Wire Wire Line
	2300 3450 2700 3450
Wire Wire Line
	2300 4200 2700 4200
Wire Wire Line
	2300 4300 2700 4300
Wire Wire Line
	2300 4500 2700 4500
Wire Wire Line
	2300 4600 2700 4600
Wire Wire Line
	2300 4800 2700 4800
Wire Wire Line
	2300 4900 2700 4900
Wire Wire Line
	2300 5100 2700 5100
Wire Wire Line
	2300 5200 2700 5200
Wire Wire Line
	2300 5400 2700 5400
Wire Wire Line
	2300 5500 2700 5500
Wire Wire Line
	2300 5900 2700 5900
Wire Wire Line
	2300 6000 2700 6000
Wire Wire Line
	1400 4200 1800 4200
Wire Wire Line
	1400 4300 1800 4300
Wire Wire Line
	1400 4400 1800 4400
Wire Wire Line
	1400 4500 1800 4500
Wire Wire Line
	1400 4600 1800 4600
Wire Wire Line
	1400 4700 1800 4700
Wire Wire Line
	1400 4800 1800 4800
Wire Wire Line
	1400 4900 1800 4900
Wire Wire Line
	1400 5000 1800 5000
Wire Wire Line
	1400 5100 1800 5100
Wire Wire Line
	1400 5200 1800 5200
Wire Wire Line
	1400 5300 1800 5300
Wire Wire Line
	1400 5400 1800 5400
Wire Wire Line
	1400 5500 1800 5500
Wire Wire Line
	1400 5600 1800 5600
Wire Wire Line
	1400 5700 1800 5700
Wire Wire Line
	1400 5800 1800 5800
Wire Wire Line
	1400 5900 1800 5900
Wire Wire Line
	1400 6000 1800 6000
Wire Wire Line
	1400 1750 1800 1750
Wire Wire Line
	1400 1850 1800 1850
Wire Wire Line
	1400 1950 1800 1950
Wire Wire Line
	1400 2050 1800 2050
Wire Wire Line
	1400 2150 1800 2150
Wire Wire Line
	1400 2250 1800 2250
Wire Wire Line
	1400 2350 1800 2350
Wire Wire Line
	1400 2450 1800 2450
Wire Wire Line
	1400 2550 1800 2550
Wire Wire Line
	1400 2650 1800 2650
Wire Wire Line
	1400 2750 1800 2750
Wire Wire Line
	1400 2850 1800 2850
Wire Wire Line
	1400 2950 1800 2950
Wire Wire Line
	1400 3050 1800 3050
Wire Wire Line
	1400 3150 1800 3150
Wire Wire Line
	1400 3250 1800 3250
Wire Wire Line
	1400 3350 1800 3350
Wire Wire Line
	1400 3450 1800 3450
Text Label 2700 1950 2    50   ~ 0
vs_exp1
Text Label 2700 2250 2    50   ~ 0
vs_exp2
Text Label 2700 2550 2    50   ~ 0
vs_byod1
Text Label 2700 2850 2    50   ~ 0
vs_byod2
Text Label 2700 3150 2    50   ~ 0
vs_12
Text Label 2700 3450 2    50   ~ 0
vs_switch
Text Label 2700 4300 2    50   ~ 0
vs_iris
Text Label 2700 4600 2    50   ~ 0
vs_b210_1
Text Label 2700 4900 2    50   ~ 0
vs_b210_2
Text Label 2700 5200 2    50   ~ 0
vs_b210_3
Text Label 2700 5500 2    50   ~ 0
vs_3_3
Wire Wire Line
	2300 1750 2800 1750
Wire Wire Line
	2300 2050 2800 2050
Wire Wire Line
	2300 2350 2800 2350
Wire Wire Line
	2300 2650 2800 2650
Wire Wire Line
	2300 2950 2800 2950
Wire Wire Line
	2300 3250 2800 3250
Wire Wire Line
	2300 4400 2800 4400
Wire Wire Line
	2300 4700 2800 4700
Wire Wire Line
	2300 5000 2800 5000
Wire Wire Line
	2300 5300 2800 5300
Text Label 2700 1850 2    50   ~ 0
is_exp1
Text Label 2700 2150 2    50   ~ 0
is_exp2
Text Label 2700 2450 2    50   ~ 0
is_byod1
Text Label 2700 2750 2    50   ~ 0
is_byod2
Text Label 2700 3050 2    50   ~ 0
is_12
Text Label 2700 3350 2    50   ~ 0
is_switch
Text Label 2700 4200 2    50   ~ 0
is_iris
Text Label 2700 4500 2    50   ~ 0
is_b210_1
Text Label 2700 5100 2    50   ~ 0
is_b210_3
Text Label 2700 4800 2    50   ~ 0
is_b210_2
Text Label 2700 5400 2    50   ~ 0
is_3_3
Wire Wire Line
	2300 5600 2800 5600
Text Label 2700 6000 2    50   ~ 0
is_pwr
Text Label 2700 5900 2    50   ~ 0
vs_pwr
Wire Wire Line
	1800 1550 1400 1550
Text Label 1400 1550 0    50   ~ 0
hp_p1
Text Label 1400 1750 0    50   ~ 0
hp_p3
Text Label 1400 1950 0    50   ~ 0
hp_p5
Text Label 1400 1650 0    50   ~ 0
hp_p2
Text Label 1400 1850 0    50   ~ 0
hp_p4
Text Label 1400 2050 0    50   ~ 0
hp_p6
Text Label 1400 2150 0    50   ~ 0
lp_p1
Text Label 1400 2350 0    50   ~ 0
lp_p3
Text Label 1400 2550 0    50   ~ 0
hp_p7
Text Label 1400 2250 0    50   ~ 0
lp_p2
Text Label 1400 2450 0    50   ~ 0
lp_p4
Text Label 1400 2650 0    50   ~ 0
lp_p5
Text Label 1400 2750 0    50   ~ 0
en_cnuc
Text Label 1400 2850 0    50   ~ 0
en_exp1
Text Label 1400 3050 0    50   ~ 0
en_byod1
Text Label 1400 2950 0    50   ~ 0
en_exp2
Text Label 1400 3150 0    50   ~ 0
en_byod2
Text Label 1400 3250 0    50   ~ 0
en_switch
Text Label 1400 3450 0    50   ~ 0
en_b210_1
Text Label 1400 4300 0    50   ~ 0
en_b210_3
Text Label 1400 3350 0    50   ~ 0
en_iris
Text Label 1400 4200 0    50   ~ 0
en_b210_2
Text Label 1400 4400 0    50   ~ 0
pg_bat
Text Label 1400 4600 0    50   ~ 0
pg_cnuc
Text Label 1400 4700 0    50   ~ 0
pg_exp1
Text Label 1400 4900 0    50   ~ 0
pg_byod1
Text Label 1400 4800 0    50   ~ 0
pg_exp2
Text Label 1400 5000 0    50   ~ 0
pg_byod2
Text Label 1400 4500 0    50   ~ 0
pg_12
Text Label 1400 5100 0    50   ~ 0
pg_b210_1
Text Label 1400 5300 0    50   ~ 0
pg_b210_3
Text Label 1400 5200 0    50   ~ 0
pg_b210_2
Text Label 1400 5400 0    50   ~ 0
pg_3_3
Text Label 1400 5500 0    50   ~ 0
bat_off
Text Label 1400 5700 0    50   ~ 0
bat_sclk
Text Label 1400 5900 0    50   ~ 0
bat_cs
Text Label 1400 5600 0    50   ~ 0
pg_pwr_3_3
Text Label 1400 5800 0    50   ~ 0
bat_mosi
Text Label 1400 6000 0    50   ~ 0
bat_miso
Wire Wire Line
	1300 6100 1800 6100
Wire Wire Line
	2300 6100 2800 6100
Wire Wire Line
	2800 4400 2800 4700
Connection ~ 2800 4700
Wire Wire Line
	2800 4700 2800 5000
Connection ~ 2800 5000
Wire Wire Line
	2800 5000 2800 5300
Connection ~ 2800 5300
Wire Wire Line
	2800 5300 2800 5600
Connection ~ 2800 5600
Connection ~ 2800 6100
Wire Wire Line
	2800 6100 2800 6300
Wire Wire Line
	1300 6100 1300 6300
Wire Wire Line
	2800 1750 2800 2050
Connection ~ 2800 2050
Wire Wire Line
	2800 2050 2800 2350
Connection ~ 2800 2350
Wire Wire Line
	2800 2350 2800 2650
Connection ~ 2800 2650
Wire Wire Line
	2800 2650 2800 2950
Connection ~ 2800 2950
Wire Wire Line
	2800 2950 2800 3250
Connection ~ 2800 3250
Wire Wire Line
	2800 3250 2800 3700
$Comp
L power:GND #PWR?
U 1 1 616752AA
P 2800 3700
F 0 "#PWR?" H 2800 3450 50  0001 C CNN
F 1 "GND" H 2805 3527 50  0000 C CNN
F 2 "" H 2800 3700 50  0001 C CNN
F 3 "" H 2800 3700 50  0001 C CNN
	1    2800 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 616752B0
P 2800 6300
F 0 "#PWR?" H 2800 6050 50  0001 C CNN
F 1 "GND" H 2805 6127 50  0000 C CNN
F 2 "" H 2800 6300 50  0001 C CNN
F 3 "" H 2800 6300 50  0001 C CNN
	1    2800 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 616752B6
P 1300 6300
F 0 "#PWR?" H 1300 6050 50  0001 C CNN
F 1 "GND" H 1305 6127 50  0000 C CNN
F 2 "" H 1300 6300 50  0001 C CNN
F 3 "" H 1300 6300 50  0001 C CNN
	1    1300 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 5600 2800 5700
Wire Wire Line
	2300 5800 2800 5800
Connection ~ 2800 5800
Wire Wire Line
	2800 5800 2800 6100
Wire Wire Line
	2300 5700 2800 5700
Connection ~ 2800 5700
Wire Wire Line
	2800 5700 2800 5800
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J3
U 1 1 61685B21
P 7300 2150
F 0 "J3" H 7350 2767 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 7350 2676 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 7300 2150 50  0001 C CNN
F 3 "~" H 7300 2150 50  0001 C CNN
	1    7300 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2050 7100 2050
Wire Wire Line
	6800 1950 7100 1950
Wire Wire Line
	6800 2250 7100 2250
Wire Wire Line
	6800 2150 7100 2150
Wire Wire Line
	6800 2450 7100 2450
Wire Wire Line
	6800 2350 7100 2350
Wire Wire Line
	6800 2650 7100 2650
Wire Wire Line
	6800 2550 7100 2550
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J5
U 1 1 616CA80A
P 9250 2150
F 0 "J5" H 9300 2767 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 9300 2676 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 9250 2150 50  0001 C CNN
F 3 "~" H 9250 2150 50  0001 C CNN
	1    9250 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 1850 9850 1850
Wire Wire Line
	9550 2050 9850 2050
Wire Wire Line
	9550 1950 9850 1950
Wire Wire Line
	9550 2250 9850 2250
Wire Wire Line
	9550 2150 9850 2150
Wire Wire Line
	9550 2450 9850 2450
Wire Wire Line
	9550 2350 9850 2350
Wire Wire Line
	9550 2650 9850 2650
Wire Wire Line
	9550 2550 9850 2550
Wire Wire Line
	8750 2550 9050 2550
Wire Wire Line
	8750 2650 9050 2650
Wire Wire Line
	8750 2350 9050 2350
Wire Wire Line
	8750 2450 9050 2450
Wire Wire Line
	8750 2150 9050 2150
Wire Wire Line
	8750 2250 9050 2250
Wire Wire Line
	8750 1950 9050 1950
Wire Wire Line
	8750 2050 9050 2050
Wire Wire Line
	8750 1750 9050 1750
Wire Wire Line
	8750 1850 9050 1850
$Comp
L POWDER_Analog:SN74CBTLV16292 U1
U 1 1 6170800F
P 4800 2950
F 0 "U1" H 5100 4600 50  0000 C CNN
F 1 "SN74CBTLV16292" V 4800 2950 50  0000 C CNN
F 2 "Package_SO:TSSOP-56_6.1x14mm_P0.5mm" H 4500 4600 50  0001 C CNN
F 3 "" H 4500 4600 50  0001 C CNN
	1    4800 2950
	-1   0    0    -1  
$EndComp
Text Label 3950 1900 0    50   ~ 0
vs_cnuc
Text Label 3950 1800 0    50   ~ 0
is_cnuc
Text Label 3950 2100 0    50   ~ 0
vs_exp1
Text Label 3950 2300 0    50   ~ 0
vs_exp2
Text Label 3950 2500 0    50   ~ 0
vs_byod1
Text Label 3950 2700 0    50   ~ 0
vs_byod2
Text Label 3950 2900 0    50   ~ 0
vs_12
Text Label 3950 3100 0    50   ~ 0
vs_switch
Text Label 3950 3300 0    50   ~ 0
vs_iris
Text Label 3950 3500 0    50   ~ 0
vs_b210_1
Text Label 3950 3700 0    50   ~ 0
vs_b210_2
Text Label 3950 3900 0    50   ~ 0
vs_b210_3
Text Label 3950 4100 0    50   ~ 0
vs_3_3
Text Label 3950 2000 0    50   ~ 0
is_exp1
Text Label 3950 2200 0    50   ~ 0
is_exp2
Text Label 3950 2400 0    50   ~ 0
is_byod1
Text Label 3950 2600 0    50   ~ 0
is_byod2
Text Label 3950 2800 0    50   ~ 0
is_12
Text Label 3950 3000 0    50   ~ 0
is_switch
Text Label 3950 3200 0    50   ~ 0
is_iris
Text Label 3950 3400 0    50   ~ 0
is_b210_1
Text Label 3950 3800 0    50   ~ 0
is_b210_3
Text Label 3950 3600 0    50   ~ 0
is_b210_2
Text Label 3950 4000 0    50   ~ 0
is_3_3
Text Label 4400 5450 0    50   ~ 0
is_pwr
Text Label 4400 5550 0    50   ~ 0
vs_pwr
Wire Wire Line
	4350 4100 3950 4100
Wire Wire Line
	4350 4000 3950 4000
Wire Wire Line
	4350 3900 3950 3900
Wire Wire Line
	4350 3800 3950 3800
Wire Wire Line
	4350 3700 3950 3700
Wire Wire Line
	4350 3600 3950 3600
Wire Wire Line
	4350 3500 3950 3500
Wire Wire Line
	4350 3400 3950 3400
Wire Wire Line
	4350 3300 3950 3300
Wire Wire Line
	4350 3200 3950 3200
Wire Wire Line
	4350 3100 3950 3100
Wire Wire Line
	4350 3000 3950 3000
Wire Wire Line
	4350 2900 3950 2900
Wire Wire Line
	4350 2800 3950 2800
Wire Wire Line
	4350 2700 3950 2700
Wire Wire Line
	4350 2600 3950 2600
Wire Wire Line
	4350 2500 3950 2500
Wire Wire Line
	4350 2400 3950 2400
Wire Wire Line
	4350 2300 3950 2300
Wire Wire Line
	4350 2200 3950 2200
Wire Wire Line
	4350 2100 3950 2100
Wire Wire Line
	4350 2000 3950 2000
Wire Wire Line
	4350 1900 3950 1900
Wire Wire Line
	4350 1800 3950 1800
Wire Wire Line
	5650 1850 5250 1850
Wire Wire Line
	5650 2050 5250 2050
Wire Wire Line
	5650 2250 5250 2250
Wire Wire Line
	5650 2450 5250 2450
Wire Wire Line
	5650 2650 5250 2650
Wire Wire Line
	5650 2850 5250 2850
Wire Wire Line
	5650 3050 5250 3050
Wire Wire Line
	5650 3250 5250 3250
Wire Wire Line
	5650 3450 5250 3450
Wire Wire Line
	5650 3650 5250 3650
Wire Wire Line
	5650 3850 5250 3850
Wire Wire Line
	5650 4050 5250 4050
Wire Wire Line
	5650 4250 5250 4250
Text Label 5650 1850 2    50   ~ 0
s_cnuc
Text Label 5650 2050 2    50   ~ 0
s_exp1
Text Label 5650 2250 2    50   ~ 0
s_exp2
Text Label 5650 2450 2    50   ~ 0
s_byod1
Text Label 5650 2650 2    50   ~ 0
s_byod2
Text Label 5650 2850 2    50   ~ 0
s_v12
Text Label 5650 3050 2    50   ~ 0
s_switch
Text Label 5650 3250 2    50   ~ 0
s_iris
Text Label 5650 3450 2    50   ~ 0
s_b210_1
Text Label 5650 3650 2    50   ~ 0
s_b210_2
Text Label 5650 3850 2    50   ~ 0
s_b210_3
Text Label 5650 4050 2    50   ~ 0
s_3v3
$Comp
L power:GND #PWR?
U 1 1 602D6322
P 4950 4850
F 0 "#PWR?" H 4950 4600 50  0001 C CNN
F 1 "GND" H 4955 4677 50  0000 C CNN
F 2 "" H 4950 4850 50  0001 C CNN
F 3 "" H 4950 4850 50  0001 C CNN
	1    4950 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4650 4650 4750
Wire Wire Line
	4650 4750 4750 4750
Wire Wire Line
	4950 4750 4950 4850
Wire Wire Line
	4950 4750 4950 4650
Connection ~ 4950 4750
Wire Wire Line
	4850 4650 4850 4750
Connection ~ 4850 4750
Wire Wire Line
	4850 4750 4950 4750
Wire Wire Line
	4750 4750 4750 4650
Connection ~ 4750 4750
Wire Wire Line
	4750 4750 4850 4750
Text Label 5650 4250 2    50   ~ 0
analog_sel
Text Label 6800 1850 0    50   ~ 0
AIN9
Text Label 6800 2350 0    50   ~ 0
AIN12
Text Label 6800 2250 0    50   ~ 0
AIN8
Text Label 9850 2350 2    50   ~ 0
AIN15
Text Label 9850 2250 2    50   ~ 0
AIN14
Text Label 4400 6750 0    50   ~ 0
s_cnuc
Text Label 4400 6650 0    50   ~ 0
s_exp1
Text Label 4400 6350 0    50   ~ 0
s_byod2
Text Label 4400 6250 0    50   ~ 0
s_v12
Text Label 4400 6150 0    50   ~ 0
s_switch
Text Label 4400 6050 0    50   ~ 0
s_iris
Text Label 4400 5850 0    50   ~ 0
s_b210_2
Text Label 4400 5750 0    50   ~ 0
s_b210_3
Text Label 4400 5650 0    50   ~ 0
s_3v3
Text Label 4400 6450 0    50   ~ 0
s_byod1
Text Label 4400 6550 0    50   ~ 0
s_exp2
Text Label 4400 5950 0    50   ~ 0
s_b210_1
Wire Wire Line
	4400 5450 5200 5450
Wire Wire Line
	4400 5550 5200 5550
Wire Wire Line
	4400 6750 5200 6750
Wire Wire Line
	4400 6650 5200 6650
Wire Wire Line
	4400 6450 5200 6450
Wire Wire Line
	4400 6350 5200 6350
Wire Wire Line
	4400 6250 5200 6250
Wire Wire Line
	4400 6150 5200 6150
Wire Wire Line
	4400 6050 5200 6050
Wire Wire Line
	4400 5850 5200 5850
Wire Wire Line
	4400 5750 5200 5750
Wire Wire Line
	4400 5650 5200 5650
Wire Wire Line
	4400 6550 5200 6550
Wire Wire Line
	4400 5950 5200 5950
Text Label 7900 2450 2    50   ~ 0
AIN5
Text Label 7900 2350 2    50   ~ 0
AIN4
Text Label 7900 2250 2    50   ~ 0
AIN0
Text Label 7900 2150 2    50   ~ 0
AIN1
Text Label 7900 2050 2    50   ~ 0
AIN2
Text Label 7900 1950 2    50   ~ 0
AIN3
Wire Wire Line
	7600 2650 7900 2650
Wire Wire Line
	7600 2450 7900 2450
Wire Wire Line
	7600 2550 7900 2550
Wire Wire Line
	7600 2250 7900 2250
Wire Wire Line
	7600 2350 7900 2350
Wire Wire Line
	7600 2050 7900 2050
Wire Wire Line
	7600 2150 7900 2150
Wire Wire Line
	7600 1950 7900 1950
Wire Wire Line
	8000 1850 8000 2850
Wire Wire Line
	7600 1850 8000 1850
$Comp
L power:GND #PWR?
U 1 1 6056A3F3
P 8000 2850
F 0 "#PWR?" H 8000 2600 50  0001 C CNN
F 1 "GND" H 8005 2677 50  0000 C CNN
F 2 "" H 8000 2850 50  0001 C CNN
F 3 "" H 8000 2850 50  0001 C CNN
	1    8000 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 1850 7100 1850
Text Label 7900 4350 2    50   ~ 0
AIN19
Text Label 7900 4250 2    50   ~ 0
AIN18
Text Label 7900 4150 2    50   ~ 0
AIN17
Text Label 7900 4050 2    50   ~ 0
AIN16
Text Label 7900 3950 2    50   ~ 0
AIN11
Text Label 7900 3850 2    50   ~ 0
AIN10
Text Label 6800 3750 0    50   ~ 0
AIN13
Wire Wire Line
	8750 4450 9050 4450
Wire Wire Line
	8750 4550 9050 4550
Wire Wire Line
	8750 4250 9050 4250
Wire Wire Line
	8750 4350 9050 4350
Wire Wire Line
	8750 4050 9050 4050
Wire Wire Line
	8750 4150 9050 4150
Wire Wire Line
	8750 3850 9050 3850
Wire Wire Line
	8750 3950 9050 3950
Wire Wire Line
	8750 3650 9050 3650
Wire Wire Line
	8750 3750 9050 3750
Wire Wire Line
	9550 4450 9850 4450
Wire Wire Line
	9550 4550 9850 4550
Wire Wire Line
	9550 4250 9850 4250
Wire Wire Line
	9550 4350 9850 4350
Wire Wire Line
	9550 4050 9850 4050
Wire Wire Line
	9550 4150 9850 4150
Wire Wire Line
	9550 3850 9850 3850
Wire Wire Line
	9550 3950 9850 3950
Wire Wire Line
	9550 3750 9850 3750
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J6
U 1 1 616D6C01
P 9250 4050
F 0 "J6" H 9300 4667 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 9300 4576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 9250 4050 50  0001 C CNN
F 3 "~" H 9250 4050 50  0001 C CNN
	1    9250 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4450 7100 4450
Wire Wire Line
	6800 4550 7100 4550
Wire Wire Line
	6800 4250 7100 4250
Wire Wire Line
	6800 4350 7100 4350
Wire Wire Line
	6800 4050 7100 4050
Wire Wire Line
	6800 4150 7100 4150
Wire Wire Line
	6800 3850 7100 3850
Wire Wire Line
	6800 3950 7100 3950
Wire Wire Line
	6800 3750 7100 3750
Wire Wire Line
	7600 4450 7900 4450
Wire Wire Line
	7600 4550 7900 4550
Wire Wire Line
	7600 4250 7900 4250
Wire Wire Line
	7600 4350 7900 4350
Wire Wire Line
	7600 4050 7900 4050
Wire Wire Line
	7600 4150 7900 4150
Wire Wire Line
	7600 3850 7900 3850
Wire Wire Line
	7600 3950 7900 3950
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J4
U 1 1 616D6BE7
P 7300 4050
F 0 "J4" H 7350 4667 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 7350 4576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 7300 4050 50  0001 C CNN
F 3 "~" H 7300 4050 50  0001 C CNN
	1    7300 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 3650 6700 3450
Wire Wire Line
	6700 3650 7100 3650
$Comp
L power:+3V3 #PWR?
U 1 1 60610AD6
P 6700 3450
F 0 "#PWR?" H 6700 3300 50  0001 C CNN
F 1 "+3V3" H 6715 3623 50  0000 C CNN
F 2 "" H 6700 3450 50  0001 C CNN
F 3 "" H 6700 3450 50  0001 C CNN
	1    6700 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 3750 8000 4750
Wire Wire Line
	7600 3750 8000 3750
$Comp
L power:GND #PWR?
U 1 1 6063C049
P 8000 4750
F 0 "#PWR?" H 8000 4500 50  0001 C CNN
F 1 "GND" H 8005 4577 50  0000 C CNN
F 2 "" H 8000 4750 50  0001 C CNN
F 3 "" H 8000 4750 50  0001 C CNN
	1    8000 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 1750 9950 2850
Wire Wire Line
	9550 1750 9950 1750
$Comp
L power:GND #PWR?
U 1 1 60653D82
P 9950 2850
F 0 "#PWR?" H 9950 2600 50  0001 C CNN
F 1 "GND" H 9955 2677 50  0000 C CNN
F 2 "" H 9950 2850 50  0001 C CNN
F 3 "" H 9950 2850 50  0001 C CNN
	1    9950 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 3650 9950 4750
Wire Wire Line
	9550 3650 9950 3650
$Comp
L power:GND #PWR?
U 1 1 60669904
P 9950 4750
F 0 "#PWR?" H 9950 4500 50  0001 C CNN
F 1 "GND" H 9955 4577 50  0000 C CNN
F 2 "" H 9950 4750 50  0001 C CNN
F 3 "" H 9950 4750 50  0001 C CNN
	1    9950 4750
	1    0    0    -1  
$EndComp
Text Label 9850 4250 2    50   ~ 0
MISO
Text Label 9850 4150 2    50   ~ 0
MOSI
Text Label 6800 4250 0    50   ~ 0
SCLK
Text Label 5200 5850 2    50   ~ 0
AIN0
Text Label 5200 5750 2    50   ~ 0
AIN1
Text Label 5200 5650 2    50   ~ 0
AIN2
Text Label 5200 5550 2    50   ~ 0
AIN3
Text Label 5200 6050 2    50   ~ 0
AIN4
Text Label 5200 6250 2    50   ~ 0
AIN5
Text Label 5200 5950 2    50   ~ 0
AIN8
Text Label 5200 5450 2    50   ~ 0
AIN9
Text Label 5200 6450 2    50   ~ 0
AIN10
Text Label 5200 6550 2    50   ~ 0
AIN11
Text Label 5200 6150 2    50   ~ 0
AIN12
Text Label 5200 6350 2    50   ~ 0
AIN13
Text Label 5200 6650 2    50   ~ 0
AIN16
Text Label 5200 6750 2    50   ~ 0
AIN17
$Comp
L power:+3V3 #PWR?
U 1 1 6067FF78
P 4800 1050
F 0 "#PWR?" H 4800 900 50  0001 C CNN
F 1 "+3V3" H 4815 1223 50  0000 C CNN
F 2 "" H 4800 1050 50  0001 C CNN
F 3 "" H 4800 1050 50  0001 C CNN
	1    4800 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1250 4800 1150
Wire Wire Line
	4800 1150 5350 1150
Connection ~ 4800 1150
Wire Wire Line
	4800 1150 4800 1050
Wire Wire Line
	4400 6950 5200 6950
Wire Wire Line
	5200 7050 4400 7050
Wire Wire Line
	4400 7150 5200 7150
Wire Wire Line
	4400 7250 5200 7250
Text Label 4400 6950 0    50   ~ 0
bat_sclk
Text Label 4400 7050 0    50   ~ 0
bat_mosi
Text Label 4400 7150 0    50   ~ 0
bat_cs
Text Label 4400 7250 0    50   ~ 0
bat_miso
Text Label 5200 6950 2    50   ~ 0
SCLK
Text Label 5200 7050 2    50   ~ 0
MOSI
Text Label 5200 7150 2    50   ~ 0
CS
Text Label 5200 7250 2    50   ~ 0
MISO
Text Label 7900 4550 0    50   ~ 0
analog_sel
Text Label 8750 4350 2    50   ~ 0
en_cnuc
Text Label 8750 4250 2    50   ~ 0
en_exp1
Text Label 8750 4050 2    50   ~ 0
en_byod1
Text Label 8750 4150 2    50   ~ 0
en_exp2
Text Label 8750 3950 2    50   ~ 0
en_byod2
Text Label 8750 3850 2    50   ~ 0
en_switch
Text Label 8750 3650 2    50   ~ 0
en_b210_1
Text Label 9850 2550 0    50   ~ 0
en_b210_3
Text Label 8750 3750 2    50   ~ 0
en_iris
Text Label 9850 2650 0    50   ~ 0
en_b210_2
Text Label 8750 2650 2    50   ~ 0
pg_bat
Text Label 8750 2450 2    50   ~ 0
pg_cnuc
Text Label 9850 2150 0    50   ~ 0
pg_byod1
Text Label 8750 2250 2    50   ~ 0
pg_exp2
Text Label 8750 2150 2    50   ~ 0
pg_byod2
Text Label 8750 2550 2    50   ~ 0
pg_12
Text Label 9850 2050 0    50   ~ 0
pg_b210_1
Text Label 9850 1950 0    50   ~ 0
pg_b210_3
Text Label 8750 2050 2    50   ~ 0
pg_b210_2
Text Label 8750 1950 2    50   ~ 0
pg_3_3
Text Label 9850 1850 0    50   ~ 0
bat_off
Text Label 8750 2350 2    50   ~ 0
pg_exp1
$Comp
L Device:C_Small C1
U 1 1 60356020
P 5350 1350
F 0 "C1" H 5442 1396 50  0000 L CNN
F 1 "0.1uF" H 5442 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5350 1350 50  0001 C CNN
F 3 "~" H 5350 1350 50  0001 C CNN
	1    5350 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60356401
P 5350 1550
F 0 "#PWR?" H 5350 1300 50  0001 C CNN
F 1 "GND" H 5355 1377 50  0000 C CNN
F 2 "" H 5350 1550 50  0001 C CNN
F 3 "" H 5350 1550 50  0001 C CNN
	1    5350 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1450 5350 1550
Wire Wire Line
	5350 1150 5350 1250
Text Label 8750 1750 2    50   ~ 0
CS
Text Label 8750 1850 2    50   ~ 0
pg_pwr_3_3
Wire Wire Line
	5600 6200 6000 6200
Wire Wire Line
	5600 6100 6000 6100
Wire Wire Line
	5600 6000 6000 6000
Wire Wire Line
	5600 5900 6000 5900
Wire Wire Line
	5600 5800 6000 5800
Wire Wire Line
	5600 5700 6000 5700
Wire Wire Line
	5600 5600 6000 5600
Wire Wire Line
	5600 5500 6000 5500
Wire Wire Line
	5600 5400 6000 5400
Wire Wire Line
	5600 5300 6000 5300
Wire Wire Line
	6000 6300 5600 6300
Text Label 5600 6300 0    50   ~ 0
hp_p1
Text Label 5600 6100 0    50   ~ 0
hp_p3
Text Label 5600 5900 0    50   ~ 0
hp_p5
Text Label 5600 6200 0    50   ~ 0
hp_p2
Text Label 5600 6000 0    50   ~ 0
hp_p4
Text Label 5600 5800 0    50   ~ 0
hp_p6
Text Label 5600 5700 0    50   ~ 0
lp_p1
Text Label 5600 5500 0    50   ~ 0
lp_p3
Text Label 5600 5300 0    50   ~ 0
hp_p7
Text Label 5600 5600 0    50   ~ 0
lp_p2
Text Label 5600 5400 0    50   ~ 0
lp_p4
Text Label 5600 5200 0    50   ~ 0
lp_p5
Wire Wire Line
	5600 5200 6000 5200
$Comp
L Connector_Generic:Conn_02x12_Odd_Even J7
U 1 1 604FF30B
P 6200 5700
F 0 "J7" H 6250 6417 50  0000 C CNN
F 1 "Conn_02x12_Odd_Even" H 6250 6326 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x12_P2.54mm_Vertical" H 6200 5700 50  0001 C CNN
F 3 "~" H 6200 5700 50  0001 C CNN
	1    6200 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5200 6600 5200
Wire Wire Line
	6600 5200 6600 5300
Wire Wire Line
	6500 5300 6600 5300
Connection ~ 6600 5300
Wire Wire Line
	6600 5300 6600 5400
Wire Wire Line
	6600 5400 6500 5400
Connection ~ 6600 5400
Wire Wire Line
	6600 5400 6600 5500
Wire Wire Line
	6500 5500 6600 5500
Connection ~ 6600 5500
Wire Wire Line
	6600 5500 6600 5600
Wire Wire Line
	6600 5600 6500 5600
Connection ~ 6600 5600
Wire Wire Line
	6600 5600 6600 5700
Wire Wire Line
	6500 5700 6600 5700
Connection ~ 6600 5700
Wire Wire Line
	6600 5700 6600 5800
Wire Wire Line
	6600 5800 6500 5800
Connection ~ 6600 5800
Wire Wire Line
	6600 5800 6600 5900
Wire Wire Line
	6500 5900 6600 5900
Connection ~ 6600 5900
Wire Wire Line
	6600 5900 6600 6000
Wire Wire Line
	6600 6000 6500 6000
Connection ~ 6600 6000
Wire Wire Line
	6600 6000 6600 6100
Wire Wire Line
	6500 6100 6600 6100
Connection ~ 6600 6100
Wire Wire Line
	6600 6100 6600 6200
Wire Wire Line
	6600 6200 6500 6200
Connection ~ 6600 6200
Wire Wire Line
	6600 6200 6600 6300
Wire Wire Line
	6500 6300 6600 6300
Connection ~ 6600 6300
Wire Wire Line
	6600 6300 6600 6500
$Comp
L power:GND #PWR?
U 1 1 606623CA
P 6600 6500
F 0 "#PWR?" H 6600 6250 50  0001 C CNN
F 1 "GND" H 6605 6327 50  0000 C CNN
F 2 "" H 6600 6500 50  0001 C CNN
F 3 "" H 6600 6500 50  0001 C CNN
	1    6600 6500
	1    0    0    -1  
$EndComp
NoConn ~ 7600 1750
NoConn ~ 7600 3650
NoConn ~ 7100 1750
NoConn ~ 6800 1950
NoConn ~ 6800 2050
NoConn ~ 6800 2150
NoConn ~ 6800 2450
NoConn ~ 6800 2550
NoConn ~ 6800 2650
NoConn ~ 7900 2550
NoConn ~ 7900 2650
NoConn ~ 9850 2250
NoConn ~ 9850 2350
NoConn ~ 9850 2450
NoConn ~ 9850 3750
NoConn ~ 9850 3850
NoConn ~ 9850 3950
NoConn ~ 9850 4050
NoConn ~ 8750 4450
NoConn ~ 8750 4550
NoConn ~ 9850 4350
NoConn ~ 9850 4450
NoConn ~ 9850 4550
NoConn ~ 7900 4450
NoConn ~ 7900 4350
NoConn ~ 7900 4250
NoConn ~ 6800 4350
NoConn ~ 6800 4450
NoConn ~ 6800 4550
NoConn ~ 6800 4150
NoConn ~ 6800 4050
NoConn ~ 6800 3950
NoConn ~ 6800 3850
$EndSCHEMATC
