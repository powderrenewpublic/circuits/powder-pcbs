EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L POWDER_Connector:Conn_02x70_Odd_Even J102
U 1 1 5F0CA06A
P 3050 4450
F 0 "J102" H 3100 8067 50  0000 C CNN
F 1 "Conn_02x70_Odd_Even" H 3100 7976 50  0000 C CNN
F 2 "POWDER_Connector:Amphenol_ICC_61082-141402LF" H 3050 4450 50  0001 C CNN
F 3 "" H 3050 4450 50  0001 C CNN
	1    3050 4450
	1    0    0    -1  
$EndComp
$Comp
L POWDER_Connector:Conn_02x70_Odd_Even J106
U 1 1 60DEAEDD
P 9550 4450
F 0 "J106" H 9600 8067 50  0000 C CNN
F 1 "Conn_02x70_Odd_Even" H 9600 7976 50  0000 C CNN
F 2 "POWDER_Connector:Amphenol_ICC_61082-141402LF" H 9550 4450 50  0001 C CNN
F 3 "" H 9550 4450 50  0001 C CNN
	1    9550 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 603D8A7A
P 2200 8050
AR Path="/6023C3F4/603D8A7A" Ref="#PWR?"  Part="1" 
AR Path="/603D8A7A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2200 7800 50  0001 C CNN
F 1 "GND" H 2200 7900 50  0000 C CNN
F 2 "" H 2200 8050 50  0001 C CNN
F 3 "" H 2200 8050 50  0001 C CNN
	1    2200 8050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2200 1150 2850 1150
Wire Wire Line
	2850 1350 2200 1350
Connection ~ 2200 1350
Wire Wire Line
	2200 1350 2200 1150
Wire Wire Line
	2200 1550 2850 1550
Connection ~ 2200 1550
Wire Wire Line
	2200 1550 2200 1350
Wire Wire Line
	2850 1750 2200 1750
Connection ~ 2200 1750
Wire Wire Line
	2200 1750 2200 1550
Wire Wire Line
	2200 1950 2850 1950
Connection ~ 2200 1950
Wire Wire Line
	2200 1950 2200 1750
Wire Wire Line
	2850 2750 2200 2750
Connection ~ 2200 2750
Wire Wire Line
	2200 2750 2200 2550
Wire Wire Line
	2850 2550 2200 2550
Connection ~ 2200 2550
Wire Wire Line
	2200 2550 2200 2350
Wire Wire Line
	2200 2350 2850 2350
Connection ~ 2200 2350
Wire Wire Line
	2200 2350 2200 2150
Wire Wire Line
	2850 2150 2200 2150
Connection ~ 2200 2150
Wire Wire Line
	2200 2150 2200 1950
Wire Wire Line
	2850 2850 2200 2850
Connection ~ 2200 2850
Wire Wire Line
	2200 2850 2200 2750
Wire Wire Line
	2850 5950 2200 5950
Connection ~ 2200 5950
Wire Wire Line
	2200 5950 2200 5750
Wire Wire Line
	2850 5850 2300 5850
Text Label 2400 5850 0    50   ~ 0
vs_pwr
Wire Wire Line
	2850 5750 2200 5750
Connection ~ 2200 5750
Wire Wire Line
	2200 5750 2200 5650
Wire Wire Line
	2850 5650 2200 5650
Connection ~ 2200 5650
Wire Wire Line
	2200 5650 2200 5550
Wire Wire Line
	2850 5550 2200 5550
Connection ~ 2200 5550
Wire Wire Line
	2850 2950 2300 2950
Text Label 2600 2950 0    50   ~ 0
hp_p1
Wire Wire Line
	2850 3050 2300 3050
Text Label 2600 3050 0    50   ~ 0
hp_p3
Wire Wire Line
	2850 3150 2300 3150
Text Label 2600 3150 0    50   ~ 0
hp_p5
Wire Wire Line
	2850 3250 2200 3250
Connection ~ 2200 3250
Wire Wire Line
	2200 3250 2200 2850
Wire Wire Line
	2850 3350 2300 3350
Text Label 2600 3350 0    50   ~ 0
lp_p1
Wire Wire Line
	2850 3450 2300 3450
Text Label 2600 3450 0    50   ~ 0
lp_p3
Wire Wire Line
	2850 3650 2200 3650
Connection ~ 2200 3650
Wire Wire Line
	2200 3650 2200 3250
Wire Wire Line
	2850 3750 2300 3750
Text Label 2400 3750 0    50   ~ 0
en_cnuc
Wire Wire Line
	2850 3850 2300 3850
Text Label 2400 3850 0    50   ~ 0
en_exp1
Wire Wire Line
	2850 3950 2300 3950
Text Label 2400 3950 0    50   ~ 0
en_byod1
Wire Wire Line
	2850 4050 2200 4050
Wire Wire Line
	2200 3650 2200 4050
Connection ~ 2200 4050
Wire Wire Line
	2200 4050 2200 4450
Wire Wire Line
	2850 4150 2300 4150
Text Label 2400 4150 0    50   ~ 0
en_switch
Wire Wire Line
	2850 4250 2300 4250
Text Label 2400 4250 0    50   ~ 0
en_b210_1
Wire Wire Line
	2850 4350 2300 4350
Text Label 2400 4350 0    50   ~ 0
en_b210_3
Wire Wire Line
	2850 4450 2200 4450
Connection ~ 2200 4450
Wire Wire Line
	2200 4450 2200 4850
Wire Wire Line
	2850 4550 2300 4550
Text Label 2400 4550 0    50   ~ 0
pg_cnuc
Wire Wire Line
	2850 4650 2300 4650
Text Label 2400 4650 0    50   ~ 0
pg_exp1
Wire Wire Line
	2850 4750 2300 4750
Text Label 2400 4750 0    50   ~ 0
pg_byod1
Wire Wire Line
	2850 4850 2200 4850
Connection ~ 2200 4850
Wire Wire Line
	2200 4850 2200 5150
Wire Wire Line
	2850 4950 2300 4950
Text Label 2400 4950 0    50   ~ 0
pg_b210_1
Wire Wire Line
	2850 5050 2300 5050
Text Label 2400 5050 0    50   ~ 0
pg_b210_3
Wire Wire Line
	2850 5150 2200 5150
Connection ~ 2200 5150
Wire Wire Line
	2850 5250 2300 5250
Text Label 2400 5250 0    50   ~ 0
bat_off
Wire Wire Line
	2850 5350 2300 5350
Text Label 2400 5350 0    50   ~ 0
bat_sclk
Wire Wire Line
	2200 5150 2200 5550
Wire Wire Line
	2850 5450 2300 5450
Text Label 2400 5450 0    50   ~ 0
bat_cs
Wire Wire Line
	2850 3550 2300 3550
Text Label 2600 3550 0    50   ~ 0
hp_p7
Wire Wire Line
	2850 7950 2200 7950
Wire Wire Line
	2200 7950 2200 8050
Connection ~ 2200 7950
Wire Wire Line
	2200 5950 2200 6850
Wire Wire Line
	2850 7850 2200 7850
Connection ~ 2200 7850
Wire Wire Line
	2200 7850 2200 7950
Wire Wire Line
	2200 6850 2850 6850
Connection ~ 2200 6850
Wire Wire Line
	2200 6850 2200 6950
Wire Wire Line
	2200 6950 2850 6950
Connection ~ 2200 6950
Wire Wire Line
	2200 6950 2200 7850
Wire Wire Line
	2300 6650 2850 6650
Wire Wire Line
	2300 6750 2850 6750
Wire Wire Line
	2300 6450 2850 6450
Wire Wire Line
	2300 6550 2850 6550
Wire Wire Line
	2300 6250 2850 6250
Wire Wire Line
	2300 6350 2850 6350
Wire Wire Line
	2300 6050 2850 6050
Wire Wire Line
	2300 6150 2850 6150
Text Label 2400 6050 0    50   ~ 0
gpio_0
Text Label 2400 6150 0    50   ~ 0
gpio_2
Text Label 2400 6250 0    50   ~ 0
gpio_4
Text Label 2400 6350 0    50   ~ 0
gpio_6
Text Label 2400 6450 0    50   ~ 0
gpio_8
Text Label 2400 6550 0    50   ~ 0
gpio_10
Text Label 2400 6650 0    50   ~ 0
gpio_12
Text Label 2400 6750 0    50   ~ 0
gpio_14
Wire Wire Line
	2300 7650 2850 7650
Wire Wire Line
	2300 7750 2850 7750
Wire Wire Line
	2300 7450 2850 7450
Wire Wire Line
	2300 7550 2850 7550
Wire Wire Line
	2300 7250 2850 7250
Wire Wire Line
	2300 7350 2850 7350
Wire Wire Line
	2300 7050 2850 7050
Wire Wire Line
	2300 7150 2850 7150
Text Label 2400 7050 0    50   ~ 0
gpio_16
Text Label 2400 7150 0    50   ~ 0
gpio_18
Text Label 2400 7250 0    50   ~ 0
gpio_20
Text Label 2400 7350 0    50   ~ 0
gpio_22
Text Label 2400 7450 0    50   ~ 0
gpio_24
Text Label 2400 7550 0    50   ~ 0
gpio_26
Text Label 2400 7650 0    50   ~ 0
gpio_28
Text Label 2400 7750 0    50   ~ 0
gpio_30
$Comp
L power:GND #PWR?
U 1 1 604D56AF
P 4000 8050
AR Path="/6023C3F4/604D56AF" Ref="#PWR?"  Part="1" 
AR Path="/604D56AF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4000 7800 50  0001 C CNN
F 1 "GND" H 4000 7900 50  0000 C CNN
F 2 "" H 4000 8050 50  0001 C CNN
F 3 "" H 4000 8050 50  0001 C CNN
	1    4000 8050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4000 1050 3350 1050
Wire Wire Line
	3350 1250 4000 1250
Connection ~ 4000 1250
Wire Wire Line
	4000 1250 4000 1050
Wire Wire Line
	4000 1450 3350 1450
Connection ~ 4000 1450
Wire Wire Line
	4000 1450 4000 1250
Wire Wire Line
	3350 1650 4000 1650
Connection ~ 4000 1650
Wire Wire Line
	4000 1650 4000 1450
Wire Wire Line
	3350 1850 4000 1850
Connection ~ 4000 1850
Wire Wire Line
	4000 1850 4000 1650
Wire Wire Line
	3350 2050 4000 2050
Connection ~ 4000 2050
Wire Wire Line
	4000 2050 4000 1850
Wire Wire Line
	4000 2250 3350 2250
Connection ~ 4000 2250
Wire Wire Line
	4000 2250 4000 2050
Wire Wire Line
	3350 2450 4000 2450
Connection ~ 4000 2450
Wire Wire Line
	4000 2450 4000 2250
Wire Wire Line
	4000 2650 3350 2650
Connection ~ 4000 2650
Wire Wire Line
	4000 2650 4000 2450
Wire Wire Line
	3350 2850 4000 2850
Connection ~ 4000 2850
Wire Wire Line
	4000 2850 4000 2650
Wire Wire Line
	3350 5950 4000 5950
Connection ~ 4000 5950
Wire Wire Line
	4000 5950 4000 5850
Wire Wire Line
	3350 5650 3650 5650
Text Label 3400 5650 0    50   ~ 0
is_pwr
Wire Wire Line
	3350 5850 4000 5850
Connection ~ 4000 5850
Wire Wire Line
	4000 5850 4000 5750
Wire Wire Line
	4000 5750 3350 5750
Connection ~ 4000 5750
Wire Wire Line
	4000 5750 4000 5550
Wire Wire Line
	3350 5550 4000 5550
Connection ~ 4000 5550
Wire Wire Line
	3900 2950 3350 2950
Text Label 3400 2950 0    50   ~ 0
hp_p2
Wire Wire Line
	3900 3050 3350 3050
Text Label 3400 3050 0    50   ~ 0
hp_p4
Wire Wire Line
	3900 3150 3350 3150
Text Label 3400 3150 0    50   ~ 0
hp_p6
Wire Wire Line
	3350 3250 4000 3250
Connection ~ 4000 3250
Wire Wire Line
	4000 3250 4000 2850
Wire Wire Line
	3900 3350 3350 3350
Text Label 3400 3350 0    50   ~ 0
lp_p2
Wire Wire Line
	3900 3450 3350 3450
Text Label 3400 3450 0    50   ~ 0
lp_p4
Wire Wire Line
	3900 3550 3350 3550
Wire Wire Line
	3350 3650 4000 3650
Connection ~ 4000 3650
Wire Wire Line
	4000 3650 4000 3250
Wire Wire Line
	3900 3850 3350 3850
Text Label 3400 3850 0    50   ~ 0
en_exp2
Wire Wire Line
	3900 3950 3350 3950
Text Label 3400 3950 0    50   ~ 0
en_byod2
Wire Wire Line
	3350 4050 4000 4050
Connection ~ 4000 4050
Wire Wire Line
	4000 4050 4000 3750
Wire Wire Line
	3900 4150 3350 4150
Text Label 3400 4150 0    50   ~ 0
en_iris
Wire Wire Line
	3900 4250 3350 4250
Text Label 3400 4250 0    50   ~ 0
en_b210_2
Wire Wire Line
	3350 4450 4000 4450
Connection ~ 4000 4450
Wire Wire Line
	4000 3750 3350 3750
Connection ~ 4000 3750
Wire Wire Line
	4000 3750 4000 3650
Wire Wire Line
	3900 4650 3350 4650
Text Label 3400 4650 0    50   ~ 0
pg_exp2
Wire Wire Line
	3900 4750 3350 4750
Text Label 3400 4750 0    50   ~ 0
pg_byod2
Wire Wire Line
	3350 4550 3900 4550
Text Label 3400 4550 0    50   ~ 0
pg_12
Wire Wire Line
	3350 4850 4000 4850
Connection ~ 4000 4850
Wire Wire Line
	4000 4850 4000 4450
Wire Wire Line
	3900 5050 3350 5050
Wire Wire Line
	3900 4950 3350 4950
Text Label 3400 4950 0    50   ~ 0
pg_b210_2
Text Label 3400 5050 0    50   ~ 0
pg_3_3
Wire Wire Line
	3350 5150 4000 5150
Connection ~ 4000 5150
Wire Wire Line
	4000 5150 4000 4850
Text Label 3400 5250 0    50   ~ 0
pg_pwr_3_3
Wire Wire Line
	3350 5350 3900 5350
Text Label 3400 5350 0    50   ~ 0
bat_mosi
Wire Wire Line
	4000 5150 4000 5550
Wire Wire Line
	3350 5450 3900 5450
Text Label 3400 5450 0    50   ~ 0
bat_miso
Wire Wire Line
	4000 4050 4000 4450
Wire Wire Line
	3350 4350 3900 4350
Text Label 3400 4350 0    50   ~ 0
pg_bat
Text Label 3400 3550 0    50   ~ 0
lp_p5
Wire Wire Line
	3350 5250 3900 5250
Wire Wire Line
	3350 7950 4000 7950
Wire Wire Line
	4000 7950 4000 8050
Connection ~ 4000 7950
Wire Wire Line
	4000 5950 4000 6850
Wire Wire Line
	3350 7850 4000 7850
Connection ~ 4000 7850
Wire Wire Line
	4000 7850 4000 7950
Wire Wire Line
	3350 6850 4000 6850
Connection ~ 4000 6850
Wire Wire Line
	4000 6850 4000 6950
Wire Wire Line
	3350 6950 4000 6950
Connection ~ 4000 6950
Wire Wire Line
	4000 6950 4000 7850
Wire Wire Line
	3350 6650 3900 6650
Wire Wire Line
	3350 6750 3900 6750
Wire Wire Line
	3350 6450 3900 6450
Wire Wire Line
	3350 6550 3900 6550
Wire Wire Line
	3350 6250 3900 6250
Wire Wire Line
	3350 6350 3900 6350
Wire Wire Line
	3350 6050 3900 6050
Wire Wire Line
	3350 6150 3900 6150
Text Label 3400 6050 0    50   ~ 0
gpio_1
Text Label 3400 6150 0    50   ~ 0
gpio_3
Text Label 3400 6250 0    50   ~ 0
gpio_5
Text Label 3400 6350 0    50   ~ 0
gpio_7
Text Label 3400 6450 0    50   ~ 0
gpio_9
Text Label 3400 6550 0    50   ~ 0
gpio_11
Text Label 3400 6650 0    50   ~ 0
gpio_13
Text Label 3400 6750 0    50   ~ 0
gpio_15
Wire Wire Line
	3350 7650 3900 7650
Wire Wire Line
	3350 7750 3900 7750
Wire Wire Line
	3350 7450 3900 7450
Wire Wire Line
	3350 7550 3900 7550
Wire Wire Line
	3350 7250 3900 7250
Wire Wire Line
	3350 7350 3900 7350
Wire Wire Line
	3350 7050 3900 7050
Wire Wire Line
	3350 7150 3900 7150
Text Label 3400 7050 0    50   ~ 0
gpio_17
Text Label 3400 7150 0    50   ~ 0
gpio_19
Text Label 3400 7250 0    50   ~ 0
gpio_21
Text Label 3400 7350 0    50   ~ 0
gpio_23
Text Label 3400 7450 0    50   ~ 0
gpio_25
Text Label 3400 7550 0    50   ~ 0
gpio_27
Text Label 3400 7650 0    50   ~ 0
gpio_29
Text Label 3400 7750 0    50   ~ 0
gpio_31
$Comp
L power:GND #PWR?
U 1 1 60543020
P 8850 8050
F 0 "#PWR?" H 8850 7800 50  0001 C CNN
F 1 "GND" H 8850 7900 50  0000 C CNN
F 2 "" H 8850 8050 50  0001 C CNN
F 3 "" H 8850 8050 50  0001 C CNN
	1    8850 8050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8850 1050 9350 1050
Wire Wire Line
	9350 1150 8850 1150
Connection ~ 8850 1150
Wire Wire Line
	8850 1150 8850 1050
Wire Wire Line
	9350 1250 8850 1250
Connection ~ 8850 1250
Wire Wire Line
	8850 1250 8850 1150
$Comp
L power:GND #PWR?
U 1 1 6054302D
P 10350 8050
F 0 "#PWR?" H 10350 7800 50  0001 C CNN
F 1 "GND" H 10350 7900 50  0000 C CNN
F 2 "" H 10350 8050 50  0001 C CNN
F 3 "" H 10350 8050 50  0001 C CNN
	1    10350 8050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10350 1050 9850 1050
Wire Wire Line
	9850 1150 10100 1150
Text Label 9950 1150 0    50   ~ 0
is_cnuc
Wire Wire Line
	9850 1250 10350 1250
Connection ~ 10350 1250
Wire Wire Line
	10350 1250 10350 1050
Wire Wire Line
	9350 1350 8350 1350
Text Label 8350 1350 0    50   ~ 0
vs_cnuc
Wire Wire Line
	9850 1350 10350 1350
Connection ~ 10350 1350
Wire Wire Line
	10350 1350 10350 1250
Wire Wire Line
	9350 1450 8850 1450
Connection ~ 8850 1450
Wire Wire Line
	8850 1450 8850 1250
Wire Wire Line
	9850 1450 10350 1450
Connection ~ 10350 1450
Wire Wire Line
	10350 1450 10350 1350
Wire Wire Line
	9850 1550 10100 1550
Text Label 9950 1550 0    50   ~ 0
is_exp1
Wire Wire Line
	9350 1550 8850 1550
Connection ~ 8850 1550
Wire Wire Line
	8850 1550 8850 1450
Wire Wire Line
	9850 1650 10350 1650
Connection ~ 10350 1650
Wire Wire Line
	10350 1650 10350 1450
Wire Wire Line
	9850 1750 10350 1750
Connection ~ 10350 1750
Wire Wire Line
	10350 1750 10350 1650
Wire Wire Line
	9350 1750 8350 1750
Text Label 8350 1750 0    50   ~ 0
vs_exp1
Wire Wire Line
	9350 1650 8850 1650
Connection ~ 8850 1650
Wire Wire Line
	8850 1650 8850 1550
Wire Wire Line
	8850 1850 9350 1850
Connection ~ 8850 1850
Wire Wire Line
	8850 1850 8850 1650
Wire Wire Line
	9850 1850 10350 1850
Connection ~ 10350 1850
Wire Wire Line
	10350 1850 10350 1750
Wire Wire Line
	9850 1950 10100 1950
Text Label 9950 1950 0    50   ~ 0
is_exp2
Wire Wire Line
	9350 1950 8850 1950
Connection ~ 8850 1950
Wire Wire Line
	8850 1950 8850 1850
Wire Wire Line
	9850 2050 10350 2050
Connection ~ 10350 2050
Wire Wire Line
	10350 2050 10350 1850
Wire Wire Line
	9350 2150 8350 2150
Text Label 8350 2150 0    50   ~ 0
vs_exp2
Wire Wire Line
	9350 2050 8850 2050
Connection ~ 8850 2050
Wire Wire Line
	8850 2050 8850 1950
Wire Wire Line
	8850 2250 9350 2250
Connection ~ 8850 2250
Wire Wire Line
	8850 2250 8850 2050
Wire Wire Line
	9850 2150 10350 2150
Connection ~ 10350 2150
Wire Wire Line
	10350 2150 10350 2050
Wire Wire Line
	10350 2250 9850 2250
Wire Wire Line
	10350 2250 10350 2150
Wire Wire Line
	9850 2350 10100 2350
Text Label 9950 2350 0    50   ~ 0
is_byod1
Wire Wire Line
	9350 2350 8850 2350
Connection ~ 8850 2350
Wire Wire Line
	8850 2350 8850 2250
Wire Wire Line
	8350 2550 9350 2550
Text Label 8350 2550 0    50   ~ 0
vs_byod1
Wire Wire Line
	9850 2750 10100 2750
Text Label 9950 2750 0    50   ~ 0
is_byod2
Wire Wire Line
	9350 2950 8350 2950
Text Label 8350 2950 0    50   ~ 0
vs_byod2
Text Label 9950 3150 0    50   ~ 0
is_12
Wire Wire Line
	9350 3350 8350 3350
Text Label 8350 3350 0    50   ~ 0
vs_12
Wire Wire Line
	9850 3550 10100 3550
Text Label 9950 3550 0    50   ~ 0
is_switch
Text Label 9950 3950 0    50   ~ 0
is_iris
Wire Wire Line
	10100 4350 9850 4350
Wire Wire Line
	10100 4750 9850 4750
Wire Wire Line
	10100 5150 9850 5150
Text Label 9950 4350 0    50   ~ 0
is_b210_1
Text Label 9950 4750 0    50   ~ 0
is_b210_2
Text Label 9950 5150 0    50   ~ 0
is_b210_3
Text Label 9950 5550 0    50   ~ 0
is_3_3
Wire Wire Line
	9350 3750 8350 3750
Wire Wire Line
	9350 4150 8350 4150
Wire Wire Line
	9350 4550 8350 4550
Wire Wire Line
	9350 4950 8350 4950
Wire Wire Line
	9350 5350 8350 5350
Wire Wire Line
	9350 5750 8350 5750
Text Label 8350 3750 0    50   ~ 0
vs_switch
Text Label 8350 4150 0    50   ~ 0
vs_iris
Text Label 8350 4550 0    50   ~ 0
vs_b210_1
Text Label 8350 4950 0    50   ~ 0
vs_b210_2
Text Label 8350 5350 0    50   ~ 0
vs_b210_3
Text Label 8350 5750 0    50   ~ 0
vs_3_3
Wire Wire Line
	9350 2450 8850 2450
Connection ~ 8850 2450
Wire Wire Line
	8850 2450 8850 2350
Wire Wire Line
	8850 2650 9350 2650
Connection ~ 8850 2650
Wire Wire Line
	8850 2650 8850 2450
Wire Wire Line
	9350 2750 8850 2750
Connection ~ 8850 2750
Wire Wire Line
	8850 2750 8850 2650
Wire Wire Line
	9350 2850 8850 2850
Connection ~ 8850 2850
Wire Wire Line
	8850 2850 8850 2750
Wire Wire Line
	8850 3050 9350 3050
Connection ~ 8850 3050
Wire Wire Line
	8850 3050 8850 2850
Wire Wire Line
	9350 3150 8850 3150
Connection ~ 8850 3150
Wire Wire Line
	8850 3150 8850 3050
Wire Wire Line
	8850 3250 9350 3250
Wire Wire Line
	8850 3250 8850 3150
Wire Wire Line
	8850 3450 9350 3450
Wire Wire Line
	9350 3550 8850 3550
Wire Wire Line
	8850 3650 9350 3650
Wire Wire Line
	8850 3850 9350 3850
Wire Wire Line
	9350 3950 8850 3950
Wire Wire Line
	8850 4050 9350 4050
Wire Wire Line
	8850 4250 9350 4250
Wire Wire Line
	9350 4350 8850 4350
Wire Wire Line
	8850 4450 9350 4450
Wire Wire Line
	8850 4650 9350 4650
Wire Wire Line
	9350 4750 8850 4750
Wire Wire Line
	8850 4850 9350 4850
Wire Wire Line
	8850 5050 9350 5050
Wire Wire Line
	9350 5150 8850 5150
Wire Wire Line
	8850 5250 9350 5250
Wire Wire Line
	8850 5450 9350 5450
Wire Wire Line
	9350 5550 8850 5550
Wire Wire Line
	8850 5650 9350 5650
Wire Wire Line
	9850 5650 10350 5650
Wire Wire Line
	9850 5750 10350 5750
Connection ~ 10350 5750
Wire Wire Line
	10350 5750 10350 5650
Wire Wire Line
	9850 5850 10350 5850
Connection ~ 10350 5850
Wire Wire Line
	10350 5850 10350 5750
Wire Wire Line
	9850 5250 10350 5250
Wire Wire Line
	9850 5350 10350 5350
Wire Wire Line
	9850 5450 10350 5450
Wire Wire Line
	9850 4850 10350 4850
Wire Wire Line
	9850 4950 10350 4950
Wire Wire Line
	9850 5050 10350 5050
Wire Wire Line
	9850 4450 10350 4450
Wire Wire Line
	9850 4550 10350 4550
Wire Wire Line
	9850 4650 10350 4650
Wire Wire Line
	9850 4050 10350 4050
Wire Wire Line
	9850 4150 10350 4150
Wire Wire Line
	9850 4250 10350 4250
Wire Wire Line
	9850 3650 10350 3650
Wire Wire Line
	9850 3750 10350 3750
Wire Wire Line
	9850 3850 10350 3850
Wire Wire Line
	9850 3250 10350 3250
Wire Wire Line
	9850 3350 10350 3350
Wire Wire Line
	9850 3450 10350 3450
Wire Wire Line
	9850 2850 10350 2850
Wire Wire Line
	9850 2950 10350 2950
Wire Wire Line
	9850 3050 10350 3050
Wire Wire Line
	9850 2450 10350 2450
Wire Wire Line
	9850 2550 10350 2550
Wire Wire Line
	9850 2650 10350 2650
Wire Wire Line
	8850 3250 8850 3450
Connection ~ 8850 3250
Connection ~ 8850 3450
Wire Wire Line
	8850 3450 8850 3550
Connection ~ 8850 3550
Wire Wire Line
	8850 3550 8850 3650
Connection ~ 8850 3650
Wire Wire Line
	8850 3650 8850 3850
Connection ~ 8850 3850
Wire Wire Line
	8850 3850 8850 3950
Connection ~ 8850 3950
Wire Wire Line
	8850 3950 8850 4050
Connection ~ 8850 4050
Wire Wire Line
	8850 4050 8850 4250
Connection ~ 8850 4250
Wire Wire Line
	8850 4250 8850 4350
Connection ~ 8850 4350
Wire Wire Line
	8850 4350 8850 4450
Connection ~ 8850 4450
Wire Wire Line
	8850 4450 8850 4650
Connection ~ 8850 4650
Wire Wire Line
	8850 4650 8850 4750
Connection ~ 8850 4750
Wire Wire Line
	8850 4750 8850 4850
Connection ~ 8850 4850
Wire Wire Line
	8850 4850 8850 5050
Connection ~ 8850 5050
Wire Wire Line
	8850 5050 8850 5150
Connection ~ 8850 5150
Wire Wire Line
	8850 5150 8850 5250
Connection ~ 8850 5250
Wire Wire Line
	8850 5250 8850 5450
Connection ~ 8850 5450
Wire Wire Line
	8850 5450 8850 5550
Connection ~ 8850 5550
Wire Wire Line
	8850 5550 8850 5650
Connection ~ 8850 5650
Wire Wire Line
	8850 5650 8850 5850
Wire Wire Line
	10350 2250 10350 2450
Connection ~ 10350 2250
Connection ~ 10350 5650
Connection ~ 10350 2450
Wire Wire Line
	10350 2450 10350 2550
Connection ~ 10350 2550
Wire Wire Line
	10350 2550 10350 2650
Connection ~ 10350 2650
Wire Wire Line
	10350 2650 10350 2850
Connection ~ 10350 2850
Wire Wire Line
	10350 2850 10350 2950
Connection ~ 10350 2950
Wire Wire Line
	10350 2950 10350 3050
Connection ~ 10350 3050
Wire Wire Line
	10350 3050 10350 3250
Connection ~ 10350 3250
Wire Wire Line
	10350 3250 10350 3350
Connection ~ 10350 3350
Wire Wire Line
	10350 3350 10350 3450
Connection ~ 10350 3450
Wire Wire Line
	10350 3450 10350 3650
Connection ~ 10350 3650
Wire Wire Line
	10350 3650 10350 3750
Connection ~ 10350 3750
Wire Wire Line
	10350 3750 10350 3850
Connection ~ 10350 3850
Wire Wire Line
	10350 3850 10350 4050
Connection ~ 10350 4050
Wire Wire Line
	10350 4050 10350 4150
Connection ~ 10350 4150
Wire Wire Line
	10350 4150 10350 4250
Connection ~ 10350 4250
Wire Wire Line
	10350 4250 10350 4450
Connection ~ 10350 4450
Wire Wire Line
	10350 4450 10350 4550
Connection ~ 10350 4550
Wire Wire Line
	10350 4550 10350 4650
Connection ~ 10350 4650
Wire Wire Line
	10350 4650 10350 4850
Connection ~ 10350 4850
Wire Wire Line
	10350 4850 10350 4950
Connection ~ 10350 4950
Wire Wire Line
	10350 4950 10350 5050
Connection ~ 10350 5050
Wire Wire Line
	10350 5050 10350 5250
Connection ~ 10350 5250
Wire Wire Line
	10350 5250 10350 5350
Connection ~ 10350 5350
Wire Wire Line
	10350 5350 10350 5450
Connection ~ 10350 5450
Wire Wire Line
	10350 5450 10350 5650
Wire Wire Line
	9850 5950 10350 5950
Connection ~ 10350 5950
Wire Wire Line
	10350 5950 10350 5850
Wire Wire Line
	9350 5850 8850 5850
Connection ~ 8850 5850
Wire Wire Line
	8850 5850 8850 5950
Wire Wire Line
	8850 5950 9350 5950
Connection ~ 8850 5950
Wire Wire Line
	10100 5550 9850 5550
Wire Wire Line
	10100 3150 9850 3150
Wire Wire Line
	10100 3950 9850 3950
Wire Wire Line
	10350 5950 10350 6150
Wire Wire Line
	8850 5950 8850 6050
Wire Wire Line
	9350 7950 8850 7950
Connection ~ 8850 7950
Wire Wire Line
	8850 7950 8850 8050
Wire Wire Line
	9350 7850 8850 7850
Connection ~ 8850 7850
Wire Wire Line
	8850 7850 8850 7950
Wire Wire Line
	9850 7850 10350 7850
Connection ~ 10350 7850
Wire Wire Line
	10350 7850 10350 7950
Wire Wire Line
	9850 7950 10350 7950
Connection ~ 10350 7950
Wire Wire Line
	10350 7950 10350 8050
Wire Wire Line
	9850 6850 10350 6850
Wire Wire Line
	9850 6950 10350 6950
Wire Wire Line
	8850 6850 9350 6850
Wire Wire Line
	8850 6950 9350 6950
Connection ~ 8850 6850
Wire Wire Line
	8850 6850 8850 6950
Connection ~ 8850 6950
Wire Wire Line
	8850 6950 8850 7050
Connection ~ 10350 6950
Wire Wire Line
	10350 6950 10350 7150
Connection ~ 10350 6850
Wire Wire Line
	10350 6850 10350 6950
Wire Wire Line
	10100 7650 9850 7650
Wire Wire Line
	10100 7450 9850 7450
Wire Wire Line
	10100 7250 9850 7250
Wire Wire Line
	10100 7050 9850 7050
Wire Wire Line
	9350 7550 9100 7550
Wire Wire Line
	9350 7750 9100 7750
Wire Wire Line
	9350 7350 9100 7350
Wire Wire Line
	9350 7150 9100 7150
Wire Wire Line
	9350 6550 9100 6550
Wire Wire Line
	9350 6750 9100 6750
Wire Wire Line
	9350 6350 9100 6350
Wire Wire Line
	9350 6150 9100 6150
Wire Wire Line
	10100 6650 9850 6650
Wire Wire Line
	10100 6450 9850 6450
Wire Wire Line
	10100 6250 9850 6250
Wire Wire Line
	10100 6050 9850 6050
Text Label 9950 6050 0    50   ~ 0
a0
Text Label 9100 6150 0    50   ~ 0
a1
Text Label 9950 6250 0    50   ~ 0
a2
Text Label 9950 6450 0    50   ~ 0
a4
Text Label 9950 6650 0    50   ~ 0
a6
Text Label 9950 7050 0    50   ~ 0
a8
Text Label 9950 7250 0    50   ~ 0
a10
Text Label 9950 7450 0    50   ~ 0
a12
Text Label 9950 7650 0    50   ~ 0
a14
Text Label 9100 6350 0    50   ~ 0
a3
Text Label 9100 6550 0    50   ~ 0
a5
Text Label 9100 6750 0    50   ~ 0
a7
Text Label 9100 7150 0    50   ~ 0
a9
Text Label 9100 7350 0    50   ~ 0
a11
Text Label 9100 7550 0    50   ~ 0
a13
Text Label 9100 7750 0    50   ~ 0
a15
Wire Wire Line
	8850 6050 9350 6050
Connection ~ 8850 6050
Connection ~ 8850 6450
Wire Wire Line
	8850 6450 8850 6650
Wire Wire Line
	8850 6450 9350 6450
Wire Wire Line
	8850 6650 9350 6650
Wire Wire Line
	9850 6750 10350 6750
Wire Wire Line
	9850 6550 10350 6550
Wire Wire Line
	9850 6350 10350 6350
Wire Wire Line
	9850 6150 10350 6150
Connection ~ 10350 6350
Wire Wire Line
	10350 6350 10350 6550
Connection ~ 10350 6550
Wire Wire Line
	10350 6550 10350 6750
Connection ~ 10350 6750
Wire Wire Line
	10350 6750 10350 6850
Connection ~ 10350 6150
Wire Wire Line
	10350 6150 10350 6350
Connection ~ 8850 6650
Wire Wire Line
	8850 6650 8850 6850
Connection ~ 8850 6250
Wire Wire Line
	8850 6250 8850 6450
Wire Wire Line
	8850 6250 9350 6250
Wire Wire Line
	8850 6050 8850 6250
Wire Wire Line
	8850 7650 9350 7650
Wire Wire Line
	8850 7450 9350 7450
Wire Wire Line
	8850 7250 9350 7250
Wire Wire Line
	8850 7050 9350 7050
Wire Wire Line
	9850 7550 10350 7550
Wire Wire Line
	9850 7750 10350 7750
Wire Wire Line
	9850 7350 10350 7350
Wire Wire Line
	9850 7150 10350 7150
Connection ~ 10350 7750
Wire Wire Line
	10350 7750 10350 7850
Connection ~ 10350 7550
Wire Wire Line
	10350 7550 10350 7750
Connection ~ 10350 7350
Wire Wire Line
	10350 7350 10350 7550
Connection ~ 10350 7150
Wire Wire Line
	10350 7150 10350 7350
Connection ~ 8850 7050
Wire Wire Line
	8850 7050 8850 7250
Connection ~ 8850 7250
Wire Wire Line
	8850 7250 8850 7450
Connection ~ 8850 7450
Wire Wire Line
	8850 7450 8850 7650
Connection ~ 8850 7650
Wire Wire Line
	8850 7650 8850 7850
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 60577FD6
P 6200 2150
F 0 "J1" H 6250 3267 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 6250 3176 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x20_P1.27mm_Vertical_SMD" H 6200 2150 50  0001 C CNN
F 3 "~" H 6200 2150 50  0001 C CNN
	1    6200 2150
	-1   0    0    -1  
$EndComp
Text Notes 4350 3700 0    50   ~ 0
39 digital signals
Text Notes 4350 3950 0    50   ~ 0
26 analog signals
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J2
U 1 1 601FC6C4
P 6200 4800
F 0 "J2" H 6250 5917 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 6250 5826 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x20_P1.27mm_Vertical_SMD" H 6200 4800 50  0001 C CNN
F 3 "~" H 6200 4800 50  0001 C CNN
	1    6200 4800
	-1   0    0    -1  
$EndComp
Text Notes 4300 4200 0    50   ~ 0
15 gnds
Wire Wire Line
	6400 1250 6800 1250
Text Label 6800 1350 2    50   ~ 0
vs_cnuc
Wire Wire Line
	6400 1350 6800 1350
Text Label 6800 1250 2    50   ~ 0
is_cnuc
Wire Wire Line
	5500 1350 5900 1350
Wire Wire Line
	6400 1550 6800 1550
Wire Wire Line
	6400 1650 6800 1650
Wire Wire Line
	6400 1850 6800 1850
Wire Wire Line
	6400 1950 6800 1950
Wire Wire Line
	6400 2150 6800 2150
Wire Wire Line
	6400 2250 6800 2250
Wire Wire Line
	6400 2450 6800 2450
Wire Wire Line
	6400 2550 6800 2550
Wire Wire Line
	6400 2750 6800 2750
Wire Wire Line
	6400 2850 6800 2850
Wire Wire Line
	6400 3050 6800 3050
Wire Wire Line
	6400 3150 6800 3150
Wire Wire Line
	6400 3900 6800 3900
Wire Wire Line
	6400 4000 6800 4000
Wire Wire Line
	6400 4200 6800 4200
Wire Wire Line
	6400 4300 6800 4300
Wire Wire Line
	6400 4500 6800 4500
Wire Wire Line
	6400 4600 6800 4600
Wire Wire Line
	6400 4800 6800 4800
Wire Wire Line
	6400 4900 6800 4900
Wire Wire Line
	6400 5100 6800 5100
Wire Wire Line
	6400 5200 6800 5200
Wire Wire Line
	6400 5600 6800 5600
Wire Wire Line
	6400 5700 6800 5700
Wire Wire Line
	5500 3900 5900 3900
Wire Wire Line
	5500 4000 5900 4000
Wire Wire Line
	5500 4100 5900 4100
Wire Wire Line
	5500 4200 5900 4200
Wire Wire Line
	5500 4300 5900 4300
Wire Wire Line
	5500 4400 5900 4400
Wire Wire Line
	5500 4500 5900 4500
Wire Wire Line
	5500 4600 5900 4600
Wire Wire Line
	5500 4700 5900 4700
Wire Wire Line
	5500 4800 5900 4800
Wire Wire Line
	5500 4900 5900 4900
Wire Wire Line
	5500 5000 5900 5000
Wire Wire Line
	5500 5100 5900 5100
Wire Wire Line
	5500 5200 5900 5200
Wire Wire Line
	5500 5300 5900 5300
Wire Wire Line
	5500 5400 5900 5400
Wire Wire Line
	5500 5500 5900 5500
Wire Wire Line
	5500 5600 5900 5600
Wire Wire Line
	5500 5700 5900 5700
Wire Wire Line
	5500 1450 5900 1450
Wire Wire Line
	5500 1550 5900 1550
Wire Wire Line
	5500 1650 5900 1650
Wire Wire Line
	5500 1750 5900 1750
Wire Wire Line
	5500 1850 5900 1850
Wire Wire Line
	5500 1950 5900 1950
Wire Wire Line
	5500 2050 5900 2050
Wire Wire Line
	5500 2150 5900 2150
Wire Wire Line
	5500 2250 5900 2250
Wire Wire Line
	5500 2350 5900 2350
Wire Wire Line
	5500 2450 5900 2450
Wire Wire Line
	5500 2550 5900 2550
Wire Wire Line
	5500 2650 5900 2650
Wire Wire Line
	5500 2750 5900 2750
Wire Wire Line
	5500 2850 5900 2850
Wire Wire Line
	5500 2950 5900 2950
Wire Wire Line
	5500 3050 5900 3050
Wire Wire Line
	5500 3150 5900 3150
Text Label 6800 1650 2    50   ~ 0
vs_exp1
Text Label 6800 1950 2    50   ~ 0
vs_exp2
Text Label 6800 2250 2    50   ~ 0
vs_byod1
Text Label 6800 2550 2    50   ~ 0
vs_byod2
Text Label 6800 2850 2    50   ~ 0
vs_12
Text Label 6800 3150 2    50   ~ 0
vs_switch
Text Label 6800 4000 2    50   ~ 0
vs_iris
Text Label 6800 4300 2    50   ~ 0
vs_b210_1
Text Label 6800 4600 2    50   ~ 0
vs_b210_2
Text Label 6800 4900 2    50   ~ 0
vs_b210_3
Text Label 6800 5200 2    50   ~ 0
vs_3_3
Wire Wire Line
	6400 1450 6900 1450
Wire Wire Line
	6400 1750 6900 1750
Wire Wire Line
	6400 2050 6900 2050
Wire Wire Line
	6400 2350 6900 2350
Wire Wire Line
	6400 2650 6900 2650
Wire Wire Line
	6400 2950 6900 2950
Wire Wire Line
	6400 4100 6900 4100
Wire Wire Line
	6400 4400 6900 4400
Wire Wire Line
	6400 4700 6900 4700
Wire Wire Line
	6400 5000 6900 5000
Text Label 6800 1550 2    50   ~ 0
is_exp1
Text Label 6800 1850 2    50   ~ 0
is_exp2
Text Label 6800 2150 2    50   ~ 0
is_byod1
Text Label 6800 2450 2    50   ~ 0
is_byod2
Text Label 6800 2750 2    50   ~ 0
is_12
Text Label 6800 3050 2    50   ~ 0
is_switch
Text Label 6800 3900 2    50   ~ 0
is_iris
Text Label 6800 4200 2    50   ~ 0
is_b210_1
Text Label 6800 4800 2    50   ~ 0
is_b210_3
Text Label 6800 4500 2    50   ~ 0
is_b210_2
Text Label 6800 5100 2    50   ~ 0
is_3_3
Wire Wire Line
	6400 5300 6900 5300
Text Label 6800 5700 2    50   ~ 0
is_pwr
Text Label 6800 5600 2    50   ~ 0
vs_pwr
Wire Wire Line
	5900 1250 5500 1250
Text Label 5500 1250 0    50   ~ 0
hp_p1
Text Label 5500 1450 0    50   ~ 0
hp_p3
Text Label 5500 1650 0    50   ~ 0
hp_p5
Text Label 5500 1350 0    50   ~ 0
hp_p2
Text Label 5500 1550 0    50   ~ 0
hp_p4
Text Label 5500 1750 0    50   ~ 0
hp_p6
Text Label 5500 1850 0    50   ~ 0
lp_p1
Text Label 5500 2050 0    50   ~ 0
lp_p3
Text Label 5500 2250 0    50   ~ 0
hp_p7
Text Label 5500 1950 0    50   ~ 0
lp_p2
Text Label 5500 2150 0    50   ~ 0
lp_p4
Text Label 5500 2350 0    50   ~ 0
lp_p5
Text Label 5500 2450 0    50   ~ 0
en_cnuc
Text Label 5500 2550 0    50   ~ 0
en_exp1
Text Label 5500 2750 0    50   ~ 0
en_byod1
Text Label 5500 2650 0    50   ~ 0
en_exp2
Text Label 5500 2850 0    50   ~ 0
en_byod2
Text Label 5500 2950 0    50   ~ 0
en_switch
Text Label 5500 3150 0    50   ~ 0
en_b210_1
Text Label 5500 4000 0    50   ~ 0
en_b210_3
Text Label 5500 3050 0    50   ~ 0
en_iris
Text Label 5500 3900 0    50   ~ 0
en_b210_2
Text Label 5500 4100 0    50   ~ 0
pg_bat
Text Label 5500 4300 0    50   ~ 0
pg_cnuc
Text Label 5500 4400 0    50   ~ 0
pg_exp1
Text Label 5500 4600 0    50   ~ 0
pg_byod1
Text Label 5500 4500 0    50   ~ 0
pg_exp2
Text Label 5500 4700 0    50   ~ 0
pg_byod2
Text Label 5500 4200 0    50   ~ 0
pg_12
Text Label 5500 4800 0    50   ~ 0
pg_b210_1
Text Label 5500 5000 0    50   ~ 0
pg_b210_3
Text Label 5500 4900 0    50   ~ 0
pg_b210_2
Text Label 5500 5100 0    50   ~ 0
pg_3_3
Text Label 5500 5200 0    50   ~ 0
bat_off
Text Label 5500 5400 0    50   ~ 0
bat_sclk
Text Label 5500 5600 0    50   ~ 0
bat_cs
Text Label 5500 5300 0    50   ~ 0
pg_pwr_3_3
Text Label 5500 5500 0    50   ~ 0
bat_mosi
Text Label 5500 5700 0    50   ~ 0
bat_miso
Wire Wire Line
	5400 5800 5900 5800
Wire Wire Line
	6400 5800 6900 5800
Wire Wire Line
	6900 4100 6900 4400
Connection ~ 6900 4400
Wire Wire Line
	6900 4400 6900 4700
Connection ~ 6900 4700
Wire Wire Line
	6900 4700 6900 5000
Connection ~ 6900 5000
Wire Wire Line
	6900 5000 6900 5300
Connection ~ 6900 5300
Connection ~ 6900 5800
Wire Wire Line
	6900 5800 6900 6000
Wire Wire Line
	5400 5800 5400 6000
Wire Wire Line
	6900 1450 6900 1750
Connection ~ 6900 1750
Wire Wire Line
	6900 1750 6900 2050
Connection ~ 6900 2050
Wire Wire Line
	6900 2050 6900 2350
Connection ~ 6900 2350
Wire Wire Line
	6900 2350 6900 2650
Connection ~ 6900 2650
Wire Wire Line
	6900 2650 6900 2950
Connection ~ 6900 2950
Wire Wire Line
	6900 2950 6900 3400
$Comp
L power:GND #PWR?
U 1 1 60C3FBB6
P 6900 3400
F 0 "#PWR?" H 6900 3150 50  0001 C CNN
F 1 "GND" H 6905 3227 50  0000 C CNN
F 2 "" H 6900 3400 50  0001 C CNN
F 3 "" H 6900 3400 50  0001 C CNN
	1    6900 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60C43381
P 6900 6000
F 0 "#PWR?" H 6900 5750 50  0001 C CNN
F 1 "GND" H 6905 5827 50  0000 C CNN
F 2 "" H 6900 6000 50  0001 C CNN
F 3 "" H 6900 6000 50  0001 C CNN
	1    6900 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60C441EF
P 5400 6000
F 0 "#PWR?" H 5400 5750 50  0001 C CNN
F 1 "GND" H 5405 5827 50  0000 C CNN
F 2 "" H 5400 6000 50  0001 C CNN
F 3 "" H 5400 6000 50  0001 C CNN
	1    5400 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 5300 6900 5400
Wire Wire Line
	6400 5500 6900 5500
Connection ~ 6900 5500
Wire Wire Line
	6900 5500 6900 5800
Wire Wire Line
	6400 5400 6900 5400
Connection ~ 6900 5400
Wire Wire Line
	6900 5400 6900 5500
NoConn ~ 2850 1050
NoConn ~ 2850 1250
NoConn ~ 2850 1450
NoConn ~ 2850 1650
NoConn ~ 2850 1850
NoConn ~ 2850 2050
NoConn ~ 2850 2250
NoConn ~ 2850 2450
NoConn ~ 2850 2650
NoConn ~ 3350 2750
NoConn ~ 3350 2550
NoConn ~ 3350 2350
NoConn ~ 3350 2150
NoConn ~ 3350 1950
NoConn ~ 3350 1750
NoConn ~ 3350 1550
NoConn ~ 3350 1350
NoConn ~ 3350 1150
NoConn ~ 10100 6050
NoConn ~ 9100 6150
NoConn ~ 9100 6350
NoConn ~ 10100 6250
NoConn ~ 10100 6450
NoConn ~ 10100 6650
NoConn ~ 10100 7050
NoConn ~ 10100 7250
NoConn ~ 10100 7450
NoConn ~ 10100 7650
NoConn ~ 9100 7750
NoConn ~ 9100 7550
NoConn ~ 9100 7350
NoConn ~ 9100 7150
NoConn ~ 9100 6750
NoConn ~ 9100 6550
NoConn ~ 2300 7050
NoConn ~ 2300 7150
NoConn ~ 2300 7250
NoConn ~ 2300 7350
NoConn ~ 2300 7450
NoConn ~ 2300 7550
NoConn ~ 2300 7650
NoConn ~ 2300 7750
NoConn ~ 3900 7750
NoConn ~ 3900 7650
NoConn ~ 3900 7550
NoConn ~ 3900 7450
NoConn ~ 3900 7350
NoConn ~ 3900 7250
NoConn ~ 3900 7150
NoConn ~ 3900 7050
NoConn ~ 3900 6050
NoConn ~ 3900 6150
NoConn ~ 3900 6250
NoConn ~ 3900 6350
NoConn ~ 3900 6450
NoConn ~ 3900 6550
NoConn ~ 3900 6650
NoConn ~ 3900 6750
NoConn ~ 2300 6750
NoConn ~ 2300 6650
NoConn ~ 2300 6550
NoConn ~ 2300 6450
NoConn ~ 2300 6350
NoConn ~ 2300 6250
NoConn ~ 2300 6150
NoConn ~ 2300 6050
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 6036E448
P 5200 8100
F 0 "H1" H 5300 8149 50  0000 L CNN
F 1 "MountingHole_Pad" H 5300 8058 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 5200 8100 50  0001 C CNN
F 3 "~" H 5200 8100 50  0001 C CNN
	1    5200 8100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 603FAEF8
P 5200 8200
F 0 "#PWR?" H 5200 7950 50  0001 C CNN
F 1 "GND" H 5205 8027 50  0000 C CNN
F 2 "" H 5200 8200 50  0001 C CNN
F 3 "" H 5200 8200 50  0001 C CNN
	1    5200 8200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 603FE718
P 6150 8100
F 0 "H3" H 6250 8149 50  0000 L CNN
F 1 "MountingHole_Pad" H 6250 8058 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 6150 8100 50  0001 C CNN
F 3 "~" H 6150 8100 50  0001 C CNN
	1    6150 8100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 603FE71E
P 6150 8200
F 0 "#PWR?" H 6150 7950 50  0001 C CNN
F 1 "GND" H 6155 8027 50  0000 C CNN
F 2 "" H 6150 8200 50  0001 C CNN
F 3 "" H 6150 8200 50  0001 C CNN
	1    6150 8200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 604431FD
P 5200 8750
F 0 "H2" H 5300 8799 50  0000 L CNN
F 1 "MountingHole_Pad" H 5300 8708 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 5200 8750 50  0001 C CNN
F 3 "~" H 5200 8750 50  0001 C CNN
	1    5200 8750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60443203
P 5200 8850
F 0 "#PWR?" H 5200 8600 50  0001 C CNN
F 1 "GND" H 5205 8677 50  0000 C CNN
F 2 "" H 5200 8850 50  0001 C CNN
F 3 "" H 5200 8850 50  0001 C CNN
	1    5200 8850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 60443209
P 6150 8750
F 0 "H4" H 6250 8799 50  0000 L CNN
F 1 "MountingHole_Pad" H 6250 8708 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 6150 8750 50  0001 C CNN
F 3 "~" H 6150 8750 50  0001 C CNN
	1    6150 8750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6044320F
P 6150 8850
F 0 "#PWR?" H 6150 8600 50  0001 C CNN
F 1 "GND" H 6155 8677 50  0000 C CNN
F 2 "" H 6150 8850 50  0001 C CNN
F 3 "" H 6150 8850 50  0001 C CNN
	1    6150 8850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
