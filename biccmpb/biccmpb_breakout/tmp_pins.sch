EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 61669F57
P 4550 2750
F 0 "J?" H 4600 3867 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 4600 3776 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x20_P1.27mm_Vertical_SMD" H 4550 2750 50  0001 C CNN
F 3 "~" H 4550 2750 50  0001 C CNN
	1    4550 2750
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 61669F5D
P 4550 5400
F 0 "J?" H 4600 6517 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 4600 6426 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_2x20_P1.27mm_Vertical_SMD" H 4550 5400 50  0001 C CNN
F 3 "~" H 4550 5400 50  0001 C CNN
	1    4550 5400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4750 1850 5150 1850
Text Label 5150 1950 2    50   ~ 0
vs_cnuc
Wire Wire Line
	4750 1950 5150 1950
Text Label 5150 1850 2    50   ~ 0
is_cnuc
Wire Wire Line
	3850 1950 4250 1950
Wire Wire Line
	4750 2150 5150 2150
Wire Wire Line
	4750 2250 5150 2250
Wire Wire Line
	4750 2450 5150 2450
Wire Wire Line
	4750 2550 5150 2550
Wire Wire Line
	4750 2750 5150 2750
Wire Wire Line
	4750 2850 5150 2850
Wire Wire Line
	4750 3050 5150 3050
Wire Wire Line
	4750 3150 5150 3150
Wire Wire Line
	4750 3350 5150 3350
Wire Wire Line
	4750 3450 5150 3450
Wire Wire Line
	4750 3650 5150 3650
Wire Wire Line
	4750 3750 5150 3750
Wire Wire Line
	4750 4500 5150 4500
Wire Wire Line
	4750 4600 5150 4600
Wire Wire Line
	4750 4800 5150 4800
Wire Wire Line
	4750 4900 5150 4900
Wire Wire Line
	4750 5100 5150 5100
Wire Wire Line
	4750 5200 5150 5200
Wire Wire Line
	4750 5400 5150 5400
Wire Wire Line
	4750 5500 5150 5500
Wire Wire Line
	4750 5700 5150 5700
Wire Wire Line
	4750 5800 5150 5800
Wire Wire Line
	4750 6200 5150 6200
Wire Wire Line
	4750 6300 5150 6300
Wire Wire Line
	3850 4500 4250 4500
Wire Wire Line
	3850 4600 4250 4600
Wire Wire Line
	3850 4700 4250 4700
Wire Wire Line
	3850 4800 4250 4800
Wire Wire Line
	3850 4900 4250 4900
Wire Wire Line
	3850 5000 4250 5000
Wire Wire Line
	3850 5100 4250 5100
Wire Wire Line
	3850 5200 4250 5200
Wire Wire Line
	3850 5300 4250 5300
Wire Wire Line
	3850 5400 4250 5400
Wire Wire Line
	3850 5500 4250 5500
Wire Wire Line
	3850 5600 4250 5600
Wire Wire Line
	3850 5700 4250 5700
Wire Wire Line
	3850 5800 4250 5800
Wire Wire Line
	3850 5900 4250 5900
Wire Wire Line
	3850 6000 4250 6000
Wire Wire Line
	3850 6100 4250 6100
Wire Wire Line
	3850 6200 4250 6200
Wire Wire Line
	3850 6300 4250 6300
Wire Wire Line
	3850 2050 4250 2050
Wire Wire Line
	3850 2150 4250 2150
Wire Wire Line
	3850 2250 4250 2250
Wire Wire Line
	3850 2350 4250 2350
Wire Wire Line
	3850 2450 4250 2450
Wire Wire Line
	3850 2550 4250 2550
Wire Wire Line
	3850 2650 4250 2650
Wire Wire Line
	3850 2750 4250 2750
Wire Wire Line
	3850 2850 4250 2850
Wire Wire Line
	3850 2950 4250 2950
Wire Wire Line
	3850 3050 4250 3050
Wire Wire Line
	3850 3150 4250 3150
Wire Wire Line
	3850 3250 4250 3250
Wire Wire Line
	3850 3350 4250 3350
Wire Wire Line
	3850 3450 4250 3450
Wire Wire Line
	3850 3550 4250 3550
Wire Wire Line
	3850 3650 4250 3650
Wire Wire Line
	3850 3750 4250 3750
Text Label 5150 2250 2    50   ~ 0
vs_exp1
Text Label 5150 2550 2    50   ~ 0
vs_exp2
Text Label 5150 2850 2    50   ~ 0
vs_byod1
Text Label 5150 3150 2    50   ~ 0
vs_byod2
Text Label 5150 3450 2    50   ~ 0
vs_12
Text Label 5150 3750 2    50   ~ 0
vs_switch
Text Label 5150 4600 2    50   ~ 0
vs_iris
Text Label 5150 4900 2    50   ~ 0
vs_b210_1
Text Label 5150 5200 2    50   ~ 0
vs_b210_2
Text Label 5150 5500 2    50   ~ 0
vs_b210_3
Text Label 5150 5800 2    50   ~ 0
vs_3_3
Wire Wire Line
	4750 2050 5250 2050
Wire Wire Line
	4750 2350 5250 2350
Wire Wire Line
	4750 2650 5250 2650
Wire Wire Line
	4750 2950 5250 2950
Wire Wire Line
	4750 3250 5250 3250
Wire Wire Line
	4750 3550 5250 3550
Wire Wire Line
	4750 4700 5250 4700
Wire Wire Line
	4750 5000 5250 5000
Wire Wire Line
	4750 5300 5250 5300
Wire Wire Line
	4750 5600 5250 5600
Text Label 5150 2150 2    50   ~ 0
is_exp1
Text Label 5150 2450 2    50   ~ 0
is_exp2
Text Label 5150 2750 2    50   ~ 0
is_byod1
Text Label 5150 3050 2    50   ~ 0
is_byod2
Text Label 5150 3350 2    50   ~ 0
is_12
Text Label 5150 3650 2    50   ~ 0
is_switch
Text Label 5150 4500 2    50   ~ 0
is_iris
Text Label 5150 4800 2    50   ~ 0
is_b210_1
Text Label 5150 5400 2    50   ~ 0
is_b210_3
Text Label 5150 5100 2    50   ~ 0
is_b210_2
Text Label 5150 5700 2    50   ~ 0
is_3_3
Wire Wire Line
	4750 5900 5250 5900
Text Label 5150 6300 2    50   ~ 0
is_pwr
Text Label 5150 6200 2    50   ~ 0
vs_pwr
Wire Wire Line
	4250 1850 3850 1850
Text Label 3850 1850 0    50   ~ 0
hp_p1
Text Label 3850 2050 0    50   ~ 0
hp_p3
Text Label 3850 2250 0    50   ~ 0
hp_p5
Text Label 3850 1950 0    50   ~ 0
hp_p2
Text Label 3850 2150 0    50   ~ 0
hp_p4
Text Label 3850 2350 0    50   ~ 0
hp_p6
Text Label 3850 2450 0    50   ~ 0
lp_p1
Text Label 3850 2650 0    50   ~ 0
lp_p3
Text Label 3850 2850 0    50   ~ 0
hp_p7
Text Label 3850 2550 0    50   ~ 0
lp_p2
Text Label 3850 2750 0    50   ~ 0
lp_p4
Text Label 3850 2950 0    50   ~ 0
lp_p5
Text Label 3850 3050 0    50   ~ 0
en_cnuc
Text Label 3850 3150 0    50   ~ 0
en_exp1
Text Label 3850 3350 0    50   ~ 0
en_byod1
Text Label 3850 3250 0    50   ~ 0
en_exp2
Text Label 3850 3450 0    50   ~ 0
en_byod2
Text Label 3850 3550 0    50   ~ 0
en_switch
Text Label 3850 3750 0    50   ~ 0
en_b210_1
Text Label 3850 4600 0    50   ~ 0
en_b210_3
Text Label 3850 3650 0    50   ~ 0
en_iris
Text Label 3850 4500 0    50   ~ 0
en_b210_2
Text Label 3850 4700 0    50   ~ 0
pg_bat
Text Label 3850 4900 0    50   ~ 0
pg_cnuc
Text Label 3850 5000 0    50   ~ 0
pg_exp1
Text Label 3850 5200 0    50   ~ 0
pg_byod1
Text Label 3850 5100 0    50   ~ 0
pg_exp2
Text Label 3850 5300 0    50   ~ 0
pg_byod2
Text Label 3850 4800 0    50   ~ 0
pg_12
Text Label 3850 5400 0    50   ~ 0
pg_b210_1
Text Label 3850 5600 0    50   ~ 0
pg_b210_3
Text Label 3850 5500 0    50   ~ 0
pg_b210_2
Text Label 3850 5700 0    50   ~ 0
pg_3_3
Text Label 3850 5800 0    50   ~ 0
bat_off
Text Label 3850 6000 0    50   ~ 0
bat_sclk
Text Label 3850 6200 0    50   ~ 0
bat_cs
Text Label 3850 5900 0    50   ~ 0
pg_pwr_3_3
Text Label 3850 6100 0    50   ~ 0
bat_mosi
Text Label 3850 6300 0    50   ~ 0
bat_miso
Wire Wire Line
	3750 6400 4250 6400
Wire Wire Line
	4750 6400 5250 6400
Wire Wire Line
	5250 4700 5250 5000
Connection ~ 5250 5000
Wire Wire Line
	5250 5000 5250 5300
Connection ~ 5250 5300
Wire Wire Line
	5250 5300 5250 5600
Connection ~ 5250 5600
Wire Wire Line
	5250 5600 5250 5900
Connection ~ 5250 5900
Connection ~ 5250 6400
Wire Wire Line
	5250 6400 5250 6600
Wire Wire Line
	3750 6400 3750 6600
Wire Wire Line
	5250 2050 5250 2350
Connection ~ 5250 2350
Wire Wire Line
	5250 2350 5250 2650
Connection ~ 5250 2650
Wire Wire Line
	5250 2650 5250 2950
Connection ~ 5250 2950
Wire Wire Line
	5250 2950 5250 3250
Connection ~ 5250 3250
Wire Wire Line
	5250 3250 5250 3550
Connection ~ 5250 3550
Wire Wire Line
	5250 3550 5250 4000
$Comp
L power:GND #PWR?
U 1 1 6166A008
P 5250 4000
F 0 "#PWR?" H 5250 3750 50  0001 C CNN
F 1 "GND" H 5255 3827 50  0000 C CNN
F 2 "" H 5250 4000 50  0001 C CNN
F 3 "" H 5250 4000 50  0001 C CNN
	1    5250 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6166A00E
P 5250 6600
F 0 "#PWR?" H 5250 6350 50  0001 C CNN
F 1 "GND" H 5255 6427 50  0000 C CNN
F 2 "" H 5250 6600 50  0001 C CNN
F 3 "" H 5250 6600 50  0001 C CNN
	1    5250 6600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6166A014
P 3750 6600
F 0 "#PWR?" H 3750 6350 50  0001 C CNN
F 1 "GND" H 3755 6427 50  0000 C CNN
F 2 "" H 3750 6600 50  0001 C CNN
F 3 "" H 3750 6600 50  0001 C CNN
	1    3750 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 5900 5250 6000
Wire Wire Line
	4750 6100 5250 6100
Connection ~ 5250 6100
Wire Wire Line
	5250 6100 5250 6400
Wire Wire Line
	4750 6000 5250 6000
Connection ~ 5250 6000
Wire Wire Line
	5250 6000 5250 6100
$EndSCHEMATC
