import itertools

import pcbnew

dy = 125

arraying = [
    [99, 0],
    [0, dy],
    [99, dy],
]

pcb = pcbnew.IO_MGR.Load(pcbnew.IO_MGR.KICAD_SEXP, "fdd_frontend_universal_array.kicad_pcb")

#pcb = pcbnew.GetBoard()
#pcb.Move(pcbnew.wxPoint(-297, 0))

# Move informational (non-silk) Drawings, also move the outline as we'll
# manually fix that back up
for drawing in pcb.GetDrawings():
    # Only move the non-fabricated items
    if drawing.GetLayerName().endswith("SilkS"):
        continue

    drawing.Move(pcbnew.wxPointMM(-297, 0))

i=0
# Tracks
# Modules
# Drawings
# Zones
for drawing in list(pcb.GetDrawings()):
    # Only copy the silk
    if not drawing.GetLayerName().endswith("SilkS"):
        continue

    print(i)
    i += 1
    for array_part_x, array_part_y in arraying:
        dup = pcb.Duplicate(drawing, True)
        dup.Move(pcbnew.wxPointMM(array_part_x, array_part_y))

for thing in list(itertools.chain(pcb.GetTracks(),
                                  pcb.GetModules(),
                                  pcb.Zones())):
    print(i)
    i += 1
    for array_part_x, array_part_y in arraying:
        dup = pcb.Duplicate(thing, True)
        dup.Move(pcbnew.wxPointMM(array_part_x, array_part_y))

pcb.Save("fdd_frontend_universal_array_out.kicad_pcb")
