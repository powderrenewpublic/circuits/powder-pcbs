EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 13 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L POWDER_Power:INA4180-Q1 U?
U 1 1 5D974465
P 4850 3100
AR Path="/5D974465" Ref="U?"  Part="1" 
AR Path="/5D949534/5D974465" Ref="U1301"  Part="1" 
F 0 "U1301" H 5100 3800 50  0000 L CNN
F 1 "INA4180-Q1" H 5000 2400 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 6750 3950 100 0001 C CNN
F 3 "" H 5100 3800 100 0001 C CNN
F 4 "3000-0000000185" H 4850 3100 50  0001 C CNN "PartSpecification"
	1    4850 3100
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5D97446B
P 4850 2300
AR Path="/5D97446B" Ref="#PWR?"  Part="1" 
AR Path="/5D949534/5D97446B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4850 2150 50  0001 C CNN
F 1 "+3.3V" H 4865 2473 50  0000 C CNN
F 2 "" H 4850 2300 50  0001 C CNN
F 3 "" H 4850 2300 50  0001 C CNN
	1    4850 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2300 4850 2350
$Comp
L power:GND #PWR?
U 1 1 5D974472
P 4850 3900
AR Path="/5D974472" Ref="#PWR?"  Part="1" 
AR Path="/5D949534/5D974472" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4850 3650 50  0001 C CNN
F 1 "GND" H 4855 3727 50  0000 C CNN
F 2 "" H 4850 3900 50  0001 C CNN
F 3 "" H 4850 3900 50  0001 C CNN
	1    4850 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 3900 4850 3800
Text HLabel 2750 2650 0    50   Output ~ 0
OUT_1
Text HLabel 2750 2950 0    50   Output ~ 0
OUT_2
Text HLabel 2750 3250 0    50   Output ~ 0
OUT_3
Text HLabel 2750 3550 0    50   Output ~ 0
OUT_4
Text HLabel 5000 1500 0    50   Input ~ 0
IN_P
$Comp
L Device:R R?
U 1 1 5D9889BE
P 5550 2300
AR Path="/5D69D382/5D9889BE" Ref="R?"  Part="1" 
AR Path="/5D9889BE" Ref="R?"  Part="1" 
AR Path="/5D949534/5D9889BE" Ref="R1301"  Part="1" 
F 0 "R1301" H 5620 2346 50  0000 L CNN
F 1 "100" H 5620 2255 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5480 2300 50  0001 C CNN
F 3 "~" H 5550 2300 50  0001 C CNN
F 4 "" H 5550 2300 50  0001 C CNN "Manufacturer"
F 5 "" H 5550 2300 50  0001 C CNN "PartNumber"
F 6 "3000-0000000149" H 5550 2300 50  0001 C CNN "PartSpecification"
	1    5550 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2450 5550 2700
Wire Wire Line
	6400 2700 6300 2700
Wire Wire Line
	5900 2450 5900 2900
Wire Wire Line
	6700 2450 6700 3300
Wire Wire Line
	7100 3300 7050 3300
Wire Wire Line
	7200 2450 7200 3500
Wire Wire Line
	5550 1500 5550 2150
Wire Wire Line
	5900 2150 5900 1500
Wire Wire Line
	6700 2150 6700 1500
Wire Wire Line
	7200 2150 7200 1500
Wire Wire Line
	5500 3200 7100 3200
Wire Wire Line
	5500 2600 6400 2600
Text HLabel 8300 2600 2    50   Input ~ 0
IN_M_1
Text HLabel 8300 3000 2    50   Input ~ 0
IN_M_2
Text HLabel 8300 3200 2    50   Input ~ 0
IN_M_3
Text HLabel 8300 3600 2    50   Input ~ 0
IN_M_4
Wire Wire Line
	8300 3600 8200 3600
Wire Wire Line
	8200 3200 8300 3200
Wire Wire Line
	8300 3000 7900 3000
Wire Wire Line
	7900 2600 8300 2600
Wire Wire Line
	2750 2650 4400 2650
Wire Wire Line
	2750 2950 4400 2950
Wire Wire Line
	2750 3250 4400 3250
Wire Wire Line
	2750 3550 4400 3550
$Comp
L 000_PreferredParts:C_100nF C1301
U 1 1 5D99CE2F
P 6150 2700
F 0 "C1301" H 6175 2800 50  0000 L CNN
F 1 "C_100nF" H 6175 2600 50  0001 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6188 2550 50  0001 C CNN
F 3 "~" H 6150 2700 50  0001 C CNN
F 4 "100nF" H 6300 2600 50  0000 C CNN "DisplayValue"
F 5 "3000-0000000125" H 200 0   50  0001 C CNN "PartSpecification"
	1    6150 2700
	0    1    1    0   
$EndComp
$Comp
L 000_PreferredParts:C_100nF C1303
U 1 1 5D99D1F3
P 6350 2900
F 0 "C1303" H 6375 3000 50  0000 L CNN
F 1 "C_100nF" H 6375 2800 50  0001 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6388 2750 50  0001 C CNN
F 3 "~" H 6350 2900 50  0001 C CNN
F 4 "100nF" H 6500 2800 50  0000 C CNN "DisplayValue"
F 5 "3000-0000000125" H 200 0   50  0001 C CNN "PartSpecification"
	1    6350 2900
	0    1    1    0   
$EndComp
$Comp
L 000_PreferredParts:C_100nF C1302
U 1 1 5D99D3DA
P 6900 3300
F 0 "C1302" H 6925 3400 50  0000 L CNN
F 1 "C_100nF" H 6925 3200 50  0001 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6938 3150 50  0001 C CNN
F 3 "~" H 6900 3300 50  0001 C CNN
F 4 "100nF" H 7050 3200 50  0000 C CNN "DisplayValue"
F 5 "3000-0000000125" H 200 0   50  0001 C CNN "PartSpecification"
	1    6900 3300
	0    1    1    0   
$EndComp
$Comp
L 000_PreferredParts:C_100nF C1304
U 1 1 5D99D665
P 7450 3500
F 0 "C1304" H 7475 3600 50  0000 L CNN
F 1 "C_100nF" H 7475 3400 50  0001 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7488 3350 50  0001 C CNN
F 3 "~" H 7450 3500 50  0001 C CNN
F 4 "100nF" H 7600 3400 50  0000 C CNN "DisplayValue"
F 5 "3000-0000000125" H 200 0   50  0001 C CNN "PartSpecification"
	1    7450 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D99DDCE
P 5900 2300
AR Path="/5D69D382/5D99DDCE" Ref="R?"  Part="1" 
AR Path="/5D99DDCE" Ref="R?"  Part="1" 
AR Path="/5D949534/5D99DDCE" Ref="R1302"  Part="1" 
F 0 "R1302" H 5970 2346 50  0000 L CNN
F 1 "100" H 5970 2255 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5830 2300 50  0001 C CNN
F 3 "~" H 5900 2300 50  0001 C CNN
F 4 "" H 5900 2300 50  0001 C CNN "Manufacturer"
F 5 "" H 5900 2300 50  0001 C CNN "PartNumber"
F 6 "3000-0000000149" H 5900 2300 50  0001 C CNN "PartSpecification"
	1    5900 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D99DFB0
P 6700 2300
AR Path="/5D69D382/5D99DFB0" Ref="R?"  Part="1" 
AR Path="/5D99DFB0" Ref="R?"  Part="1" 
AR Path="/5D949534/5D99DFB0" Ref="R1303"  Part="1" 
F 0 "R1303" H 6770 2346 50  0000 L CNN
F 1 "100" H 6770 2255 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6630 2300 50  0001 C CNN
F 3 "~" H 6700 2300 50  0001 C CNN
F 4 "" H 6700 2300 50  0001 C CNN "Manufacturer"
F 5 "" H 6700 2300 50  0001 C CNN "PartNumber"
F 6 "3000-0000000149" H 6700 2300 50  0001 C CNN "PartSpecification"
	1    6700 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D99E228
P 7200 2300
AR Path="/5D69D382/5D99E228" Ref="R?"  Part="1" 
AR Path="/5D99E228" Ref="R?"  Part="1" 
AR Path="/5D949534/5D99E228" Ref="R1304"  Part="1" 
F 0 "R1304" H 7270 2346 50  0000 L CNN
F 1 "100" H 7270 2255 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7130 2300 50  0001 C CNN
F 3 "~" H 7200 2300 50  0001 C CNN
F 4 "" H 7200 2300 50  0001 C CNN "Manufacturer"
F 5 "" H 7200 2300 50  0001 C CNN "PartNumber"
F 6 "3000-0000000149" H 7200 2300 50  0001 C CNN "PartSpecification"
	1    7200 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D99E393
P 7750 2600
AR Path="/5D69D382/5D99E393" Ref="R?"  Part="1" 
AR Path="/5D99E393" Ref="R?"  Part="1" 
AR Path="/5D949534/5D99E393" Ref="R1305"  Part="1" 
F 0 "R1305" H 7820 2646 50  0000 L CNN
F 1 "100" H 7820 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7680 2600 50  0001 C CNN
F 3 "~" H 7750 2600 50  0001 C CNN
F 4 "" H 7750 2600 50  0001 C CNN "Manufacturer"
F 5 "" H 7750 2600 50  0001 C CNN "PartNumber"
F 6 "3000-0000000149" H 7750 2600 50  0001 C CNN "PartSpecification"
	1    7750 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D99ED8A
P 7750 3000
AR Path="/5D69D382/5D99ED8A" Ref="R?"  Part="1" 
AR Path="/5D99ED8A" Ref="R?"  Part="1" 
AR Path="/5D949534/5D99ED8A" Ref="R1306"  Part="1" 
F 0 "R1306" H 7820 3046 50  0000 L CNN
F 1 "100" H 7820 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7680 3000 50  0001 C CNN
F 3 "~" H 7750 3000 50  0001 C CNN
F 4 "" H 7750 3000 50  0001 C CNN "Manufacturer"
F 5 "" H 7750 3000 50  0001 C CNN "PartNumber"
F 6 "3000-0000000149" H 7750 3000 50  0001 C CNN "PartSpecification"
	1    7750 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D99EFF1
P 8050 3200
AR Path="/5D69D382/5D99EFF1" Ref="R?"  Part="1" 
AR Path="/5D99EFF1" Ref="R?"  Part="1" 
AR Path="/5D949534/5D99EFF1" Ref="R1307"  Part="1" 
F 0 "R1307" H 8120 3246 50  0000 L CNN
F 1 "100" H 8120 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7980 3200 50  0001 C CNN
F 3 "~" H 8050 3200 50  0001 C CNN
F 4 "" H 8050 3200 50  0001 C CNN "Manufacturer"
F 5 "" H 8050 3200 50  0001 C CNN "PartNumber"
F 6 "3000-0000000149" H 8050 3200 50  0001 C CNN "PartSpecification"
	1    8050 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D99F234
P 8050 3600
AR Path="/5D69D382/5D99F234" Ref="R?"  Part="1" 
AR Path="/5D99F234" Ref="R?"  Part="1" 
AR Path="/5D949534/5D99F234" Ref="R1308"  Part="1" 
F 0 "R1308" H 8120 3646 50  0000 L CNN
F 1 "100" H 8120 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7980 3600 50  0001 C CNN
F 3 "~" H 8050 3600 50  0001 C CNN
F 4 "" H 8050 3600 50  0001 C CNN "Manufacturer"
F 5 "" H 8050 3600 50  0001 C CNN "PartNumber"
F 6 "3000-0000000149" H 8050 3600 50  0001 C CNN "PartSpecification"
	1    8050 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 2700 6400 2600
Connection ~ 6400 2600
Wire Wire Line
	6400 2600 7600 2600
Connection ~ 5550 1500
Connection ~ 5550 2700
Wire Wire Line
	5550 2700 6000 2700
Wire Wire Line
	5550 1500 5900 1500
Connection ~ 5900 1500
Connection ~ 5900 2900
Wire Wire Line
	5900 2900 6200 2900
Wire Wire Line
	5900 1500 6700 1500
Wire Wire Line
	6600 2900 6600 3000
Wire Wire Line
	6600 2900 6500 2900
Connection ~ 6600 3000
Wire Wire Line
	6600 3000 7600 3000
Connection ~ 6700 1500
Connection ~ 6700 3300
Wire Wire Line
	6700 3300 6750 3300
Wire Wire Line
	6700 1500 7200 1500
Wire Wire Line
	7100 3300 7100 3200
Connection ~ 7100 3200
Wire Wire Line
	7100 3200 7900 3200
Connection ~ 7200 3500
Wire Wire Line
	7300 3500 7200 3500
Wire Wire Line
	7600 3500 7700 3500
Wire Wire Line
	7700 3500 7700 3600
Connection ~ 7700 3600
Wire Wire Line
	7700 3600 7900 3600
Text Label 6400 2600 0    50   ~ 0
IN_M_1_F
Text Label 5650 2700 0    50   ~ 0
IN_P_1_F
Text Label 5550 2900 0    50   ~ 0
IN_P_2_F
Text Label 5550 3000 0    50   ~ 0
IN_M_2_F
Text Label 5550 3200 0    50   ~ 0
IN_M_3_F
Text Label 5550 3300 0    50   ~ 0
IN_P_3_F
Text Label 5550 3500 0    50   ~ 0
IN_P_4_F
Text Label 5550 3600 0    50   ~ 0
IN_M_4_F
$Comp
L 000_PreferredParts:C_0_1uF C1305
U 1 1 5ECFE980
P 4200 2200
F 0 "C1305" H 4225 2300 50  0000 L CNN
F 1 "C_0_1uF" H 4225 2100 50  0001 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4238 2050 50  0001 C CNN
F 3 "~" H 4200 2200 50  0001 C CNN
F 4 "0.1uF" H 4350 2100 50  0000 C CNN "DisplayValue"
F 5 "3000-0000000125" H 0   0   50  0001 C CNN "PartSpecification"
	1    4200 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5ED0106D
P 4200 2450
AR Path="/5ED0106D" Ref="#PWR?"  Part="1" 
AR Path="/5D949534/5ED0106D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4200 2200 50  0001 C CNN
F 1 "GND" H 4205 2277 50  0000 C CNN
F 2 "" H 4200 2450 50  0001 C CNN
F 3 "" H 4200 2450 50  0001 C CNN
	1    4200 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2450 4200 2350
Wire Wire Line
	4200 2050 4200 2000
Wire Wire Line
	4200 2000 4700 2000
Wire Wire Line
	4700 2000 4700 2350
Wire Wire Line
	4700 2350 4850 2350
Connection ~ 4850 2350
Wire Wire Line
	4850 2350 4850 2400
Wire Wire Line
	5000 1500 5550 1500
Wire Wire Line
	5300 3600 7700 3600
Wire Wire Line
	5300 3500 7200 3500
Wire Wire Line
	5400 3300 5400 3200
Wire Wire Line
	5400 3200 5300 3200
Wire Wire Line
	5400 3300 6700 3300
Wire Wire Line
	5300 3300 5350 3300
Wire Wire Line
	5350 3300 5350 3250
Wire Wire Line
	5350 3250 5500 3250
Wire Wire Line
	5500 3250 5500 3200
Wire Wire Line
	5300 3000 6600 3000
Wire Wire Line
	5300 2900 5900 2900
Wire Wire Line
	5300 2600 5400 2600
Wire Wire Line
	5400 2600 5400 2700
Wire Wire Line
	5400 2700 5550 2700
Wire Wire Line
	5500 2600 5500 2650
Wire Wire Line
	5500 2650 5350 2650
Wire Wire Line
	5350 2650 5350 2700
Wire Wire Line
	5350 2700 5300 2700
$EndSCHEMATC
