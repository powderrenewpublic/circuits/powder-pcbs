EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L POWDER_RF:SKY65162-70LF U201
U 1 1 5B5B5736
P 5550 3450
AR Path="/5B60A9BE/5B5B5736" Ref="U201"  Part="1" 
AR Path="/5B60D3AD/5B5B5736" Ref="U301"  Part="1" 
AR Path="/5BB89AD3/5B5B5736" Ref="U601"  Part="1" 
AR Path="/5BB89AD7/5B5B5736" Ref="U701"  Part="1" 
F 0 "U601" H 5575 3915 50  0000 C CNN
F 1 "SKY65162-70LF" H 5575 3824 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 5550 3450 50  0001 C CNN
F 3 "" H 5550 3450 50  0001 C CNN
	1    5550 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5B57A9
P 5550 3900
AR Path="/5B60A9BE/5B5B57A9" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5B57A9" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5B57A9" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5B57A9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5550 3650 50  0001 C CNN
F 1 "GND" H 5555 3727 50  0000 C CNN
F 2 "" H 5550 3900 50  0001 C CNN
F 3 "" H 5550 3900 50  0001 C CNN
	1    5550 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3900 5550 3700
$Comp
L Device:L L203
U 1 1 5B5B57E5
P 6250 3150
AR Path="/5B60A9BE/5B5B57E5" Ref="L203"  Part="1" 
AR Path="/5B60D3AD/5B5B57E5" Ref="L303"  Part="1" 
AR Path="/5BB89AD3/5B5B57E5" Ref="L603"  Part="1" 
AR Path="/5BB89AD7/5B5B57E5" Ref="L703"  Part="1" 
F 0 "L603" H 6303 3196 50  0000 L CNN
F 1 "L" H 6303 3105 50  0000 L CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 6250 3150 50  0001 C CNN
F 3 "~" H 6250 3150 50  0001 C CNN
	1    6250 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C207
U 1 1 5B5B58DC
P 4900 3450
AR Path="/5B60A9BE/5B5B58DC" Ref="C207"  Part="1" 
AR Path="/5B60D3AD/5B5B58DC" Ref="C307"  Part="1" 
AR Path="/5BB89AD3/5B5B58DC" Ref="C607"  Part="1" 
AR Path="/5BB89AD7/5B5B58DC" Ref="C707"  Part="1" 
F 0 "C607" V 4648 3450 50  0000 C CNN
F 1 "C" V 4739 3450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 4938 3300 50  0001 C CNN
F 3 "~" H 4900 3450 50  0001 C CNN
	1    4900 3450
	0    1    1    0   
$EndComp
$Comp
L Device:C C206
U 1 1 5B5B596A
P 4500 3750
AR Path="/5B60A9BE/5B5B596A" Ref="C206"  Part="1" 
AR Path="/5B60D3AD/5B5B596A" Ref="C306"  Part="1" 
AR Path="/5BB89AD3/5B5B596A" Ref="C606"  Part="1" 
AR Path="/5BB89AD7/5B5B596A" Ref="C706"  Part="1" 
F 0 "C606" H 4385 3704 50  0000 R CNN
F 1 "C" H 4385 3795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 4538 3600 50  0001 C CNN
F 3 "~" H 4500 3750 50  0001 C CNN
	1    4500 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C205
U 1 1 5B5B59A3
P 4150 3450
AR Path="/5B60A9BE/5B5B59A3" Ref="C205"  Part="1" 
AR Path="/5B60D3AD/5B5B59A3" Ref="C305"  Part="1" 
AR Path="/5BB89AD3/5B5B59A3" Ref="C605"  Part="1" 
AR Path="/5BB89AD7/5B5B59A3" Ref="C705"  Part="1" 
F 0 "C605" V 4402 3450 50  0000 C CNN
F 1 "C" V 4311 3450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 4188 3300 50  0001 C CNN
F 3 "~" H 4150 3450 50  0001 C CNN
	1    4150 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C204
U 1 1 5B5B59DD
P 3750 3750
AR Path="/5B60A9BE/5B5B59DD" Ref="C204"  Part="1" 
AR Path="/5B60D3AD/5B5B59DD" Ref="C304"  Part="1" 
AR Path="/5BB89AD3/5B5B59DD" Ref="C604"  Part="1" 
AR Path="/5BB89AD7/5B5B59DD" Ref="C704"  Part="1" 
F 0 "C604" H 3865 3796 50  0000 L CNN
F 1 "C" H 3865 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 3788 3600 50  0001 C CNN
F 3 "~" H 3750 3750 50  0001 C CNN
	1    3750 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C203
U 1 1 5B5B5A2A
P 3050 3750
AR Path="/5B60A9BE/5B5B5A2A" Ref="C203"  Part="1" 
AR Path="/5B60D3AD/5B5B5A2A" Ref="C303"  Part="1" 
AR Path="/5BB89AD3/5B5B5A2A" Ref="C603"  Part="1" 
AR Path="/5BB89AD7/5B5B5A2A" Ref="C703"  Part="1" 
F 0 "C603" H 2935 3704 50  0000 R CNN
F 1 "C" H 2935 3795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 3088 3600 50  0001 C CNN
F 3 "~" H 3050 3750 50  0001 C CNN
	1    3050 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C202
U 1 1 5B5B5A88
P 2350 3750
AR Path="/5B60A9BE/5B5B5A88" Ref="C202"  Part="1" 
AR Path="/5B60D3AD/5B5B5A88" Ref="C302"  Part="1" 
AR Path="/5BB89AD3/5B5B5A88" Ref="C602"  Part="1" 
AR Path="/5BB89AD7/5B5B5A88" Ref="C702"  Part="1" 
F 0 "C602" H 2235 3704 50  0000 R CNN
F 1 "C" H 2235 3795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 2388 3600 50  0001 C CNN
F 3 "~" H 2350 3750 50  0001 C CNN
	1    2350 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C201
U 1 1 5B5B5AC4
P 1900 3450
AR Path="/5B60A9BE/5B5B5AC4" Ref="C201"  Part="1" 
AR Path="/5B60D3AD/5B5B5AC4" Ref="C301"  Part="1" 
AR Path="/5BB89AD3/5B5B5AC4" Ref="C601"  Part="1" 
AR Path="/5BB89AD7/5B5B5AC4" Ref="C701"  Part="1" 
F 0 "C601" V 2152 3450 50  0000 C CNN
F 1 "C" V 2061 3450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 1938 3300 50  0001 C CNN
F 3 "~" H 1900 3450 50  0001 C CNN
	1    1900 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L201
U 1 1 5B5B5B12
P 2700 3450
AR Path="/5B60A9BE/5B5B5B12" Ref="L201"  Part="1" 
AR Path="/5B60D3AD/5B5B5B12" Ref="L301"  Part="1" 
AR Path="/5BB89AD3/5B5B5B12" Ref="L601"  Part="1" 
AR Path="/5BB89AD7/5B5B5B12" Ref="L701"  Part="1" 
F 0 "L601" V 2890 3450 50  0000 C CNN
F 1 "L" V 2799 3450 50  0000 C CNN
F 2 "Inductor_SMD:L_0201_0603Metric" H 2700 3450 50  0001 C CNN
F 3 "~" H 2700 3450 50  0001 C CNN
	1    2700 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C208
U 1 1 5B5B5CA1
P 6250 3750
AR Path="/5B60A9BE/5B5B5CA1" Ref="C208"  Part="1" 
AR Path="/5B60D3AD/5B5B5CA1" Ref="C308"  Part="1" 
AR Path="/5BB89AD3/5B5B5CA1" Ref="C608"  Part="1" 
AR Path="/5BB89AD7/5B5B5CA1" Ref="C708"  Part="1" 
F 0 "C608" H 6135 3704 50  0000 R CNN
F 1 "C" H 6135 3795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 6288 3600 50  0001 C CNN
F 3 "~" H 6250 3750 50  0001 C CNN
	1    6250 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C209
U 1 1 5B5B5D85
P 6700 3450
AR Path="/5B60A9BE/5B5B5D85" Ref="C209"  Part="1" 
AR Path="/5B60D3AD/5B5B5D85" Ref="C309"  Part="1" 
AR Path="/5BB89AD3/5B5B5D85" Ref="C609"  Part="1" 
AR Path="/5BB89AD7/5B5B5D85" Ref="C709"  Part="1" 
F 0 "C609" V 6952 3450 50  0000 C CNN
F 1 "C" V 6861 3450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 6738 3300 50  0001 C CNN
F 3 "~" H 6700 3450 50  0001 C CNN
	1    6700 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C211
U 1 1 5B5B5DE4
P 7050 3750
AR Path="/5B60A9BE/5B5B5DE4" Ref="C211"  Part="1" 
AR Path="/5B60D3AD/5B5B5DE4" Ref="C311"  Part="1" 
AR Path="/5BB89AD3/5B5B5DE4" Ref="C611"  Part="1" 
AR Path="/5BB89AD7/5B5B5DE4" Ref="C711"  Part="1" 
F 0 "C611" H 7165 3796 50  0000 L CNN
F 1 "C" H 7165 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 7088 3600 50  0001 C CNN
F 3 "~" H 7050 3750 50  0001 C CNN
	1    7050 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C213
U 1 1 5B5B5E36
P 7700 3750
AR Path="/5B60A9BE/5B5B5E36" Ref="C213"  Part="1" 
AR Path="/5B60D3AD/5B5B5E36" Ref="C313"  Part="1" 
AR Path="/5BB89AD3/5B5B5E36" Ref="C613"  Part="1" 
AR Path="/5BB89AD7/5B5B5E36" Ref="C713"  Part="1" 
F 0 "C613" H 7585 3704 50  0000 R CNN
F 1 "C" H 7585 3795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 7738 3600 50  0001 C CNN
F 3 "~" H 7700 3750 50  0001 C CNN
	1    7700 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C215
U 1 1 5B5B5EEE
P 8300 3750
AR Path="/5B60A9BE/5B5B5EEE" Ref="C215"  Part="1" 
AR Path="/5B60D3AD/5B5B5EEE" Ref="C315"  Part="1" 
AR Path="/5BB89AD3/5B5B5EEE" Ref="C615"  Part="1" 
AR Path="/5BB89AD7/5B5B5EEE" Ref="C715"  Part="1" 
F 0 "C615" H 8185 3704 50  0000 R CNN
F 1 "C" H 8185 3795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 8338 3600 50  0001 C CNN
F 3 "~" H 8300 3750 50  0001 C CNN
	1    8300 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C216
U 1 1 5B5B5F30
P 8900 3750
AR Path="/5B60A9BE/5B5B5F30" Ref="C216"  Part="1" 
AR Path="/5B60D3AD/5B5B5F30" Ref="C316"  Part="1" 
AR Path="/5BB89AD3/5B5B5F30" Ref="C616"  Part="1" 
AR Path="/5BB89AD7/5B5B5F30" Ref="C716"  Part="1" 
F 0 "C616" H 8785 3704 50  0000 R CNN
F 1 "C" H 8785 3795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 8938 3600 50  0001 C CNN
F 3 "~" H 8900 3750 50  0001 C CNN
	1    8900 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:C C217
U 1 1 5B5B5F78
P 9350 3450
AR Path="/5B60A9BE/5B5B5F78" Ref="C217"  Part="1" 
AR Path="/5B60D3AD/5B5B5F78" Ref="C317"  Part="1" 
AR Path="/5BB89AD3/5B5B5F78" Ref="C617"  Part="1" 
AR Path="/5BB89AD7/5B5B5F78" Ref="C717"  Part="1" 
F 0 "C617" V 9602 3450 50  0000 C CNN
F 1 "C" V 9511 3450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 9388 3300 50  0001 C CNN
F 3 "~" H 9350 3450 50  0001 C CNN
	1    9350 3450
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5B635F
P 4500 4150
AR Path="/5B60A9BE/5B5B635F" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5B635F" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5B635F" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5B635F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4500 3900 50  0001 C CNN
F 1 "GND" H 4505 3977 50  0000 C CNN
F 2 "" H 4500 4150 50  0001 C CNN
F 3 "" H 4500 4150 50  0001 C CNN
	1    4500 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5B6390
P 3750 4150
AR Path="/5B60A9BE/5B5B6390" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5B6390" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5B6390" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5B6390" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3750 3900 50  0001 C CNN
F 1 "GND" H 3755 3977 50  0000 C CNN
F 2 "" H 3750 4150 50  0001 C CNN
F 3 "" H 3750 4150 50  0001 C CNN
	1    3750 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5B63C1
P 3050 4150
AR Path="/5B60A9BE/5B5B63C1" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5B63C1" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5B63C1" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5B63C1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3050 3900 50  0001 C CNN
F 1 "GND" H 3055 3977 50  0000 C CNN
F 2 "" H 3050 4150 50  0001 C CNN
F 3 "" H 3050 4150 50  0001 C CNN
	1    3050 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5B63F2
P 2350 4150
AR Path="/5B60A9BE/5B5B63F2" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5B63F2" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5B63F2" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5B63F2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2350 3900 50  0001 C CNN
F 1 "GND" H 2355 3977 50  0000 C CNN
F 2 "" H 2350 4150 50  0001 C CNN
F 3 "" H 2350 4150 50  0001 C CNN
	1    2350 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 3450 1750 3450
$Comp
L Device:L L202
U 1 1 5B5B65E3
P 3400 3450
AR Path="/5B60A9BE/5B5B65E3" Ref="L202"  Part="1" 
AR Path="/5B60D3AD/5B5B65E3" Ref="L302"  Part="1" 
AR Path="/5BB89AD3/5B5B65E3" Ref="L602"  Part="1" 
AR Path="/5BB89AD7/5B5B65E3" Ref="L702"  Part="1" 
F 0 "L602" V 3590 3450 50  0000 C CNN
F 1 "L" V 3499 3450 50  0000 C CNN
F 2 "Inductor_SMD:L_0201_0603Metric" H 3400 3450 50  0001 C CNN
F 3 "~" H 3400 3450 50  0001 C CNN
	1    3400 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L204
U 1 1 5B5B6639
P 7400 3450
AR Path="/5B60A9BE/5B5B6639" Ref="L204"  Part="1" 
AR Path="/5B60D3AD/5B5B6639" Ref="L304"  Part="1" 
AR Path="/5BB89AD3/5B5B6639" Ref="L604"  Part="1" 
AR Path="/5BB89AD7/5B5B6639" Ref="L704"  Part="1" 
F 0 "L604" V 7590 3450 50  0000 C CNN
F 1 "L" V 7499 3450 50  0000 C CNN
F 2 "Inductor_SMD:L_0201_0603Metric" H 7400 3450 50  0001 C CNN
F 3 "~" H 7400 3450 50  0001 C CNN
	1    7400 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L205
U 1 1 5B5B6747
P 8000 3450
AR Path="/5B60A9BE/5B5B6747" Ref="L205"  Part="1" 
AR Path="/5B60D3AD/5B5B6747" Ref="L305"  Part="1" 
AR Path="/5BB89AD3/5B5B6747" Ref="L605"  Part="1" 
AR Path="/5BB89AD7/5B5B6747" Ref="L705"  Part="1" 
F 0 "L605" V 8190 3450 50  0000 C CNN
F 1 "L" V 8099 3450 50  0000 C CNN
F 2 "Inductor_SMD:L_0201_0603Metric" H 8000 3450 50  0001 C CNN
F 3 "~" H 8000 3450 50  0001 C CNN
	1    8000 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L207
U 1 1 5B5B67AB
P 8600 3450
AR Path="/5B60A9BE/5B5B67AB" Ref="L207"  Part="1" 
AR Path="/5B60D3AD/5B5B67AB" Ref="L307"  Part="1" 
AR Path="/5BB89AD3/5B5B67AB" Ref="L607"  Part="1" 
AR Path="/5BB89AD7/5B5B67AB" Ref="L707"  Part="1" 
F 0 "L607" V 8790 3450 50  0000 C CNN
F 1 "L" V 8699 3450 50  0000 C CNN
F 2 "Inductor_SMD:L_0201_0603Metric" H 8600 3450 50  0001 C CNN
F 3 "~" H 8600 3450 50  0001 C CNN
	1    8600 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C210
U 1 1 5B5B67FD
P 6800 2600
AR Path="/5B60A9BE/5B5B67FD" Ref="C210"  Part="1" 
AR Path="/5B60D3AD/5B5B67FD" Ref="C310"  Part="1" 
AR Path="/5BB89AD3/5B5B67FD" Ref="C610"  Part="1" 
AR Path="/5BB89AD7/5B5B67FD" Ref="C710"  Part="1" 
F 0 "C610" H 6915 2646 50  0000 L CNN
F 1 "C" H 6915 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6838 2450 50  0001 C CNN
F 3 "~" H 6800 2600 50  0001 C CNN
	1    6800 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C212
U 1 1 5B5B6873
P 7350 2600
AR Path="/5B60A9BE/5B5B6873" Ref="C212"  Part="1" 
AR Path="/5B60D3AD/5B5B6873" Ref="C312"  Part="1" 
AR Path="/5BB89AD3/5B5B6873" Ref="C612"  Part="1" 
AR Path="/5BB89AD7/5B5B6873" Ref="C712"  Part="1" 
F 0 "C612" H 7465 2646 50  0000 L CNN
F 1 "C" H 7465 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7388 2450 50  0001 C CNN
F 3 "~" H 7350 2600 50  0001 C CNN
	1    7350 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C214
U 1 1 5B5B68CB
P 7850 2600
AR Path="/5B60A9BE/5B5B68CB" Ref="C214"  Part="1" 
AR Path="/5B60D3AD/5B5B68CB" Ref="C314"  Part="1" 
AR Path="/5BB89AD3/5B5B68CB" Ref="C614"  Part="1" 
AR Path="/5BB89AD7/5B5B68CB" Ref="C714"  Part="1" 
F 0 "C614" H 7965 2646 50  0000 L CNN
F 1 "C" H 7965 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7888 2450 50  0001 C CNN
F 3 "~" H 7850 2600 50  0001 C CNN
	1    7850 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:L L206
U 1 1 5B5B6ADD
P 8250 2300
AR Path="/5B60A9BE/5B5B6ADD" Ref="L206"  Part="1" 
AR Path="/5B60D3AD/5B5B6ADD" Ref="L306"  Part="1" 
AR Path="/5BB89AD3/5B5B6ADD" Ref="L606"  Part="1" 
AR Path="/5BB89AD7/5B5B6ADD" Ref="L706"  Part="1" 
F 0 "L606" V 8440 2300 50  0000 C CNN
F 1 "BEAD" V 8349 2300 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 8250 2300 50  0001 C CNN
F 3 "~" H 8250 2300 50  0001 C CNN
	1    8250 2300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5B6DC1
P 6250 4150
AR Path="/5B60A9BE/5B5B6DC1" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5B6DC1" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5B6DC1" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5B6DC1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6250 3900 50  0001 C CNN
F 1 "GND" H 6255 3977 50  0000 C CNN
F 2 "" H 6250 4150 50  0001 C CNN
F 3 "" H 6250 4150 50  0001 C CNN
	1    6250 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5B6E02
P 7050 4150
AR Path="/5B60A9BE/5B5B6E02" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5B6E02" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5B6E02" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5B6E02" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7050 3900 50  0001 C CNN
F 1 "GND" H 7055 3977 50  0000 C CNN
F 2 "" H 7050 4150 50  0001 C CNN
F 3 "" H 7050 4150 50  0001 C CNN
	1    7050 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5B6E43
P 7700 4150
AR Path="/5B60A9BE/5B5B6E43" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5B6E43" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5B6E43" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5B6E43" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7700 3900 50  0001 C CNN
F 1 "GND" H 7705 3977 50  0000 C CNN
F 2 "" H 7700 4150 50  0001 C CNN
F 3 "" H 7700 4150 50  0001 C CNN
	1    7700 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5B6E84
P 8300 4150
AR Path="/5B60A9BE/5B5B6E84" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5B6E84" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5B6E84" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5B6E84" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8300 3900 50  0001 C CNN
F 1 "GND" H 8305 3977 50  0000 C CNN
F 2 "" H 8300 4150 50  0001 C CNN
F 3 "" H 8300 4150 50  0001 C CNN
	1    8300 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5B6EC5
P 8900 4150
AR Path="/5B60A9BE/5B5B6EC5" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5B6EC5" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5B6EC5" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5B6EC5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8900 3900 50  0001 C CNN
F 1 "GND" H 8905 3977 50  0000 C CNN
F 2 "" H 8900 4150 50  0001 C CNN
F 3 "" H 8900 4150 50  0001 C CNN
	1    8900 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 3450 9500 3450
Wire Wire Line
	9200 3450 8900 3450
Wire Wire Line
	8900 3600 8900 3450
Connection ~ 8900 3450
Wire Wire Line
	8900 3450 8750 3450
Wire Wire Line
	8900 4150 8900 3900
Wire Wire Line
	8300 3600 8300 3450
Wire Wire Line
	8300 3450 8450 3450
Wire Wire Line
	8300 3900 8300 4150
Wire Wire Line
	8150 3450 8300 3450
Connection ~ 8300 3450
Wire Wire Line
	7700 4150 7700 3900
Wire Wire Line
	7050 3900 7050 4150
Wire Wire Line
	6250 4150 6250 3900
Wire Wire Line
	4500 3900 4500 4150
Wire Wire Line
	3750 4150 3750 3900
Wire Wire Line
	3050 3900 3050 4150
Wire Wire Line
	2350 4150 2350 3900
Wire Wire Line
	2350 3600 2350 3450
Wire Wire Line
	2350 3450 2050 3450
Wire Wire Line
	2550 3450 2350 3450
Connection ~ 2350 3450
Wire Wire Line
	3050 3600 3050 3450
Wire Wire Line
	3050 3450 2850 3450
Wire Wire Line
	3250 3450 3050 3450
Connection ~ 3050 3450
Wire Wire Line
	3750 3600 3750 3450
Wire Wire Line
	3750 3450 3550 3450
Wire Wire Line
	4000 3450 3750 3450
Connection ~ 3750 3450
Wire Wire Line
	4500 3600 4500 3450
Wire Wire Line
	4500 3450 4300 3450
Wire Wire Line
	4750 3450 4500 3450
Connection ~ 4500 3450
Wire Wire Line
	5200 3450 5050 3450
Wire Wire Line
	5950 3450 6250 3450
Wire Wire Line
	6250 3450 6250 3300
Wire Wire Line
	6250 3600 6250 3450
Connection ~ 6250 3450
Wire Wire Line
	6250 3450 6550 3450
Wire Wire Line
	6250 3000 6250 2300
Wire Wire Line
	6250 2300 6800 2300
Wire Wire Line
	6800 2300 6800 2450
$Comp
L power:GND #PWR?
U 1 1 5B5BB7F2
P 6800 2900
AR Path="/5B60A9BE/5B5BB7F2" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5BB7F2" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5BB7F2" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5BB7F2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6800 2650 50  0001 C CNN
F 1 "GND" H 6805 2727 50  0000 C CNN
F 2 "" H 6800 2900 50  0001 C CNN
F 3 "" H 6800 2900 50  0001 C CNN
	1    6800 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2900 6800 2750
Wire Wire Line
	7350 2450 7350 2300
Wire Wire Line
	7350 2300 6800 2300
Connection ~ 6800 2300
$Comp
L power:GND #PWR?
U 1 1 5B5BC921
P 7350 2900
AR Path="/5B60A9BE/5B5BC921" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5BC921" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5BC921" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5BC921" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7350 2650 50  0001 C CNN
F 1 "GND" H 7355 2727 50  0000 C CNN
F 2 "" H 7350 2900 50  0001 C CNN
F 3 "" H 7350 2900 50  0001 C CNN
	1    7350 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5BC962
P 7850 2900
AR Path="/5B60A9BE/5B5BC962" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5BC962" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5BC962" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5BC962" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7850 2650 50  0001 C CNN
F 1 "GND" H 7855 2727 50  0000 C CNN
F 2 "" H 7850 2900 50  0001 C CNN
F 3 "" H 7850 2900 50  0001 C CNN
	1    7850 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 2900 7850 2750
Wire Wire Line
	7350 2750 7350 2900
Wire Wire Line
	7850 2450 7850 2300
Wire Wire Line
	7850 2300 7350 2300
Connection ~ 7350 2300
Wire Wire Line
	8100 2300 7850 2300
Connection ~ 7850 2300
Wire Wire Line
	8400 2300 8750 2300
Wire Wire Line
	7250 3450 7050 3450
Wire Wire Line
	7050 3600 7050 3450
Connection ~ 7050 3450
Wire Wire Line
	7050 3450 6850 3450
Wire Wire Line
	7550 3450 7700 3450
Wire Wire Line
	7700 3450 7700 3600
Connection ~ 7700 3450
Wire Wire Line
	7700 3450 7850 3450
$Comp
L Connector:Conn_01x02_Male J201
U 1 1 5B5C3B1F
P 4550 2000
AR Path="/5B60A9BE/5B5C3B1F" Ref="J201"  Part="1" 
AR Path="/5B60D3AD/5B5C3B1F" Ref="J301"  Part="1" 
AR Path="/5BB89AD3/5B5C3B1F" Ref="J601"  Part="1" 
AR Path="/5BB89AD7/5B5C3B1F" Ref="J701"  Part="1" 
F 0 "J601" H 4656 2178 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4656 2087 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4550 2000 50  0001 C CNN
F 3 "~" H 4550 2000 50  0001 C CNN
	1    4550 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5C3BD6
P 5050 2250
AR Path="/5B60A9BE/5B5C3BD6" Ref="#PWR?"  Part="1" 
AR Path="/5B60D3AD/5B5C3BD6" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD3/5B5C3BD6" Ref="#PWR?"  Part="1" 
AR Path="/5BB89AD7/5B5C3BD6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5050 2000 50  0001 C CNN
F 1 "GND" H 5055 2077 50  0000 C CNN
F 2 "" H 5050 2250 50  0001 C CNN
F 3 "" H 5050 2250 50  0001 C CNN
	1    5050 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2250 5050 2100
Wire Wire Line
	5050 2100 4750 2100
Wire Wire Line
	4750 2000 5050 2000
Wire Wire Line
	5050 2000 5050 1850
Text HLabel 1400 3450 0    50   Input ~ 0
in
Text HLabel 9900 3450 2    50   Output ~ 0
out
Wire Wire Line
	5050 1850 8750 1850
Wire Wire Line
	8750 1850 8750 2300
Text Label 5250 1850 0    50   ~ 0
PA2_VCC
Text Label 6300 2300 0    50   ~ 0
PA2_VCC_filt
Text Label 2150 3450 0    50   ~ 0
PA2_RF_in_1
Text Label 2900 3450 0    50   ~ 0
PA2_RF_in_2
Text Label 3600 3450 0    50   ~ 0
PA2_RF_in_3
Text Label 4350 3450 0    50   ~ 0
PA2_RF_in_4
Text Label 5100 3450 1    50   ~ 0
PA2_RF_in_5
Text Label 6000 3450 0    50   ~ 0
PA2_RF_out_5
Text Label 6900 3450 0    50   ~ 0
PA2_RF_out_4
Text Label 7650 3450 1    50   ~ 0
PA2_RF_out_3
Text Label 8250 3450 1    50   ~ 0
PA2_RF_out_2
Text Label 8850 3450 1    50   ~ 0
PA2_RF_out_1
$Comp
L Connector:TestPoint TP201
U 1 1 5BBDE45D
P 6250 2050
AR Path="/5B60A9BE/5BBDE45D" Ref="TP201"  Part="1" 
AR Path="/5B60D3AD/5BBDE45D" Ref="TP301"  Part="1" 
AR Path="/5BB89AD3/5BBDE45D" Ref="TP601"  Part="1" 
AR Path="/5BB89AD7/5BBDE45D" Ref="TP701"  Part="1" 
F 0 "TP601" H 6308 2170 50  0000 L CNN
F 1 "TestPoint" H 6308 2079 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6450 2050 50  0001 C CNN
F 3 "~" H 6450 2050 50  0001 C CNN
	1    6250 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 2050 6250 2300
Connection ~ 6250 2300
$EndSCHEMATC
