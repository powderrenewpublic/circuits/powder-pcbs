EESchema Schematic File Version 4
LIBS:filter-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5B5560FB
P 2250 2350
F 0 "J1" H 2180 2588 50  0000 C CNN
F 1 "Conn_Coaxial" H 2180 2497 50  0000 C CNN
F 2 "rf:SMA_endlaunch_SD-73251-115" H 2250 2350 50  0001 C CNN
F 3 " ~" H 2250 2350 50  0001 C CNN
	1    2250 2350
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B556280
P 2250 2950
F 0 "#PWR?" H 2250 2700 50  0001 C CNN
F 1 "GND" H 2255 2777 50  0000 C CNN
F 2 "" H 2250 2950 50  0001 C CNN
F 3 "" H 2250 2950 50  0001 C CNN
	1    2250 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 2950 2250 2550
$Comp
L Device:EMI_Filter_CLC FL1
U 1 1 5B5577B1
P 3150 2450
F 0 "FL1" H 3150 2767 50  0000 C CNN
F 1 "EMI_Filter_CLC" H 3150 2676 50  0000 C CNN
F 2 "rf:FI168L2200G9-T_SS" V 3150 2450 50  0001 C CNN
F 3 "http://www.murata.com/~/media/webrenewal/support/library/catalog/products/emc/emifil/c31e.ashx?la=en-gb" V 3150 2450 50  0001 C CNN
	1    3150 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B55781F
P 3150 2950
F 0 "#PWR?" H 3150 2700 50  0001 C CNN
F 1 "GND" H 3155 2777 50  0000 C CNN
F 2 "" H 3150 2950 50  0001 C CNN
F 3 "" H 3150 2950 50  0001 C CNN
	1    3150 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2950 3150 2550
Wire Wire Line
	2450 2350 2850 2350
$Comp
L Connector:Conn_Coaxial J2
U 1 1 5B557841
P 3950 2350
F 0 "J2" H 4049 2326 50  0000 L CNN
F 1 "Conn_Coaxial" H 4049 2235 50  0000 L CNN
F 2 "rf:SMA_endlaunch_SD-73251-115" H 3950 2350 50  0001 C CNN
F 3 " ~" H 3950 2350 50  0001 C CNN
	1    3950 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B5578BB
P 3950 2950
F 0 "#PWR?" H 3950 2700 50  0001 C CNN
F 1 "GND" H 3955 2777 50  0000 C CNN
F 2 "" H 3950 2950 50  0001 C CNN
F 3 "" H 3950 2950 50  0001 C CNN
	1    3950 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2950 3950 2550
Wire Wire Line
	3750 2350 3450 2350
$EndSCHEMATC
