EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 39
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5D630FA1
P 1800 1600
AR Path="/5D630FA1" Ref="H?"  Part="1" 
AR Path="/5D614D04/5D630FA1" Ref="H101"  Part="1" 
AR Path="/5D99036F/5D630FA1" Ref="H?"  Part="1" 
F 0 "H101" H 1900 1649 50  0000 L CNN
F 1 "MountingHole_Pad" H 1900 1558 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 1800 1600 50  0001 C CNN
F 3 "~" H 1800 1600 50  0001 C CNN
	1    1800 1600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5D630FA7
P 2800 1600
AR Path="/5D630FA7" Ref="H?"  Part="1" 
AR Path="/5D614D04/5D630FA7" Ref="H102"  Part="1" 
AR Path="/5D99036F/5D630FA7" Ref="H?"  Part="1" 
F 0 "H102" H 2900 1649 50  0000 L CNN
F 1 "MountingHole_Pad" H 2900 1558 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 2800 1600 50  0001 C CNN
F 3 "~" H 2800 1600 50  0001 C CNN
	1    2800 1600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5D630FAD
P 3800 1600
AR Path="/5D630FAD" Ref="H?"  Part="1" 
AR Path="/5D614D04/5D630FAD" Ref="H103"  Part="1" 
AR Path="/5D99036F/5D630FAD" Ref="H?"  Part="1" 
F 0 "H103" H 3900 1649 50  0000 L CNN
F 1 "MountingHole_Pad" H 3900 1558 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 3800 1600 50  0001 C CNN
F 3 "~" H 3800 1600 50  0001 C CNN
	1    3800 1600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 5D630FB3
P 4800 1600
AR Path="/5D630FB3" Ref="H?"  Part="1" 
AR Path="/5D614D04/5D630FB3" Ref="H104"  Part="1" 
AR Path="/5D99036F/5D630FB3" Ref="H?"  Part="1" 
F 0 "H104" H 4900 1649 50  0000 L CNN
F 1 "MountingHole_Pad" H 4900 1558 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 4800 1600 50  0001 C CNN
F 3 "~" H 4800 1600 50  0001 C CNN
	1    4800 1600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D630FB9
P 1800 1800
AR Path="/5D630FB9" Ref="#PWR?"  Part="1" 
AR Path="/5D614D04/5D630FB9" Ref="#PWR?"  Part="1" 
AR Path="/5D99036F/5D630FB9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1800 1550 50  0001 C CNN
F 1 "GND" H 1805 1627 50  0000 C CNN
F 2 "" H 1800 1800 50  0001 C CNN
F 3 "" H 1800 1800 50  0001 C CNN
	1    1800 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1800 1800 1700
$Comp
L power:GND #PWR?
U 1 1 5D630FC0
P 2800 1800
AR Path="/5D630FC0" Ref="#PWR?"  Part="1" 
AR Path="/5D614D04/5D630FC0" Ref="#PWR?"  Part="1" 
AR Path="/5D99036F/5D630FC0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2800 1550 50  0001 C CNN
F 1 "GND" H 2805 1627 50  0000 C CNN
F 2 "" H 2800 1800 50  0001 C CNN
F 3 "" H 2800 1800 50  0001 C CNN
	1    2800 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 1800 2800 1700
$Comp
L power:GND #PWR?
U 1 1 5D630FC7
P 3800 1800
AR Path="/5D630FC7" Ref="#PWR?"  Part="1" 
AR Path="/5D614D04/5D630FC7" Ref="#PWR?"  Part="1" 
AR Path="/5D99036F/5D630FC7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3800 1550 50  0001 C CNN
F 1 "GND" H 3805 1627 50  0000 C CNN
F 2 "" H 3800 1800 50  0001 C CNN
F 3 "" H 3800 1800 50  0001 C CNN
	1    3800 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 1800 3800 1700
$Comp
L power:GND #PWR?
U 1 1 5D630FCE
P 4800 1800
AR Path="/5D630FCE" Ref="#PWR?"  Part="1" 
AR Path="/5D614D04/5D630FCE" Ref="#PWR?"  Part="1" 
AR Path="/5D99036F/5D630FCE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4800 1550 50  0001 C CNN
F 1 "GND" H 4805 1627 50  0000 C CNN
F 2 "" H 4800 1800 50  0001 C CNN
F 3 "" H 4800 1800 50  0001 C CNN
	1    4800 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1800 4800 1700
$Comp
L Mechanical:Fiducial FID801
U 1 1 5E2AB34E
P 5000 3500
F 0 "FID801" H 5000 3700 50  0000 C CNN
F 1 "Fiducial" H 5000 3625 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 5000 3500 50  0001 C CNN
F 3 "~" H 5000 3500 50  0001 C CNN
	1    5000 3500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID803
U 1 1 5E2AB683
P 5500 3500
F 0 "FID803" H 5500 3700 50  0000 C CNN
F 1 "Fiducial" H 5500 3625 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 5500 3500 50  0001 C CNN
F 3 "~" H 5500 3500 50  0001 C CNN
	1    5500 3500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID804
U 1 1 5E2AB772
P 5500 4000
F 0 "FID804" H 5500 4200 50  0000 C CNN
F 1 "Fiducial" H 5500 4125 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 5500 4000 50  0001 C CNN
F 3 "~" H 5500 4000 50  0001 C CNN
	1    5500 4000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID802
U 1 1 5E2AB890
P 5000 4000
F 0 "FID802" H 5000 4200 50  0000 C CNN
F 1 "Fiducial" H 5000 4125 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 5000 4000 50  0001 C CNN
F 3 "~" H 5000 4000 50  0001 C CNN
	1    5000 4000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID402
U 1 1 62C073A3
P 5500 4500
F 0 "FID402" H 5500 4700 50  0000 C CNN
F 1 "Fiducial" H 5500 4625 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 5500 4500 50  0001 C CNN
F 3 "~" H 5500 4500 50  0001 C CNN
	1    5500 4500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID401
U 1 1 62C073A9
P 5000 4500
F 0 "FID401" H 5000 4700 50  0000 C CNN
F 1 "Fiducial" H 5000 4625 50  0000 C CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 5000 4500 50  0001 C CNN
F 3 "~" H 5000 4500 50  0001 C CNN
	1    5000 4500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 62C0DD2E
P 6000 1550
AR Path="/62C0DD2E" Ref="H?"  Part="1" 
AR Path="/5D614D04/62C0DD2E" Ref="H401"  Part="1" 
AR Path="/5D99036F/62C0DD2E" Ref="H?"  Part="1" 
F 0 "H401" H 6100 1599 50  0000 L CNN
F 1 "MountingHole_Pad" H 6100 1508 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 6000 1550 50  0001 C CNN
F 3 "~" H 6000 1550 50  0001 C CNN
	1    6000 1550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 62C0DD34
P 7000 1550
AR Path="/62C0DD34" Ref="H?"  Part="1" 
AR Path="/5D614D04/62C0DD34" Ref="H402"  Part="1" 
AR Path="/5D99036F/62C0DD34" Ref="H?"  Part="1" 
F 0 "H402" H 7100 1599 50  0000 L CNN
F 1 "MountingHole_Pad" H 7100 1508 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 7000 1550 50  0001 C CNN
F 3 "~" H 7000 1550 50  0001 C CNN
	1    7000 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62C0DD3A
P 6000 1750
AR Path="/62C0DD3A" Ref="#PWR?"  Part="1" 
AR Path="/5D614D04/62C0DD3A" Ref="#PWR?"  Part="1" 
AR Path="/5D99036F/62C0DD3A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6000 1500 50  0001 C CNN
F 1 "GND" H 6005 1577 50  0000 C CNN
F 2 "" H 6000 1750 50  0001 C CNN
F 3 "" H 6000 1750 50  0001 C CNN
	1    6000 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1750 6000 1650
$Comp
L power:GND #PWR?
U 1 1 62C0DD41
P 7000 1750
AR Path="/62C0DD41" Ref="#PWR?"  Part="1" 
AR Path="/5D614D04/62C0DD41" Ref="#PWR?"  Part="1" 
AR Path="/5D99036F/62C0DD41" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7000 1500 50  0001 C CNN
F 1 "GND" H 7005 1577 50  0000 C CNN
F 2 "" H 7000 1750 50  0001 C CNN
F 3 "" H 7000 1750 50  0001 C CNN
	1    7000 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 1750 7000 1650
$EndSCHEMATC
