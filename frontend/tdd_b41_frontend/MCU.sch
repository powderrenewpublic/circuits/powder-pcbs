EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 15 39
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 6000 2500 650  500 
U 6083FA2C
F0 "microcontroller_ethernet" 50
F1 "microcontroller_ethernet.sch" 50
F2 "G_LED" I L 6000 2600 50 
F3 "R_LED" I L 6000 2700 50 
F4 "RST" I L 6000 2900 50 
$EndSheet
Wire Wire Line
	5500 2600 6000 2600
Wire Wire Line
	6000 2700 5500 2700
Wire Wire Line
	5500 2900 6000 2900
$Sheet
S 4500 6000 1000 850 
U 608510BE
F0 "microcontroller_power" 50
F1 "microcontroller_power.sch" 50
$EndSheet
Text HLabel 3900 2600 0    50   BiDi ~ 0
MCU_IO[0..35]
Text HLabel 3900 2700 0    50   Output ~ 0
EN_[0..3]
Text HLabel 3900 2800 0    50   Input ~ 0
PG_[0..3]
$Sheet
S 4000 2500 1500 1000
U 6083FA27
F0 "microcontroller_gpio" 50
F1 "microcontroller_gpio.sch" 50
F2 "LINK_LED" O R 5500 2600 50 
F3 "ACT_LED" O R 5500 2700 50 
F4 "RST" O R 5500 2900 50 
F5 "EPIOS[0..35]" B L 4000 2600 50 
F6 "EN_[0..3]" O L 4000 2700 50 
F7 "PG_[0..3]" I L 4000 2800 50 
F8 "PG_3_3" I L 4000 2900 50 
F9 "CONFIG_SEL" O L 4000 3100 50 
F10 "CRC_ERROR" I L 4000 3200 50 
F11 "~STATUS" I L 4000 3300 50 
F12 "CONF_DONE" I L 4000 3400 50 
$EndSheet
Wire Bus Line
	3900 2600 4000 2600
Wire Bus Line
	4000 2700 3900 2700
Wire Bus Line
	3900 2800 4000 2800
Text HLabel 3900 2900 0    50   Input ~ 0
PG_3_3
Wire Wire Line
	4000 2900 3900 2900
Text HLabel 3900 3100 0    50   Output ~ 0
CONFIG_SEL
Text HLabel 3900 3400 0    50   Input ~ 0
CRC_ERROR
Text HLabel 3900 3300 0    50   Input ~ 0
~STATUS
Text HLabel 3900 3200 0    50   Input ~ 0
CONF_DONE
Wire Wire Line
	3900 3100 4000 3100
Wire Wire Line
	4000 3200 3900 3200
Wire Wire Line
	3900 3300 4000 3300
Wire Wire Line
	4000 3400 3900 3400
$EndSCHEMATC
