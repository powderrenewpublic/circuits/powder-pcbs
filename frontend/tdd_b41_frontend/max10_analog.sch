EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 31 39
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L POWDER_Analog:REF34xx-Q1 U3101
U 1 1 5FB2E0FE
P 2150 3150
F 0 "U3101" H 1950 3450 50  0000 C CNN
F 1 "REF34xx-Q1" H 2450 3450 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 2150 3150 50  0001 C CNN
F 3 "" H 2150 3150 50  0001 C CNN
F 4 "REF3425-Q1" H 2150 3150 50  0001 C CNN "PartNumber"
F 5 "3000-0000001301" H 2150 3150 50  0001 C CNN "PartSpecification"
	1    2150 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 3250 3250 3250
Wire Wire Line
	3250 3250 3250 3150
Wire Wire Line
	3250 3150 3350 3150
$Comp
L Device:Ferrite_Bead_Small FB3103
U 1 1 5FB2ED6B
P 2150 2050
F 0 "FB3103" H 2225 2100 50  0000 L CNN
F 1 "Ferrite_Bead_Small" H 2225 2000 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric" V 2080 2050 50  0001 C CNN
F 3 "~" H 2150 2050 50  0001 C CNN
F 4 "BLM18SP601SH1D" H 2150 2050 50  0001 C CNN "PartNumber"
F 5 "3000-0000001272" H 2150 2050 50  0001 C CNN "PartSpecification"
	1    2150 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT3101
U 1 1 5FB2FFC8
P 3050 3150
F 0 "NT3101" H 3050 3200 50  0000 C CNN
F 1 "Net-Tie_2" H 3050 3100 50  0000 C CNN
F 2 "POWDER_Misc:NetTie-2_SMD_Pad0.25mm" H 3050 3150 50  0001 C CNN
F 3 "~" H 3050 3150 50  0001 C CNN
	1    3050 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3150 3250 3150
Connection ~ 3250 3150
Wire Wire Line
	2950 3150 2550 3150
$Comp
L Device:Net-Tie_2 NT3102
U 1 1 5FB30C5E
P 3150 3400
F 0 "NT3102" H 3150 3450 50  0000 C CNN
F 1 "Net-Tie_2" H 3150 3350 50  0000 C CNN
F 2 "POWDER_Misc:NetTie-2_SMD_Pad0.25mm" H 3150 3400 50  0001 C CNN
F 3 "~" H 3150 3400 50  0001 C CNN
	1    3150 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3250 3350 3400
Wire Wire Line
	3350 3400 3250 3400
Wire Wire Line
	2800 3400 3050 3400
Wire Wire Line
	3350 3700 3350 3400
Wire Wire Line
	2200 3700 3350 3700
Connection ~ 3350 3400
$Comp
L power:GND #PWR?
U 1 1 5FB3257A
P 2800 3500
F 0 "#PWR?" H 2800 3250 50  0001 C CNN
F 1 "GND" H 2800 3350 50  0000 C CNN
F 2 "" H 2800 3500 50  0001 C CNN
F 3 "" H 2800 3500 50  0001 C CNN
	1    2800 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 3500 2800 3400
$Comp
L power:GND #PWR?
U 1 1 5FB32DC4
P 2100 3800
F 0 "#PWR?" H 2100 3550 50  0001 C CNN
F 1 "GND" H 2100 3650 50  0000 C CNN
F 2 "" H 2100 3800 50  0001 C CNN
F 3 "" H 2100 3800 50  0001 C CNN
	1    2100 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3800 2100 3700
$Comp
L 000_PreferredParts:C_0_1uF C3106
U 1 1 5FB34691
P 1600 2450
F 0 "C3106" H 1625 2550 50  0000 L CNN
F 1 "C_0_1uF" H 1625 2350 50  0001 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1638 2300 50  0001 C CNN
F 3 "~" H 1600 2450 50  0001 C CNN
F 4 "0.1uF" H 1750 2350 50  0000 C CNN "DisplayValue"
F 5 "3000-0000000125" H 1600 2450 50  0001 C CNN "PartSpecification"
F 6 "100V" H 1600 2450 50  0001 C CNN "Voltage"
	1    1600 2450
	1    0    0    -1  
$EndComp
$Comp
L 000_PreferredParts:C_1nF C3105
U 1 1 5FB356FF
P 1300 2450
F 0 "C3105" H 1325 2550 50  0000 L CNN
F 1 "C_1nF" H 1325 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1338 2300 50  0001 C CNN
F 3 "~" H 1300 2450 50  0001 C CNN
F 4 "3000-0000000128" H 1300 2450 50  0001 C CNN "PartSpecification"
	1    1300 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2150 2150 2250
Wire Wire Line
	1600 2300 1600 2250
Wire Wire Line
	1600 2250 1850 2250
Connection ~ 2150 2250
Wire Wire Line
	2150 2250 2150 2800
Wire Wire Line
	1600 2250 1300 2250
Wire Wire Line
	1300 2250 1300 2300
Connection ~ 1600 2250
$Comp
L power:GND #PWR?
U 1 1 5FB36E66
P 1600 2700
F 0 "#PWR?" H 1600 2450 50  0001 C CNN
F 1 "GND" H 1600 2550 50  0000 C CNN
F 2 "" H 1600 2700 50  0001 C CNN
F 3 "" H 1600 2700 50  0001 C CNN
	1    1600 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FB37484
P 1300 2700
F 0 "#PWR?" H 1300 2450 50  0001 C CNN
F 1 "GND" H 1300 2550 50  0000 C CNN
F 2 "" H 1300 2700 50  0001 C CNN
F 3 "" H 1300 2700 50  0001 C CNN
	1    1300 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2700 1300 2600
Wire Wire Line
	1600 2700 1600 2600
$Comp
L power:+3.3V #PWR?
U 1 1 5FB385A6
P 2150 1850
F 0 "#PWR?" H 2150 1700 50  0001 C CNN
F 1 "+3.3V" H 2150 1990 50  0000 C CNN
F 2 "" H 2150 1850 50  0001 C CNN
F 3 "" H 2150 1850 50  0001 C CNN
	1    2150 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 1850 2150 1950
$Comp
L Device:Ferrite_Bead_Small FB3101
U 1 1 5FB3EB72
P 4150 1700
F 0 "FB3101" H 4225 1750 50  0000 L CNN
F 1 "Ferrite_Bead_Small" H 4225 1650 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric" V 4080 1700 50  0001 C CNN
F 3 "~" H 4150 1700 50  0001 C CNN
F 4 "BLM18SP601SH1D" H 4150 1700 50  0001 C CNN "PartNumber"
F 5 "3000-0000001272" H 4150 1700 50  0001 C CNN "PartSpecification"
	1    4150 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB3102
U 1 1 5FB3FBA4
P 4400 1900
F 0 "FB3102" H 4475 1950 50  0000 L CNN
F 1 "Ferrite_Bead_Small" H 4475 1850 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric" V 4330 1900 50  0001 C CNN
F 3 "~" H 4400 1900 50  0001 C CNN
F 4 "BLM18KG221SH1D" H 4400 1900 50  0001 C CNN "PartNumber"
	1    4400 1900
	1    0    0    -1  
$EndComp
$Comp
L POWDER_PowerGlobals:+2V5_A #PWR?
U 1 1 5FB4218D
P 4150 1500
AR Path="/5FB4218D" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9DEEB3/5FB4218D" Ref="#PWR?"  Part="1" 
AR Path="/605746B5/5F8E5420/5F9DEEB3/5FB4218D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4150 1350 50  0001 C CNN
F 1 "+2V5_A" H 4150 1640 50  0000 C CNN
F 2 "" H 4150 1500 50  0001 C CNN
F 3 "" H 4150 1500 50  0001 C CNN
	1    4150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1500 4150 1600
$Comp
L 000_PreferredParts:C_0_1uF C3102
U 1 1 5FB44D62
P 3600 2050
F 0 "C3102" H 3625 2150 50  0000 L CNN
F 1 "C_0_1uF" H 3625 1950 50  0001 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3638 1900 50  0001 C CNN
F 3 "~" H 3600 2050 50  0001 C CNN
F 4 "0.1uF" H 3750 1950 50  0000 C CNN "DisplayValue"
F 5 "3000-0000000125" H 3600 2050 50  0001 C CNN "PartSpecification"
F 6 "100V" H 3600 2050 50  0001 C CNN "Voltage"
	1    3600 2050
	1    0    0    -1  
$EndComp
$Comp
L 000_PreferredParts:C_1nF C3101
U 1 1 5FB44D69
P 3300 2050
F 0 "C3101" H 3325 2150 50  0000 L CNN
F 1 "C_1nF" H 3325 1950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3338 1900 50  0001 C CNN
F 3 "~" H 3300 2050 50  0001 C CNN
F 4 "3000-0000000128" H 3300 2050 50  0001 C CNN "PartSpecification"
	1    3300 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 1900 3600 1850
Wire Wire Line
	3600 1850 4150 1850
Wire Wire Line
	3600 1850 3300 1850
Wire Wire Line
	3300 1850 3300 1900
Connection ~ 3600 1850
$Comp
L power:GND #PWR?
U 1 1 5FB44D74
P 3600 2300
F 0 "#PWR?" H 3600 2050 50  0001 C CNN
F 1 "GND" H 3600 2150 50  0000 C CNN
F 2 "" H 3600 2300 50  0001 C CNN
F 3 "" H 3600 2300 50  0001 C CNN
	1    3600 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FB44D7A
P 3300 2300
F 0 "#PWR?" H 3300 2050 50  0001 C CNN
F 1 "GND" H 3300 2150 50  0000 C CNN
F 2 "" H 3300 2300 50  0001 C CNN
F 3 "" H 3300 2300 50  0001 C CNN
	1    3300 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2300 3300 2200
Wire Wire Line
	3600 2300 3600 2200
Wire Wire Line
	4150 1800 4150 1850
Connection ~ 4150 1850
Wire Wire Line
	4150 1850 4150 2350
$Comp
L power:+1V2 #PWR?
U 1 1 5FB46DAA
P 4400 1500
F 0 "#PWR?" H 4400 1350 50  0001 C CNN
F 1 "+1V2" H 4400 1640 50  0000 C CNN
F 2 "" H 4400 1500 50  0001 C CNN
F 3 "" H 4400 1500 50  0001 C CNN
	1    4400 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 1500 4400 1800
$Comp
L 000_PreferredParts:C_0_1uF C3103
U 1 1 5FB4D3A4
P 5150 2300
F 0 "C3103" H 5175 2400 50  0000 L CNN
F 1 "C_0_1uF" H 5175 2200 50  0001 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 2150 50  0001 C CNN
F 3 "~" H 5150 2300 50  0001 C CNN
F 4 "0.1uF" H 5300 2200 50  0000 C CNN "DisplayValue"
F 5 "3000-0000000125" H 5150 2300 50  0001 C CNN "PartSpecification"
F 6 "100V" H 5150 2300 50  0001 C CNN "Voltage"
	1    5150 2300
	-1   0    0    -1  
$EndComp
$Comp
L 000_PreferredParts:C_1nF C3104
U 1 1 5FB4D3AB
P 5450 2300
F 0 "C3104" H 5475 2400 50  0000 L CNN
F 1 "C_1nF" H 5475 2200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5488 2150 50  0001 C CNN
F 3 "~" H 5450 2300 50  0001 C CNN
F 4 "3000-0000000128" H 5450 2300 50  0001 C CNN "PartSpecification"
	1    5450 2300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5150 2150 5150 2100
Wire Wire Line
	5150 2100 5450 2100
Connection ~ 5150 2100
$Comp
L power:GND #PWR?
U 1 1 5FB4D3B6
P 5150 2550
F 0 "#PWR?" H 5150 2300 50  0001 C CNN
F 1 "GND" H 5150 2400 50  0000 C CNN
F 2 "" H 5150 2550 50  0001 C CNN
F 3 "" H 5150 2550 50  0001 C CNN
	1    5150 2550
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FB4D3BC
P 5450 2550
F 0 "#PWR?" H 5450 2300 50  0001 C CNN
F 1 "GND" H 5450 2400 50  0000 C CNN
F 2 "" H 5450 2550 50  0001 C CNN
F 3 "" H 5450 2550 50  0001 C CNN
	1    5450 2550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5450 2550 5450 2450
Wire Wire Line
	5150 2550 5150 2450
Wire Wire Line
	4400 2100 4400 2000
Wire Wire Line
	4400 2100 5150 2100
Wire Wire Line
	4400 2100 4400 2350
Connection ~ 4400 2100
$Comp
L power:GND #PWR?
U 1 1 5FB619B8
P 5150 3350
F 0 "#PWR?" H 5150 3100 50  0001 C CNN
F 1 "GND" H 5150 3200 50  0000 C CNN
F 2 "" H 5150 3350 50  0001 C CNN
F 3 "" H 5150 3350 50  0001 C CNN
	1    5150 3350
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FB619BE
P 5400 3350
F 0 "#PWR?" H 5400 3100 50  0001 C CNN
F 1 "GND" H 5400 3200 50  0000 C CNN
F 2 "" H 5400 3350 50  0001 C CNN
F 3 "" H 5400 3350 50  0001 C CNN
	1    5400 3350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5150 3350 5150 3250
$Comp
L POWDER_Digital:10M50DAF256 U2201
U 10 1 5F9F7C85
P 4250 3200
F 0 "U2201" H 4250 3350 50  0000 C CNN
F 1 "‎10M08DAF256C8G" H 4250 3200 50  0000 C CNN
F 2 "Package_BGA:BGA-256_17.0x17.0mm_Layout16x16_P1.0mm_Ball0.5mm_Pad0.4mm_NSMD" H 4250 3200 50  0001 C CNN
F 3 "" H 4250 3200 50  0001 C CNN
F 4 "3000-0000001300" H 4250 3200 50  0001 C CNN "PartSpecification"
	10   4250 3200
	1    0    0    -1  
$EndComp
Text Label 3300 3150 1    50   ~ 0
ADC_VREF
Text Label 3000 3700 0    50   ~ 0
REF_GND
Text Label 3650 1850 0    50   ~ 0
VCCA_ADC
Text Label 4600 2100 0    50   ~ 0
VCCINT
Text Label 2150 2450 0    50   ~ 0
VREF_IN
Text Label 2800 3150 1    50   ~ 0
ADC_VREF_F
Text Label 1550 3150 0    50   ~ 0
ref_en
Text HLabel 1500 3150 0    50   Input ~ 0
REF_EN
Wire Wire Line
	1500 3150 1750 3150
$Comp
L power:PWR_FLAG #FLG?
U 1 1 61311437
P 1850 2150
F 0 "#FLG?" H 1850 2225 50  0001 C CNN
F 1 "PWR_FLAG" H 1850 2323 50  0000 C CNN
F 2 "" H 1850 2150 50  0001 C CNN
F 3 "~" H 1850 2150 50  0001 C CNN
	1    1850 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2150 1850 2250
Connection ~ 1850 2250
Wire Wire Line
	1850 2250 2150 2250
Wire Wire Line
	5400 3150 5150 3150
Wire Wire Line
	5400 3150 5400 3350
$Comp
L power:PWR_FLAG #FLG?
U 1 1 6172358A
P 5450 2000
F 0 "#FLG?" H 5450 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 5450 2173 50  0000 C CNN
F 2 "" H 5450 2000 50  0001 C CNN
F 3 "~" H 5450 2000 50  0001 C CNN
	1    5450 2000
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 61723B2A
P 3300 1750
F 0 "#FLG?" H 3300 1825 50  0001 C CNN
F 1 "PWR_FLAG" H 3300 1923 50  0000 C CNN
F 2 "" H 3300 1750 50  0001 C CNN
F 3 "~" H 3300 1750 50  0001 C CNN
	1    3300 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 1750 3300 1850
Connection ~ 3300 1850
Wire Wire Line
	5450 2000 5450 2100
Connection ~ 5450 2100
Wire Wire Line
	5450 2100 5450 2150
$Comp
L 000_PreferredParts:C_10pF C3109
U 1 1 61E9E6D2
P 800 2450
F 0 "C3109" H 915 2496 50  0000 L CNN
F 1 "C_10pF" H 915 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 838 2300 50  0001 C CNN
F 3 "~" H 800 2450 50  0001 C CNN
F 4 "3000-0000000582" H 800 2450 50  0001 C CNN "PartSpecification"
	1    800  2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61E9EDAE
P 800 2700
F 0 "#PWR?" H 800 2450 50  0001 C CNN
F 1 "GND" H 800 2550 50  0000 C CNN
F 2 "" H 800 2700 50  0001 C CNN
F 3 "" H 800 2700 50  0001 C CNN
	1    800  2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  2700 800  2600
Wire Wire Line
	800  2300 800  2250
Wire Wire Line
	800  2250 1300 2250
Connection ~ 1300 2250
$Comp
L 000_PreferredParts:C_10pF C3107
U 1 1 61EA1073
P 2800 2050
F 0 "C3107" H 2915 2096 50  0000 L CNN
F 1 "C_10pF" H 2915 2005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2838 1900 50  0001 C CNN
F 3 "~" H 2800 2050 50  0001 C CNN
F 4 "3000-0000000582" H 2800 2050 50  0001 C CNN "PartSpecification"
	1    2800 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61EA1079
P 2800 2300
F 0 "#PWR?" H 2800 2050 50  0001 C CNN
F 1 "GND" H 2800 2150 50  0000 C CNN
F 2 "" H 2800 2300 50  0001 C CNN
F 3 "" H 2800 2300 50  0001 C CNN
	1    2800 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2300 2800 2200
Wire Wire Line
	2800 1900 2800 1850
Wire Wire Line
	2800 1850 3300 1850
$Comp
L 000_PreferredParts:C_10pF C3108
U 1 1 61EA26B1
P 5950 2300
F 0 "C3108" H 6065 2346 50  0000 L CNN
F 1 "C_10pF" H 6065 2255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5988 2150 50  0001 C CNN
F 3 "~" H 5950 2300 50  0001 C CNN
F 4 "3000-0000000582" H 5950 2300 50  0001 C CNN "PartSpecification"
	1    5950 2300
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61EA26B7
P 5950 2550
F 0 "#PWR?" H 5950 2300 50  0001 C CNN
F 1 "GND" H 5950 2400 50  0000 C CNN
F 2 "" H 5950 2550 50  0001 C CNN
F 3 "" H 5950 2550 50  0001 C CNN
	1    5950 2550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5950 2550 5950 2450
Wire Wire Line
	5950 2150 5950 2100
Wire Wire Line
	5950 2100 5450 2100
$EndSCHEMATC
