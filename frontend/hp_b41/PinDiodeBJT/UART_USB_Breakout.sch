EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x06 J10
U 1 1 6125BEC8
P 2700 3550
F 0 "J10" H 2618 3967 50  0000 C CNN
F 1 "Conn_01x06" H 2618 3876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 2700 3550 50  0001 C CNN
F 3 "~" H 2700 3550 50  0001 C CNN
	1    2700 3550
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J11
U 1 1 6125D9EE
P 4150 3950
F 0 "J11" V 4350 3850 50  0000 C CNN
F 1 "Conn_01x06" V 4250 3850 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4150 3950 50  0001 C CNN
F 3 "~" H 4150 3950 50  0001 C CNN
	1    4150 3950
	0    1    -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J12
U 1 1 6125E084
P 4150 3100
F 0 "J12" V 4250 3100 50  0000 C CNN
F 1 "Conn_01x06" V 4350 3100 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4150 3100 50  0001 C CNN
F 3 "~" H 4150 3100 50  0001 C CNN
	1    4150 3100
	0    1    -1   0   
$EndComp
NoConn ~ 3850 3300
NoConn ~ 3950 3300
NoConn ~ 4050 3300
NoConn ~ 4150 3300
NoConn ~ 4250 3300
NoConn ~ 4350 3300
NoConn ~ 3950 4150
NoConn ~ 4050 4150
NoConn ~ 4150 4150
NoConn ~ 4250 4150
NoConn ~ 4350 4150
Wire Wire Line
	2900 3750 3200 3750
Wire Wire Line
	2900 3650 3200 3650
Wire Wire Line
	2900 3350 3000 3350
Wire Wire Line
	3000 3350 3000 3950
$Comp
L power:GND #PWR?
U 1 1 6126829E
P 3000 3950
F 0 "#PWR?" H 3000 3700 50  0001 C CNN
F 1 "GND" H 3005 3777 50  0000 C CNN
F 2 "" H 3000 3950 50  0001 C CNN
F 3 "" H 3000 3950 50  0001 C CNN
	1    3000 3950
	1    0    0    -1  
$EndComp
Text HLabel 3200 3650 2    50   Output ~ 0
USB_TX
Text HLabel 3200 3750 2    50   Input ~ 0
USB_RX
Text Notes 3750 3600 0    50   ~ 0
Extra headers for mechanical support
NoConn ~ 2900 3550
Wire Wire Line
	2900 3450 3200 3450
Wire Wire Line
	2900 3850 3200 3850
Text HLabel 3200 3450 2    50   Input ~ 0
USB_CTS
Text HLabel 3200 3850 2    50   Output ~ 0
USB_DTR
Wire Wire Line
	3850 4150 3850 4350
Text GLabel 3850 4350 3    50   BiDi ~ 0
SER_GPIO
$EndSCHEMATC
