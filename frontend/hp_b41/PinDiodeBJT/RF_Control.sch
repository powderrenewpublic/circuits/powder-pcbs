EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4200 2900 1000 500 
U 61477C38
F0 "Pin_Diode_Bias_1" 50
F1 "Pin_Diode_Bias.sch" 50
F2 "bias_ctrl_a" I L 4200 3100 50 
F3 "bias_ctrl_b" I L 4200 3200 50 
F4 "bias_out" O R 5200 3150 50 
$EndSheet
$Sheet
S 4200 3700 1000 500 
U 61486372
F0 "Pin_Diode_Bias_2" 50
F1 "Pin_Diode_Bias.sch" 50
F2 "bias_ctrl_a" I L 4200 3900 50 
F3 "bias_ctrl_b" I L 4200 4000 50 
F4 "bias_out" O R 5200 3950 50 
$EndSheet
Wire Wire Line
	5200 3150 5300 3150
Wire Wire Line
	5200 3950 5400 3950
Text Notes 4000 2300 0    50   ~ 0
TODO: wishlist item would be to make an actual footprint for rf connection
$Comp
L Device:R R?
U 1 1 61493B34
P 3750 5950
AR Path="/61493B34" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61493B34" Ref="R27"  Part="1" 
F 0 "R27" V 3650 5950 50  0000 C CNN
F 1 "10k" V 3750 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3680 5950 50  0001 C CNN
F 3 "~" H 3750 5950 50  0001 C CNN
	1    3750 5950
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 61493B3A
P 4400 5150
AR Path="/61493B3A" Ref="#PWR?"  Part="1" 
AR Path="/6142C7A6/61493B3A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4400 5000 50  0001 C CNN
F 1 "+5V" H 4415 5323 50  0000 C CNN
F 2 "" H 4400 5150 50  0001 C CNN
F 3 "" H 4400 5150 50  0001 C CNN
	1    4400 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5550 4400 5650
Wire Wire Line
	4400 6150 4400 6250
Text Label 5000 5650 2    50   ~ 0
AMP_Disable
Text Notes 4800 5400 0    50   ~ 0
To PA DSUB Pin 5\n(PA has 20k pulldown resistance)
Connection ~ 4400 5650
Wire Wire Line
	4400 5650 4400 5750
$Comp
L power:GND #PWR?
U 1 1 61493B47
P 4400 6250
AR Path="/61493B47" Ref="#PWR?"  Part="1" 
AR Path="/6142C7A6/61493B47" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4400 6000 50  0001 C CNN
F 1 "GND" H 4405 6077 50  0000 C CNN
F 2 "" H 4400 6250 50  0001 C CNN
F 3 "" H 4400 6250 50  0001 C CNN
	1    4400 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 61493B4D
P 4400 5400
AR Path="/61493B4D" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61493B4D" Ref="R28"  Part="1" 
F 0 "R28" H 4500 5400 50  0000 C CNN
F 1 "1k" V 4400 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4330 5400 50  0001 C CNN
F 3 "~" H 4400 5400 50  0001 C CNN
	1    4400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5150 4400 5250
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 61493B54
P 4300 5950
AR Path="/61493B54" Ref="Q?"  Part="1" 
AR Path="/6142C7A6/61493B54" Ref="Q3"  Part="1" 
F 0 "Q3" H 4491 5996 50  0000 L CNN
F 1 "Q_NPN_BEC" H 4491 5905 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4500 6050 50  0001 C CNN
F 3 "~" H 4300 5950 50  0001 C CNN
	1    4300 5950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 61493B5A
P 5300 5650
AR Path="/61493B5A" Ref="J?"  Part="1" 
AR Path="/6142C7A6/61493B5A" Ref="J16"  Part="1" 
F 0 "J16" H 5218 5325 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 5218 5416 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5300 5650 50  0001 C CNN
F 3 "~" H 5300 5650 50  0001 C CNN
	1    5300 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5650 5100 5650
Wire Wire Line
	5100 5750 5000 5750
Wire Wire Line
	5000 5750 5000 5850
$Comp
L power:GND #PWR?
U 1 1 61493B63
P 5000 5850
AR Path="/61493B63" Ref="#PWR?"  Part="1" 
AR Path="/6142C7A6/61493B63" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5000 5600 50  0001 C CNN
F 1 "GND" H 5005 5677 50  0000 C CNN
F 2 "" H 5000 5850 50  0001 C CNN
F 3 "" H 5000 5850 50  0001 C CNN
	1    5000 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 5950 4000 5950
$Comp
L Device:D_Schottky D7
U 1 1 61498BF9
P 3750 5700
F 0 "D7" H 3750 5917 50  0000 C CNN
F 1 "D_Schottky" H 3750 5826 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123F" H 3750 5700 50  0001 C CNN
F 3 "~" H 3750 5700 50  0001 C CNN
	1    3750 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 5700 4000 5700
Wire Wire Line
	4000 5700 4000 5950
Connection ~ 4000 5950
Wire Wire Line
	4000 5950 4100 5950
Wire Wire Line
	3600 5700 3500 5700
Wire Wire Line
	3500 5700 3500 5950
Wire Wire Line
	3500 5950 3600 5950
Wire Wire Line
	1850 5050 1850 4850
Wire Wire Line
	1850 5350 1850 5550
$Comp
L power:GND #PWR?
U 1 1 616B7078
P 1850 5550
F 0 "#PWR?" H 1850 5300 50  0001 C CNN
F 1 "GND" H 1855 5377 50  0000 C CNN
F 2 "" H 1850 5550 50  0001 C CNN
F 3 "" H 1850 5550 50  0001 C CNN
	1    1850 5550
	1    0    0    -1  
$EndComp
$Sheet
S 7400 3600 1100 1300
U 6147CF8F
F0 "SDR_GPIO" 50
F1 "SDR_GPIO.sch" 50
F2 "SDR_GPIO[0..11]" B L 7400 4150 50 
$EndSheet
Wire Wire Line
	1950 6850 1850 6850
Wire Wire Line
	1850 6850 1850 7050
$Comp
L power:GND #PWR?
U 1 1 61498ED3
P 1850 7050
F 0 "#PWR?" H 1850 6800 50  0001 C CNN
F 1 "GND" H 1855 6877 50  0000 C CNN
F 2 "" H 1850 7050 50  0001 C CNN
F 3 "" H 1850 7050 50  0001 C CNN
	1    1850 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 6450 1850 6450
Wire Wire Line
	1850 6450 1850 6200
$Comp
L power:+3V3 #PWR?
U 1 1 61499E3D
P 1850 6200
F 0 "#PWR?" H 1850 6050 50  0001 C CNN
F 1 "+3V3" H 1865 6373 50  0000 C CNN
F 2 "" H 1850 6200 50  0001 C CNN
F 3 "" H 1850 6200 50  0001 C CNN
	1    1850 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 6750 1450 6750
Wire Wire Line
	1450 6650 1950 6650
Wire Wire Line
	1950 6550 1450 6550
$Comp
L Connector_Generic:Conn_01x05 J15
U 1 1 614A847B
P 2150 6650
F 0 "J15" H 2230 6692 50  0000 L CNN
F 1 "Conn_01x05" H 2230 6601 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 2150 6650 50  0001 C CNN
F 3 "~" H 2150 6650 50  0001 C CNN
	1    2150 6650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J17
U 1 1 614E6F5A
P 5800 3550
F 0 "J17" H 5718 3225 50  0000 C CNN
F 1 "Conn_01x03" H 5718 3316 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5800 3550 50  0001 C CNN
F 3 "~" H 5800 3550 50  0001 C CNN
	1    5800 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 614EC1E7
P 5500 3850
F 0 "#PWR?" H 5500 3600 50  0001 C CNN
F 1 "GND" H 5505 3677 50  0000 C CNN
F 2 "" H 5500 3850 50  0001 C CNN
F 3 "" H 5500 3850 50  0001 C CNN
	1    5500 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3950 5400 3650
Wire Bus Line
	7400 4150 7050 4150
Text GLabel 7050 4150 0    50   BiDi ~ 0
SDR_GPIO[0..11]
Text GLabel 1550 5250 0    50   Input ~ 0
RF_Power_Sense_en
Text GLabel 1550 5150 0    50   Output ~ 0
RF_Power_Sense
Text GLabel 1450 6750 0    50   Input ~ 0
Att_SI
Text GLabel 1450 6650 0    50   Input ~ 0
ATT_CLK
Text GLabel 1450 6550 0    50   Input ~ 0
ATT_LE
Text GLabel 4000 3100 0    50   Input ~ 0
FPGA_1A
Text GLabel 4000 3200 0    50   Input ~ 0
FPGA_1B
Text GLabel 4000 3900 0    50   Input ~ 0
FPGA_2A
Text GLabel 4000 4000 0    50   Input ~ 0
FPGA_2B
Text GLabel 3300 5950 0    50   Input ~ 0
AMP_EN
Wire Wire Line
	4000 3900 4200 3900
Wire Wire Line
	4000 4000 4200 4000
Wire Wire Line
	4000 3200 4200 3200
Wire Wire Line
	4000 3100 4200 3100
Wire Wire Line
	3500 5950 3300 5950
Connection ~ 3500 5950
$Comp
L power:+5V #PWR?
U 1 1 614BFA79
P 1850 4850
F 0 "#PWR?" H 1850 4700 50  0001 C CNN
F 1 "+5V" H 1865 5023 50  0000 C CNN
F 2 "" H 1850 4850 50  0001 C CNN
F 3 "" H 1850 4850 50  0001 C CNN
	1    1850 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 5350 1850 5350
Wire Wire Line
	1950 5250 1550 5250
Wire Wire Line
	1950 5150 1550 5150
Wire Wire Line
	1950 5050 1850 5050
$Comp
L Connector_Generic:Conn_01x04 J14
U 1 1 616B4CF9
P 2150 5150
F 0 "J14" H 2230 5142 50  0000 L CNN
F 1 "Conn_01x04" H 2230 5051 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2150 5150 50  0001 C CNN
F 3 "~" H 2150 5150 50  0001 C CNN
	1    2150 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3550 5600 3550
Wire Wire Line
	5500 3550 5500 3850
Wire Wire Line
	5300 3150 5300 3450
Wire Wire Line
	5400 3650 5600 3650
Wire Wire Line
	5300 3450 5600 3450
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 615A84EF
P 5300 6850
AR Path="/615A84EF" Ref="J?"  Part="1" 
AR Path="/6142C7A6/615A84EF" Ref="J13"  Part="1" 
F 0 "J13" H 5218 6525 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 5218 6616 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5300 6850 50  0001 C CNN
F 3 "~" H 5300 6850 50  0001 C CNN
	1    5300 6850
	1    0    0    -1  
$EndComp
Text GLabel 4900 6950 0    50   Output ~ 0
AMP_isense
Wire Wire Line
	4900 6850 5100 6850
Text GLabel 4900 6850 0    50   Output ~ 0
AMP_temp
Wire Wire Line
	4900 6950 5100 6950
$EndSCHEMATC
