EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 607146FC
P 6200 3050
AR Path="/607146FC" Ref="J?"  Part="1" 
AR Path="/6070220E/607146FC" Ref="J?"  Part="1" 
AR Path="/6127367A/607146FC" Ref="J3"  Part="1" 
AR Path="/6127CD30/607146FC" Ref="J4"  Part="1" 
F 0 "J4" H 6280 3092 50  0000 L CNN
F 1 "Conn_01x04" H 6280 3001 50  0000 L CNN
F 2 "Connector:FanPinHeader_1x04_P2.54mm_Vertical" H 6200 3050 50  0001 C CNN
F 3 "~" H 6200 3050 50  0001 C CNN
F 4 "Sullins Connector Solutions" H 6200 3050 50  0001 C CNN "Manufacturer"
F 5 "SWR25X-NRTC-S03-ST-BA" H 6200 3050 50  0001 C CNN "PartNumber"
F 6 "3000-0000000172" H 6200 3050 50  0001 C CNN "PartSpecification"
	1    6200 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60714702
P 3250 3150
AR Path="/60714702" Ref="#PWR?"  Part="1" 
AR Path="/6070220E/60714702" Ref="#PWR?"  Part="1" 
AR Path="/6127367A/60714702" Ref="#PWR?"  Part="1" 
AR Path="/6127CD30/60714702" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3250 2900 50  0001 C CNN
F 1 "GND" H 3255 2977 50  0000 C CNN
F 2 "" H 3250 3150 50  0001 C CNN
F 3 "" H 3250 3150 50  0001 C CNN
	1    3250 3150
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 60714708
P 3500 2850
AR Path="/60714708" Ref="#PWR?"  Part="1" 
AR Path="/6070220E/60714708" Ref="#PWR?"  Part="1" 
AR Path="/6127367A/60714708" Ref="#PWR?"  Part="1" 
AR Path="/6127CD30/60714708" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3500 2700 50  0001 C CNN
F 1 "+12V" H 3515 3023 50  0000 C CNN
F 2 "" H 3500 2850 50  0001 C CNN
F 3 "" H 3500 2850 50  0001 C CNN
	1    3500 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3150 4000 3150
$Comp
L Device:R R?
U 1 1 60714712
P 3500 3300
AR Path="/60714712" Ref="R?"  Part="1" 
AR Path="/6070220E/60714712" Ref="R?"  Part="1" 
AR Path="/6127367A/60714712" Ref="R1"  Part="1" 
AR Path="/6127CD30/60714712" Ref="R5"  Part="1" 
F 0 "R5" V 3293 3300 50  0000 C CNN
F 1 "10k" V 3384 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3430 3300 50  0001 C CNN
F 3 "~" H 3500 3300 50  0001 C CNN
F 4 "Delta Electronics/Cyntec" H 3500 3300 50  0001 C CNN "Manufacturer"
F 5 "PFR05S-103-FNH" H 3500 3300 50  0001 C CNN "PartNumber"
F 6 "3000-0000000131" H 3500 3300 50  0001 C CNN "PartSpecification"
	1    3500 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 6071471B
P 4000 3700
AR Path="/6071471B" Ref="R?"  Part="1" 
AR Path="/6070220E/6071471B" Ref="R?"  Part="1" 
AR Path="/6127367A/6071471B" Ref="R2"  Part="1" 
AR Path="/6127CD30/6071471B" Ref="R6"  Part="1" 
F 0 "R6" V 3793 3700 50  0000 C CNN
F 1 "1k" V 3884 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 3700 50  0001 C CNN
F 3 "~" H 4000 3700 50  0001 C CNN
F 4 "Delta Electronics/Cyntec" H 4000 3700 50  0001 C CNN "Manufacturer"
F 5 "PFR05S-102-FNH" H 4000 3700 50  0001 C CNN "PartNumber"
F 6 "3000-0000000156" H 4000 3700 50  0001 C CNN "PartSpecification"
	1    4000 3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 3150 4000 3500
$Comp
L Diode:BZX84Cxx D?
U 1 1 60714725
P 4000 4450
AR Path="/60714725" Ref="D?"  Part="1" 
AR Path="/6070220E/60714725" Ref="D?"  Part="1" 
AR Path="/6127367A/60714725" Ref="D1"  Part="1" 
AR Path="/6127CD30/60714725" Ref="D2"  Part="1" 
F 0 "D2" V 3954 4529 50  0000 L CNN
F 1 "BZX84Cxx" V 4045 4529 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" H 4000 4450 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzx84c2v4.pdf" H 4000 4450 50  0001 C CNN
F 4 "ON Semiconductor" H 4000 4450 50  0001 C CNN "Manufacturer"
F 5 "BZX84C2V4LT1G" H 4000 4450 50  0001 C CNN "PartNumber"
F 6 "3000-0000001270" H 4000 4450 50  0001 C CNN "PartSpecification"
	1    4000 4450
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 6071472E
P 4000 4050
AR Path="/6071472E" Ref="R?"  Part="1" 
AR Path="/6070220E/6071472E" Ref="R?"  Part="1" 
AR Path="/6127367A/6071472E" Ref="R3"  Part="1" 
AR Path="/6127CD30/6071472E" Ref="R7"  Part="1" 
F 0 "R7" V 3793 4050 50  0000 C CNN
F 1 "1k" V 3884 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 4050 50  0001 C CNN
F 3 "~" H 4000 4050 50  0001 C CNN
F 4 "Delta Electronics/Cyntec" H 4000 4050 50  0001 C CNN "Manufacturer"
F 5 "PFR05S-102-FNH" H 4000 4050 50  0001 C CNN "PartNumber"
F 6 "3000-0000000156" H 4000 4050 50  0001 C CNN "PartSpecification"
	1    4000 4050
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 4300 4000 4250
$Comp
L power:GND #PWR?
U 1 1 60714735
P 4000 4650
AR Path="/60714735" Ref="#PWR?"  Part="1" 
AR Path="/6070220E/60714735" Ref="#PWR?"  Part="1" 
AR Path="/6127367A/60714735" Ref="#PWR?"  Part="1" 
AR Path="/6127CD30/60714735" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4000 4400 50  0001 C CNN
F 1 "GND" H 4005 4477 50  0000 C CNN
F 2 "" H 4000 4650 50  0001 C CNN
F 3 "" H 4000 4650 50  0001 C CNN
	1    4000 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4650 4000 4600
Text Label 4000 3350 0    50   ~ 0
fan_tach_in
Text Label 4000 3900 2    50   ~ 0
fan_tach_mid
Wire Wire Line
	3500 2850 3500 3050
Wire Wire Line
	3250 2950 3250 3150
Wire Wire Line
	6000 3050 3500 3050
Wire Wire Line
	3250 2950 6000 2950
Wire Wire Line
	3500 3150 3500 3050
Connection ~ 3500 3050
Wire Wire Line
	3500 3450 3500 3500
Wire Wire Line
	3500 3500 4000 3500
Wire Wire Line
	4000 3900 4000 3850
Wire Wire Line
	4000 3550 4000 3500
Connection ~ 4000 3500
Connection ~ 4000 4250
Wire Wire Line
	4000 4250 4000 4200
Text HLabel 4500 4250 2    50   Output ~ 0
FAN_TACH
Wire Wire Line
	4000 4250 4500 4250
$Comp
L Device:Q_NPN_BEC Q1
U 1 1 60718C13
P 5650 3700
AR Path="/6127367A/60718C13" Ref="Q1"  Part="1" 
AR Path="/6127CD30/60718C13" Ref="Q2"  Part="1" 
F 0 "Q2" H 5841 3746 50  0000 L CNN
F 1 "Q_NPN_BEC" H 5841 3655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5850 3800 50  0001 C CNN
F 3 "~" H 5650 3700 50  0001 C CNN
F 4 "MMBT2222A" H 5650 3700 50  0001 C CNN "PartNumber"
F 5 "3000-0000001285" H 5650 3700 50  0001 C CNN "PartSpecification"
	1    5650 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60719A76
P 5750 4000
AR Path="/60719A76" Ref="#PWR?"  Part="1" 
AR Path="/6070220E/60719A76" Ref="#PWR?"  Part="1" 
AR Path="/6127367A/60719A76" Ref="#PWR?"  Part="1" 
AR Path="/6127CD30/60719A76" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5750 3750 50  0001 C CNN
F 1 "GND" H 5755 3827 50  0000 C CNN
F 2 "" H 5750 4000 50  0001 C CNN
F 3 "" H 5750 4000 50  0001 C CNN
	1    5750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4000 5750 3900
Wire Wire Line
	5750 3500 5750 3250
Wire Wire Line
	5750 3250 6000 3250
Text Label 5800 3250 0    50   ~ 0
pwm
$Comp
L Device:R R4
U 1 1 6071BA21
P 5200 3700
AR Path="/6127367A/6071BA21" Ref="R4"  Part="1" 
AR Path="/6127CD30/6071BA21" Ref="R8"  Part="1" 
F 0 "R8" V 4993 3700 50  0000 C CNN
F 1 "10k" V 5084 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5130 3700 50  0001 C CNN
F 3 "~" H 5200 3700 50  0001 C CNN
F 4 "3000-0000000131" H 5200 3700 50  0001 C CNN "PartSpecification"
	1    5200 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 3700 5450 3700
Text HLabel 5000 3700 0    50   Input ~ 0
FAN_PWM
Wire Wire Line
	5000 3700 5050 3700
Text Label 5450 3700 3    50   ~ 0
FAN_PWM_B
$EndSCHEMATC
