EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3600 4350 2000 1750
U 6144B829
F0 "Pin_Diode_Bias" 50
F1 "../Pin_Diode_Bias.sch" 50
F2 "bias_ctrl_a" I L 3600 4650 50 
F3 "bias_ctrl_b" I L 3600 5650 50 
F4 "bias_out" O R 5600 5050 50 
$EndSheet
Wire Wire Line
	5600 5050 6100 5050
Text Label 6100 5050 0    50   ~ 0
bias_out
$Comp
L Simulation_SPICE:VDC V3
U 1 1 6144DA47
P 4350 2300
F 0 "V3" H 4480 2391 50  0000 L CNN
F 1 "VDC" H 4480 2300 50  0000 L CNN
F 2 "" H 4350 2300 50  0001 C CNN
F 3 "~" H 4350 2300 50  0001 C CNN
F 4 "Y" H 4350 2300 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 4350 2300 50  0001 L CNN "Spice_Primitive"
F 6 "dc(5)" H 4480 2209 50  0000 L CNN "Spice_Model"
	1    4350 2300
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:VDC V4
U 1 1 6144E49C
P 4350 2900
F 0 "V4" H 4480 2991 50  0000 L CNN
F 1 "VDC" H 4480 2900 50  0000 L CNN
F 2 "" H 4350 2900 50  0001 C CNN
F 3 "~" H 4350 2900 50  0001 C CNN
F 4 "Y" H 4350 2900 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 4350 2900 50  0001 L CNN "Spice_Primitive"
F 6 "dc(50)" H 4480 2809 50  0000 L CNN "Spice_Model"
	1    4350 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2600 4850 2700
$Comp
L power:GND #PWR?
U 1 1 6144F75B
P 4850 2700
F 0 "#PWR?" H 4850 2450 50  0001 C CNN
F 1 "GND" H 4855 2527 50  0000 C CNN
F 2 "" H 4850 2700 50  0001 C CNN
F 3 "" H 4850 2700 50  0001 C CNN
	1    4850 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2500 4350 2600
Wire Wire Line
	4350 2600 4850 2600
Connection ~ 4350 2600
Wire Wire Line
	4350 2600 4350 2700
$Comp
L power:+5V #PWR?
U 1 1 61451152
P 4350 2000
F 0 "#PWR?" H 4350 1850 50  0001 C CNN
F 1 "+5V" H 4365 2173 50  0000 C CNN
F 2 "" H 4350 2000 50  0001 C CNN
F 3 "" H 4350 2000 50  0001 C CNN
	1    4350 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2000 4350 2100
Wire Wire Line
	4350 3100 4350 3200
$Comp
L power:VEE #PWR?
U 1 1 61451995
P 4350 3200
F 0 "#PWR?" H 4350 3050 50  0001 C CNN
F 1 "VEE" H 4365 3373 50  0000 C CNN
F 2 "" H 4350 3200 50  0001 C CNN
F 3 "" H 4350 3200 50  0001 C CNN
	1    4350 3200
	-1   0    0    1   
$EndComp
$Comp
L Simulation_SPICE:VPULSE V1
U 1 1 61453B5E
P 1450 4950
F 0 "V1" H 1580 5041 50  0000 L CNN
F 1 "VPULSE" H 1580 4950 50  0000 L CNN
F 2 "" H 1450 4950 50  0001 C CNN
F 3 "~" H 1450 4950 50  0001 C CNN
F 4 "Y" H 1450 4950 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 1450 4950 50  0001 L CNN "Spice_Primitive"
F 6 "pulse(0 3.3 2n 2n 2n 10u 20u)" H 1580 4859 50  0000 L CNN "Spice_Model"
	1    1450 4950
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:VPULSE V2
U 1 1 61454F3D
P 1450 5950
F 0 "V2" H 1580 6041 50  0000 L CNN
F 1 "VPULSE" H 1580 5950 50  0000 L CNN
F 2 "" H 1450 5950 50  0001 C CNN
F 3 "~" H 1450 5950 50  0001 C CNN
F 4 "Y" H 1450 5950 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 1450 5950 50  0001 L CNN "Spice_Primitive"
F 6 "pulse(0 3.3 2n 2n 2n 10u 20u)" H 1580 5859 50  0000 L CNN "Spice_Model"
	1    1450 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 614550F3
P 1450 6250
F 0 "#PWR?" H 1450 6000 50  0001 C CNN
F 1 "GND" H 1455 6077 50  0000 C CNN
F 2 "" H 1450 6250 50  0001 C CNN
F 3 "" H 1450 6250 50  0001 C CNN
	1    1450 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 6150 1450 6250
$Comp
L power:GND #PWR?
U 1 1 61455703
P 1450 5250
F 0 "#PWR?" H 1450 5000 50  0001 C CNN
F 1 "GND" H 1455 5077 50  0000 C CNN
F 2 "" H 1450 5250 50  0001 C CNN
F 3 "" H 1450 5250 50  0001 C CNN
	1    1450 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 5250 1450 5150
Wire Wire Line
	1450 5750 1450 5650
Wire Wire Line
	1450 5650 3600 5650
Wire Wire Line
	1450 4750 1450 4650
Wire Wire Line
	1450 4650 3600 4650
$Comp
L Simulation_SPICE:VDC V5
U 1 1 6146CA2A
P 3800 2300
F 0 "V5" H 3930 2391 50  0000 L CNN
F 1 "VDC" H 3930 2300 50  0000 L CNN
F 2 "" H 3800 2300 50  0001 C CNN
F 3 "~" H 3800 2300 50  0001 C CNN
F 4 "Y" H 3800 2300 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 3800 2300 50  0001 L CNN "Spice_Primitive"
F 6 "dc(3.3)" H 3930 2209 50  0000 L CNN "Spice_Model"
	1    3800 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2500 3800 2600
$Comp
L power:GND #PWR?
U 1 1 6146D0A3
P 3800 2600
F 0 "#PWR?" H 3800 2350 50  0001 C CNN
F 1 "GND" H 3805 2427 50  0000 C CNN
F 2 "" H 3800 2600 50  0001 C CNN
F 3 "" H 3800 2600 50  0001 C CNN
	1    3800 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2100 3800 2000
$Comp
L power:+3V3 #PWR?
U 1 1 6146D88A
P 3800 2000
F 0 "#PWR?" H 3800 1850 50  0001 C CNN
F 1 "+3V3" H 3815 2173 50  0000 C CNN
F 2 "" H 3800 2000 50  0001 C CNN
F 3 "" H 3800 2000 50  0001 C CNN
	1    3800 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6147D148
P 8250 4150
AR Path="/6142C7A6/6147D148" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61477C38/6147D148" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61486372/6147D148" Ref="R?"  Part="1" 
AR Path="/6144B829/6147D148" Ref="R?"  Part="1" 
AR Path="/6147D148" Ref="R12"  Part="1" 
F 0 "R12" V 8150 4150 50  0000 C CNN
F 1 "10k" V 8250 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8180 4150 50  0001 C CNN
F 3 "~" H 8250 4150 50  0001 C CNN
F 4 "R" H 8250 4150 50  0001 C CNN "Spice_Primitive"
F 5 "10k" H 8250 4150 50  0001 C CNN "Spice_Model"
F 6 "Y" H 8250 4150 50  0001 C CNN "Spice_Netlist_Enabled"
	1    8250 4150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 6147D154
P 8650 2200
AR Path="/6142C7A6/6147D154" Ref="#PWR?"  Part="1" 
AR Path="/6142C7A6/61477C38/6147D154" Ref="#PWR?"  Part="1" 
AR Path="/6142C7A6/61486372/6147D154" Ref="#PWR?"  Part="1" 
AR Path="/6144B829/6147D154" Ref="#PWR?"  Part="1" 
AR Path="/6147D154" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8650 2050 50  0001 C CNN
F 1 "+5V" H 8665 2373 50  0000 C CNN
F 2 "" H 8650 2200 50  0001 C CNN
F 3 "" H 8650 2200 50  0001 C CNN
	1    8650 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3000 8650 3200
$Comp
L power:GND #PWR?
U 1 1 6147D15C
P 7900 4200
AR Path="/6142C7A6/6147D15C" Ref="#PWR?"  Part="1" 
AR Path="/6142C7A6/61477C38/6147D15C" Ref="#PWR?"  Part="1" 
AR Path="/6142C7A6/61486372/6147D15C" Ref="#PWR?"  Part="1" 
AR Path="/6144B829/6147D15C" Ref="#PWR?"  Part="1" 
AR Path="/6147D15C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7900 3950 50  0001 C CNN
F 1 "GND" H 7905 4027 50  0000 C CNN
F 2 "" H 7900 4200 50  0001 C CNN
F 3 "" H 7900 4200 50  0001 C CNN
	1    7900 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6147D165
P 8650 3350
AR Path="/6142C7A6/6147D165" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61477C38/6147D165" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61486372/6147D165" Ref="R?"  Part="1" 
AR Path="/6144B829/6147D165" Ref="R?"  Part="1" 
AR Path="/6147D165" Ref="R13"  Part="1" 
F 0 "R13" H 8500 3350 50  0000 L CNN
F 1 "160" V 8650 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 8580 3350 50  0001 C CNN
F 3 "~" H 8650 3350 50  0001 C CNN
F 4 "R" H 8650 3350 50  0001 C CNN "Spice_Primitive"
F 5 "160" H 8650 3350 50  0001 C CNN "Spice_Model"
F 6 "Y" H 8650 3350 50  0001 C CNN "Spice_Netlist_Enabled"
	1    8650 3350
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 6147D17A
P 8550 3900
AR Path="/6142C7A6/6147D17A" Ref="Q?"  Part="1" 
AR Path="/6142C7A6/61477C38/6147D17A" Ref="Q?"  Part="1" 
AR Path="/6142C7A6/61486372/6147D17A" Ref="Q?"  Part="1" 
AR Path="/6144B829/6147D17A" Ref="Q?"  Part="1" 
AR Path="/6147D17A" Ref="Q7"  Part="1" 
F 0 "Q7" H 8741 3946 50  0000 L CNN
F 1 "FMMT6517" H 8741 3855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8750 4000 50  0001 C CNN
F 3 "~" H 8550 3900 50  0001 C CNN
F 4 "Q" H 8550 3900 50  0001 C CNN "Spice_Primitive"
F 5 "FMMT6517" H 8550 3900 50  0001 C CNN "Spice_Model"
F 6 "Y" H 8550 3900 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "spice_models.lib" H 8550 3900 50  0001 C CNN "Spice_Lib_File"
F 8 "3,1,2" H 8550 3900 50  0001 C CNN "Spice_Node_Sequence"
	1    8550 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PNP_BEC Q?
U 1 1 6147D185
P 8550 2800
AR Path="/6142C7A6/6147D185" Ref="Q?"  Part="1" 
AR Path="/6142C7A6/61477C38/6147D185" Ref="Q?"  Part="1" 
AR Path="/6142C7A6/61486372/6147D185" Ref="Q?"  Part="1" 
AR Path="/6144B829/6147D185" Ref="Q?"  Part="1" 
AR Path="/6147D185" Ref="Q6"  Part="1" 
F 0 "Q6" H 8740 2754 50  0000 L CNN
F 1 "FMMT597" H 8740 2845 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8750 2900 50  0001 C CNN
F 3 "~" H 8550 2800 50  0001 C CNN
F 4 "Q" H 8550 2800 50  0001 C CNN "Spice_Primitive"
F 5 "FMMT597" H 8550 2800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 8550 2800 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "spice_models.lib" H 8550 2800 50  0001 C CNN "Spice_Lib_File"
F 8 "3,1,2" H 8550 2800 50  0001 C CNN "Spice_Node_Sequence"
	1    8550 2800
	1    0    0    1   
$EndComp
Text Label 8650 3100 0    50   ~ 0
a
Wire Wire Line
	8650 3500 8650 3600
Wire Wire Line
	8650 3600 9550 3600
Connection ~ 8650 3600
Wire Wire Line
	8650 3600 8650 3700
$Comp
L power:VEE #PWR?
U 1 1 6148108C
P 8650 4500
F 0 "#PWR?" H 8650 4350 50  0001 C CNN
F 1 "VEE" H 8665 4673 50  0000 C CNN
F 2 "" H 8650 4500 50  0001 C CNN
F 3 "" H 8650 4500 50  0001 C CNN
	1    8650 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	8350 3900 8250 3900
Wire Wire Line
	8250 3900 8250 4000
Wire Wire Line
	8650 2200 8650 2300
$Comp
L Device:R R?
U 1 1 614854CA
P 8250 2550
AR Path="/6142C7A6/614854CA" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61477C38/614854CA" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61486372/614854CA" Ref="R?"  Part="1" 
AR Path="/6144B829/614854CA" Ref="R?"  Part="1" 
AR Path="/614854CA" Ref="R10"  Part="1" 
F 0 "R10" V 8150 2550 50  0000 C CNN
F 1 "10k" V 8250 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8180 2550 50  0001 C CNN
F 3 "~" H 8250 2550 50  0001 C CNN
F 4 "R" H 8250 2550 50  0001 C CNN "Spice_Primitive"
F 5 "10k" H 8250 2550 50  0001 C CNN "Spice_Model"
F 6 "Y" H 8250 2550 50  0001 C CNN "Spice_Netlist_Enabled"
	1    8250 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	8250 2700 8250 2800
Wire Wire Line
	8250 2800 8350 2800
Wire Wire Line
	8250 2400 8250 2300
Wire Wire Line
	8250 2300 8650 2300
Connection ~ 8650 2300
Wire Wire Line
	8650 2300 8650 2600
Wire Wire Line
	8250 4300 8250 4400
Wire Wire Line
	8250 4400 8650 4400
Wire Wire Line
	8650 4500 8650 4400
Connection ~ 8650 4400
Wire Wire Line
	8650 4100 8650 4400
$Comp
L Device:Q_PNP_BEC Q?
U 1 1 6148A472
P 7900 4000
AR Path="/6142C7A6/6148A472" Ref="Q?"  Part="1" 
AR Path="/6142C7A6/61477C38/6148A472" Ref="Q?"  Part="1" 
AR Path="/6142C7A6/61486372/6148A472" Ref="Q?"  Part="1" 
AR Path="/6144B829/6148A472" Ref="Q?"  Part="1" 
AR Path="/6148A472" Ref="Q5"  Part="1" 
F 0 "Q5" H 8090 3954 50  0000 L CNN
F 1 "FMMT597" H 8090 4045 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8100 4100 50  0001 C CNN
F 3 "~" H 7900 4000 50  0001 C CNN
F 4 "Q" H 7900 4000 50  0001 C CNN "Spice_Primitive"
F 5 "FMMT597" H 7900 4000 50  0001 C CNN "Spice_Model"
F 6 "Y" H 7900 4000 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "spice_models.lib" H 7900 4000 50  0001 C CNN "Spice_Lib_File"
F 8 "3,1,2" H 7900 4000 50  0001 C CNN "Spice_Node_Sequence"
	1    7900 4000
	0    1    -1   0   
$EndComp
Wire Wire Line
	8100 3900 8250 3900
Connection ~ 8250 3900
$Comp
L Device:R R?
U 1 1 6148BB3C
P 8250 3350
AR Path="/6142C7A6/6148BB3C" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61477C38/6148BB3C" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61486372/6148BB3C" Ref="R?"  Part="1" 
AR Path="/6144B829/6148BB3C" Ref="R?"  Part="1" 
AR Path="/6148BB3C" Ref="R11"  Part="1" 
F 0 "R11" V 8150 3350 50  0000 C CNN
F 1 "10k" V 8250 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8180 3350 50  0001 C CNN
F 3 "~" H 8250 3350 50  0001 C CNN
F 4 "R" H 8250 3350 50  0001 C CNN "Spice_Primitive"
F 5 "10k" H 8250 3350 50  0001 C CNN "Spice_Model"
F 6 "Y" H 8250 3350 50  0001 C CNN "Spice_Netlist_Enabled"
	1    8250 3350
	-1   0    0    1   
$EndComp
Wire Wire Line
	8250 2800 8250 3000
Connection ~ 8250 2800
$Comp
L Device:R R?
U 1 1 6148ECFB
P 7350 4050
AR Path="/6142C7A6/6148ECFB" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61477C38/6148ECFB" Ref="R?"  Part="1" 
AR Path="/6142C7A6/61486372/6148ECFB" Ref="R?"  Part="1" 
AR Path="/6144B829/6148ECFB" Ref="R?"  Part="1" 
AR Path="/6148ECFB" Ref="R9"  Part="1" 
F 0 "R9" V 7250 4050 50  0000 C CNN
F 1 "10k" V 7350 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7280 4050 50  0001 C CNN
F 3 "~" H 7350 4050 50  0001 C CNN
F 4 "R" H 7350 4050 50  0001 C CNN "Spice_Primitive"
F 5 "10k" H 7350 4050 50  0001 C CNN "Spice_Model"
F 6 "Y" H 7350 4050 50  0001 C CNN "Spice_Netlist_Enabled"
	1    7350 4050
	-1   0    0    1   
$EndComp
Wire Wire Line
	7400 3900 7400 3500
Wire Wire Line
	7400 3500 7150 3500
Connection ~ 7400 3500
Text Label 2750 4650 0    50   ~ 0
ctrl_a
Text Label 6900 3500 0    50   ~ 0
ctrl_a
Text Label 9550 3600 0    50   ~ 0
bias_out2
$Comp
L Device:C C1
U 1 1 614945E2
P 8250 3750
F 0 "C1" H 8365 3796 50  0000 L CNN
F 1 "10n" H 8365 3705 50  0000 L CNN
F 2 "" H 8288 3600 50  0001 C CNN
F 3 "~" H 8250 3750 50  0001 C CNN
F 4 "C" H 8250 3750 50  0001 C CNN "Spice_Primitive"
F 5 "10n" H 8250 3750 50  0001 C CNN "Spice_Model"
F 6 "N" H 8250 3750 50  0001 C CNN "Spice_Netlist_Enabled"
	1    8250 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 3600 8250 3500
Connection ~ 8250 3500
Text Label 8250 3950 0    50   ~ 0
aaa
$Comp
L Device:C C2
U 1 1 614963D1
P 7150 3750
F 0 "C2" H 7265 3796 50  0000 L CNN
F 1 "100n" H 7265 3705 50  0000 L CNN
F 2 "" H 7188 3600 50  0001 C CNN
F 3 "~" H 7150 3750 50  0001 C CNN
F 4 "C" H 7150 3750 50  0001 C CNN "Spice_Primitive"
F 5 "100n" H 7150 3750 50  0001 C CNN "Spice_Model"
F 6 "N" H 7150 3750 50  0001 C CNN "Spice_Netlist_Enabled"
	1    7150 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3500 7150 3600
Connection ~ 7150 3500
Wire Wire Line
	7150 3500 6900 3500
Wire Wire Line
	7700 4200 7350 4200
Wire Wire Line
	7150 3900 7150 4200
Wire Wire Line
	7700 3900 7700 4200
Connection ~ 7350 4200
Wire Wire Line
	7350 4200 7150 4200
Wire Wire Line
	7400 3900 7350 3900
Wire Wire Line
	7700 3200 8050 3200
Wire Wire Line
	8050 3200 8050 3000
Wire Wire Line
	8050 3000 8250 3000
Connection ~ 8250 3000
Wire Wire Line
	8250 3000 8250 3200
Wire Wire Line
	7400 3500 7700 3500
$Comp
L Device:C C3
U 1 1 6149CC01
P 7700 3350
F 0 "C3" H 7815 3396 50  0000 L CNN
F 1 "100n" H 7815 3305 50  0000 L CNN
F 2 "" H 7738 3200 50  0001 C CNN
F 3 "~" H 7700 3350 50  0001 C CNN
F 4 "C" H 7700 3350 50  0001 C CNN "Spice_Primitive"
F 5 "100n" H 7700 3350 50  0001 C CNN "Spice_Model"
F 6 "N" H 7700 3350 50  0001 C CNN "Spice_Netlist_Enabled"
	1    7700 3350
	1    0    0    -1  
$EndComp
Connection ~ 7700 3500
Wire Wire Line
	7700 3500 8250 3500
Text Label 7500 4200 0    50   ~ 0
ve
$EndSCHEMATC
