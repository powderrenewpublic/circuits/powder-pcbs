EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4850 5050 700  500 
U 6127367A
F0 "Fan1" 50
F1 "fan.sch" 50
F2 "FAN_TACH" O L 4850 5200 50 
F3 "FAN_PWM" I L 4850 5350 50 
$EndSheet
$Sheet
S 4850 5800 700  500 
U 6127CD30
F0 "Fan2" 50
F1 "fan.sch" 50
F2 "FAN_TACH" O L 4850 5950 50 
F3 "FAN_PWM" I L 4850 6100 50 
$EndSheet
$Sheet
S 8950 4100 850  700 
U 612ADC7E
F0 "vreg_lna" 50
F1 "vreg_lna.sch" 50
F2 "EN" I L 8950 4250 50 
F3 "V_OUT" O L 8950 4450 50 
F4 "LDO_PG" O L 8950 4600 50 
$EndSheet
$Sheet
S 2650 4150 750  500 
U 614BD53A
F0 "Power_In" 50
F1 "Power_In.sch" 50
$EndSheet
$Sheet
S 2200 5700 950  700 
U 614F83C4
F0 "clock_in" 50
F1 "clock_in.sch" 50
F2 "PPS" O R 3150 5900 50 
F3 "10MHz_p" O R 3150 6100 50 
F4 "10MHz_n" O R 3150 6200 50 
$EndSheet
Wire Wire Line
	8950 4450 7000 4450
Wire Wire Line
	7450 2750 7300 2750
Wire Wire Line
	7450 2650 7250 2650
Wire Wire Line
	7250 2650 7250 2500
$Comp
L power:+5V #PWR?
U 1 1 612C7750
P 7250 2500
F 0 "#PWR?" H 7250 2350 50  0001 C CNN
F 1 "+5V" H 7265 2673 50  0000 C CNN
F 2 "" H 7250 2500 50  0001 C CNN
F 3 "" H 7250 2500 50  0001 C CNN
	1    7250 2500
	-1   0    0    -1  
$EndComp
Text Notes 6650 3400 0    50   ~ 0
DE10 has 1/2 voltage divider on analog frontend\nUsing 5v on temperature sensor gives 0-2.5v on analog input
Wire Wire Line
	3150 5900 3400 5900
Wire Wire Line
	3150 6100 3400 6100
Text Label 3400 5900 0    50   ~ 0
PPS
Text Label 3400 6100 0    50   ~ 0
10MHz_p
Wire Wire Line
	2900 1400 3200 1400
Text Label 3200 1400 0    50   ~ 0
PPS
Wire Wire Line
	2900 1500 3200 1500
Text Label 3200 1500 0    50   ~ 0
10MHz_p
Text Label 4400 5200 0    50   ~ 0
Fan1_TACH
Wire Wire Line
	4400 5200 4850 5200
Wire Wire Line
	4850 5350 4400 5350
Text Label 4400 5350 0    50   ~ 0
Fan1_PWM
Wire Wire Line
	4850 5950 4400 5950
Wire Wire Line
	4400 6100 4850 6100
Text Label 4400 5950 0    50   ~ 0
Fan2_TACH
Text Label 4400 6100 0    50   ~ 0
Fan2_PWM
Text Label 3350 1700 2    50   ~ 0
Fan1_TACH
Wire Wire Line
	3350 1700 2900 1700
Wire Wire Line
	2900 1800 3350 1800
Text Label 3350 1800 2    50   ~ 0
Fan1_PWM
Wire Wire Line
	2900 1900 3350 1900
Wire Wire Line
	3350 2000 2900 2000
Text Label 3350 1900 2    50   ~ 0
Fan2_TACH
Text Label 3350 2000 2    50   ~ 0
Fan2_PWM
Text Label 3050 2200 0    50   ~ 0
Temperature_Sense
Wire Wire Line
	2900 2200 3050 2200
Text Label 6600 1100 0    50   ~ 0
USB_TX
Text Label 6600 1200 0    50   ~ 0
USB_RX
Text Label 6600 1300 0    50   ~ 0
USB_CTS
Text Label 6600 1400 0    50   ~ 0
USB_DTR
Wire Wire Line
	6600 1400 7000 1400
Wire Wire Line
	6600 1300 7000 1300
Wire Wire Line
	6600 1200 7000 1200
Wire Wire Line
	6600 1100 7000 1100
$Sheet
S 7000 1000 1000 500 
U 6125A126
F0 "UART_USB_Breakout" 50
F1 "UART_USB_Breakout.sch" 50
F2 "USB_TX" O L 7000 1100 50 
F3 "USB_RX" I L 7000 1200 50 
F4 "USB_CTS" I L 7000 1300 50 
F5 "USB_DTR" O L 7000 1400 50 
$EndSheet
Wire Wire Line
	1450 1400 1050 1400
Wire Wire Line
	1050 1500 1450 1500
Wire Wire Line
	1450 1600 1050 1600
Wire Wire Line
	1050 1700 1450 1700
Text Label 1050 1400 0    50   ~ 0
USB_TX
Text Label 1050 1500 0    50   ~ 0
USB_RX
Text Label 1050 1600 0    50   ~ 0
USB_CTS
Text Label 1050 1700 0    50   ~ 0
USB_DTR
Text Label 3400 2950 2    50   ~ 0
LNA_PWR_EN
Wire Wire Line
	2900 2950 3400 2950
Text Label 6400 4250 0    50   ~ 0
LNA_PWR_EN
Wire Wire Line
	8950 4250 6400 4250
Wire Wire Line
	1450 1900 1050 1900
Text Label 1050 1900 0    50   ~ 0
AMP_EN
$Sheet
S 1300 4400 700  500 
U 6142C7A6
F0 "RF_Control" 50
F1 "RF_Control.sch" 50
$EndSheet
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 614F14B7
P 6200 4550
F 0 "J1" H 6118 4225 50  0000 C CNN
F 1 "Conn_01x03" H 6118 4316 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6200 4550 50  0001 C CNN
F 3 "~" H 6200 4550 50  0001 C CNN
	1    6200 4550
	-1   0    0    1   
$EndComp
Wire Wire Line
	6400 4650 6500 4650
Wire Wire Line
	6500 4650 6500 4850
$Comp
L power:GND #PWR?
U 1 1 614F6142
P 6500 4850
F 0 "#PWR?" H 6500 4600 50  0001 C CNN
F 1 "GND" H 6505 4677 50  0000 C CNN
F 2 "" H 6500 4850 50  0001 C CNN
F 3 "" H 6500 4850 50  0001 C CNN
	1    6500 4850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 614F8E70
P 7650 2750
F 0 "J2" H 7568 2425 50  0000 C CNN
F 1 "Conn_01x03" H 7568 2516 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7650 2750 50  0001 C CNN
F 3 "~" H 7650 2750 50  0001 C CNN
	1    7650 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 2850 7350 2850
Wire Wire Line
	7350 2850 7350 3050
$Comp
L power:GND #PWR?
U 1 1 614FDD6A
P 7350 3050
F 0 "#PWR?" H 7350 2800 50  0001 C CNN
F 1 "GND" H 7355 2877 50  0000 C CNN
F 2 "" H 7350 3050 50  0001 C CNN
F 3 "" H 7350 3050 50  0001 C CNN
	1    7350 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 6200 3400 6200
Text Label 3400 6200 0    50   ~ 0
10MHz_n
Wire Wire Line
	2900 1600 3200 1600
Text Label 3200 1600 0    50   ~ 0
10MHz_n
Text GLabel 7300 2750 0    50   Output ~ 0
Temp_Sense
$Sheet
S 1450 1300 1450 1750
U 61273593
F0 "FPGA_Conn" 50
F1 "FPGA_Conn.sch" 50
F2 "PPS" I R 2900 1400 50 
F3 "Fan1_TACH" I R 2900 1700 50 
F4 "Fan1_PWM" O R 2900 1800 50 
F5 "Fan2_TACH" I R 2900 1900 50 
F6 "Fan2_PWM" O R 2900 2000 50 
F7 "Temperature_Sense" I R 2900 2200 50 
F8 "USB_TX" I L 1450 1400 50 
F9 "USB_RX" O L 1450 1500 50 
F10 "USB_CTS" O L 1450 1600 50 
F11 "USB_DTR" I L 1450 1700 50 
F12 "LNA_PWR_EN" O R 2900 2950 50 
F13 "AMP_EN" O L 1450 1900 50 
F14 "10MHz_p" I R 2900 1500 50 
F15 "10MHz_n" I R 2900 1600 50 
$EndSheet
$Sheet
S 7300 4950 1100 650 
U 614D8252
F0 "LNA_bias" 50
F1 "LNA_bias.sch" 50
F2 "LNA_BIAS_OUT" O L 7300 5150 50 
F3 "VSENSE_OUT" O L 7300 5300 50 
F4 "LNA_V" I L 7300 5050 50 
$EndSheet
Wire Wire Line
	7300 5150 6800 5150
Wire Wire Line
	6800 5150 6800 4550
Wire Wire Line
	6400 4550 6800 4550
Wire Wire Line
	7300 5050 7000 5050
Wire Wire Line
	7000 5050 7000 4450
Connection ~ 7000 4450
Wire Wire Line
	7000 4450 6400 4450
Text GLabel 7100 5300 0    50   Output ~ 0
LNA_BIAS_SENSE
Wire Wire Line
	7300 5300 7100 5300
Wire Wire Line
	8950 4600 8650 4600
Text GLabel 8650 4600 0    50   Input ~ 0
LDO_PG
$EndSCHEMATC
