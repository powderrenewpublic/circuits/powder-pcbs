count = 7

with open("strip_cut_left.txt", "w") as cut_left_file:
    cut_left_file.write("-u1 board_top\n")
    for i in range(1, count+1, 2):
        cut_left_file.write(f"""-u1 v7_{i}
-u1_{i} v10_{i}
-u1_{i} v9_{i}
-u1 v6_{i}
""")
    cut_left_file.write("""-u1 board_bottom
-u2 board_bottom
-u2 board_top
-u1 board_top
""")

with open("strip_cut_right.txt", "w") as cut_left_file:
    cut_left_file.write("u1 board_bottom\n")
    for i in range(count-count%2, 1, -2):
        cut_left_file.write(f"""u1 v7_{i}
u1_{i} v10_{i}
u1_{i} v9_{i}
u1 v6_{i}
""")
    cut_left_file.write("""u1 board_top
u2 board_top
u2 board_bottom
u1 board_bottom
""")


for i in range(1, count+1, 2):
    with open(f"left_{i}.txt", "w") as left_poly_file:
        left_poly_file.write(
f"""-u6_{i} v1_{i}
-u5_{i} v1_{i}
-u2_{i} v4_{i}
-u2_{i} v5_{i}
-u3_{i} v5_{i}
-u3_{i} v3_{i}
-u4_{i} v2_{i}
-u6_{i} v2_{i}
-u6_{i} v1_{i}
""")

for i in range(count-count%2, 1, -2):
    with open(f"right_{i}.txt", "w") as right_poly_file:
        right_poly_file.write(
f"""u6_{i} v1_{i}
u6_{i} v2_{i}
u4_{i} v2_{i}
u3_{i} v3_{i}
u3_{i} v5_{i}
u2_{i} v5_{i}
u2_{i} v4_{i}
u5_{i} v1_{i}
u6_{i} v1_{i}
""")
