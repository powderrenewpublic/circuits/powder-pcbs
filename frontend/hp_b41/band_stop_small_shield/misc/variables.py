non_indexed = {
    "u1": "cbcpw_W/2",
    "u2": "u1+cbcpw_gap",
    "strip_cut": "0.5",
    "overlap": "1.0",
    "end_overlap": "0.5",
    "chamfer_w": "0.5",
    "board_top": "-PCB_l/2",
    "board_bottom": "PCB_l/2",
    "filter_start_y": "-top_PCB_L/2+2",
    "inner_chamfer": "0.5",
    "drill_offset": "1.0",
}

def print_var(var, value):
    print(f'{var}="{value}" ""')


for var, value in non_indexed.items():
    print_var(var, value)

count = 7

l_on = [3.5, 3.6, 3.7, 3.8, 3.9, 4.0, 4.1]
l_off = [3.5, 3.6, 3.7, 3.8, 3.9, 4.0, 4.1]

for i in range(1, count+1):
    print_var(f"strip_cut_{i}", "strip_cut")

for i in range(1, count+1):
    print_var(f"overlap_{i}", "overlap")

for i in range(1, count+1):
    print_var(f"w_on_{i}", "0.5")

for i in range(1, count+1):
    print_var(f"w_off_{i}", "1.0")

for i in range(1, count+1):
    print_var(f"l_on_{i}", f"{l_on[i-1]}")

for i in range(1, count+1):
    print_var(f"l_off_{i}", f"{l_off[i-1]}")

for i in range(1, count+1):
    print_var(f"outer_chamfer_{i}",
              f"inner_chamfer+max(w_on_{i},w_off_{i})-1.414*chamfer_w")


for i in range(1, count+1):
    if i != 1:
        print_var(f"dy_{i-1}_{i}", "1.0")


for i in range(1, count+1):
    print_var(f"v1_{i}",
              "filter_start_y" if i==1 else f"v5_{i-1}+dy_{i-1}_{i}")


for i in range(1, count+1):
    print_var(f"u1_{i}", f"u1-strip_cut_{i}")

for i in range(1, count+1):
    print_var(f"u3_{i}", f"u1-overlap")

for i in range(1, count+1):
    print_var(f"u2_{i}", f"u3_{i}-w_on_{i}")

for i in range(1, count+1):
    print_var(f"u4_{i}", f"u3_{i}+inner_chamfer")

for i in range(1, count+1):
    print_var(f"u5_{i}", f"u2_{i}+outer_chamfer_{i}")

for i in range(1, count+1):
    print_var(f"u6_{i}", f"u4_{i}+l_off_{i}+drill_offset")

for i in range(1, count+1):
    print_var(f"u7_{i}", f"u6_{i}-drill_offset")


for i in range(1, count+1):
    print_var(f"v2_{i}", f"v1_{i}+w_off_{i}")

for i in range(1, count+1):
    print_var(f"v3_{i}", f"v2_{i}+inner_chamfer")

for i in range(1, count+1):
    print_var(f"v4_{i}", f"v1_{i}+outer_chamfer_{i}")

for i in range(1, count+1):
    print_var(f"v5_{i}", f"v3_{i}+l_on_{i}")

for i in range(1, count+1):
    print_var(f"v6_{i}", f"v5_{i}-end_overlap")

for i in range(1, count+1):
    print_var(f"v7_{i}", f"v1_{i}+end_overlap")

for i in range(1, count+1):
    print_var(f"v8_{i}", f"v1_{i}+w_off_{i}/2")

for i in range(1, count+1):
    print_var(f"v9_{i}", f"v6_{i}-strip_cut_{i}")

for i in range(1, count+1):
    print_var(f"v10_{i}", f"v7_{i}+strip_cut_{i}")
