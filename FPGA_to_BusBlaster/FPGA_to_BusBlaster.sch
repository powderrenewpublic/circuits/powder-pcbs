EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J1
U 1 1 5CEDA94E
P 4350 3150
F 0 "J1" H 4400 3767 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 4400 3676 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 4350 3150 50  0001 C CNN
F 3 "~" H 4350 3150 50  0001 C CNN
	1    4350 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2750 3950 2750
Text Label 3950 2750 2    50   ~ 0
VTG
Wire Wire Line
	4150 2850 3950 2850
Wire Wire Line
	3950 2950 4150 2950
Wire Wire Line
	3950 3050 4150 3050
Wire Wire Line
	4150 3150 3950 3150
Wire Wire Line
	4150 3250 3950 3250
Wire Wire Line
	3950 3350 4150 3350
Wire Wire Line
	3950 3450 4150 3450
Wire Wire Line
	4150 3550 3950 3550
Wire Wire Line
	4150 3650 3950 3650
Wire Wire Line
	4650 2950 4850 2950
Wire Wire Line
	4650 3050 4850 3050
Wire Wire Line
	4850 3150 4650 3150
Wire Wire Line
	4850 3250 4650 3250
Wire Wire Line
	4650 3350 4850 3350
Wire Wire Line
	4650 3450 4850 3450
Wire Wire Line
	4850 3550 4650 3550
Wire Wire Line
	4850 3650 4650 3650
Wire Wire Line
	4650 2750 4850 2750
Wire Wire Line
	4650 2850 4850 2850
Wire Wire Line
	4850 2850 4850 2950
Connection ~ 4850 2950
Wire Wire Line
	4850 2950 4850 3050
Connection ~ 4850 3050
Wire Wire Line
	4850 3050 4850 3150
Connection ~ 4850 3150
Wire Wire Line
	4850 3150 4850 3250
Connection ~ 4850 3250
Wire Wire Line
	4850 3250 4850 3350
Connection ~ 4850 3350
Wire Wire Line
	4850 3350 4850 3450
Connection ~ 4850 3450
Wire Wire Line
	4850 3450 4850 3550
Connection ~ 4850 3550
Wire Wire Line
	4850 3550 4850 3650
Connection ~ 4850 3650
Wire Wire Line
	4850 3650 4850 3850
$Comp
L power:GND #PWR?
U 1 1 5CEDDE82
P 4850 3850
F 0 "#PWR?" H 4850 3600 50  0001 C CNN
F 1 "GND" H 4855 3677 50  0000 C CNN
F 2 "" H 4850 3850 50  0001 C CNN
F 3 "" H 4850 3850 50  0001 C CNN
	1    4850 3850
	1    0    0    -1  
$EndComp
Text Label 3950 2850 2    50   ~ 0
TRST
Text Label 3950 2950 2    50   ~ 0
TDI
Text Label 3950 3050 2    50   ~ 0
TMS
Text Label 3950 3250 2    50   ~ 0
RTCK
Text Label 3950 3350 2    50   ~ 0
TDO
Text Label 3950 3450 2    50   ~ 0
TSRST
Text Label 3950 3550 2    50   ~ 0
DBGRQ
Text Label 3950 3650 2    50   ~ 0
DBGACK
Text Label 3950 3150 2    50   ~ 0
TCK
NoConn ~ 3950 2850
NoConn ~ 3950 3250
NoConn ~ 3950 3450
NoConn ~ 3950 3550
NoConn ~ 3950 3650
Text Label 4850 2750 0    50   ~ 0
VTG
Wire Wire Line
	6150 3250 5950 3250
Wire Wire Line
	6150 3350 5950 3350
Wire Wire Line
	6150 3450 5950 3450
Wire Wire Line
	5950 3650 6150 3650
Wire Wire Line
	6650 3450 6850 3450
Wire Wire Line
	6650 3550 6850 3550
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5CEE79DD
P 6350 3450
F 0 "J2" H 6400 3867 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 6400 3776 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 6350 3450 50  0001 C CNN
F 3 "~" H 6350 3450 50  0001 C CNN
	1    6350 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3550 6150 3550
NoConn ~ 5950 3550
NoConn ~ 6850 3450
Text Label 5950 3250 2    50   ~ 0
TCK
Text Label 5950 3350 2    50   ~ 0
TDO
Text Label 5950 3450 2    50   ~ 0
TMS
Text Label 5950 3650 2    50   ~ 0
TDI
NoConn ~ 6850 3550
Wire Wire Line
	6950 3650 6950 3850
Wire Wire Line
	6650 3650 6950 3650
Wire Wire Line
	6950 3250 6950 3650
Wire Wire Line
	6650 3250 6950 3250
Connection ~ 6950 3650
Wire Wire Line
	6650 3350 7050 3350
$Comp
L power:GND #PWR?
U 1 1 5CEEAEDA
P 6950 3850
F 0 "#PWR?" H 6950 3600 50  0001 C CNN
F 1 "GND" H 6955 3677 50  0000 C CNN
F 2 "" H 6950 3850 50  0001 C CNN
F 3 "" H 6950 3850 50  0001 C CNN
	1    6950 3850
	1    0    0    -1  
$EndComp
Text Notes 4100 2300 0    50   ~ 0
Outside Keyed Cable
Text Notes 6050 2900 0    50   ~ 0
Inside Keyed Cable
Text Label 7050 3350 0    50   ~ 0
VTG
$EndSCHEMATC
