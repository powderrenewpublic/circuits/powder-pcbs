EESchema Schematic File Version 4
LIBS:updated_gpsdo_buffer-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3100 4000 3000 4000
Wire Wire Line
	3000 4000 3000 4200
Wire Wire Line
	3000 4400 3100 4400
Wire Wire Line
	3100 4200 3000 4200
Connection ~ 3000 4200
Wire Wire Line
	3000 4200 3000 4400
Wire Wire Line
	3100 4100 2900 4100
Wire Wire Line
	2900 4100 2900 4300
Wire Wire Line
	2900 4300 3100 4300
Wire Wire Line
	2900 4300 2900 4500
Wire Wire Line
	2900 4500 3100 4500
Connection ~ 2900 4300
Connection ~ 3000 4000
Wire Wire Line
	3800 4100 3900 4100
Wire Wire Line
	3900 4100 3900 4300
Wire Wire Line
	3900 4300 3800 4300
Wire Wire Line
	3900 4300 3900 4500
Wire Wire Line
	3900 4500 3800 4500
Connection ~ 3900 4300
Wire Wire Line
	3800 4600 4000 4600
Wire Wire Line
	4000 4600 4000 4400
Wire Wire Line
	4000 4400 3800 4400
Wire Wire Line
	3800 4200 4000 4200
Wire Wire Line
	4000 4200 4000 4400
Connection ~ 4000 4400
Connection ~ 3900 4100
Connection ~ 4000 4200
$Comp
L Device:C_Small C?
U 1 1 5C534D55
P 3200 3050
AR Path="/5C534D55" Ref="C?"  Part="1" 
AR Path="/5C53436C/5C534D55" Ref="C8"  Part="1" 
AR Path="/5C536099/5C534D55" Ref="C?"  Part="1" 
AR Path="/5C566284/5C534D55" Ref="C10"  Part="1" 
AR Path="/5C5662B1/5C534D55" Ref="C12"  Part="1" 
AR Path="/5C5662C4/5C534D55" Ref="C14"  Part="1" 
F 0 "C14" H 3292 3096 50  0000 L CNN
F 1 "1uF" H 3292 3005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3200 3050 50  0001 C CNN
F 3 "~" H 3200 3050 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "CL10A105KA8NNNC" H 0   0   50  0001 C CNN "PartNumber"
	1    3200 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C534D5C
P 3550 3050
AR Path="/5C534D5C" Ref="C?"  Part="1" 
AR Path="/5C53436C/5C534D5C" Ref="C9"  Part="1" 
AR Path="/5C536099/5C534D5C" Ref="C?"  Part="1" 
AR Path="/5C566284/5C534D5C" Ref="C11"  Part="1" 
AR Path="/5C5662B1/5C534D5C" Ref="C13"  Part="1" 
AR Path="/5C5662C4/5C534D5C" Ref="C15"  Part="1" 
F 0 "C15" H 3642 3096 50  0000 L CNN
F 1 "10nF" H 3642 3005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3550 3050 50  0001 C CNN
F 3 "~" H 3550 3050 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "CL10B103KB8NCNC" H 0   0   50  0001 C CNN "PartNumber"
	1    3550 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2950 3200 2850
Wire Wire Line
	3200 3250 3200 3150
$Comp
L power:+3.3V #PWR?
U 1 1 5C534D65
P 3200 2750
AR Path="/5C534D65" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5C534D65" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5C534D65" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5C534D65" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5C534D65" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5C534D65" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3200 2600 50  0001 C CNN
F 1 "+3.3V" H 3215 2923 50  0000 C CNN
F 2 "" H 3200 2750 50  0001 C CNN
F 3 "" H 3200 2750 50  0001 C CNN
	1    3200 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2750 3200 2850
Connection ~ 3200 2850
$Comp
L power:GND #PWR?
U 1 1 5C534D6D
P 3200 3350
AR Path="/5C534D6D" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5C534D6D" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5C534D6D" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5C534D6D" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5C534D6D" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5C534D6D" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3200 3100 50  0001 C CNN
F 1 "GND" H 3205 3177 50  0000 C CNN
F 2 "" H 3200 3350 50  0001 C CNN
F 3 "" H 3200 3350 50  0001 C CNN
	1    3200 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 3250 3200 3350
Connection ~ 3200 3250
Wire Wire Line
	3200 2850 3550 2850
Wire Wire Line
	3550 3250 3200 3250
Wire Wire Line
	3550 3150 3550 3250
Wire Wire Line
	3550 2850 3550 2950
$Comp
L power:GND #PWR?
U 1 1 5C534D79
P 3000 4700
AR Path="/5C534D79" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5C534D79" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5C534D79" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5C534D79" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5C534D79" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5C534D79" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3000 4450 50  0001 C CNN
F 1 "GND" H 3005 4527 50  0000 C CNN
F 2 "" H 3000 4700 50  0001 C CNN
F 3 "" H 3000 4700 50  0001 C CNN
	1    3000 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4600 3000 4600
Wire Wire Line
	3000 4600 3000 4700
$Comp
L power:+3.3V #PWR?
U 1 1 5C534D81
P 3900 3900
AR Path="/5C534D81" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5C534D81" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5C534D81" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5C534D81" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5C534D81" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5C534D81" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3900 3750 50  0001 C CNN
F 1 "+3.3V" H 3915 4073 50  0000 C CNN
F 2 "" H 3900 3900 50  0001 C CNN
F 3 "" H 3900 3900 50  0001 C CNN
	1    3900 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4000 3900 4000
Wire Wire Line
	3900 4000 3900 3900
$Comp
L Connector:Conn_Coaxial J4
U 1 1 5C5387F8
P 6800 3900
AR Path="/5C53436C/5C5387F8" Ref="J4"  Part="1" 
AR Path="/5C566284/5C5387F8" Ref="J7"  Part="1" 
AR Path="/5C5662B1/5C5387F8" Ref="J10"  Part="1" 
AR Path="/5C5662C4/5C5387F8" Ref="J13"  Part="1" 
F 0 "J13" H 6899 3876 50  0000 L CNN
F 1 "Conn_Coaxial" H 6899 3785 50  0000 L CNN
F 2 "RF:SMA_endlaunch_SD-73251-115" H 6800 3900 50  0001 C CNN
F 3 " ~" H 6800 3900 50  0001 C CNN
F 4 "Molex" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "0732511150" H 0   0   50  0001 C CNN "PartNumber"
	1    6800 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J5
U 1 1 5C53884C
P 6800 4650
AR Path="/5C53436C/5C53884C" Ref="J5"  Part="1" 
AR Path="/5C566284/5C53884C" Ref="J8"  Part="1" 
AR Path="/5C5662B1/5C53884C" Ref="J11"  Part="1" 
AR Path="/5C5662C4/5C53884C" Ref="J14"  Part="1" 
F 0 "J14" H 6899 4626 50  0000 L CNN
F 1 "Conn_Coaxial" H 6899 4535 50  0000 L CNN
F 2 "RF:SMA_endlaunch_SD-73251-115" H 6800 4650 50  0001 C CNN
F 3 " ~" H 6800 4650 50  0001 C CNN
F 4 "Molex" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "0732511150" H 0   0   50  0001 C CNN "PartNumber"
	1    6800 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 5C538E6E
P 5550 3150
AR Path="/5C53436C/5C538E6E" Ref="J3"  Part="1" 
AR Path="/5C566284/5C538E6E" Ref="J6"  Part="1" 
AR Path="/5C5662B1/5C538E6E" Ref="J9"  Part="1" 
AR Path="/5C5662C4/5C538E6E" Ref="J12"  Part="1" 
F 0 "J12" H 5630 3192 50  0000 L CNN
F 1 "Conn_01x03" H 5630 3101 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 5550 3150 50  0001 C CNN
F 3 "~" H 5550 3150 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "77311-118-03LF" H 0   0   50  0001 C CNN "PartNumber"
	1    5550 3150
	1    0    0    1   
$EndComp
Connection ~ 2900 4100
Text Label 2250 4100 0    50   ~ 0
10MHz_Buffered
Wire Wire Line
	2250 4100 2900 4100
Wire Wire Line
	2150 4000 3000 4000
Text Label 4600 4200 2    50   ~ 0
1PPS_Buffered
Wire Wire Line
	4000 4200 4600 4200
Wire Wire Line
	3900 4100 4700 4100
$Comp
L power:GND #PWR?
U 1 1 5C522337
P 6800 4200
AR Path="/5C53436C/5C522337" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5C522337" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5C522337" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5C522337" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6800 3950 50  0001 C CNN
F 1 "GND" H 6805 4027 50  0000 C CNN
F 2 "" H 6800 4200 50  0001 C CNN
F 3 "" H 6800 4200 50  0001 C CNN
	1    6800 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C52238C
P 6800 4950
AR Path="/5C53436C/5C52238C" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5C52238C" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5C52238C" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5C52238C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6800 4700 50  0001 C CNN
F 1 "GND" H 6805 4777 50  0000 C CNN
F 2 "" H 6800 4950 50  0001 C CNN
F 3 "" H 6800 4950 50  0001 C CNN
	1    6800 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4850 6800 4950
Wire Wire Line
	6800 4200 6800 4100
Wire Wire Line
	6600 4650 6000 4650
Wire Wire Line
	6600 3900 6000 3900
Text Label 6000 4650 0    50   ~ 0
1PPS_Buffered
Text Label 6000 3900 0    50   ~ 0
10MHz_Buffered
$Comp
L power:GND #PWR?
U 1 1 5C5240D3
P 5250 3350
AR Path="/5C53436C/5C5240D3" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5C5240D3" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5C5240D3" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5C5240D3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5250 3100 50  0001 C CNN
F 1 "GND" H 5255 3177 50  0000 C CNN
F 2 "" H 5250 3350 50  0001 C CNN
F 3 "" H 5250 3350 50  0001 C CNN
	1    5250 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3250 5250 3250
Wire Wire Line
	5250 3250 5250 3350
Text HLabel 4850 3050 0    50   Input ~ 0
NMEA_TXD
Text HLabel 4850 3150 0    50   Input ~ 0
LOCK_OK
Wire Wire Line
	4850 3050 5350 3050
Wire Wire Line
	5350 3150 4850 3150
$Comp
L GPSDO:SN74AC14-Q1 U7
U 1 1 5C5CB0A4
P 3450 4300
AR Path="/5C5662C4/5C5CB0A4" Ref="U7"  Part="1" 
AR Path="/5C5662B1/5C5CB0A4" Ref="U6"  Part="1" 
AR Path="/5C53436C/5C5CB0A4" Ref="U4"  Part="1" 
AR Path="/5C566284/5C5CB0A4" Ref="U5"  Part="1" 
AR Path="/5C5CB0A4" Ref="U6"  Part="1" 
F 0 "U7" H 3450 4825 50  0000 C CNN
F 1 "SN74AC14-Q1" H 3450 4734 50  0000 C CNN
F 2 "GPSDO:SN74AC14-Q1" H 3450 3950 50  0001 C CNN
F 3 "" H 3450 3950 50  0001 C CNN
F 4 "Texas Instruments" H 0   0   50  0001 C CNN "Manufacturer"
F 5 "SN74AC14QPWRQ1" H 0   0   50  0001 C CNN "PartNumber"
	1    3450 4300
	1    0    0    -1  
$EndComp
Text HLabel 2150 4000 0    50   Input ~ 0
10MHz
Text HLabel 4700 4100 2    50   Input ~ 0
1PPS
$Comp
L Connector:TestPoint TP1
U 1 1 5D291754
P 3300 5350
AR Path="/5C53436C/5D291754" Ref="TP1"  Part="1" 
AR Path="/5C566284/5D291754" Ref="TP3"  Part="1" 
AR Path="/5C5662B1/5D291754" Ref="TP5"  Part="1" 
AR Path="/5C5662C4/5D291754" Ref="TP7"  Part="1" 
F 0 "TP7" V 3254 5538 50  0000 L CNN
F 1 "TestPoint" V 3345 5538 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D2.5mm" H 3500 5350 50  0001 C CNN
F 3 "~" H 3500 5350 50  0001 C CNN
	1    3300 5350
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5D2B09AE
P 3300 5600
AR Path="/5C53436C/5D2B09AE" Ref="TP2"  Part="1" 
AR Path="/5C566284/5D2B09AE" Ref="TP4"  Part="1" 
AR Path="/5C5662B1/5D2B09AE" Ref="TP6"  Part="1" 
AR Path="/5C5662C4/5D2B09AE" Ref="TP8"  Part="1" 
F 0 "TP8" V 3254 5788 50  0000 L CNN
F 1 "TestPoint" V 3345 5788 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D2.5mm" H 3500 5600 50  0001 C CNN
F 3 "~" H 3500 5600 50  0001 C CNN
	1    3300 5600
	0    1    1    0   
$EndComp
Text HLabel 3200 5350 0    50   Input ~ 0
10MHz
Wire Wire Line
	3200 5350 3300 5350
Text HLabel 3200 5600 0    50   Input ~ 0
1PPS
Wire Wire Line
	3200 5600 3300 5600
$EndSCHEMATC
