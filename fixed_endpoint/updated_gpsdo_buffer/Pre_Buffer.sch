EESchema Schematic File Version 4
LIBS:updated_gpsdo_buffer-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GPSDO:SN74AC14-Q1 U?
U 1 1 5D75C105
P 4350 2450
AR Path="/5D75C105" Ref="U?"  Part="1" 
AR Path="/5D753A3E/5D75C105" Ref="U?"  Part="1" 
F 0 "U?" H 4350 2975 50  0000 C CNN
F 1 "SN74AC14-Q1" H 4350 2884 50  0000 C CNN
F 2 "GPSDO:SN74AC14-Q1" H 4350 2100 50  0001 C CNN
F 3 "" H 4350 2100 50  0001 C CNN
F 4 "Texas Instruments" H 150 1200 50  0001 C CNN "Manufacturer"
F 5 "SN74AC14QPWRQ1" H 150 1200 50  0001 C CNN "PartNumber"
	1    4350 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D75C10C
P 3950 3100
AR Path="/5D75C10C" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D75C10C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 2850 50  0001 C CNN
F 1 "GND" H 3955 2927 50  0000 C CNN
F 2 "" H 3950 3100 50  0001 C CNN
F 3 "" H 3950 3100 50  0001 C CNN
	1    3950 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 2150 4800 2150
$Comp
L power:+3.3V #PWR?
U 1 1 5D75C113
P 4800 1950
AR Path="/5D75C113" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D75C113" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4800 1800 50  0001 C CNN
F 1 "+3.3V" H 4815 2123 50  0000 C CNN
F 2 "" H 4800 1950 50  0001 C CNN
F 3 "" H 4800 1950 50  0001 C CNN
	1    4800 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5D75C11B
P 2400 2450
AR Path="/5D75C11B" Ref="C?"  Part="1" 
AR Path="/5C53436C/5D75C11B" Ref="C?"  Part="1" 
AR Path="/5C536099/5D75C11B" Ref="C?"  Part="1" 
AR Path="/5C566284/5D75C11B" Ref="C?"  Part="1" 
AR Path="/5C5662B1/5D75C11B" Ref="C?"  Part="1" 
AR Path="/5C5662C4/5D75C11B" Ref="C?"  Part="1" 
AR Path="/5D753A3E/5D75C11B" Ref="C?"  Part="1" 
F 0 "C?" H 2492 2496 50  0000 L CNN
F 1 "1uF" H 2492 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2400 2450 50  0001 C CNN
F 3 "~" H 2400 2450 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H -100 1250 50  0001 C CNN "Manufacturer"
F 5 "CL10A105KA8NNNC" H -100 1250 50  0001 C CNN "PartNumber"
	1    2400 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5D75C123
P 2750 2450
AR Path="/5D75C123" Ref="C?"  Part="1" 
AR Path="/5C53436C/5D75C123" Ref="C?"  Part="1" 
AR Path="/5C536099/5D75C123" Ref="C?"  Part="1" 
AR Path="/5C566284/5D75C123" Ref="C?"  Part="1" 
AR Path="/5C5662B1/5D75C123" Ref="C?"  Part="1" 
AR Path="/5C5662C4/5D75C123" Ref="C?"  Part="1" 
AR Path="/5D753A3E/5D75C123" Ref="C?"  Part="1" 
F 0 "C?" H 2842 2496 50  0000 L CNN
F 1 "10nF" H 2842 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2750 2450 50  0001 C CNN
F 3 "~" H 2750 2450 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H -100 1250 50  0001 C CNN "Manufacturer"
F 5 "CL10B103KB8NCNC" H -100 1250 50  0001 C CNN "PartNumber"
	1    2750 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2350 2400 2250
Wire Wire Line
	2400 2650 2400 2550
$Comp
L power:+3.3V #PWR?
U 1 1 5D75C12B
P 2400 2150
AR Path="/5D75C12B" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5D75C12B" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5D75C12B" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5D75C12B" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5D75C12B" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5D75C12B" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D75C12B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2400 2000 50  0001 C CNN
F 1 "+3.3V" H 2415 2323 50  0000 C CNN
F 2 "" H 2400 2150 50  0001 C CNN
F 3 "" H 2400 2150 50  0001 C CNN
	1    2400 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2150 2400 2250
Connection ~ 2400 2250
$Comp
L power:GND #PWR?
U 1 1 5D75C133
P 2400 2750
AR Path="/5D75C133" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5D75C133" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5D75C133" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5D75C133" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5D75C133" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5D75C133" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D75C133" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2400 2500 50  0001 C CNN
F 1 "GND" H 2405 2577 50  0000 C CNN
F 2 "" H 2400 2750 50  0001 C CNN
F 3 "" H 2400 2750 50  0001 C CNN
	1    2400 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2650 2400 2750
Connection ~ 2400 2650
Wire Wire Line
	2400 2250 2750 2250
Wire Wire Line
	2750 2650 2400 2650
Wire Wire Line
	2750 2550 2750 2650
Wire Wire Line
	2750 2250 2750 2350
Wire Wire Line
	4800 1950 4800 2150
Wire Wire Line
	3600 2150 4000 2150
Wire Wire Line
	3900 2250 3900 2350
Wire Wire Line
	3900 2350 4000 2350
Wire Wire Line
	3900 2250 4000 2250
Wire Wire Line
	3900 2350 3900 2550
Wire Wire Line
	3900 2550 4000 2550
Connection ~ 3900 2350
Wire Wire Line
	4000 2750 3950 2750
Wire Wire Line
	3950 2750 3950 3100
Wire Wire Line
	3900 2550 3900 2850
Connection ~ 3900 2550
$Comp
L Device:C_Small C?
U 1 1 5D76BA07
P 2400 4350
AR Path="/5D76BA07" Ref="C?"  Part="1" 
AR Path="/5C53436C/5D76BA07" Ref="C?"  Part="1" 
AR Path="/5C536099/5D76BA07" Ref="C?"  Part="1" 
AR Path="/5C566284/5D76BA07" Ref="C?"  Part="1" 
AR Path="/5C5662B1/5D76BA07" Ref="C?"  Part="1" 
AR Path="/5C5662C4/5D76BA07" Ref="C?"  Part="1" 
AR Path="/5D753A3E/5D76BA07" Ref="C?"  Part="1" 
F 0 "C?" H 2492 4396 50  0000 L CNN
F 1 "1uF" H 2492 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2400 4350 50  0001 C CNN
F 3 "~" H 2400 4350 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H -150 1300 50  0001 C CNN "Manufacturer"
F 5 "CL10A105KA8NNNC" H -150 1300 50  0001 C CNN "PartNumber"
	1    2400 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5D76BA0F
P 2750 4350
AR Path="/5D76BA0F" Ref="C?"  Part="1" 
AR Path="/5C53436C/5D76BA0F" Ref="C?"  Part="1" 
AR Path="/5C536099/5D76BA0F" Ref="C?"  Part="1" 
AR Path="/5C566284/5D76BA0F" Ref="C?"  Part="1" 
AR Path="/5C5662B1/5D76BA0F" Ref="C?"  Part="1" 
AR Path="/5C5662C4/5D76BA0F" Ref="C?"  Part="1" 
AR Path="/5D753A3E/5D76BA0F" Ref="C?"  Part="1" 
F 0 "C?" H 2842 4396 50  0000 L CNN
F 1 "10nF" H 2842 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2750 4350 50  0001 C CNN
F 3 "~" H 2750 4350 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H -150 1300 50  0001 C CNN "Manufacturer"
F 5 "CL10B103KB8NCNC" H -150 1300 50  0001 C CNN "PartNumber"
	1    2750 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 4250 2400 4150
Wire Wire Line
	2400 4550 2400 4450
$Comp
L power:+3.3V #PWR?
U 1 1 5D76BA17
P 2400 4050
AR Path="/5D76BA17" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5D76BA17" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5D76BA17" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5D76BA17" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5D76BA17" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5D76BA17" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D76BA17" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2400 3900 50  0001 C CNN
F 1 "+3.3V" H 2415 4223 50  0000 C CNN
F 2 "" H 2400 4050 50  0001 C CNN
F 3 "" H 2400 4050 50  0001 C CNN
	1    2400 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 4050 2400 4150
Connection ~ 2400 4150
$Comp
L power:GND #PWR?
U 1 1 5D76BA1F
P 2400 4650
AR Path="/5D76BA1F" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5D76BA1F" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5D76BA1F" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5D76BA1F" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5D76BA1F" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5D76BA1F" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D76BA1F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2400 4400 50  0001 C CNN
F 1 "GND" H 2405 4477 50  0000 C CNN
F 2 "" H 2400 4650 50  0001 C CNN
F 3 "" H 2400 4650 50  0001 C CNN
	1    2400 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 4550 2400 4650
Connection ~ 2400 4550
Wire Wire Line
	2400 4150 2750 4150
Wire Wire Line
	2750 4550 2400 4550
Wire Wire Line
	2750 4450 2750 4550
Wire Wire Line
	2750 4150 2750 4250
$Comp
L GPSDO:SN74AC14-Q1 U?
U 1 1 5D76DF5A
P 8050 2450
F 0 "U?" H 8050 2975 50  0000 C CNN
F 1 "SN74AC14-Q1" H 8050 2884 50  0000 C CNN
F 2 "" H 8050 2100 50  0001 C CNN
F 3 "" H 8050 2100 50  0001 C CNN
	1    8050 2450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5D76F8A7
P 8500 1950
F 0 "#PWR?" H 8500 1800 50  0001 C CNN
F 1 "+3.3V" H 8515 2123 50  0000 C CNN
F 2 "" H 8500 1950 50  0001 C CNN
F 3 "" H 8500 1950 50  0001 C CNN
	1    8500 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 2150 8500 2150
Wire Wire Line
	8500 2150 8500 1950
$Comp
L power:GND #PWR?
U 1 1 5D771617
P 7600 2950
F 0 "#PWR?" H 7600 2700 50  0001 C CNN
F 1 "GND" H 7605 2777 50  0000 C CNN
F 2 "" H 7600 2950 50  0001 C CNN
F 3 "" H 7600 2950 50  0001 C CNN
	1    7600 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 2950 7600 2750
Wire Wire Line
	7600 2750 7700 2750
Wire Wire Line
	4000 2450 3600 2450
Wire Wire Line
	4000 2650 3600 2650
Text Label 3600 2450 2    50   ~ 0
10MHz_1_2
Text Label 3600 2650 2    50   ~ 0
10MHz_3_4
Wire Wire Line
	5100 2750 4700 2750
Text Label 7300 2350 2    50   ~ 0
10MHz_1_2
Wire Wire Line
	7700 2450 7300 2450
Text HLabel 7300 2450 0    50   Input ~ 0
10MHz_1
Text HLabel 8800 2550 2    50   Input ~ 0
10MHz_2
Wire Wire Line
	7300 2350 7700 2350
Wire Wire Line
	8400 2550 8800 2550
Wire Wire Line
	8400 2450 8800 2450
Text Label 8800 2450 0    50   ~ 0
10MHz_1_2
Wire Wire Line
	8400 2250 8800 2250
Text Label 8800 2250 0    50   ~ 0
1PPS_1_2
Wire Wire Line
	8400 2350 8800 2350
Text HLabel 8800 2350 2    50   Input ~ 0
1PPS_2
Wire Wire Line
	7700 2150 7300 2150
Text Label 7300 2150 2    50   ~ 0
1PPS_1_2
Wire Wire Line
	7700 2250 7300 2250
Text HLabel 7300 2250 0    50   Input ~ 0
1PPS_1
NoConn ~ 7700 2650
Wire Wire Line
	7700 2550 7600 2550
Wire Wire Line
	7600 2550 7600 2750
Connection ~ 7600 2750
NoConn ~ 8400 2750
$Comp
L power:GND #PWR?
U 1 1 5D7AF6A1
P 8500 2950
F 0 "#PWR?" H 8500 2700 50  0001 C CNN
F 1 "GND" H 8505 2777 50  0000 C CNN
F 2 "" H 8500 2950 50  0001 C CNN
F 3 "" H 8500 2950 50  0001 C CNN
	1    8500 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 2950 8500 2650
Wire Wire Line
	8500 2650 8400 2650
$Comp
L GPSDO:SN74AC14-Q1 U?
U 1 1 5D7B5EC7
P 8050 4350
F 0 "U?" H 8050 4875 50  0000 C CNN
F 1 "SN74AC14-Q1" H 8050 4784 50  0000 C CNN
F 2 "" H 8050 4000 50  0001 C CNN
F 3 "" H 8050 4000 50  0001 C CNN
	1    8050 4350
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5D7B5ECD
P 8500 3850
F 0 "#PWR?" H 8500 3700 50  0001 C CNN
F 1 "+3.3V" H 8515 4023 50  0000 C CNN
F 2 "" H 8500 3850 50  0001 C CNN
F 3 "" H 8500 3850 50  0001 C CNN
	1    8500 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 4050 8500 4050
Wire Wire Line
	8500 4050 8500 3850
$Comp
L power:GND #PWR?
U 1 1 5D7B5ED5
P 7600 4850
F 0 "#PWR?" H 7600 4600 50  0001 C CNN
F 1 "GND" H 7605 4677 50  0000 C CNN
F 2 "" H 7600 4850 50  0001 C CNN
F 3 "" H 7600 4850 50  0001 C CNN
	1    7600 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 4850 7600 4650
Wire Wire Line
	7600 4650 7700 4650
Text Label 7300 4250 2    50   ~ 0
10MHz_3_4
Wire Wire Line
	7700 4350 7300 4350
Text HLabel 7300 4350 0    50   Input ~ 0
10MHz_3
Text HLabel 8800 4450 2    50   Input ~ 0
10MHz_4
Wire Wire Line
	7300 4250 7700 4250
Wire Wire Line
	8400 4450 8800 4450
Wire Wire Line
	8400 4350 8800 4350
Text Label 8800 4350 0    50   ~ 0
10MHz_3_4
Wire Wire Line
	8400 4150 8800 4150
Text Label 8800 4150 0    50   ~ 0
1PPS_3_4
Wire Wire Line
	8400 4250 8800 4250
Text HLabel 8800 4250 2    50   Input ~ 0
1PPS_4
Wire Wire Line
	7700 4050 7300 4050
Text Label 7300 4050 2    50   ~ 0
1PPS_3_4
Wire Wire Line
	7700 4150 7300 4150
Text HLabel 7300 4150 0    50   Input ~ 0
1PPS_3
NoConn ~ 7700 4550
Wire Wire Line
	7700 4450 7600 4450
Wire Wire Line
	7600 4450 7600 4650
Connection ~ 7600 4650
NoConn ~ 8400 4650
$Comp
L power:GND #PWR?
U 1 1 5D7B5EF2
P 8500 4850
F 0 "#PWR?" H 8500 4600 50  0001 C CNN
F 1 "GND" H 8505 4677 50  0000 C CNN
F 2 "" H 8500 4850 50  0001 C CNN
F 3 "" H 8500 4850 50  0001 C CNN
	1    8500 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 4850 8500 4550
Wire Wire Line
	8500 4550 8400 4550
Text HLabel 3600 2150 0    50   Input ~ 0
10MHz
$Comp
L GPSDO:SN74AC14-Q1 U?
U 1 1 5D7C42EE
P 4350 4350
AR Path="/5D7C42EE" Ref="U?"  Part="1" 
AR Path="/5D753A3E/5D7C42EE" Ref="U?"  Part="1" 
F 0 "U?" H 4350 4875 50  0000 C CNN
F 1 "SN74AC14-Q1" H 4350 4784 50  0000 C CNN
F 2 "GPSDO:SN74AC14-Q1" H 4350 4000 50  0001 C CNN
F 3 "" H 4350 4000 50  0001 C CNN
F 4 "Texas Instruments" H 150 3100 50  0001 C CNN "Manufacturer"
F 5 "SN74AC14QPWRQ1" H 150 3100 50  0001 C CNN "PartNumber"
	1    4350 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D7C42F4
P 3950 5000
AR Path="/5D7C42F4" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D7C42F4" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 4750 50  0001 C CNN
F 1 "GND" H 3955 4827 50  0000 C CNN
F 2 "" H 3950 5000 50  0001 C CNN
F 3 "" H 3950 5000 50  0001 C CNN
	1    3950 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4050 4800 4050
$Comp
L power:+3.3V #PWR?
U 1 1 5D7C42FB
P 4800 3850
AR Path="/5D7C42FB" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D7C42FB" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4800 3700 50  0001 C CNN
F 1 "+3.3V" H 4815 4023 50  0000 C CNN
F 2 "" H 4800 3850 50  0001 C CNN
F 3 "" H 4800 3850 50  0001 C CNN
	1    4800 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 4050 4000 4050
Wire Wire Line
	3900 4150 3900 4250
Wire Wire Line
	3900 4250 4000 4250
Wire Wire Line
	3900 4150 4000 4150
Wire Wire Line
	3900 4250 3900 4450
Wire Wire Line
	3900 4450 4000 4450
Connection ~ 3900 4250
Wire Wire Line
	4000 4650 3950 4650
Wire Wire Line
	3950 4650 3950 5000
Wire Wire Line
	3900 4450 3900 4750
Wire Wire Line
	3900 4750 4800 4750
Wire Wire Line
	4800 4750 4800 4550
Wire Wire Line
	4800 4550 4700 4550
Connection ~ 3900 4450
Wire Wire Line
	4000 4350 3600 4350
Wire Wire Line
	4000 4550 3600 4550
Text Label 3600 4350 2    50   ~ 0
1PPS_1_2
Text Label 3600 4550 2    50   ~ 0
1PPS_3_4
Wire Wire Line
	5100 4650 4700 4650
Text HLabel 3600 4050 0    50   Input ~ 0
1PPS
Wire Wire Line
	4700 4350 4800 4350
Wire Wire Line
	4800 4350 4800 4550
Connection ~ 4800 4550
Wire Wire Line
	4700 4450 5100 4450
Text HLabel 5100 4450 2    50   Input ~ 0
1PPS_IRIS_1
Text HLabel 5100 4650 2    50   Input ~ 0
1PPS_IRIS_2
Text HLabel 5100 2750 2    50   Input ~ 0
10MHz_IRIS
$Comp
L Device:C_Small C?
U 1 1 5D7CF031
P 6200 2400
AR Path="/5D7CF031" Ref="C?"  Part="1" 
AR Path="/5C53436C/5D7CF031" Ref="C?"  Part="1" 
AR Path="/5C536099/5D7CF031" Ref="C?"  Part="1" 
AR Path="/5C566284/5D7CF031" Ref="C?"  Part="1" 
AR Path="/5C5662B1/5D7CF031" Ref="C?"  Part="1" 
AR Path="/5C5662C4/5D7CF031" Ref="C?"  Part="1" 
AR Path="/5D753A3E/5D7CF031" Ref="C?"  Part="1" 
F 0 "C?" H 6292 2446 50  0000 L CNN
F 1 "1uF" H 6292 2355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6200 2400 50  0001 C CNN
F 3 "~" H 6200 2400 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H 3700 1200 50  0001 C CNN "Manufacturer"
F 5 "CL10A105KA8NNNC" H 3700 1200 50  0001 C CNN "PartNumber"
	1    6200 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5D7CF039
P 6550 2400
AR Path="/5D7CF039" Ref="C?"  Part="1" 
AR Path="/5C53436C/5D7CF039" Ref="C?"  Part="1" 
AR Path="/5C536099/5D7CF039" Ref="C?"  Part="1" 
AR Path="/5C566284/5D7CF039" Ref="C?"  Part="1" 
AR Path="/5C5662B1/5D7CF039" Ref="C?"  Part="1" 
AR Path="/5C5662C4/5D7CF039" Ref="C?"  Part="1" 
AR Path="/5D753A3E/5D7CF039" Ref="C?"  Part="1" 
F 0 "C?" H 6642 2446 50  0000 L CNN
F 1 "10nF" H 6642 2355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6550 2400 50  0001 C CNN
F 3 "~" H 6550 2400 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H 3700 1200 50  0001 C CNN "Manufacturer"
F 5 "CL10B103KB8NCNC" H 3700 1200 50  0001 C CNN "PartNumber"
	1    6550 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2300 6200 2200
Wire Wire Line
	6200 2600 6200 2500
$Comp
L power:+3.3V #PWR?
U 1 1 5D7CF041
P 6200 2100
AR Path="/5D7CF041" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5D7CF041" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5D7CF041" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5D7CF041" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5D7CF041" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5D7CF041" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D7CF041" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6200 1950 50  0001 C CNN
F 1 "+3.3V" H 6215 2273 50  0000 C CNN
F 2 "" H 6200 2100 50  0001 C CNN
F 3 "" H 6200 2100 50  0001 C CNN
	1    6200 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2100 6200 2200
Connection ~ 6200 2200
$Comp
L power:GND #PWR?
U 1 1 5D7CF049
P 6200 2700
AR Path="/5D7CF049" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5D7CF049" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5D7CF049" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5D7CF049" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5D7CF049" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5D7CF049" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D7CF049" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6200 2450 50  0001 C CNN
F 1 "GND" H 6205 2527 50  0000 C CNN
F 2 "" H 6200 2700 50  0001 C CNN
F 3 "" H 6200 2700 50  0001 C CNN
	1    6200 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2600 6200 2700
Connection ~ 6200 2600
Wire Wire Line
	6200 2200 6550 2200
Wire Wire Line
	6550 2600 6200 2600
Wire Wire Line
	6550 2500 6550 2600
Wire Wire Line
	6550 2200 6550 2300
$Comp
L Device:C_Small C?
U 1 1 5D7D327F
P 6200 4250
AR Path="/5D7D327F" Ref="C?"  Part="1" 
AR Path="/5C53436C/5D7D327F" Ref="C?"  Part="1" 
AR Path="/5C536099/5D7D327F" Ref="C?"  Part="1" 
AR Path="/5C566284/5D7D327F" Ref="C?"  Part="1" 
AR Path="/5C5662B1/5D7D327F" Ref="C?"  Part="1" 
AR Path="/5C5662C4/5D7D327F" Ref="C?"  Part="1" 
AR Path="/5D753A3E/5D7D327F" Ref="C?"  Part="1" 
F 0 "C?" H 6292 4296 50  0000 L CNN
F 1 "1uF" H 6292 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6200 4250 50  0001 C CNN
F 3 "~" H 6200 4250 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H 3700 3050 50  0001 C CNN "Manufacturer"
F 5 "CL10A105KA8NNNC" H 3700 3050 50  0001 C CNN "PartNumber"
	1    6200 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5D7D3287
P 6550 4250
AR Path="/5D7D3287" Ref="C?"  Part="1" 
AR Path="/5C53436C/5D7D3287" Ref="C?"  Part="1" 
AR Path="/5C536099/5D7D3287" Ref="C?"  Part="1" 
AR Path="/5C566284/5D7D3287" Ref="C?"  Part="1" 
AR Path="/5C5662B1/5D7D3287" Ref="C?"  Part="1" 
AR Path="/5C5662C4/5D7D3287" Ref="C?"  Part="1" 
AR Path="/5D753A3E/5D7D3287" Ref="C?"  Part="1" 
F 0 "C?" H 6642 4296 50  0000 L CNN
F 1 "10nF" H 6642 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6550 4250 50  0001 C CNN
F 3 "~" H 6550 4250 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H 3700 3050 50  0001 C CNN "Manufacturer"
F 5 "CL10B103KB8NCNC" H 3700 3050 50  0001 C CNN "PartNumber"
	1    6550 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4150 6200 4050
Wire Wire Line
	6200 4450 6200 4350
$Comp
L power:+3.3V #PWR?
U 1 1 5D7D328F
P 6200 3950
AR Path="/5D7D328F" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5D7D328F" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5D7D328F" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5D7D328F" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5D7D328F" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5D7D328F" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D7D328F" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6200 3800 50  0001 C CNN
F 1 "+3.3V" H 6215 4123 50  0000 C CNN
F 2 "" H 6200 3950 50  0001 C CNN
F 3 "" H 6200 3950 50  0001 C CNN
	1    6200 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3950 6200 4050
Connection ~ 6200 4050
$Comp
L power:GND #PWR?
U 1 1 5D7D3297
P 6200 4550
AR Path="/5D7D3297" Ref="#PWR?"  Part="1" 
AR Path="/5C53436C/5D7D3297" Ref="#PWR?"  Part="1" 
AR Path="/5C536099/5D7D3297" Ref="#PWR?"  Part="1" 
AR Path="/5C566284/5D7D3297" Ref="#PWR?"  Part="1" 
AR Path="/5C5662B1/5D7D3297" Ref="#PWR?"  Part="1" 
AR Path="/5C5662C4/5D7D3297" Ref="#PWR?"  Part="1" 
AR Path="/5D753A3E/5D7D3297" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6200 4300 50  0001 C CNN
F 1 "GND" H 6205 4377 50  0000 C CNN
F 2 "" H 6200 4550 50  0001 C CNN
F 3 "" H 6200 4550 50  0001 C CNN
	1    6200 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4450 6200 4550
Connection ~ 6200 4450
Wire Wire Line
	6200 4050 6550 4050
Wire Wire Line
	6550 4450 6200 4450
Wire Wire Line
	6550 4350 6550 4450
Wire Wire Line
	6550 4050 6550 4150
NoConn ~ 4700 2350
NoConn ~ 4700 2550
Wire Wire Line
	4900 2850 4900 2650
Wire Wire Line
	3900 2850 4900 2850
Wire Wire Line
	4700 2650 4900 2650
Wire Wire Line
	4700 2450 4800 2450
Wire Wire Line
	4800 2450 4800 2250
Connection ~ 4800 2150
Wire Wire Line
	4700 2250 4800 2250
Connection ~ 4800 2250
Wire Wire Line
	4800 2250 4800 2150
Wire Wire Line
	4700 4150 4800 4150
Wire Wire Line
	4800 3850 4800 4050
Connection ~ 4800 4050
Wire Wire Line
	4800 4050 4800 4150
NoConn ~ 4700 4250
$EndSCHEMATC
