// Edge cuts for the Modified GPSDO Buffer Board

// Standard M3 clearance hole size
mounting_hole_d = 3.4;

board_width = 75;
board_height = 75;
board_offset = [-5, 0];

corner_d = 8;

// Number of circle segments
$fn = 100;

difference() {
	edgecuts_pos();
	edgecuts_neg();
}

module edgecuts_pos() {
	translate(board_offset) {
		//square([board_width, board_height], center=true);
		hull() {
			translate([(board_width - corner_d)/2, (board_height - corner_d)/2]) {
				circle(d = corner_d);
			}

			translate([-(board_width - corner_d)/2, (board_height - corner_d)/2]) {
				circle(d = corner_d);
			}

			translate([-(board_width - corner_d)/2, -(board_height - corner_d)/2]) {
				circle(d = corner_d);
			}

			translate([(board_width - corner_d)/2, -(board_height - corner_d)/2]) {
				circle(d = corner_d);
			}
		}
	}
}

module edgecuts_neg() {
	translate(board_offset) {
		translate([(board_width - corner_d)/2, (board_height - corner_d)/2]) {
			circle(d = mounting_hole_d);
		}

		translate([-(board_width - corner_d)/2, (board_height - corner_d)/2]) {
			circle(d = mounting_hole_d);
		}

		translate([-(board_width - corner_d)/2, -(board_height - corner_d)/2]) {
			circle(d = mounting_hole_d);
		}

		translate([(board_width - corner_d)/2, -(board_height - corner_d)/2]) {
			circle(d = mounting_hole_d);
		}
	}
}
