EESchema Schematic File Version 4
LIBS:endpoint_shield-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5CDF9AF9
P 5300 5900
F 0 "#PWR?" H 5300 5650 50  0001 C CNN
F 1 "GND" H 5305 5727 50  0000 C CNN
F 2 "" H 5300 5900 50  0001 C CNN
F 3 "" H 5300 5900 50  0001 C CNN
	1    5300 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 5500 5300 5500
$Comp
L Device:R_Small R308
U 1 1 5CDFD4FE
P 4300 5600
F 0 "R308" H 4359 5646 50  0000 L CNN
F 1 "10k" H 4359 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4300 5600 50  0001 C CNN
F 3 "~" H 4300 5600 50  0001 C CNN
	1    4300 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R307
U 1 1 5CDFE2AB
P 4050 5600
F 0 "R307" H 4109 5646 50  0000 L CNN
F 1 "10k" H 4109 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4050 5600 50  0001 C CNN
F 3 "~" H 4050 5600 50  0001 C CNN
	1    4050 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R306
U 1 1 5CDFF57D
P 3800 5600
F 0 "R306" H 3859 5646 50  0000 L CNN
F 1 "10k" H 3859 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3800 5600 50  0001 C CNN
F 3 "~" H 3800 5600 50  0001 C CNN
	1    3800 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R305
U 1 1 5CDFF583
P 3550 5600
F 0 "R305" H 3609 5646 50  0000 L CNN
F 1 "10k" H 3609 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3550 5600 50  0001 C CNN
F 3 "~" H 3550 5600 50  0001 C CNN
	1    3550 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R304
U 1 1 5CE0179B
P 3300 5600
F 0 "R304" H 3359 5646 50  0000 L CNN
F 1 "10k" H 3359 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3300 5600 50  0001 C CNN
F 3 "~" H 3300 5600 50  0001 C CNN
	1    3300 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R303
U 1 1 5CE017A1
P 3050 5600
F 0 "R303" H 3109 5646 50  0000 L CNN
F 1 "10k" H 3109 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3050 5600 50  0001 C CNN
F 3 "~" H 3050 5600 50  0001 C CNN
	1    3050 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R302
U 1 1 5CE017A7
P 2800 5600
F 0 "R302" H 2859 5646 50  0000 L CNN
F 1 "10k" H 2859 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2800 5600 50  0001 C CNN
F 3 "~" H 2800 5600 50  0001 C CNN
	1    2800 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R301
U 1 1 5CE017AD
P 2550 5600
F 0 "R301" H 2609 5646 50  0000 L CNN
F 1 "10k" H 2609 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2550 5600 50  0001 C CNN
F 3 "~" H 2550 5600 50  0001 C CNN
	1    2550 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 5500 4500 4350
Wire Wire Line
	4500 5500 4700 5500
$Comp
L power:+3.3V #PWR?
U 1 1 5CE09093
P 4500 4350
F 0 "#PWR?" H 4500 4200 50  0001 C CNN
F 1 "+3.3V" H 4515 4523 50  0000 C CNN
F 2 "" H 4500 4350 50  0001 C CNN
F 3 "" H 4500 4350 50  0001 C CNN
	1    4500 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 5500 4300 5400
Wire Wire Line
	4300 5400 4700 5400
Wire Wire Line
	4700 5300 4050 5300
Wire Wire Line
	4050 5300 4050 5500
Wire Wire Line
	3800 5500 3800 5200
Wire Wire Line
	3800 5200 4700 5200
Wire Wire Line
	4700 5100 3550 5100
Wire Wire Line
	3550 5100 3550 5500
Wire Wire Line
	3300 5500 3300 5000
Wire Wire Line
	3300 5000 4700 5000
Wire Wire Line
	4700 4900 3050 4900
Wire Wire Line
	3050 4900 3050 5500
Wire Wire Line
	2800 5500 2800 4800
Wire Wire Line
	2550 4700 2550 5500
Wire Wire Line
	2550 5700 2550 5800
Wire Wire Line
	2550 5800 2800 5800
Wire Wire Line
	4300 5800 4300 5700
Wire Wire Line
	4050 5700 4050 5800
Connection ~ 4050 5800
Wire Wire Line
	4050 5800 4300 5800
Wire Wire Line
	3800 5800 3800 5700
Connection ~ 3800 5800
Wire Wire Line
	3800 5800 4050 5800
Wire Wire Line
	3550 5700 3550 5800
Connection ~ 3550 5800
Wire Wire Line
	3550 5800 3800 5800
Wire Wire Line
	3300 5800 3300 5700
Connection ~ 3300 5800
Wire Wire Line
	3300 5800 3550 5800
Wire Wire Line
	3050 5800 3050 5700
Connection ~ 3050 5800
Wire Wire Line
	3050 5800 3300 5800
Wire Wire Line
	2800 5700 2800 5800
Connection ~ 2800 5800
Wire Wire Line
	2800 5800 3050 5800
$Comp
L power:GND #PWR?
U 1 1 5CE25566
P 4300 5900
F 0 "#PWR?" H 4300 5650 50  0001 C CNN
F 1 "GND" H 4305 5727 50  0000 C CNN
F 2 "" H 4300 5900 50  0001 C CNN
F 3 "" H 4300 5900 50  0001 C CNN
	1    4300 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 5800 4300 5900
Connection ~ 4300 5800
$Comp
L Device:R_Small R318
U 1 1 5CE366D5
P 7250 5600
F 0 "R318" H 7309 5646 50  0000 L CNN
F 1 "10k" H 7309 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7250 5600 50  0001 C CNN
F 3 "~" H 7250 5600 50  0001 C CNN
	1    7250 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R317
U 1 1 5CE366DB
P 7000 5600
F 0 "R317" H 7059 5646 50  0000 L CNN
F 1 "10k" H 7059 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7000 5600 50  0001 C CNN
F 3 "~" H 7000 5600 50  0001 C CNN
	1    7000 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R316
U 1 1 5CE366E1
P 6750 5600
F 0 "R316" H 6809 5646 50  0000 L CNN
F 1 "10k" H 6809 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6750 5600 50  0001 C CNN
F 3 "~" H 6750 5600 50  0001 C CNN
	1    6750 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R315
U 1 1 5CE366E7
P 6500 5600
F 0 "R315" H 6559 5646 50  0000 L CNN
F 1 "10k" H 6559 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6500 5600 50  0001 C CNN
F 3 "~" H 6500 5600 50  0001 C CNN
	1    6500 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R314
U 1 1 5CE366ED
P 6250 5600
F 0 "R314" H 6309 5646 50  0000 L CNN
F 1 "10k" H 6309 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6250 5600 50  0001 C CNN
F 3 "~" H 6250 5600 50  0001 C CNN
	1    6250 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R313
U 1 1 5CE366F3
P 6000 5600
F 0 "R313" H 6059 5646 50  0000 L CNN
F 1 "10k" H 6059 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6000 5600 50  0001 C CNN
F 3 "~" H 6000 5600 50  0001 C CNN
	1    6000 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R312
U 1 1 5CE366F9
P 5750 5600
F 0 "R312" H 5809 5646 50  0000 L CNN
F 1 "10k" H 5809 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5750 5600 50  0001 C CNN
F 3 "~" H 5750 5600 50  0001 C CNN
	1    5750 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R310
U 1 1 5CE366FF
P 5500 5600
F 0 "R310" H 5559 5646 50  0000 L CNN
F 1 "10k" H 5559 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5500 5600 50  0001 C CNN
F 3 "~" H 5500 5600 50  0001 C CNN
	1    5500 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 5700 5500 5800
Wire Wire Line
	5500 5800 5750 5800
Wire Wire Line
	7250 5800 7250 5700
Wire Wire Line
	7000 5700 7000 5800
Connection ~ 7000 5800
Wire Wire Line
	7000 5800 7250 5800
Wire Wire Line
	6750 5800 6750 5700
Connection ~ 6750 5800
Wire Wire Line
	6750 5800 7000 5800
Wire Wire Line
	6500 5700 6500 5800
Connection ~ 6500 5800
Wire Wire Line
	6500 5800 6750 5800
Wire Wire Line
	6250 5800 6250 5700
Connection ~ 6250 5800
Wire Wire Line
	6250 5800 6500 5800
Wire Wire Line
	6000 5800 6000 5700
Connection ~ 6000 5800
Wire Wire Line
	6000 5800 6250 5800
Wire Wire Line
	5750 5700 5750 5800
Connection ~ 5750 5800
Wire Wire Line
	5750 5800 6000 5800
$Comp
L power:GND #PWR?
U 1 1 5CE36722
P 7250 5900
F 0 "#PWR?" H 7250 5650 50  0001 C CNN
F 1 "GND" H 7255 5727 50  0000 C CNN
F 2 "" H 7250 5900 50  0001 C CNN
F 3 "" H 7250 5900 50  0001 C CNN
	1    7250 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 5800 7250 5900
Connection ~ 7250 5800
Wire Wire Line
	5500 5500 5500 5400
Wire Wire Line
	5500 5400 5200 5400
Wire Wire Line
	5200 5300 5750 5300
Wire Wire Line
	5750 5300 5750 5500
Wire Wire Line
	6000 5500 6000 5200
Wire Wire Line
	6250 5100 6250 5500
Wire Wire Line
	6500 5500 6500 5000
Wire Wire Line
	6750 4900 6750 5500
Wire Wire Line
	7000 5500 7000 4800
Wire Wire Line
	7250 4700 7250 5500
Wire Wire Line
	5300 5500 5300 5900
Wire Wire Line
	2350 5400 4300 5400
Connection ~ 4300 5400
Wire Wire Line
	4050 5300 2350 5300
Connection ~ 4050 5300
Wire Wire Line
	2350 5200 3800 5200
Connection ~ 3800 5200
Wire Wire Line
	3550 5100 2350 5100
Connection ~ 3550 5100
Wire Wire Line
	2350 5000 3300 5000
Connection ~ 3300 5000
Wire Wire Line
	3050 4900 2350 4900
Connection ~ 3050 4900
Wire Wire Line
	2350 4800 2800 4800
Connection ~ 2800 4800
Wire Wire Line
	2550 4700 2350 4700
Connection ~ 2550 4700
Wire Wire Line
	7450 5400 5500 5400
Connection ~ 5500 5400
Wire Wire Line
	5750 5300 7450 5300
Connection ~ 5750 5300
Wire Wire Line
	5200 5200 6000 5200
Connection ~ 6000 5200
Wire Wire Line
	6000 5200 7450 5200
Connection ~ 6250 5100
Wire Wire Line
	6250 5100 7450 5100
Connection ~ 6500 5000
Wire Wire Line
	6500 5000 7450 5000
Connection ~ 6750 4900
Wire Wire Line
	6750 4900 7450 4900
Connection ~ 7000 4800
Wire Wire Line
	7000 4800 7450 4800
Connection ~ 7250 4700
Wire Wire Line
	7250 4700 7450 4700
Text Label 7550 1600 0    50   ~ 0
Front_End_Control1
Text Label 7550 1700 0    50   ~ 0
Front_End_Control2
$Comp
L POWDER_Digital:TCA9539-Q1 U302
U 1 1 5CE9FCFE
P 6650 2300
F 0 "U302" H 6200 3250 50  0000 C CNN
F 1 "TCA9539-Q1" H 6950 3250 50  0000 C CNN
F 2 "Package_SO:TSSOP-24_4.4x7.8mm_P0.65mm" H 7700 1300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tca9535.pdf" H 6150 3200 50  0001 C CNN
	1    6650 2300
	1    0    0    -1  
$EndComp
Text Label 7550 1800 0    50   ~ 0
Front_End_Control3
Text Label 7550 1500 0    50   ~ 0
Front_End_Control0
Text Label 7550 2100 0    50   ~ 0
Front_End_Control6
Text Label 7550 1900 0    50   ~ 0
Front_End_Control4
Text Label 7550 2000 0    50   ~ 0
Front_End_Control5
Text Label 7550 2200 0    50   ~ 0
Front_End_Control7
Wire Wire Line
	7350 2200 7550 2200
Wire Wire Line
	7550 2100 7350 2100
Wire Wire Line
	7350 2000 7550 2000
Wire Wire Line
	7550 1900 7350 1900
Wire Wire Line
	7350 1800 7550 1800
Wire Wire Line
	7550 1700 7350 1700
Wire Wire Line
	7350 1600 7550 1600
Wire Wire Line
	7350 1500 7550 1500
$Comp
L power:GND #PWR?
U 1 1 5CE9FD12
P 6650 3500
F 0 "#PWR?" H 6650 3250 50  0001 C CNN
F 1 "GND" H 6655 3327 50  0000 C CNN
F 2 "" H 6650 3500 50  0001 C CNN
F 3 "" H 6650 3500 50  0001 C CNN
	1    6650 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3400 6650 3500
$Comp
L power:GND #PWR?
U 1 1 5CE9FD19
P 5850 3500
F 0 "#PWR?" H 5850 3250 50  0001 C CNN
F 1 "GND" H 5855 3327 50  0000 C CNN
F 2 "" H 5850 3500 50  0001 C CNN
F 3 "" H 5850 3500 50  0001 C CNN
	1    5850 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3500 5850 3100
Wire Wire Line
	5850 3000 5950 3000
Wire Wire Line
	5950 3100 5850 3100
Connection ~ 5850 3100
Wire Wire Line
	5850 3100 5850 3000
Wire Wire Line
	7350 2400 7550 2400
Wire Wire Line
	7350 2500 7550 2500
Wire Wire Line
	7350 2600 7550 2600
Wire Wire Line
	7350 2700 7550 2700
Wire Wire Line
	7350 2800 7550 2800
$Comp
L Device:C_Small C304
U 1 1 5CE9FD29
P 5450 2550
F 0 "C304" H 5542 2596 50  0000 L CNN
F 1 "10nF" H 5542 2505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5450 2550 50  0001 C CNN
F 3 "~" H 5450 2550 50  0001 C CNN
	1    5450 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C303
U 1 1 5CE9FD2F
P 5050 2550
F 0 "C303" H 5142 2596 50  0000 L CNN
F 1 "1uF" H 5142 2505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5050 2550 50  0001 C CNN
F 3 "~" H 5050 2550 50  0001 C CNN
	1    5050 2550
	1    0    0    -1  
$EndComp
$Comp
L endpoint_shield:+3V #PWR?
U 1 1 5CE9FD39
P 6650 1100
F 0 "#PWR?" H 6650 950 50  0001 C CNN
F 1 "+3V" H 6665 1273 50  0000 C CNN
F 2 "" H 6650 1100 50  0001 C CNN
F 3 "" H 6650 1100 50  0001 C CNN
	1    6650 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 1100 6650 1200
Wire Wire Line
	5450 2450 5450 2350
Wire Wire Line
	5450 2350 5050 2350
Wire Wire Line
	5050 2350 5050 2450
Wire Wire Line
	5050 2650 5050 2750
Wire Wire Line
	5050 2750 5450 2750
Wire Wire Line
	5450 2750 5450 2650
Wire Wire Line
	5050 2750 5050 2850
Connection ~ 5050 2750
$Comp
L power:GND #PWR?
U 1 1 5CE9FD48
P 5050 2850
F 0 "#PWR?" H 5050 2600 50  0001 C CNN
F 1 "GND" H 5055 2677 50  0000 C CNN
F 2 "" H 5050 2850 50  0001 C CNN
F 3 "" H 5050 2850 50  0001 C CNN
	1    5050 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2350 5050 2250
Connection ~ 5050 2350
$Comp
L endpoint_shield:+3V #PWR?
U 1 1 5CE9FD50
P 5050 2250
F 0 "#PWR?" H 5050 2100 50  0001 C CNN
F 1 "+3V" H 5065 2423 50  0000 C CNN
F 2 "" H 5050 2250 50  0001 C CNN
F 3 "" H 5050 2250 50  0001 C CNN
	1    5050 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 2900 7550 2900
Wire Wire Line
	7550 3000 7350 3000
Wire Wire Line
	7350 3100 7550 3100
$Comp
L Device:R_Small R311
U 1 1 5CE9FD61
P 5550 1250
F 0 "R311" H 5609 1296 50  0000 L CNN
F 1 "10k" H 5609 1205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5550 1250 50  0001 C CNN
F 3 "~" H 5550 1250 50  0001 C CNN
	1    5550 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1800 5550 1800
Wire Wire Line
	5550 1350 5550 1800
Connection ~ 5550 1800
Wire Wire Line
	5550 1800 5950 1800
$Comp
L power:+5V #PWR?
U 1 1 5CE9FD6F
P 5550 1050
F 0 "#PWR?" H 5550 900 50  0001 C CNN
F 1 "+5V" H 5565 1223 50  0000 C CNN
F 2 "" H 5550 1050 50  0001 C CNN
F 3 "" H 5550 1050 50  0001 C CNN
	1    5550 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 1050 5550 1150
Text Label 7550 2500 0    50   ~ 0
Front_End_Control9
Text Label 7550 2600 0    50   ~ 0
Front_End_Control10
Text Label 7550 2700 0    50   ~ 0
Front_End_Control11
Text Label 7550 2400 0    50   ~ 0
Front_End_Control8
Text Label 7550 3000 0    50   ~ 0
Front_End_Control14
Text Label 7550 2800 0    50   ~ 0
Front_End_Control12
Text Label 7550 2900 0    50   ~ 0
Front_End_Control13
Text Label 7550 3100 0    50   ~ 0
Front_End_Control15
Text Label 2350 5300 2    50   ~ 0
Front_End_Control1
Text Label 2350 5200 2    50   ~ 0
Front_End_Control2
Text Label 2350 5100 2    50   ~ 0
Front_End_Control3
Text Label 2350 5400 2    50   ~ 0
Front_End_Control0
Text Label 2350 4800 2    50   ~ 0
Front_End_Control6
Text Label 2350 5000 2    50   ~ 0
Front_End_Control4
Text Label 2350 4900 2    50   ~ 0
Front_End_Control5
Text Label 2350 4700 2    50   ~ 0
Front_End_Control7
Text Label 7450 4800 0    50   ~ 0
Front_End_Control9
Text Label 7450 4900 0    50   ~ 0
Front_End_Control10
Text Label 7450 5000 0    50   ~ 0
Front_End_Control11
Text Label 7450 4700 0    50   ~ 0
Front_End_Control8
Text Label 7450 5300 0    50   ~ 0
Front_End_Control14
Text Label 7450 5100 0    50   ~ 0
Front_End_Control12
Text Label 7450 5200 0    50   ~ 0
Front_End_Control13
Text Label 7450 5400 0    50   ~ 0
Front_End_Control15
Wire Wire Line
	5150 1500 5950 1500
Wire Wire Line
	5150 1600 5950 1600
Text HLabel 5150 1500 0    50   Input ~ 0
SDA
Text HLabel 5150 1600 0    50   Input ~ 0
SCL
Text HLabel 5150 1800 0    50   Input ~ 0
TCA_RESET
$Comp
L POWDER_Power:LDL1117S30R U?
U 1 1 5CE6C186
P 2500 1950
AR Path="/5CE6C186" Ref="U?"  Part="1" 
AR Path="/5C88CE2B/5CE6C186" Ref="U301"  Part="1" 
F 0 "U301" H 2500 2192 50  0000 C CNN
F 1 "LDL1117S30R" H 2500 2101 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 2500 2150 50  0001 C CNN
F 3 "http://www.diodes.com/datasheets/AP1117.pdf" H 2600 1700 50  0001 C CNN
	1    2500 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5CE6C18C
P 2000 2250
AR Path="/5CE6C18C" Ref="C?"  Part="1" 
AR Path="/5C88CE2B/5CE6C18C" Ref="C301"  Part="1" 
F 0 "C301" H 2092 2296 50  0000 L CNN
F 1 "1uF" H 2092 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2000 2250 50  0001 C CNN
F 3 "~" H 2000 2250 50  0001 C CNN
	1    2000 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5CE6C192
P 3000 2250
AR Path="/5CE6C192" Ref="C?"  Part="1" 
AR Path="/5C88CE2B/5CE6C192" Ref="C302"  Part="1" 
F 0 "C302" H 3092 2296 50  0000 L CNN
F 1 "4.7uF" H 3092 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3000 2250 50  0001 C CNN
F 3 "~" H 3000 2250 50  0001 C CNN
	1    3000 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 1950 2000 1950
Wire Wire Line
	2000 1950 2000 2150
Wire Wire Line
	2800 1950 3000 1950
Wire Wire Line
	3000 1950 3000 2150
$Comp
L power:GND #PWR?
U 1 1 5CE6C19C
P 3000 2550
AR Path="/5CE6C19C" Ref="#PWR?"  Part="1" 
AR Path="/5C88CE2B/5CE6C19C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3000 2300 50  0001 C CNN
F 1 "GND" H 3005 2377 50  0000 C CNN
F 2 "" H 3000 2550 50  0001 C CNN
F 3 "" H 3000 2550 50  0001 C CNN
	1    3000 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2350 2000 2450
Wire Wire Line
	2000 2450 2500 2450
Wire Wire Line
	3000 2450 3000 2550
Wire Wire Line
	3000 2450 3000 2350
Connection ~ 3000 2450
Wire Wire Line
	2500 2250 2500 2450
Connection ~ 2500 2450
Wire Wire Line
	2500 2450 3000 2450
Wire Wire Line
	2000 1950 1800 1950
Wire Wire Line
	1800 1950 1800 1750
Connection ~ 2000 1950
$Comp
L power:+5V #PWR?
U 1 1 5CE6C1AD
P 1800 1750
AR Path="/5CE6C1AD" Ref="#PWR?"  Part="1" 
AR Path="/5C88CE2B/5CE6C1AD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1800 1600 50  0001 C CNN
F 1 "+5V" H 1815 1923 50  0000 C CNN
F 2 "" H 1800 1750 50  0001 C CNN
F 3 "" H 1800 1750 50  0001 C CNN
	1    1800 1750
	1    0    0    -1  
$EndComp
Connection ~ 3000 1950
Wire Wire Line
	3000 1950 3200 1950
Wire Wire Line
	3200 1950 3200 1750
$Comp
L endpoint_shield:+3V #PWR?
U 1 1 5CE6C1B6
P 3200 1750
AR Path="/5CE6C1B6" Ref="#PWR?"  Part="1" 
AR Path="/5C88CE2B/5CE6C1B6" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3200 1600 50  0001 C CNN
F 1 "+3V" H 3215 1923 50  0000 C CNN
F 2 "" H 3200 1750 50  0001 C CNN
F 3 "" H 3200 1750 50  0001 C CNN
	1    3200 1750
	1    0    0    -1  
$EndComp
NoConn ~ 5950 1700
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J301
U 1 1 5CF923A5
P 4900 5000
F 0 "J301" H 4950 5617 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 4950 5526 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 4900 5000 50  0001 C CNN
F 3 "~" H 4900 5000 50  0001 C CNN
	1    4900 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 4800 4700 4800
Wire Wire Line
	2550 4700 4700 4700
Wire Wire Line
	5200 4700 7250 4700
Wire Wire Line
	5200 4800 7000 4800
Wire Wire Line
	5200 4900 6750 4900
Wire Wire Line
	5200 5000 6500 5000
Wire Wire Line
	5200 5100 6250 5100
NoConn ~ 4700 4600
NoConn ~ 5200 4600
$EndSCHEMATC
