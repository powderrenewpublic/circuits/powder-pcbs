import math

def do_calc(Io, Vo, R_L, Vi_max, Vi_min, V_fwd, R_sw, f_sw, K_ind, Lo, Co,
            C_j, I_step, V_step, V_ripple, Ci, t_ss, V_start, V_stop, Rfb1,
            R_esr):
    f_sw_skip = 1/100e-9*(Io*R_L+Vo+V_fwd)/(Vo-Io*R_sw+V_fwd)
    # 6A max current
    f_sw_shift = 8/100e-9*(6*R_L+0.1+V_fwd)/(Vo-Io*R_sw+V_fwd)
    print("f_sw_skip", f_sw_skip)
    print("f_sw_shift", f_sw_shift)

    R_T = 101756/(f_sw/1e3)**1.008*1e3
    print("R_T", R_T)

    Lo_min = (Vi_max-Vo)/(Io*K_ind)*Vo/(Vi_max*f_sw)
    print("Lo_min", Lo_min)

    assert Lo > Lo_min

    I_ripple = (Vo*(Vi_max-Vo))/(Vi_max*Lo*f_sw)
    I_L_RMS = math.sqrt(Io**2+1/12*I_ripple**2)
    I_L_peak = Io + I_ripple/2
    print("I_ripple", I_ripple)
    print("I_L_RMS", I_L_RMS)
    print("I_L_peak", I_L_peak)

    Co_min1 = 2*I_step/(f_sw*V_step)
    Co_min2 = Lo * (I_step**2 - 0**2) / ((Vo+V_step)**2 - Vo**2)
    Co_min3 = 1/(8*f_sw*V_ripple/I_ripple)
    print("Co_min1", Co_min1)
    print("Co_min2", Co_min2)
    print("Co_min3", Co_min3)

    assert Co > max(Co_min1, Co_min2, Co_min3)

    R_esr_max = V_ripple / I_ripple
    I_CO_RMS = Vo*(Vi_min-Vo)/(math.sqrt(12)*Vi_min*Lo*f_sw)

    print("R_esr_max", R_esr_max)
    assert R_esr < R_esr_max
    print("I_CO_RMS", I_CO_RMS)

    P_D = (Vi_max-Vo)*Io*V_fwd/Vi_max + C_j*f_sw*(Vi_max+V_fwd)**2/2
    print("P_D", P_D)

    I_CI_RMS = Io * math.sqrt(Vo/Vi_min * (Vi_min - Vo) / Vi_min)
    print("I_CI_RMS", I_CI_RMS)

    Vi_ripple = Io * 0.25 / (Ci * f_sw)
    print("Vi_ripple", Vi_ripple)

    I_ss = 1.7e-6 # Device constant
    V_ref = 0.8 # Device constant

    C_ss = t_ss * I_ss / (V_ref * 0.8)
    print("C_ss", C_ss)

    I_hys = 3.4e-6 # Device constant
    V_en_th = 1.2 # Device constant
    I_1 = 1.2e-6 # Device constant

    Ren1 = (V_start - V_stop) / I_hys
    Ren2 = V_en_th / ((V_start - V_en_th) / Ren1) + I_1
    print("Ren1", Ren1)
    print("Ren2", Ren2)

    Rfb2 = Rfb1 * (Vo - V_ref) / V_ref
    print("Rfb2", Rfb2)

    f_P_mod = Io / (2*math.pi*Vo*Co)
    f_Z_mod = 1 / (2*math.pi*R_esr*Co)
    f_CO1 = math.sqrt(f_P_mod*f_Z_mod)
    f_CO2 = math.sqrt(f_P_mod*f_sw/2)

    print("f_P_mod", f_P_mod)
    print("f_Z_mod", f_Z_mod)
    print("f_CO1", f_CO1)
    print("f_CO2", f_CO2)

    f_CO = math.sqrt(f_CO1 * f_CO2)
    print("f_CO", f_CO)

    g_m_ps = 17
    g_m_ea = 350e-6

    R_comp = 2*math.pi*f_CO * Co / g_m_ps * Vo / (V_ref * g_m_ea)
    C_comp = 1 / (2*math.pi*R_comp*f_P_mod)
    print("R_comp", R_comp)
    print("C_comp", C_comp)

    C_pole_comp1 = Co * R_esr / R_comp
    C_pole_comp2 = 1 / (math.pi*R_comp*f_sw)
    C_pole_comp = max(C_pole_comp1, C_pole_comp2)

    print("C_pole_comp", C_pole_comp)

    tr = Vi_max * 0.16e-9 + 3e-9
    Qg = 3e-9
    Iq = 152e-6

    P_cond = Io**2 * R_sw * Vo / Vi_max
    P_sw = Vi_max * f_sw * Io * tr
    P_G = Vi_max * Qg * f_sw
    P_Q = Vi_max * Iq
    P_tot = P_cond + P_sw + P_G + P_Q
    print("P_tot", P_tot)


print("Exp NUC1")
do_calc(Io = 5,
        Vo = 19,
        R_L = 32.5e-3,
        Vi_max = 60,
        Vi_min = 30,
        V_fwd = 0.542,
        R_sw = 87e-3,
        f_sw = 400e3,

        K_ind = 0.3,

        Lo = 22e-6,

        # 3x 22uF caps +/-20% tol +/-15% w/ temp
        Co = 22e-6*3*0.8*0.85,
        R_esr = 2e-3 / 3,

        # diode junction cap
        C_j = 672e-12,

        I_step = 3,
        V_step = 0.5,
        V_ripple = 0.05,

        Ci = 2.2e-6 * 4 * 0.8 * 0.85,

        t_ss = 1e-3,

        V_start = 45,
        V_stop = 30,

        Rfb1 = 10e3,
       )

print()
print()
print()
print("Exp NUC2")
do_calc(Io = 5,
        Vo = 19,
        R_L = 32.5e-3,
        Vi_max = 60,
        Vi_min = 36,
        V_fwd = 0.542,
        R_sw = 87e-3,
        f_sw = 400e3,

        K_ind = 0.3,

        Lo = 22e-6,

        # 3x 22uF caps +/-20% tol +/-15% w/ temp
        Co = 22e-6*3*0.8*0.85,
        R_esr = 2e-3 / 3,

        # diode junction cap
        C_j = 672e-12,

        I_step = 3,
        V_step = 0.5,
        V_ripple = 0.05,

        Ci = 2.2e-6 * 4 * 0.8 * 0.85,

        t_ss = 1e-3,

        V_start = 45,
        V_stop = 36,

        Rfb1 = 10e3,
       )

print()
print()
print()
print("IRIS")
do_calc(Io = 2,
        Vo = 12,
        R_L = 32.5e-3,
        Vi_max = 60,
        Vi_min = 42,
        V_fwd = 0.542,
        R_sw = 87e-3,
        f_sw = 400e3,

        K_ind = 0.6,

        Lo = 22e-6,

        # 3x 22uF caps +/-20% tol +/-15% w/ temp
        Co = 22e-6*3*0.8*0.85,
        R_esr = 2e-3 / 3,

        # diode junction cap
        C_j = 672e-12,

        I_step = 3,
        V_step = 0.5,
        V_ripple = 0.05,

        Ci = 2.2e-6 * 4 * 0.8 * 0.85,

        t_ss = 1e-3,

        V_start = 45,
        V_stop = 42,

        Rfb1 = 10e3,
       )

d = 0.254 # Oshpark min via
cu_t = 0.070 # 2 oz copper
sigma_cu = 392 # copper thermal conductivity
board_t = 0.8
print("0.254 via, 2oz 0.8mm thick, R_therm = ",
      math.pi*d*cu_t*sigma_cu/board_t)
