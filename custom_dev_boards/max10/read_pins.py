with open("pinouts_04.txt", "r") as pinouts_file:
    pinouts_file.readline()
    i = -200
    for line in pinouts_file:
        bank_number, vref, pin_name, optional_function, config_function, dedicated_txrx, emulated_lvds_output, io_performance, pin = line.split("\t")
        pin = pin.strip()
        #print(f"{pin:4} {pin_name:10} {vref:8} {optional_function:20} {config_function:10} {bank_number}")
        #print(bank_number, vref, pin_name, pin)
        name = ""

        if pin_name == "IO":
            function = "B"
        if pin_name == "Input_only":
            function = "I"
        # So far unseen, just to mark the "O" function
        if pin_name == "Output_only":
            function = "O"

        if pin_name != "IO" and pin_name != "Input_only":
            name = pin_name
            function = "W"
        elif optional_function != "":
            name = optional_function
        elif config_function != "":
            name = config_function

        if dedicated_txrx != "" and config_function == "":
            if name != "":
                name += "_"
            name += dedicated_txrx

        if name == "":
            name = f"IO_{pin}"

        if name[0] == "n":
            name = "~" + name[1:]

        print(f"X {name} {pin} 0 {i} 100 R 50 50 1 1 {function}")
        i -= 100

with open("pinouts_50.txt", "r") as pinouts_file:
    pinouts_file.readline()
    i = -200
    for line in pinouts_file:
        bank_number, vref, pin_name, optional_function, config_function, dedicated_txrx, emulated_lvds_output, io_performance, pin, dqs_x8, dqs_x16 = line.split("\t")
        #print(f"{pin:4} {pin_name:10} {vref:8} {optional_function:20} {config_function:10} {bank_number}")
        #print(bank_number, vref, pin_name, pin)
        name = ""

        if pin_name == "IO":
            function = "B"
        if pin_name == "Input_only":
            function = "I"
        # So far unseen, just to mark the "O" function
        if pin_name == "Output_only":
            function = "O"

        if pin_name != "IO" and pin_name != "Input_only":
            name = pin_name
            function = "W"
        elif optional_function != "":
            name = optional_function
        elif config_function != "":
            name = config_function

        if dedicated_txrx != "" and config_function == "":
            if name != "":
                name += "_"
            name += dedicated_txrx

        if name == "":
            name = f"IO_{pin}"

        if name[0] == "n":
            name = "~" + name[1:]

        print(f"X {name} {pin} 0 {i} 100 R 50 50 1 1 {function}")
        i -= 100
