EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2000 2500 1500 1500
U 5F853053
F0 "reg1" 50
F1 "low_voltage_rulator.sch" 50
F2 "SYNC_1" I L 2000 3000 50 
F3 "VIN" I L 2000 2750 50 
F4 "SYNC_2" I L 2000 3100 50 
F5 "SYNC_3" I L 2000 3200 50 
F6 "PG_1" O R 3500 3000 50 
F7 "PG_2" O R 3500 3100 50 
F8 "PG_3" O R 3500 3200 50 
F9 "EN_1" I L 2000 3500 50 
F10 "EN_2" I L 2000 3600 50 
F11 "EN_3" I L 2000 3700 50 
$EndSheet
$Sheet
S 5500 2000 2000 2000
U 5F8E5420
F0 "CPLD" 50
F1 "CPLD.sch" 50
$EndSheet
Text Notes 1450 2600 0    50   ~ 0
VIN 3.3V
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 605153FE
P 800 2950
F 0 "J1" H 908 3231 50  0000 C CNN
F 1 "Conn_01x04_Male" H 908 3140 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 800 2950 50  0001 C CNN
F 3 "~" H 800 2950 50  0001 C CNN
	1    800  2950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J2
U 1 1 605159C9
P 800 3700
F 0 "J2" H 908 3981 50  0000 C CNN
F 1 "Conn_01x04_Male" H 908 3890 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 800 3700 50  0001 C CNN
F 3 "~" H 800 3700 50  0001 C CNN
	1    800  3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J3
U 1 1 60515D7E
P 800 4450
F 0 "J3" H 908 4731 50  0000 C CNN
F 1 "Conn_01x04_Male" H 908 4640 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 800 4450 50  0001 C CNN
F 3 "~" H 800 4450 50  0001 C CNN
	1    800  4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60519A99
P 1150 2850
F 0 "#PWR?" H 1150 2600 50  0001 C CNN
F 1 "GND" V 1155 2722 50  0000 R CNN
F 2 "" H 1150 2850 50  0001 C CNN
F 3 "" H 1150 2850 50  0001 C CNN
	1    1150 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1150 2850 1000 2850
$Comp
L power:GND #PWR?
U 1 1 6051AC20
P 1150 3600
F 0 "#PWR?" H 1150 3350 50  0001 C CNN
F 1 "GND" V 1155 3472 50  0000 R CNN
F 2 "" H 1150 3600 50  0001 C CNN
F 3 "" H 1150 3600 50  0001 C CNN
	1    1150 3600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1150 3600 1000 3600
$Comp
L power:GND #PWR?
U 1 1 6051AE4D
P 1150 4350
F 0 "#PWR?" H 1150 4100 50  0001 C CNN
F 1 "GND" V 1155 4222 50  0000 R CNN
F 2 "" H 1150 4350 50  0001 C CNN
F 3 "" H 1150 4350 50  0001 C CNN
	1    1150 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1150 4350 1000 4350
Wire Wire Line
	1000 2950 1200 2950
Wire Wire Line
	3500 3000 3750 3000
Wire Wire Line
	3500 3100 3750 3100
Wire Wire Line
	3500 3200 3750 3200
Wire Wire Line
	1000 3050 1200 3050
Wire Wire Line
	1000 3150 1200 3150
Wire Wire Line
	1000 3700 1200 3700
Wire Wire Line
	1000 3800 1200 3800
Wire Wire Line
	1000 3900 1200 3900
Wire Wire Line
	1000 4450 1200 4450
Wire Wire Line
	1000 4550 1200 4550
Wire Wire Line
	1000 4650 1200 4650
Text Label 1700 3000 0    50   ~ 0
SYNC_1
Text Label 1700 3100 0    50   ~ 0
SYNC_2
Text Label 1700 3200 0    50   ~ 0
SYNC_3
Wire Wire Line
	1700 3200 2000 3200
Wire Wire Line
	1700 3100 2000 3100
Wire Wire Line
	1700 3000 2000 3000
Text Label 1700 3500 0    50   ~ 0
EN_1
Wire Wire Line
	1700 3500 2000 3500
Wire Wire Line
	1700 3600 2000 3600
Wire Wire Line
	1700 3700 2000 3700
Text Label 1700 3600 0    50   ~ 0
EN_2
Text Label 1700 3700 0    50   ~ 0
EN_3
Text Label 3600 3000 0    50   ~ 0
PG_1
Text Label 3600 3100 0    50   ~ 0
PG_2
Text Label 3600 3200 0    50   ~ 0
PG_3
Text Label 1050 3050 0    50   ~ 0
SYNC_1
Text Label 1050 3800 0    50   ~ 0
SYNC_2
Text Label 1050 4550 0    50   ~ 0
SYNC_3
Text Label 1050 2950 0    50   ~ 0
EN_1
Text Label 1050 3700 0    50   ~ 0
EN_2
Text Label 1050 4450 0    50   ~ 0
EN_3
Text Label 1050 3150 0    50   ~ 0
PG_1
Text Label 1050 3900 0    50   ~ 0
PG_2
Text Label 1050 4650 0    50   ~ 0
PG_3
$Comp
L Connector:Screw_Terminal_01x02 J4
U 1 1 60525308
P 900 2350
F 0 "J4" H 818 2025 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 818 2116 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 900 2350 50  0001 C CNN
F 3 "~" H 900 2350 50  0001 C CNN
	1    900  2350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 605259E7
P 1200 2450
F 0 "#PWR?" H 1200 2200 50  0001 C CNN
F 1 "GND" H 1205 2277 50  0000 C CNN
F 2 "" H 1200 2450 50  0001 C CNN
F 3 "" H 1200 2450 50  0001 C CNN
	1    1200 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 2450 1200 2350
Wire Wire Line
	1200 2350 1100 2350
Wire Wire Line
	1100 2250 1850 2250
Wire Wire Line
	1850 2250 1850 2750
Wire Wire Line
	1850 2750 2000 2750
Text Label 1500 2250 0    50   ~ 0
VIN
$EndSCHEMATC
