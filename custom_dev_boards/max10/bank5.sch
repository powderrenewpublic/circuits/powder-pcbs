EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L POWDER_Digital:10M50DAF256 U401
U 5 1 5F9B3C6F
P 4500 3250
F 0 "U401" H 4500 3400 50  0000 C CNN
F 1 "10M50DAF256" H 4500 3250 50  0000 C CNN
F 2 "Package_BGA:BGA-256_17.0x17.0mm_Layout16x16_P1.0mm_Ball0.5mm_Pad0.4mm_NSMD" H 4500 3250 50  0001 C CNN
F 3 "" H 4500 3250 50  0001 C CNN
	5    4500 3250
	1    0    0    -1  
$EndComp
$Comp
L 000_PreferredParts:C_1nF C?
U 1 1 5FBA70C6
P 4800 2000
AR Path="/5F8E5420/5F971A6D/5FBA70C6" Ref="C?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FBA70C6" Ref="C803"  Part="1" 
F 0 "C803" H 4825 2100 50  0000 L CNN
F 1 "C_1nF" H 4825 1900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4838 1850 50  0001 C CNN
F 3 "~" H 4800 2000 50  0001 C CNN
F 4 "3000-0000000128" H 4800 2000 50  0001 C CNN "PartSpecification"
	1    4800 2000
	1    0    0    -1  
$EndComp
$Comp
L 000_PreferredParts:C_1nF C?
U 1 1 5FBA70CD
P 3750 2000
AR Path="/5F8E5420/5F971A6D/5FBA70CD" Ref="C?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FBA70CD" Ref="C801"  Part="1" 
F 0 "C801" H 3775 2100 50  0000 L CNN
F 1 "C_1nF" H 3775 1900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3788 1850 50  0001 C CNN
F 3 "~" H 3750 2000 50  0001 C CNN
F 4 "3000-0000000128" H 3750 2000 50  0001 C CNN "PartSpecification"
	1    3750 2000
	1    0    0    -1  
$EndComp
$Comp
L 000_PreferredParts:C_0_1uF C?
U 1 1 5FBA70D6
P 5150 2000
AR Path="/5F8E5420/5F971A6D/5FBA70D6" Ref="C?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FBA70D6" Ref="C804"  Part="1" 
F 0 "C804" H 5175 2100 50  0000 L CNN
F 1 "C_0_1uF" H 5175 1900 50  0001 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5188 1850 50  0001 C CNN
F 3 "~" H 5150 2000 50  0001 C CNN
F 4 "0.1uF" H 5300 1900 50  0000 C CNN "DisplayValue"
F 5 "3000-0000000125" H 5150 2000 50  0001 C CNN "PartSpecification"
F 6 "100V" H 5150 2000 50  0001 C CNN "Voltage"
	1    5150 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FBA70DC
P 5500 2000
AR Path="/5F8E5420/5F971A6D/5FBA70DC" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FBA70DC" Ref="R806"  Part="1" 
F 0 "R806" V 5580 2000 50  0000 C CNN
F 1 "10k" V 5500 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5430 2000 50  0001 C CNN
F 3 "~" H 5500 2000 50  0001 C CNN
	1    5500 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBA70E2
P 5500 2200
AR Path="/5F8E5420/5F971A6D/5FBA70E2" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FBA70E2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5500 1950 50  0001 C CNN
F 1 "GND" H 5500 2050 50  0000 C CNN
F 2 "" H 5500 2200 50  0001 C CNN
F 3 "" H 5500 2200 50  0001 C CNN
	1    5500 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2200 5500 2150
Wire Wire Line
	4800 2150 4800 2200
Wire Wire Line
	4800 2200 5150 2200
Connection ~ 5500 2200
Wire Wire Line
	5150 2200 5150 2150
Connection ~ 5150 2200
Wire Wire Line
	5150 2200 5500 2200
Wire Wire Line
	4600 2350 4600 1750
Wire Wire Line
	4600 1750 4800 1750
Wire Wire Line
	5500 1750 5500 1850
Wire Wire Line
	5150 1850 5150 1750
Connection ~ 5150 1750
Wire Wire Line
	5150 1750 5500 1750
Wire Wire Line
	4800 1850 4800 1750
Connection ~ 4800 1750
Wire Wire Line
	4800 1750 5150 1750
$Comp
L power:GND #PWR?
U 1 1 5FBA70F8
P 3750 2200
AR Path="/5F8E5420/5F971A6D/5FBA70F8" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FBA70F8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3750 1950 50  0001 C CNN
F 1 "GND" H 3750 2050 50  0000 C CNN
F 2 "" H 3750 2200 50  0001 C CNN
F 3 "" H 3750 2200 50  0001 C CNN
	1    3750 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2200 3750 2150
Wire Wire Line
	3750 1850 3750 1750
Wire Wire Line
	3750 1750 4500 1750
Wire Wire Line
	4500 1750 4500 2350
Wire Wire Line
	4500 1750 4500 1650
Wire Wire Line
	4500 1650 4600 1650
Wire Wire Line
	4600 1650 4600 1750
Connection ~ 4500 1750
Connection ~ 4600 1750
$Comp
L 000_PreferredParts:C_1nF C?
U 1 1 5FBA7108
P 4100 2000
AR Path="/5F8E5420/5F971A6D/5FBA7108" Ref="C?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FBA7108" Ref="C802"  Part="1" 
F 0 "C802" H 4125 2100 50  0000 L CNN
F 1 "C_1nF" H 4125 1900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4138 1850 50  0001 C CNN
F 3 "~" H 4100 2000 50  0001 C CNN
F 4 "3000-0000000128" H 4100 2000 50  0001 C CNN "PartSpecification"
	1    4100 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBA710E
P 4100 2200
AR Path="/5F8E5420/5F971A6D/5FBA710E" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FBA710E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4100 1950 50  0001 C CNN
F 1 "GND" H 4100 2050 50  0000 C CNN
F 2 "" H 4100 2200 50  0001 C CNN
F 3 "" H 4100 2200 50  0001 C CNN
	1    4100 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2200 4100 2150
Wire Wire Line
	4100 1850 4400 1850
Wire Wire Line
	4400 1850 4400 2350
Wire Wire Line
	4400 1850 4400 1650
Wire Wire Line
	4400 1650 4500 1650
Connection ~ 4400 1850
Connection ~ 4500 1650
Text Label 5300 1750 0    50   ~ 0
VDDIO_5
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5FDE38AD
P 6100 1850
AR Path="/5F8E5420/5F971A6D/5FDE38AD" Ref="J?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FDE38AD" Ref="J802"  Part="1" 
F 0 "J802" H 6100 1950 50  0000 C CNN
F 1 "Conn_01x02" H 6100 1650 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 6100 1850 50  0001 C CNN
F 3 "~" H 6100 1850 50  0001 C CNN
	1    6100 1850
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDE38B3
P 5800 1950
AR Path="/5F8E5420/5F971A6D/5FDE38B3" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FDE38B3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5800 1700 50  0001 C CNN
F 1 "GND" H 5800 1800 50  0000 C CNN
F 2 "" H 5800 1950 50  0001 C CNN
F 3 "" H 5800 1950 50  0001 C CNN
	1    5800 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1950 5800 1850
Wire Wire Line
	5800 1850 5900 1850
Wire Wire Line
	5900 1750 5500 1750
Connection ~ 5500 1750
Text Label 3150 2800 2    50   ~ 0
DIFF_R1_P
Text Label 3150 3000 2    50   ~ 0
DIFF_R1_N
$Comp
L Device:R_Small R?
U 1 1 5FE16AC1
P 2550 2900
AR Path="/5F8E5420/5F955280/5FE16AC1" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE16AC1" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE16AC1" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE16AC1" Ref="R803"  Part="1" 
F 0 "R803" H 2580 2920 50  0000 L CNN
F 1 "DNP" H 2580 2860 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 2550 2900 50  0001 C CNN
F 3 "~" H 2550 2900 50  0001 C CNN
	1    2550 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2550 3000 3350 3000
Wire Wire Line
	2550 2800 3350 2800
Text Label 3150 2900 2    50   ~ 0
DIFF_R2_P
Text Label 3150 3100 2    50   ~ 0
DIFF_R2_N
$Comp
L Device:R_Small R?
U 1 1 5FE16ACB
P 2200 3000
AR Path="/5F8E5420/5F955280/5FE16ACB" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE16ACB" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE16ACB" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE16ACB" Ref="R801"  Part="1" 
F 0 "R801" H 2230 3020 50  0000 L CNN
F 1 "DNP" H 2230 2960 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 2200 3000 50  0001 C CNN
F 3 "~" H 2200 3000 50  0001 C CNN
	1    2200 3000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2200 3100 3350 3100
Wire Wire Line
	2200 2900 3350 2900
Text Label 3150 3700 2    50   ~ 0
DIFF_R28_N
Text Label 3150 3500 2    50   ~ 0
DIFF_R28_P
$Comp
L Device:R_Small R?
U 1 1 5FE1AD1C
P 2550 3600
AR Path="/5F8E5420/5F955280/5FE1AD1C" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE1AD1C" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE1AD1C" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE1AD1C" Ref="R805"  Part="1" 
F 0 "R805" H 2580 3620 50  0000 L CNN
F 1 "DNP" H 2580 3560 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 2550 3600 50  0001 C CNN
F 3 "~" H 2550 3600 50  0001 C CNN
	1    2550 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	2550 3500 3350 3500
Wire Wire Line
	2550 3700 3350 3700
Text Label 3150 3600 2    50   ~ 0
DIFF_R27_N
Text Label 3150 3400 2    50   ~ 0
DIFF_R27_P
$Comp
L Device:R_Small R?
U 1 1 5FE1AD26
P 2200 3500
AR Path="/5F8E5420/5F955280/5FE1AD26" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE1AD26" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE1AD26" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE1AD26" Ref="R802"  Part="1" 
F 0 "R802" H 2230 3520 50  0000 L CNN
F 1 "DNP" H 2230 3460 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 2200 3500 50  0001 C CNN
F 3 "~" H 2200 3500 50  0001 C CNN
	1    2200 3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	2200 3400 3350 3400
Wire Wire Line
	2200 3600 3350 3600
Wire Wire Line
	3350 3300 2700 3300
Wire Wire Line
	3350 3200 2700 3200
Wire Wire Line
	2550 3150 2700 3150
Wire Wire Line
	2700 3150 2700 3200
Wire Wire Line
	2550 3350 2700 3350
Wire Wire Line
	2700 3350 2700 3300
$Comp
L Device:R_Small R?
U 1 1 5FE1CE61
P 2550 3250
AR Path="/5F8E5420/5F955280/5FE1CE61" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE1CE61" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE1CE61" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE1CE61" Ref="R804"  Part="1" 
F 0 "R804" H 2580 3270 50  0000 L CNN
F 1 "DNP" H 2580 3210 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 2550 3250 50  0001 C CNN
F 3 "~" H 2550 3250 50  0001 C CNN
	1    2550 3250
	-1   0    0    1   
$EndComp
Text Label 3150 3200 2    50   ~ 0
DIFF_R25_P
Text Label 3150 3300 2    50   ~ 0
DIFF_R25_N
Text Label 5850 3700 0    50   ~ 0
DIFF_R33_N
Text Label 5850 3500 0    50   ~ 0
DIFF_R33_P
$Comp
L Device:R_Small R?
U 1 1 5FE1FEDD
P 6450 3600
AR Path="/5F8E5420/5F955280/5FE1FEDD" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE1FEDD" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE1FEDD" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE1FEDD" Ref="R809"  Part="1" 
F 0 "R809" H 6480 3620 50  0000 L CNN
F 1 "DNP" H 6480 3560 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 6450 3600 50  0001 C CNN
F 3 "~" H 6450 3600 50  0001 C CNN
	1    6450 3600
	1    0    0    1   
$EndComp
Wire Wire Line
	6450 3500 5650 3500
Wire Wire Line
	6450 3700 5650 3700
Text Label 5850 3600 0    50   ~ 0
DIFF_R32_N
Text Label 5850 3400 0    50   ~ 0
DIFF_R32_P
$Comp
L Device:R_Small R?
U 1 1 5FE1FEE7
P 6800 3500
AR Path="/5F8E5420/5F955280/5FE1FEE7" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE1FEE7" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE1FEE7" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE1FEE7" Ref="R810"  Part="1" 
F 0 "R810" H 6830 3520 50  0000 L CNN
F 1 "DNP" H 6830 3460 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 6800 3500 50  0001 C CNN
F 3 "~" H 6800 3500 50  0001 C CNN
	1    6800 3500
	1    0    0    1   
$EndComp
Wire Wire Line
	6800 3400 5650 3400
Wire Wire Line
	6800 3600 5650 3600
Wire Wire Line
	5650 3300 6300 3300
Wire Wire Line
	5650 3200 6300 3200
Wire Wire Line
	6450 3150 6300 3150
Wire Wire Line
	6300 3150 6300 3200
Wire Wire Line
	6450 3350 6300 3350
Wire Wire Line
	6300 3350 6300 3300
$Comp
L Device:R_Small R?
U 1 1 5FE2350B
P 6450 3250
AR Path="/5F8E5420/5F955280/5FE2350B" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE2350B" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE2350B" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE2350B" Ref="R808"  Part="1" 
F 0 "R808" H 6480 3270 50  0000 L CNN
F 1 "DNP" H 6480 3210 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 6450 3250 50  0001 C CNN
F 3 "~" H 6450 3250 50  0001 C CNN
	1    6450 3250
	1    0    0    1   
$EndComp
Text Label 5850 3200 0    50   ~ 0
DIFF_R30_P
Text Label 5850 3300 0    50   ~ 0
DIFF_R30_N
Text Label 5850 3000 0    50   ~ 0
DIFF_R29_N
Text Label 5850 2800 0    50   ~ 0
DIFF_R29_P
$Comp
L Device:R_Small R?
U 1 1 5FE25F75
P 6450 2900
AR Path="/5F8E5420/5F955280/5FE25F75" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE25F75" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE25F75" Ref="R?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE25F75" Ref="R807"  Part="1" 
F 0 "R807" H 6480 2920 50  0000 L CNN
F 1 "DNP" H 6480 2860 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 6450 2900 50  0001 C CNN
F 3 "~" H 6450 2900 50  0001 C CNN
	1    6450 2900
	1    0    0    1   
$EndComp
Wire Wire Line
	6450 2800 5650 2800
Wire Wire Line
	6450 3000 5650 3000
Wire Wire Line
	5650 3100 6300 3100
Wire Wire Line
	5650 2900 6300 2900
Text Label 5850 3100 0    50   ~ 0
VREF_B5
Text Label 5850 2900 0    50   ~ 0
IO_R15
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J?
U 1 1 5FE3C4AB
P 7550 4900
AR Path="/5F8E5420/5F955280/5FE3C4AB" Ref="J?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE3C4AB" Ref="J?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE3C4AB" Ref="J?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE3C4AB" Ref="J803"  Part="1" 
F 0 "J803" H 7600 5400 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 7600 4300 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 7550 4900 50  0001 C CNN
F 3 "~" H 7550 4900 50  0001 C CNN
	1    7550 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE3C4B1
P 7100 5550
AR Path="/5F8E5420/5F955280/5FE3C4B1" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE3C4B1" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE3C4B1" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE3C4B1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7100 5300 50  0001 C CNN
F 1 "GND" H 7100 5400 50  0000 C CNN
F 2 "" H 7100 5550 50  0001 C CNN
F 3 "" H 7100 5550 50  0001 C CNN
	1    7100 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE3C4B7
P 8100 5550
AR Path="/5F8E5420/5F955280/5FE3C4B7" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE3C4B7" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE3C4B7" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE3C4B7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8100 5300 50  0001 C CNN
F 1 "GND" H 8100 5400 50  0000 C CNN
F 2 "" H 8100 5550 50  0001 C CNN
F 3 "" H 8100 5550 50  0001 C CNN
	1    8100 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 5550 8100 5400
Wire Wire Line
	8100 5400 7850 5400
Wire Wire Line
	7350 5400 7100 5400
Wire Wire Line
	7100 5400 7100 5550
Wire Wire Line
	7950 4600 7850 4600
Wire Wire Line
	7950 4700 7850 4700
Wire Wire Line
	7950 4800 7850 4800
Wire Wire Line
	7950 4900 7850 4900
Wire Wire Line
	7950 5000 7850 5000
Wire Wire Line
	7950 5100 7850 5100
Wire Wire Line
	7950 5200 7850 5200
Wire Wire Line
	7950 5300 7850 5300
Wire Wire Line
	7350 4600 7250 4600
Wire Wire Line
	7350 4700 7250 4700
Wire Wire Line
	7350 4800 7250 4800
Wire Wire Line
	7350 4900 7250 4900
Wire Wire Line
	7350 5000 7250 5000
Wire Wire Line
	7350 5100 7250 5100
Wire Wire Line
	7350 5200 7250 5200
Wire Wire Line
	7350 5300 7250 5300
$Comp
L power:GND #PWR?
U 1 1 5FE3C4D1
P 6700 4650
AR Path="/5F8E5420/5F955280/5FE3C4D1" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE3C4D1" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE3C4D1" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE3C4D1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6700 4400 50  0001 C CNN
F 1 "GND" H 6700 4500 50  0000 C CNN
F 2 "" H 6700 4650 50  0001 C CNN
F 3 "" H 6700 4650 50  0001 C CNN
	1    6700 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4500 6700 4650
$Comp
L power:GND #PWR?
U 1 1 5FE3C4D8
P 8500 4650
AR Path="/5F8E5420/5F955280/5FE3C4D8" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE3C4D8" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE3C4D8" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE3C4D8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8500 4400 50  0001 C CNN
F 1 "GND" H 8500 4500 50  0000 C CNN
F 2 "" H 8500 4650 50  0001 C CNN
F 3 "" H 8500 4650 50  0001 C CNN
	1    8500 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 4650 8500 4500
Wire Wire Line
	7850 4500 8500 4500
Wire Wire Line
	6700 4500 7350 4500
Text Label 7950 4900 0    50   ~ 0
DIFF_R25_N
Text Label 7950 4800 0    50   ~ 0
DIFF_R25_P
Text Label 4100 4800 0    50   ~ 0
DIFF_R27_N
Text Label 3400 4800 2    50   ~ 0
DIFF_R28_N
Text Label 4100 4600 0    50   ~ 0
DIFF_R29_N
Text Label 7250 4900 2    50   ~ 0
DIFF_R30_N
Text Label 4100 5000 0    50   ~ 0
DIFF_R32_N
Text Label 7950 4700 0    50   ~ 0
DIFF_R33_N
Text Label 7250 5300 2    50   ~ 0
VREF_B5
Text Label 7250 5200 2    50   ~ 0
IO_R15
Text Label 7950 4600 0    50   ~ 0
DIFF_R33_P
Text Label 4100 5100 0    50   ~ 0
DIFF_R32_P
Text Label 7250 4800 2    50   ~ 0
DIFF_R30_P
Text Label 4100 4700 0    50   ~ 0
DIFF_R29_P
Text Label 3400 4900 2    50   ~ 0
DIFF_R28_P
Text Label 4100 4900 0    50   ~ 0
DIFF_R27_P
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J?
U 1 1 5FE3C4F1
P 3700 4900
AR Path="/5F8E5420/5F955280/5FE3C4F1" Ref="J?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE3C4F1" Ref="J?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE3C4F1" Ref="J?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE3C4F1" Ref="J801"  Part="1" 
F 0 "J801" H 3750 5400 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 3750 4300 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical" H 3700 4900 50  0001 C CNN
F 3 "~" H 3700 4900 50  0001 C CNN
	1    3700 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE3C4F7
P 3250 5550
AR Path="/5F8E5420/5F955280/5FE3C4F7" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE3C4F7" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE3C4F7" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE3C4F7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3250 5300 50  0001 C CNN
F 1 "GND" H 3250 5400 50  0000 C CNN
F 2 "" H 3250 5550 50  0001 C CNN
F 3 "" H 3250 5550 50  0001 C CNN
	1    3250 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE3C4FD
P 4250 5550
AR Path="/5F8E5420/5F955280/5FE3C4FD" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE3C4FD" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE3C4FD" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE3C4FD" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4250 5300 50  0001 C CNN
F 1 "GND" H 4250 5400 50  0000 C CNN
F 2 "" H 4250 5550 50  0001 C CNN
F 3 "" H 4250 5550 50  0001 C CNN
	1    4250 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 5550 4250 5400
Wire Wire Line
	4250 5400 4000 5400
Wire Wire Line
	3500 5400 3250 5400
Wire Wire Line
	3250 5400 3250 5550
Wire Wire Line
	4100 5200 4000 5200
Wire Wire Line
	4100 5300 4000 5300
Wire Wire Line
	3500 5200 3400 5200
Wire Wire Line
	3500 5300 3400 5300
$Comp
L power:GND #PWR?
U 1 1 5FE3C517
P 2850 5150
AR Path="/5F8E5420/5F955280/5FE3C517" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE3C517" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE3C517" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE3C517" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2850 4900 50  0001 C CNN
F 1 "GND" H 2850 5000 50  0000 C CNN
F 2 "" H 2850 5150 50  0001 C CNN
F 3 "" H 2850 5150 50  0001 C CNN
	1    2850 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE3C51E
P 4650 5150
AR Path="/5F8E5420/5F955280/5FE3C51E" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F971A6D/5FE3C51E" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F989551/5FE3C51E" Ref="#PWR?"  Part="1" 
AR Path="/5F8E5420/5F9AA234/5FE3C51E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4650 4900 50  0001 C CNN
F 1 "GND" H 4650 5000 50  0000 C CNN
F 2 "" H 4650 5150 50  0001 C CNN
F 3 "" H 4650 5150 50  0001 C CNN
	1    4650 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4500 4650 4500
Wire Wire Line
	2850 4500 3500 4500
Text Label 7250 4600 2    50   ~ 0
DIFF_R1_N
Text Label 7250 4700 2    50   ~ 0
DIFF_R1_P
Text HLabel 1900 2900 0    50   BiDi ~ 0
DIFF_R2_P
Text HLabel 1900 3100 0    50   BiDi ~ 0
DIFF_R2_N
Wire Wire Line
	2850 4500 2850 5150
Wire Wire Line
	4650 4500 4650 5150
Wire Wire Line
	4100 4600 4000 4600
Wire Wire Line
	3500 4600 3400 4600
Wire Wire Line
	3500 4700 3400 4700
Wire Wire Line
	3500 4800 3400 4800
Wire Wire Line
	3500 4900 3400 4900
Wire Wire Line
	3500 5000 3400 5000
Wire Wire Line
	3500 5100 3400 5100
Wire Wire Line
	4100 4700 4000 4700
Wire Wire Line
	4100 4800 4000 4800
Wire Wire Line
	4100 4900 4000 4900
Wire Wire Line
	4100 5000 4000 5000
Wire Wire Line
	4100 5100 4000 5100
Text HLabel 3400 4600 0    50   BiDi ~ 0
DIFF_R40_N
Text HLabel 3400 4700 0    50   BiDi ~ 0
DIFF_R40_P
Text HLabel 3400 5100 0    50   BiDi ~ 0
DIFF_R39_N
Text HLabel 3400 5000 0    50   BiDi ~ 0
DIFF_R39_P
Text HLabel 7250 5000 0    50   BiDi ~ 0
DIFF_R38_N
Text HLabel 7250 5100 0    50   BiDi ~ 0
DIFF_R38_P
Text HLabel 7950 5100 2    50   BiDi ~ 0
DIFF_R44_N
Text HLabel 7950 5000 2    50   BiDi ~ 0
DIFF_R44_P
Text HLabel 7950 5300 2    50   BiDi ~ 0
DIFF_R51_N
Text HLabel 7950 5200 2    50   BiDi ~ 0
DIFF_R51_P
Text HLabel 3400 5300 0    50   BiDi ~ 0
DIFF_R46_N
Text HLabel 3400 5200 0    50   BiDi ~ 0
DIFF_R46_P
Text HLabel 4100 5200 2    50   BiDi ~ 0
DIFF_R41_N
Text HLabel 4100 5300 2    50   BiDi ~ 0
DIFF_R41_P
$EndSCHEMATC
