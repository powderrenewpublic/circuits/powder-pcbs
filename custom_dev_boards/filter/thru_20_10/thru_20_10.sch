EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_Coaxial J2
U 1 1 5FDB2CD4
P 5200 3500
F 0 "J2" H 5300 3475 50  0000 L CNN
F 1 "Conn_Coaxial" H 5300 3384 50  0000 L CNN
F 2 "POWDER_RF:SMA_EndLaunch_142-0761-881" H 5200 3500 50  0001 C CNN
F 3 " ~" H 5200 3500 50  0001 C CNN
	1    5200 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5FDB327D
P 3800 3500
F 0 "J1" H 3728 3738 50  0000 C CNN
F 1 "Conn_Coaxial" H 3728 3647 50  0000 C CNN
F 2 "POWDER_RF:SMA_EndLaunch_142-0761-881" H 3800 3500 50  0001 C CNN
F 3 " ~" H 3800 3500 50  0001 C CNN
	1    3800 3500
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDB39B0
P 3800 4000
F 0 "#PWR?" H 3800 3750 50  0001 C CNN
F 1 "GND" H 3805 3827 50  0000 C CNN
F 2 "" H 3800 4000 50  0001 C CNN
F 3 "" H 3800 4000 50  0001 C CNN
	1    3800 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FDB3C5F
P 5200 4000
F 0 "#PWR?" H 5200 3750 50  0001 C CNN
F 1 "GND" H 5205 3827 50  0000 C CNN
F 2 "" H 5200 4000 50  0001 C CNN
F 3 "" H 5200 4000 50  0001 C CNN
	1    5200 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4000 5200 3700
Wire Wire Line
	5000 3500 4000 3500
Wire Wire Line
	3800 3700 3800 4000
Text Label 4500 3500 0    50   ~ 0
thru
$EndSCHEMATC
